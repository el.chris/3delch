this:RequireNamespace("_3Delch")
this:RequireNamespace("GuiGL")
this:RequireNamespace("GLAPI")
this:RequireNamespace("network")
this:RequireNamespace("utils")
this:RequireNamespace("lua")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("SDL2")
this:RequireNamespace("stl")

local ConnectionWindow      = require "classes/ConnectionWindow"
local StreamConnection      = require "classes/StreamConnection"
local RequestConnection     = require "classes/RequestConnection"
local LarryHUD              = require "classes/LarryHUD"
local EventHandling         = require "classes/EventHandling"
local LarryServiceHandler   = require "classes/LarryServiceHandler"

local connectionWindow      = nil
local hud                   = nil
local eventHandler          = nil
local motionRequestResponse = nil
local larryService          = nil

local isConnected           = false
local wheelSpeed            = { left = 0, right = 0 }

function Exit() 
    _3Delch.GetInstance():StopMainLoop() 
end

function Init3DElch()
    _3Delch.Init("cfg/globalConfig.lua")
end

function AddStateMessage(message) 
    connectionWindow:AddStateMessage(message) 
end

function Connect()
    local ipAddress = connectionWindow:SwitchToConnectView()

    larryService        = LarryServiceHandler.new()
    larryService:SetPrint(AddStateMessage)
    larryService:SetIPAddress(ipAddress)
    larryService:ConnectServices()
end

function Disconnect()
    hud:Deactivate()
    connectionWindow:SwitchToConnectView()

    larryService:Disconnect()
    larryService = nil
    isConnected = false
end

function ReceiveAndShowWebcamStream(frame)
    if frame:has_value() then
        local frameData         = frame:value():Get_data()
        local frameNumber       = frame:value():Get_frame_number()
        local frameTimeStamp    = frame:value():Get_timestamp()
        local image             = elImage.CreateFromByteStream(frameData,
                                                       SDL_PIXELFORMAT_RGBA32)
        if image:has_value() then
            hud:UpdateSensorData()
            hud:UpdateCameraImage(image, frameNumber,
                                  frameData:Get_stream():size(), frameTimeStamp)
            return true
        else
            return false
        end
    end
    return true
end

function HandleEvents()
    if eventHandler:GetMovement().z == 1 and eventHandler:GetMovement().x == 0 then
        hud:SetMotionIndicator(POSITION_TOP, POSITION_MIDDLE)
        wheelSpeed = { left = 100, right = 100 }
    elseif eventHandler:GetMovement().z == 1 and eventHandler:GetMovement().x == -1 then
        hud:SetMotionIndicator(POSITION_TOP, POSITION_LEFT)
        wheelSpeed = { left = 50, right = 100 }
    elseif eventHandler:GetMovement().z == 1 and eventHandler:GetMovement().x == 1 then
        hud:SetMotionIndicator(POSITION_TOP, POSITION_RIGHT)
        wheelSpeed = { left = 100, right = 50 }
    elseif eventHandler:GetMovement().z == 0 and eventHandler:GetMovement().x == 0 then
        hud:SetMotionIndicator(POSITION_MIDDLE, POSITION_MIDDLE)
        wheelSpeed = { left = 0, right = 0 }
    elseif eventHandler:GetMovement().z == 0 and eventHandler:GetMovement().x == -1 then
        hud:SetMotionIndicator(POSITION_MIDDLE, POSITION_LEFT)
        wheelSpeed = { left = -100, right = 100 }
    elseif eventHandler:GetMovement().z == 0 and eventHandler:GetMovement().x == 1 then
        hud:SetMotionIndicator(POSITION_MIDDLE, POSITION_RIGHT)
        wheelSpeed = { left = 100, right = -100 }
    elseif eventHandler:GetMovement().z == -1 and eventHandler:GetMovement().x == 0 then
        hud:SetMotionIndicator(POSITION_BOTTOM, POSITION_MIDDLE)
        wheelSpeed = { left = -100, right = -100 }
    elseif eventHandler:GetMovement().z == -1 and eventHandler:GetMovement().x == -1 then
        hud:SetMotionIndicator(POSITION_BOTTOM, POSITION_LEFT)
        wheelSpeed = { left = -50, right = -100 }
    elseif eventHandler:GetMovement().z == -1 and eventHandler:GetMovement().x == 1 then
        hud:SetMotionIndicator(POSITION_BOTTOM, POSITION_RIGHT)
        wheelSpeed = { left = -100, right = -50 }
    end

    local left = tostring(wheelSpeed.left) .. " %"
    local right = tostring(wheelSpeed.right) .. " %"
    hud:SetWheelSpeed(left, left, left, right, right, right)
end

function Callback()
    if larryService ~= nil then
        larryService:ConnectionLoop()

        if not isConnected then
            local state = larryService:IsConnectionEstablished()
            if state.camera then --and state.drive then
                hud:Activate()
                connectionWindow:SwitchToReceivingView()
                isConnected = true
            end
        end

        if isConnected then
            ReceiveAndShowWebcamStream(larryService:GetCurrentCameraFrame())
            larryService:SetWheelSpeed(wheelSpeed.left, wheelSpeed.right)
        end
    end

    hud:UpdateFPS(_3Delch.GetInstance():Window():GetFramesPerSecond())
    HandleEvents()
end



Init3DElch()


hud                 = LarryHUD.new()
eventHandler        = EventHandling.new()

connectionWindow = ConnectionWindow.new()
connectionWindow:SetClickOnConnectCallback("Connect")
connectionWindow:SetClickOnDisconnectCallback("Disconnect")

callback = this:CreateCallbackToFunction("Callback")
callbackID = _3Delch.GetInstance():AddMainLoopCallback(callback)
