local LarryHUD = {}
LarryHUD.__index = LarryHUD
setmetatable(LarryHUD, {__call = function(cls, ...) return cls.new() end})

function LarryHUD.new()
    local self = setmetatable({}, LarryHUD)
    self.camera = {}
    self.motion = {}
    self.mainView = {}
    self.sensor = {}
    self.isActivated = false
    self.systemInformation = {}

    self.camera.window, self.camera.frameID, self.camera.frameSize, self.camera
    .frameTimestamp, self.camera.idleAnimation = self:CreateCameraWindow()
    self.camera.resolution = { x = 1280, y = 720 }

    self.motion.window, self.motion.leftWheel, self.motion.rightWheel, self.motion
    .indicatorFrame, self.motion.indicator = self:CreateMotionWindow()

    self.mainView.window, self.mainView.idleAnimation = self:CreateMainView()

    self.sensor.window, self.sensor.idleAnimation, self.sensor.graph = self:CreateSensorWindow()
    self.systemInformation.window, self.systemInformation.fps = self:CreateSystemInformation()

    self.motion.vertical = POSITION_MIDDLE
    self.motion.horizontal = POSITION_MIDDLE

    self:Deactivate()

    return self
end

function LarryHUD.Activate(self)
    if not self.isActivated then
        self.isActivated = true;
        self.sensor.window:Start()
        self.sensor.idleAnimation:SetDoRenderWidget(false)
        self.sensor.idleAnimation:Stop()

        self.mainView.idleAnimation:SetDoRenderWidget(false)
        self.mainView.idleAnimation:Stop()

        self.camera.idleAnimation:SetDoRenderWidget(false)
        self.camera.idleAnimation:Stop()
    end
end

function LarryHUD.Deactivate(self)
    if self.isActivated then
        self.isActivated = false;
        self.sensor.window:Stop()
        self.sensor.idleAnimation:SetDoRenderWidget(true)
        self.sensor.idleAnimation:Start()

        self.mainView.idleAnimation:SetDoRenderWidget(true)
        self.mainView.idleAnimation:Start()

        self.camera.idleAnimation:SetDoRenderWidget(true)
        self.camera.idleAnimation:Start()
    end
end

function LarryHUD.CreateSystemInformation(self)
    local window = _3Delch.GetInstance():GUI():Create_Window("")
    window:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)
    window:SetSize(glm_vec2.new(480, 400))
    window:SetPosition(glm_vec2.new(10, 400))

    local fpsHint = window:Create_Label("")
    fpsHint:SetText("FPS: ")
    fpsHint:SetPosition(glm_vec2.new(10, 10))

    local fps = window:Create_Label("")
    fps:SetText("111121")
    fps:SetPosition(glm_vec2.new(100, 10))

    return window, fps
end

function LarryHUD.CreateSensorWindow(self)
    local window = _3Delch.GetInstance():GUI():Create_ShaderAnimation("")
    window:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)
    window:SetSize(glm_vec2.new(480, 360))
    window:SetPosition(glm_vec2.new(10, 10))
    window:SetUp("data/shaderTextures/demonstrator.glsl", "default",
                          true, 1, 640, 480, false)

    local idleAnimation = window:Create_ShaderAnimation("")
    idleAnimation:SetStickToParentSize(glm_vec2.new(0, 0))
    idleAnimation:SetIsAlwaysOnBottom(true)
    idleAnimation:SetUp("data/shaderTextures/noise.glsl", "default",
                          true, 1, 640, 480, false)

    local overlayTop = window:Create_Window("")
    overlayTop:SetIsScrollbarEnabled(false, false, false, false)
    overlayTop:SetDisableEventHandling(true)
    overlayTop:SetPosition(glm_vec2.new(0, 0))
    overlayTop:SetSize(glm_vec2.new(1, 30))
    overlayTop:SetStickToParentSize(glm_vec2.new(0, -1))
    overlayTop:SetBaseColorForState(glm_vec4.new(0, 0, 0, 0.6), STATE_DEFAULT)

    local label = overlayTop:Create_Label("")
    label:SetText("Various Sensor Feed")
    label:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)

    local overlayBottom = window:Create_Window("")
    overlayBottom:SetIsScrollbarEnabled(false, false, false, false)
    overlayBottom:SetDisableEventHandling(true)
    overlayBottom:SetPosition(glm_vec2.new(0, 0))
    overlayBottom:SetSize(glm_vec2.new(1,70))
    overlayBottom:SetPositionPointOfOrigin(POSITION_BOTTOM_LEFT, false)
    overlayBottom:SetStickToParentSize(glm_vec2.new(0, -1))
    overlayBottom:SetBaseColorForState(glm_vec4.new(0, 0, 0, 0.6), STATE_DEFAULT)

    local graph = overlayBottom:Create_Graph("")
    graph:SetStickToParentSize(glm_vec2.new(0, 0))
    graph:SetUpGraphShader("glsl/gui/shaderTexture_Graph.glsl", "default")
    graph:AddGraphDataValue(0)
    graph:SetGraphDataYDrawRange(glm_vec2.new(-2.2, 2.2))
    graph:UpdateGraphData()
    
    return window, idleAnimation, graph
end

function LarryHUD.CreateMainView(self)
    local image = _3Delch.GetInstance():GUI():Create_Image("")
    image:SetStickToParentSize(glm_vec2.new(0, 0))
    image:SetIsAlwaysOnBottom(true)
    image:SetDisableEventHandling(true)

    local idleAnimation = image:Create_ShaderAnimation("")
    idleAnimation:SetStickToParentSize(glm_vec2.new(0, 0))
    idleAnimation:SetIsAlwaysOnBottom(true)
    idleAnimation:SetUp("data/shaderTextures/floatingCubes.glsl", "default",
                          true, 1, 640, 480, false)
    idleAnimation:SetDisableEventHandling(true)

    return image, idleAnimation
end

function LarryHUD.CreateMotionWindow(self)
    local movementWindow = _3Delch.GetInstance():GUI():Create_Window("")
    movementWindow:SetPositionPointOfOrigin(POSITION_BOTTOM_RIGHT, false)
    movementWindow:SetPosition(glm_vec2.new(50, 50))
    movementWindow:SetSize(glm_vec2.new(320, 160))

    local leftWheels = movementWindow:Create_Listbox("")
    leftWheels:SetSize(glm_vec2.new(80, 75))
    leftWheels:SetPosition(glm_vec2.new(10, 0))
    leftWheels:SetPositionPointOfOrigin(POSITION_CENTER_LEFT, false)
    leftWheels:AddElement(" 0 %", 0)
    leftWheels:AddElement(" 0 %", 1)
    leftWheels:AddElement(" 0 %", 2)
    local leftWheelLabel = movementWindow:Create_Label("")
    leftWheelLabel:SetText("left wheel")
    leftWheelLabel:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)
    leftWheelLabel:SetPosition(glm_vec2.new(10, 10))

    local rightWheels = movementWindow:Create_Listbox("")
    rightWheels:SetSize(glm_vec2.new(80, 75))
    rightWheels:SetPosition(glm_vec2.new(10, 0))
    rightWheels:SetPositionPointOfOrigin(POSITION_CENTER_RIGHT, false)
    rightWheels:AddElement(" 0 %", 0)
    rightWheels:AddElement(" 0 %", 1)
    rightWheels:AddElement(" 0 %", 2)
    local rightWheelLabel = movementWindow:Create_Label("")
    rightWheelLabel:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    rightWheelLabel:SetText("right wheel")
    rightWheelLabel:SetPosition(glm_vec2.new(10, 10))

    local indicatorFrame = movementWindow:Create_Base("")
    indicatorFrame:SetBaseColorForState(glm_vec4.new(0, 0, 0, 0.4),
                                        STATE_DEFAULT)
    indicatorFrame:SetSize(glm_vec2.new(100, 100))
    indicatorFrame:SetPosition(glm_vec2.new(0, 10))
    indicatorFrame:SetPositionPointOfOrigin(POSITION_CENTER, false)

    local indicator = indicatorFrame:Create_Base("")
    indicator:SetPositionPointOfOrigin(POSITION_CENTER, false)
    indicator:SetSize(glm_vec2.new(15, 15))
    indicator:SetAccentColorForState(glm_vec4.new(0, 1, 1, 1), STATE_DEFAULT)
    indicator:SetBorderSize(glm_vec4.new(2, 2, 2, 2))

    return movementWindow, leftWheels, rightWheels, indicatorFrame, indicator
end

function LarryHUD.CreateCameraWindow(self)
    local image = _3Delch.GetInstance():GUI():Create_Image("")
    image:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    image:SetSize(glm_vec2.new(480, 360))
    image:SetPosition(glm_vec2.new(10, 10))

    local overlayTop = image:Create_Window("")
    overlayTop:SetIsScrollbarEnabled(false, false, false, false)
    overlayTop:SetDisableEventHandling(true)
    overlayTop:SetPosition(glm_vec2.new(0, 0))
    overlayTop:SetSize(glm_vec2.new(1, 30))
    overlayTop:SetStickToParentSize(glm_vec2.new(0, -1))
    overlayTop:SetBaseColorForState(glm_vec4.new(0, 0, 0, 0.6), STATE_DEFAULT)

    local label = overlayTop:Create_Label("")
    label:SetText("Camera Feed")
    label:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)

    local frameID = overlayTop:Create_Label("")
    frameID:SetText("ID: 0")
    frameID:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    frameID:SetPosition(glm_vec2.new(0, 0))

    local overlayBottom = image:Create_Window("")
    overlayBottom:SetIsScrollbarEnabled(false, false, false, false)
    overlayBottom:SetDisableEventHandling(true)
    overlayBottom:SetPosition(glm_vec2.new(0, 0))
    overlayBottom:SetSize(glm_vec2.new(1, 30))
    overlayBottom:SetPositionPointOfOrigin(POSITION_BOTTOM_LEFT, false)
    overlayBottom:SetStickToParentSize(glm_vec2.new(0, -1))
    overlayBottom:SetBaseColorForState(glm_vec4.new(0, 0, 0, 0.6), STATE_DEFAULT)

    local frameSize = overlayBottom:Create_Label("")
    frameSize:SetText("0 bytes")
    frameSize:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    frameSize:SetPosition(glm_vec2.new(0, 0))

    local frameTimestamp = overlayBottom:Create_Label("")
    frameTimestamp:SetText("timestamp: 0")
    frameTimestamp:SetPositionPointOfOrigin(POSITION_TOP_LEFT, false)
    frameTimestamp:SetPosition(glm_vec2.new(0, 0))

    local idleAnimation = image:Create_ShaderAnimation("")
    idleAnimation:SetStickToParentSize(glm_vec2.new(0, 0))
    idleAnimation:SetIsAlwaysOnBottom(true)
    idleAnimation:SetUp("data/shaderTextures/noise.glsl", "default",
                          true, 1, 640, 480, false)

    return image, frameID, frameSize, frameTimestamp, idleAnimation
end

function LarryHUD.SetMotionIndicator(self, vertical, horizontal)
    if self.motion.vertical ~= vertical or self.motion.horizontal ~= horizontal then
        local height = self.motion.indicatorFrame:GetSize():Get_y() -
                           self.motion.indicator:GetSize():Get_y()
        local width = self.motion.indicatorFrame:GetSize():Get_x() -
                          self.motion.indicator:GetSize():Get_x()
        local range = {min = {x = -width/2, y = -height/2}, max = {x = width/2, y = height/2}}
        local factor = 1.4

        if vertical == POSITION_TOP and horizontal == POSITION_MIDDLE then
            self.motion.indicator:MoveTo(glm_vec2.new(0, range.min.y))
        elseif vertical == POSITION_TOP and horizontal == POSITION_LEFT then
            self.motion.indicator:MoveTo(
                glm_vec2.new(range.min.x / factor, range.min.y / factor))
        elseif vertical == POSITION_TOP and horizontal == POSITION_RIGHT then
            self.motion.indicator:MoveTo(
                glm_vec2.new(range.max.x / factor, range.min.y / factor))

        elseif vertical == POSITION_MIDDLE and horizontal == POSITION_MIDDLE then
            self.motion.indicator:MoveTo(glm_vec2.new(0, 0))
        elseif vertical == POSITION_MIDDLE and horizontal == POSITION_LEFT then
            self.motion.indicator:MoveTo(glm_vec2.new(range.min.x, 0))
        elseif vertical == POSITION_MIDDLE and horizontal == POSITION_RIGHT then
            self.motion.indicator:MoveTo(glm_vec2.new(range.max.x, 0))

        elseif vertical == POSITION_BOTTOM and horizontal == POSITION_MIDDLE then
            self.motion.indicator:MoveTo(glm_vec2.new(0, range.max.y))
        elseif vertical == POSITION_BOTTOM and horizontal == POSITION_LEFT then
            self.motion.indicator:MoveTo(
                glm_vec2.new(range.min.x / factor, range.max.y / factor))
        elseif vertical == POSITION_BOTTOM and horizontal == POSITION_RIGHT then
            self.motion.indicator:MoveTo(
                glm_vec2.new(range.max.x / factor, range.max.y / factor))
        end

        self.motion.vertical = vertical
        self.motion.horizontal = horizontal
    end
end

function LarryHUD.SetWheelSpeed(self, leftFront, leftMiddle, leftBack,
                                rightFront, rightMiddle, rightBack)
    self.motion.leftWheel:GetElementAtIndex(0):SetText(leftFront)
    self.motion.leftWheel:GetElementAtIndex(1):SetText(leftMiddle)
    self.motion.leftWheel:GetElementAtIndex(2):SetText(leftBack)

    self.motion.rightWheel:GetElementAtIndex(0):SetText(rightFront)
    self.motion.rightWheel:GetElementAtIndex(1):SetText(rightMiddle)
    self.motion.rightWheel:GetElementAtIndex(2):SetText(rightBack)
end

function LarryHUD.UpdateCameraImage(self, imagePtr, frameNumber, size,
                                    timestamp)
    self.camera.window:SetImageFromBytePointer( imagePtr:get():GetBytePointer(), 
        self.camera.resolution.x, self.camera.resolution.y)
    self.camera.frameID:SetText("ID: " .. frameNumber)
    self.camera.frameSize:SetText(size .. " bytes")
    self.camera.frameTimestamp:SetText("timestamp: " .. timestamp)

    self.mainView.window:SetImageFromBytePointer(imagePtr:get():GetBytePointer(), 
        self.camera.resolution.x, self.camera.resolution.y)
end

function LarryHUD.UpdateSensorData(self)
    local runtime = _3Delch.GetInstance():Window():GetRuntime()
    self.sensor.graph:ResetGraphData()
    for i = 0,10,0.1
    do
        self.sensor.graph:AddGraphDataValue(math.sin(i + runtime) + 
            0.1 * math.sin(8.2 * i + 3 * runtime) * math.sin(3.6 * i) +
            0.2 * math.sin(11.4 * i + 4 * runtime) * math.sin(2.2 * i)+
            0.3 * math.sin(4.2 * i + 2 * runtime) * math.sin(8.1 * i) +
            0.4 * math.sin(0.4 * i + 0.5 * runtime) * math.sin(12 * i ) +
            0.11 * math.sin(22 * i + 7 * runtime)
            )
    end
    self.sensor.graph:SetGraphDataYDrawRange(glm_vec2.new(-2.2, 2.2))
    self.sensor.graph:UpdateGraphData()
end

function LarryHUD.UpdateFPS(self, fps)
    self.systemInformation.fps:SetText(tostring(fps))
end

return LarryHUD
