RequestConnection = {}
RequestConnection.__index = RequestConnection
setmetatable(RequestConnection, {__call = function(cls, ...) return cls.new() end, })

local state = { connected = 0, disconnected = -999, connectionFailure = -1, init = 1, establish = 2}

function RequestConnection.new(hostname, port, serviceName, serviceVersion)
    local self              = setmetatable({}, RequestConnection)

    self.hostname           = hostname
    self.port               = port
    self.serviceName        = serviceName
    self.serviceVersion     = serviceVersion
    self.connection         = elEasyTCPMessaging_Client.new(serviceName, serviceVersion)
    self.currentState       = state.init

    self.print              = print

    return self
end

function RequestConnection.SetPrint(self, newPrint)
    self.print = newPrint
end

function RequestConnection.Connect(self)
    self:Log("initiating connection to " .. self.hostname .. ":" .. self.port)
    self.connectionState    = self.connection:ConnectTo(self.hostname, self.port)
    self.currentState       = state.establish
end

function RequestConnection.Log(self, message)
    self.print("[ " .. self.serviceName .. ":" .. self.serviceVersion .. " ] " .. message)
end

function RequestConnection.IsConnected(self, message)
    if self.currentState == state.connected and not self.connection:IsConnected() then
        self:Disconnect()
    elseif self.currentState == state.establish then
        if self.connectionState:value_available() then
            if self.connectionState:get() then
                self.currentState = state.connected
                self:Log("connection established")
            else 
                self.currentState = state.connectionFailure
                self:Log("connection failed")
            end
        end
    end

    return self.currentState == state.connected
end

function RequestConnection.Disconnect(self)
    self.connection:Disconnect()
    self.currentState = state.disconnected
    self:Log(self.hostname .. ":" .. self.port .. " disconnected")
end

function RequestConnection.SendPingRequest(self)
    return self.connection:SendPingRequest()
end

function RequestConnection.SendInfoRequest(self)
    return self.connection:SendInfoRequest()
end

function RequestConnection.SendCustomRequest(self, byteStream)
    return self.connection:SendCustomRequest(byteStream)
end



return RequestConnection
