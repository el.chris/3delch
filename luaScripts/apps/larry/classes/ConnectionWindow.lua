local ConnectionWindow = {}
ConnectionWindow.__index = ConnectionWindow
setmetatable(ConnectionWindow, {__call = function(cls, ...) return cls.new() end, })

function ConnectionWindow.new ()
    local self = setmetatable({}, ConnectionWindow)

    self.currentView = 0

    self.window = _3Delch.GetInstance():GUI():Create_Window("")
    self.window:SetPositionPointOfOrigin(POSITION_CENTER, false)
    self.window:SetPosition(glm_vec2.new(0, 0))
    self.window:SetSize(glm_vec2.new(450, 150))
    self.window:SetIsScrollbarEnabled(false, false, false, false)
    self.window:SetIsMovable(false)
    self.window:SetIsResizable(false)

    self.initFrame = self.window:Create_Base("")
    self.initFrame:SetStickToParentSize(glm_vec2.new(0, 0))
    self.initFrame:SetDisableEventHandling(true)

    self.label = self.initFrame:Create_Label("")
    self.label:SetText("Please enter the IP-Address of Larry")
    self.label:SetPosition(glm_vec2.new(10.0, 10.0))

    self.ipAddress = self.initFrame:Create_Entry("")
    self.ipAddress:SetText("192.168.178.38")
    self.ipAddress:SetPosition(glm_vec2.new(40, 50))
    self.ipAddress:SetSize(glm_vec2.new(350, 25))

    self.connectButton = self.initFrame:Create_Button("")
    self.connectButton:SetText("connect")
    self.connectButton:SetPositionPointOfOrigin(POSITION_BOTTOM_LEFT, false)
    self.connectButton:SetSize(glm_vec2.new(150, 40))

    self.abortButton = self.initFrame:Create_Button("")
    self.abortButton:SetText("abort")
    self.abortButton:SetPositionPointOfOrigin(POSITION_BOTTOM_RIGHT, false)
    self.abortButton:SetSize(glm_vec2.new(150, 40))
    self.abortButton:SetClickCallback(this:CreateCallbackToFunction("Exit"))

    self.state = self.initFrame:Create_Textbox("")
    self.state:SetPosition(glm_vec2.new(5, 100))
    self.state:SetSize(glm_vec2.new(640, 200))
    self.state:SetDoRenderWidget(false)
    self.state:SetIsEditable(false)

    self.receivingFrame = self.window:Create_Base("")
    self.receivingFrame:SetStickToParentSize(glm_vec2.new(0, 0))
    self.receivingFrame:SetDisableEventHandling(true)
    self.receivingFrame:SetDoRenderWidget(false)

    self.disconnectButton = self.receivingFrame:Create_Button("")
    self.disconnectButton:SetText("disconnect")
    self.disconnectButton:SetPositionPointOfOrigin(POSITION_CENTER_RIGHT, false)
 
    return self
end

function ConnectionWindow.SwitchToConnectView(self)

    if self.currentView ~= 1 then
        self.initFrame:SetDoRenderWidget(true)
        self.receivingFrame:SetDoRenderWidget(false)
        self.state:SetDoRenderWidget(true)

        self.window:SetPositionPointOfOrigin(POSITION_CENTER, true)
        self.window:MoveTo(glm_vec2.new(0, 0))
        self.window:ResizeTo(glm_vec2.new(650, 350))

        self.currentView = 1
    end

    return self.ipAddress:GetText()
end

function ConnectionWindow.SwitchToReceivingView(self)
    if self.currentView ~= 2 then
        self.initFrame:SetDoRenderWidget(false)
        self.receivingFrame:SetDoRenderWidget(true)

        self.window:SetPositionPointOfOrigin(POSITION_BOTTOM_LEFT, true)
        self.window:MoveTo(glm_vec2.new(0, 0))
        self.window:ResizeTo(glm_vec2.new(400, 30))

        self.currentView = 2
    end
end

function ConnectionWindow.SetClickOnConnectCallback(self, callback)
    self.connectButton:SetClickCallback(this:CreateCallbackToFunction(callback))
end

function ConnectionWindow.SetClickOnDisconnectCallback(self, callback)
    self.disconnectButton:SetClickCallback(this:CreateCallbackToFunction(callback))
end


function ConnectionWindow.AddStateMessage(self, message)
    self.state:InsertLine(message, 0)
end

return ConnectionWindow
