#include "3Delch/3Delch.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "world/elInstanceModel.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();
    scene->GUI()->SetShowMouseCursor( false );

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    class MyWorld : public world::elWorld
    {
      public:
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->sun = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( -0.3f, -1.0f, 0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.2f,
                .doCastShadow     = true } );

            this->hexagons = this->Create< world::elInstanceModel >(
                HighGL::elObjectGeometryBuilder::CreateHexagonTube()
                    .GetObjectGeometry() );
        }

        bb::product_ptr< world::elSky >           sky;
        bb::product_ptr< world::elSun >           sun;
        bb::product_ptr< world::elInstanceModel > hexagons;

    } myWorld( *scene );


    int64_t limit    = 20;
    auto    callback = instance.CreateMainLoopCallback(
           [&]
           {
            std::vector< OpenGL::instanceGeometry_t > geometry;

            float spacing = 0.05f;
            float time    = static_cast< float >(
                             instance.Window()->GetRuntime().GetSeconds() ) *
                         0.5f;
            float factor = 0.3f;
            for ( int64_t x = -limit; x < limit; ++x )
                for ( int64_t y = -limit; y < limit; ++y )
                {
                    float x_ = static_cast< float >( x ),
                          y_ = static_cast< float >( y );
                    glm::vec3 coord;
                    coord.x = 2.0f * x_ * 0.766f * ( 1.0f + spacing );
                    coord.y = sin( x_ * factor + time ) *
                              cos( y_ * factor + time ) *
                              cos( x_ * y_ * factor * factor + time ) * 2.0f;
                    coord.z = 2.0f * y_ * 0.866f * ( 1.0f + spacing );

                    if ( x % 2 )
                    {
                        coord.z += 0.866f + spacing;
                    }

                    geometry.emplace_back( OpenGL::instanceGeometry_t{
                        coord, glm::vec3( 1.0f ),
                        glm::vec4( 1.0, 0.0, 0.0, M_PI_2 ) } );
                }

            myWorld.hexagons->SetGeometry( geometry );
           } );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
