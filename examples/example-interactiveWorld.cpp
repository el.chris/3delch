#include "3Delch/3Delch.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "world/elCoordinateSystem.hpp"
#include "world/elInteractiveSet.hpp"
#include "world/elModel.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->sun = this->Create< world::elSun >( world::SunProperties{
                .direction = glm::vec3( 0.5f, -1.0f, 0.5f ) } );
            this->interactiveSet = this->Create< world::elInteractiveSet >();
            this->interactiveSet->SetupInteractiveMesh(
                { .click = [&]( auto v )
                  { this->ClickOnCoordinateSystem( v ); } },
                20.0f, 400U );
            this->interactiveSet->GetInteractiveMesh().SetStickToCamera( true );


            for ( float x = -10.0f; x < 10.0f; x += 2.0f )
                for ( float y = -10.0f; y < 10.0f; y += 2.0f )
                {
                    this->boxes.emplace_back( this->Create< world::elModel >(
                        HighGL::elObjectGeometryBuilder::CreateCube()
                            .GetObjectGeometry() ) );
                    auto &box = this->boxes.back();

                    box->SetPosition( { x, 0.25, y } );
                    box->SetScaling( glm::vec3{ 0.25 } );
                    box->SetDiffuseColor( { 0x66, 0x66, 0x66, 0xff } );
                    box->SetShininessAndRoughness( 0x88, 0x44 );
                    box->SetSpecularColor( { 0xff, 0xff, 0xff }, 0xff );

                    this->interactiveSet->Attach(
                        *box,
                        { .select = [&]( auto ptr ) { this->SelectBox( ptr ); },
                          .deselect = [&]( auto ptr )
                          { this->DeselectBox( ptr ); } } );
                }

            this->rover = this->Create< world::elModelGroup >(
                "data/MarsRover-Mesh_v1.0.obj", 0.1f );
            this->rover->SetPosition( { 0.0, 1.0, 0.0 } );
            this->rover->SetScaling( glm::vec3{ 2.0 } );
            this->rover->Rotate( { 0.0, 1.0, 0.0 }, 0.5f );
            this->interactiveSet->AttachMembers( *this->rover, {} );

            this->coordinateSystem = this->Create< world::elCoordinateSystem >(
                world::CoordinateSystemProperties{
                    { 25, 25 },
                    { 0.0, 0.0, 0.0 },
                    HighGL::elCoordinateSystemGenerator::GridStyle::Lines,
                    world::CoordinateSystemStyle::MayorMinorPlane } );
        }

        void
        SelectBox( world::elModel *box )
        {
            this->selectedBox = box;
        }

        void
        DeselectBox( world::elModel * )
        {
            this->selectedBox = nullptr;
        }

        void
        ClickOnCoordinateSystem( const glm::vec3 v )
        {
            if ( this->selectedBox )
                this->selectedBox->MoveTo( { v.x, 0.25f, v.z }, 0.5f );
        }

        world::elModel                                  *selectedBox = nullptr;
        bb::product_ptr< world::elCoordinateSystem >     coordinateSystem;
        std::vector< bb::product_ptr< world::elModel > > boxes;
        bb::product_ptr< world::elSky >                  sky;
        bb::product_ptr< world::elSun >                  sun;
        bb::product_ptr< world::elInteractiveSet >       interactiveSet;
        bb::product_ptr< world::elModelGroup >           rover;
    } world( *scene );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
