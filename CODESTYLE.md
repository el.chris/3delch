# General

 * Use clang-format with the .clang-format file from the git-repository. This guarantees a consistent
      code styling with multiple IDEs on multiple platforms.

 * Use doxygen for documentation

 * Use precision specification for floating point values unless there is an explicit need for a double.

        GOOD: float f = 0.5f;                   BAD: float f = 0.5;

 * Use 'const' as much as possible. It helps to avoid a lot of common mistakes.

        GOOD:   const int *p;           // pointer to const int
                int * const p;          // const pointer to int
                const int * const p;    // const pointer to const int

        BAD:    int const *p;      // it is confusing

 * Use 'const' as much as possible! Again and again. Consider also class methods and const iterators!

        GOOD:   int sum(const int a, const int b);

        BAD:    int sum(int a, int b);

 * Use 'const' as much as possible - part 3; Use const iterators!

        GOOD:   for( auto it = vec.cbegin(); it != vec.cend(); ++it )

        BAD:    for( auto it = vec.begin(); it != vec.end(); ++it )

* Always prefer range based loops before  Except when you need to iterate through a subset of the 
      container.

        GOOD:   for( auto e : vec )
        GOOD:   for( int i = 2; i < vec.size(); i += 4 )

        BAD:    for( int i = 0; i < vec.size(); i++ )

* Avoid 'using namespace ...', never use it globally!

* Try to use free/delete and malloc/new in the same function to avoid pointer problems. If you have
      global pointers in a class, than they should be initialized by the constructor and deleted by the
      destructor, if it is possible, and not in the middle of the class by some method.

* Never use NULL or 0 instead of nullptr!

* Always try to avoid the usage of pointers!

* Use std::unique_ptr or std::shared_ptr instead of new and delete

# Performance

* Function arguments should be always const references except for PODtypes.

        GOOD: void func1(const class1_t & bla);
        BAD:  void func1(const class1_t bla);

        GOOD: void func2(const int x);
        BAD:  void func2(const int & x);

* use lambdas instead of std::bind

        GOOD:  auto blubb = [](int a) -> int { return sum(a, 3); };
        BAD:   auto blubb = std::bind(sum, std::placeholders::_1, 3);

# Logic

* If you need "and" or "or" to describe the functionality of a function, write two!
      Except when you can bring the performance argument.

* Always fetch segmentation fault causing events at the lowest possible level. For example if you're 
      writing a container which allows adding and removing then always check if the removing element exists
      directly in the removing function so that when a higher function tries to remove a non existing element
      no segmentation fault occurs.

# Classes

 * Function overloading should be avoided in most cases

 * The class constructor should have as much arguments as needed so that parameter setting and a later
      initialization can be avoided. But these arguments should have as much default settings as possible
      so that only the non default arguments need to be set.

        GOOD: elNoodleShaker fuu(noodleSize, cookingTime, color);

        BAD: elNoodleShaker fuu();
             fuu.SetNoodleSize(12);
             fuu.SetCookingTime(100);
             fuu.SetColor(BLUE);
             fuu.Init();

* Every class which throws an exception should have an exception subclass. If needed, multiple exception
      subclasses should be used to differentiate between multiple exceptions cases. DO NOT USE string 
      comparision to handle exceptions!

        GOOD: class SomeException : public std::runtime_error { ... };
                ...
              throw SomeException("message");

        BAD:  throw std::runtime_error("message");

* Follow the rule of 5 strictly! That means you always declare the constructor, the move and the copy
      constructor and the move and copy assignment operator. Declare them =default oder =delete if you must.

        GOOD:   class elFuu {
                    elFuu();
                    ~elFuu();
                    elFuu(elFuu &&) =default;               // move ctor
                    elFuu(const elFuu &) =delete;          // copy ctor

                    elFuu& operator=(elFuu &&) =default;    // move assignment
                    elFuu& operator=(const elFuu &) = delete;  // copy assign
                };
        BAD:    class elFuu {
                    elFuu();
                    // do not rely on auto generation even when its in the cpp
                    // standard!
                };

* Always implement the move and copy ctor/assignment safe! Pay attention to
      self assignment.


# Styling
Use the .clang-format file in the 3Delch repository

# Naming

* Typedef and struct names use the same naming convention as variables, however they always end with 
      "_t"

        struct point3D_t;

* Enum names use the same naming convention as variables, however they always end with "_t". The enum
      constants use all upper case characters. Multiple words are separated with an underscore.

        enum head_t {
            BIGHEAD,
            FATHEAD
        };

* Defined names use all upper case characters. Multiple words are separated with an underscore.

        #define DR_DODO     true

* Function names start with an upper case:

        void DoSomething(void);

* In multi-word function names each word starts with an upper case
        void DoSomethingReallyStupid(void);

* Variable names start with a lower case character.

        char n;

* In multi-word variable names the first word starts with a lower case character and each successive
      word starts with an upper case. (camel case)

        float distanceToNewCoke;

* Class names start with "el" and each successive word starts with an upper case.

        class elNoodleShaker;

* Class methods have the same naming convention as functions.

        class elNoodleShaker
        {
            int         GetNumberOfNoodles(void) const;
        };

* Indent the names of class variables and class method to make nice columns. The variable type or
      method return type is in the first column and the variable or method name in the second.

        class elNoodleShaker
        {
            int         count;
            float       direction;
            int         GetNumberOfNoodles(void) const;
            int*        linkToContinuum;
        };

# File Naming

 * Each class should be in a separate source file unless it makes sense to group several smaller classes.
      The file name should be the same as the name of the class.
          class elNoodleShaker;

          files:
            elNoodleShaker.cpp
            elNoodleShaker.hpp
            elNoodleShaker_local.hpp

          contents:
            elNoodleShaker.cpp - should just have one include line: #include "elNoodleShaker.hpp"

            elNoodleShaker.hpp - should just have one include line: #include "elNoodleShaker_local.hpp"
            it should content the interface of this class and struct's and typedef's which could be used
            outside of this class.
                struct point3D_t
                {
                    int x;
                    int y;
                };

                class
                {
                    int     count;
                };

            elNoodleShaker_local.hpp - should contain all the necessary #include lines for this class and
            struct's and typedef's which are used solely in private/protected part of this class

* When a class spans across multiple files these files have a name that starts with the name of the class
      followed by an underscore and a subsection name.
        class elNoodleFinder;

        files:
          elNoodleFinder_water.cpp;
          elNoodleFinder_air.cpp;
          elNoodleFinder_space.cpp;


