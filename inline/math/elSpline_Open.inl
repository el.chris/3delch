namespace el3D
{
namespace math
{
template < typename T >
inline elSpline_Open< T >::elSpline_Open( const path_t< T >& path,
                                          const float        factor,
                                          const T&           tangentStart,
                                          const T& tangentEnd ) noexcept
{
    uint64_t pathSize = path.size();

    this->GenerateSpline( pathSize, path,
                          this->GenerateTangents( pathSize, path, factor,
                                                  tangentStart, tangentEnd ) );
}

template < typename T >
inline T
elSpline_Open< T >::operator()( const float t ) const noexcept
{
    if ( this->curves.empty() ) return T( 0 );
    if ( t < 0.0f ) return this->curves.front().value( 0.0f );
    if ( t > this->splineLength ) return this->curves.back().value( 1.0f );

    return elSpline_base< T >::operator()( t );
}
} // namespace math
} // namespace el3D
