namespace el3D
{
namespace RenderPipeline
{
template < typename render_T, typename... Targs >
inline render_T *
elRenderPipeline::AddRenderer( const GLAPI::elWindow *const window,
                               const Targs &...t )
{
    render_T *newRenderer = new render_T(
        { this->sceneName, this->config, window,
          this->outputTextures[TEX_FINAL].get(),
          this->outputTextures[TEX_DEPTH].get(),
          this->outputTextures[TEX_EVENT].get(),
          this->outputTextures[TEX_GLOW].get(),
          this->outputTextures[TEX_MATERIAL].get(),
          &this->optionalSkyboxTexture, this->eventTexture,
          &this->factory.GetProducts< HighGL::elLight_DirectionalLight >(),
          &this->factory.GetProducts< HighGL::elLight_PointLight >() },
        t... );

    this->renderPipe.emplace_back( newRenderer );
    return newRenderer;
}


} // namespace RenderPipeline
} // namespace el3D
