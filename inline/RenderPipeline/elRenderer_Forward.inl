namespace el3D
{
namespace RenderPipeline
{
template < typename T >
inline uint64_t
elRenderer_Forward::AcquireGeometricShaderId(
    Forward::Object< T >          &object,
    const OpenGL::elShaderProgram *geometricShader ) noexcept
{
    uint64_t geometricShaderId = AUTO_SET_GEOMETRIC_SHADER;
    if ( geometricShader != nullptr )
    {
        auto shaderId = object->AddShader( geometricShader );
        if ( shaderId.HasError() )
            LOG_ERROR( 0 ) << "unable to attach provided shader "
                           << geometricShader->GetFileAndGroupName()
                           << " to newly created object in forward renderer";
        else
            geometricShaderId = shaderId.GetValue();
    }

    return geometricShaderId;
}

template < typename T, typename... Targs >
inline Forward::Object< T >
elRenderer_Forward::CreateObject(
    const OpenGL::elShaderProgram *geometricShader, const RenderOrder order,
    Targs &&...args ) noexcept
{
    Forward::Object< T > newObject;
    newObject =
        this->factory[static_cast< uint64_t >( order )].CreateProduct< T >(
            std::forward< Targs >( args )... );

    newObject.Extension() = this->GenerateObjectSettings(
        *newObject,
        this->AcquireGeometricShaderId( newObject, geometricShader ) );
    return newObject;
}
} // namespace RenderPipeline
} // namespace el3D
