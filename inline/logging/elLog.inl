namespace el3D
{
namespace logging
{
template < typename T >
const elLog::Proxy &
elLog::Proxy::operator<<( const T &s ) const noexcept
{
    msg.append( detail::toString( s ) );
    return *this;
}
} // namespace logging
} // namespace el3D
