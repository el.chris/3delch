namespace el3D
{
namespace concurrent
{
template < typename T >
inline elTrigger< T >::elTrigger( const T&           initialValue,
                                  const predicate_t& predicate ) noexcept
    : value{initialValue}, predicate{( predicate )
                                         ? predicate
                                         : []( const T& ) { return true; }}
{
}

template < typename T >
inline elTrigger< T >::~elTrigger() noexcept
{
    {
        std::lock_guard< std::mutex > lock( this->valueMutex );
        this->destructorCalled = true;
    }

    this->TriggerAll();

    /// busy waiting till really every wait has returned
    while ( this->inWaitCounter.load( std::memory_order_relaxed ) != 0 )
    {
    }
}

template < typename T >
inline std::optional< T >
elTrigger< T >::BlockingWait( const predicate_t& customPredicate ) const
    noexcept
{
    this->inWaitCounter++;
    std::unique_lock< std::mutex > lock( this->valueMutex );
    T                              currentValue;

    this->conditionVariable.wait( lock, [&] {
        currentValue = this->value;
        return ( ( !customPredicate ) ? this->predicate( this->value )
                                      : customPredicate( this->value ) ) ||
               this->destructorCalled;
    } );
    bool destructorWasCalled = this->destructorCalled;

    this->inWaitCounter--;

    if ( !destructorWasCalled )
        return currentValue;
    else
        return std::nullopt;
}

template < typename T >
inline std::optional< T >
elTrigger< T >::TimedWait( const units::Time& timeout,
                           const predicate_t& customPredicate ) const noexcept
{
    this->inWaitCounter++;
    std::unique_lock< std::mutex > lock( this->valueMutex );
    T                              currentValue;

    auto timeoutOrDestructorCalled =
        !( this->conditionVariable.wait_for(
               lock,
               std::chrono::nanoseconds(
                   static_cast< uint64_t >( timeout.GetNanoSeconds() ) ),
               [&] {
                   currentValue = this->value;
                   return ( ( !customPredicate )
                                ? this->predicate( this->value )
                                : customPredicate( this->value ) ) ||
                          this->destructorCalled;
               } ) &&
           !this->destructorCalled );

    this->inWaitCounter--;

    if ( !timeoutOrDestructorCalled )
        return currentValue;
    else
        return std::nullopt;
}

template < typename T >
inline std::optional< T >
elTrigger< T >::TryWait( const predicate_t& customPredicate ) const noexcept
{
    std::lock_guard< std::mutex > lock( this->valueMutex );
    if ( ( !customPredicate ) ? this->predicate( this->value )
                              : customPredicate( this->value ) )
        return this->value;
    else
        return std::nullopt;
}

template < typename T >
inline void
elTrigger< T >::Set( const T& value ) noexcept
{
    std::lock_guard< std::mutex > lock( this->valueMutex );
    this->value = value;
}

template < typename T >
inline void
elTrigger< T >::SetWithSetter(
    const std::function< T( const T& ) >& setter ) noexcept
{
    if ( !setter ) return;
    std::lock_guard< std::mutex > lock( this->valueMutex );
    this->value = setter( this->value );
}

template < typename T >
inline T
elTrigger< T >::Get() const noexcept
{
    std::lock_guard< std::mutex > lock( this->valueMutex );
    return this->value;
}

template < typename T >
inline void
elTrigger< T >::TriggerOne() const noexcept
{
    this->conditionVariable.notify_one();
}

template < typename T >
inline void
elTrigger< T >::TriggerAll() const noexcept
{
    this->conditionVariable.notify_all();
}


} // namespace concurrent
} // namespace el3D
