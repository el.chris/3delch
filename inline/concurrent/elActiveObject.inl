namespace el3D
{

namespace concurrent
{
template < typename T >
inline std::optional< std::future< T > >
elActiveObject::AddTaskWithReturn( const std::function< T() >& f ) noexcept
{
    {
        std::lock_guard< std::mutex > lock( this->mtx );
        if ( !this->addingTasksIsAllowed ) return std::nullopt;
        this->numberOfActiveTasks++;
    }

    std::shared_ptr< std::promise< T > > p( new std::promise< T >() );
    auto                                 returnValue = p->get_future();

    this->threadPool.AddTask( [this, p, f] {
        p->set_value( f() );
        {
            std::lock_guard< std::mutex > lock( this->mtx );
            this->numberOfActiveTasks--;
        }
        this->waitForTasksToFinish.notify_one();
    } );

    return std::optional< std::future< T > >( std::move( returnValue ) );
}
} // namespace concurrent
} // namespace el3D
