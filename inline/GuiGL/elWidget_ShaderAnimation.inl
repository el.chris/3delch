namespace el3D
{
namespace GuiGL
{
template < typename T >
inline uint64_t
elWidget_ShaderAnimation::AnimateUniform( const GLint uniformID,
                                          const T &source, const T &destination,
                                          const float speed ) noexcept
{
    linearAnimation_t< T > *newAnimation = new linearAnimation_t< T >();
    newAnimation->SetSource( source )
        .SetDestination( destination )
        .SetVelocity( speed )
        .SetVelocityType( animation::AnimationVelocity::Absolute )
        .SetOnUpdateCallback( [this, uniformID, newAnimation] {
            if ( this->animation )
                this->animation->GetShader().SetUniform(
                    uniformID, newAnimation->GetCurrentPosition() );
        } );

    return this->animatedUniforms.emplace_back( newAnimation ).index();
}

template < typename T >
inline uint64_t
elWidget_ShaderAnimation::UpdateAnimatedUniform( const uint64_t animationID,
                                                 const GLint    uniformID,
                                                 const T &      destination,
                                                 const float    speed ) noexcept
{
    if ( this->animatedUniforms.has_index( animationID ) )
    {
        dynamic_cast< linearAnimation_t< T > * >(
            this->animatedUniforms[animationID].get() )
            ->SetDestination( destination )
            .SetVelocity( speed );
        return animationID;
    }
    else if ( this->animation )
    {
        return this->AnimateUniform(
            uniformID,
            this->animation->GetShader().GetUniform< T >( uniformID ),
            destination, speed );
    }
    else
    {
        LOG_ERROR( 0 )
            << "unable to animate shader uniform when no shader is configured!";
        return INVALID_ANIMATION_ID;
    }
}

} // namespace GuiGL
} // namespace el3D
