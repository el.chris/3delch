namespace el3D
{
namespace GuiGL
{
template < typename T >
T*
elWidget_Listbox::GetElementAtIndex( const size_t index ) noexcept
{
    return this->GetElement< T >( 0, index );
}

template < typename T >
T*
elWidget_Listbox::GetElement( const size_t col, const size_t row ) noexcept
{
    return elWidget_Table::GetElement< T >( col, row );
}
} // namespace GuiGL
} // namespace el3D
