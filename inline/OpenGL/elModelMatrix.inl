namespace el3D
{
namespace OpenGL
{
template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::MoveTo(
    const glm::vec3& destination, const animation::velocity_t velocity,
    const animation::AnimationVelocity    velocityType,
    const animation::onArrivalCallback_t& onArrivalCallback ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetPosition )
        .SetDestination( destination )
        .SetSource( this->GetPosition() )
        .SetVelocity( velocity )
        .SetVelocityType( velocityType )
        .SetOnArrivalCallback( onArrivalCallback );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::ResizeTo(
    const glm::vec3& size, const animation::velocity_t velocity,
    const animation::AnimationVelocity    velocityType,
    const animation::onArrivalCallback_t& onArrivalCallback ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetScaling )
        .SetDestination( size )
        .SetSource( this->GetScaling() )
        .SetVelocity( velocity )
        .SetVelocityType( velocityType )
        .SetOnArrivalCallback( onArrivalCallback );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::RotateTo(
    const glm::vec4& rotation, const animation::velocity_t velocity,
    const animation::AnimationVelocity    velocityType,
    const animation::onArrivalCallback_t& onArrivalCallback ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetRotation )
        .SetDestination( rotation )
        .SetSource( { this->GetRotationAxis(), this->GetRotationDegree() } )
        .SetVelocity( velocity )
        .SetVelocityType( velocityType )
        .SetOnArrivalCallback( onArrivalCallback );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::Move(
    const glm::vec3& direction, const animation::velocity_t velocity ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetPosition )
        .SetDirection( direction )
        .SetSource( this->GetPosition() )
        .SetVelocity( velocity )
        .SetVelocityType( animation::AnimationVelocity::Absolute );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::Resize(
    const glm::vec3& direction, const animation::velocity_t velocity ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetScaling )
        .SetDirection( direction )
        .SetSource( this->GetScaling() )
        .SetVelocity( velocity )
        .SetVelocityType( animation::AnimationVelocity::Absolute );
}

template < typename Child, template < typename > class Interpolation >
inline glm::quat
elModelMatrix< Child, Interpolation >::GetRotation() const noexcept
{
    return glm::quat( this->GetRotationAxis() * this->GetRotationDegree() );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::Rotate(
    const glm::vec3& axis, const animation::velocity_t velocity ) noexcept
{
    this->SetRotation( { axis, this->GetRotationDegree() } );
    this->template Animate< animation::elAnimate_SourceDestination,
                            Interpolation >( &Child::SetRotation )
        .SetDirection( { 0.0, 0.0, 0.0, 1.0 } )
        .SetSource( { this->GetRotationAxis(), this->GetRotationDegree() } )
        .SetVelocity( velocity )
        .SetVelocityType( animation::AnimationVelocity::Absolute );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::StopMoving() noexcept
{
    this->template RemoveAnimation< decltype( &Child::SetPosition ) >(
        &Child::SetPosition );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::StopResizing() noexcept
{
    this->template RemoveAnimation< decltype( &Child::SetScaling ) >(
        &Child::SetScaling );
}

template < typename Child, template < typename > class Interpolation >
inline void
elModelMatrix< Child, Interpolation >::StopRotating() noexcept
{
    this->template RemoveAnimation< decltype( &Child::SetRotation ) >(
        &Child::SetRotation );
}


} // namespace OpenGL
} // namespace el3D
