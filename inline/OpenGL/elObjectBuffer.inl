namespace el3D
{
namespace OpenGL
{

template < typename... Targs >
inline elObjectBuffer::elObjectBuffer( const uint64_t                dimension,
                                       const std::vector< GLfloat > &vertices,
                                       const std::vector< GLuint > & elements,
                                       const Targs &...buffer ) noexcept
    : elObjectBuffer( dimension, 2 + sizeof...( Targs ), 1 )
{
    this->numberOfElements = elements.size();

    this->UpdateVertexBuffer( vertices );

    // UpdateBuffer for elements without consistency check
    vbo.BufferData(
        static_cast< GLsizeiptr >( elements.size() * sizeof( GLuint ) ),
        elements.data(), 1 );

    this->InitialUpdateBuffer( 2, buffer... );
}

template < typename T >
inline void
elObjectBuffer::UpdateBuffer( const size_t            buffer,
                              const std::vector< T > &data ) noexcept
{
    if ( buffer == 0 )
    {
        this->UpdateVertexBuffer( data );
        return;
    }

    uint64_t dataSize = data.size();
    uint64_t requiredDataSize =
        this->numberOfVertices * this->bufferDimension[buffer];

    if ( dataSize < requiredDataSize )
    {
        if ( !data.empty() )
            LOG_WARN( 0 )
                << "provided data has a size of " << dataSize
                << " with a dimension of " << this->bufferDimension[buffer]
                << ". This is insufficient for the corresponding vertex "
                   "buffer (dimension="
                << this->dimension << ", size=" << this->numberOfVertices
                << "). Filling provided data with blanks.";

        std::vector< T > adjustedData = data;
        adjustedData.resize( requiredDataSize );
        vbo.BufferData(
            static_cast< GLsizeiptr >( requiredDataSize * sizeof( T ) ),
            adjustedData.data(), buffer );
    }
    else
    {
        vbo.BufferData( static_cast< GLsizeiptr >( dataSize * sizeof( T ) ),
                        data.data(), buffer );
    }
}

template < typename T >
inline void
elObjectBuffer::UpdateSubBuffer( const size_t            buffer,
                                 const std::vector< T > &data,
                                 const uint64_t elementOffset ) noexcept
{
    vbo.BufferSubData( static_cast< GLintptr >( elementOffset * sizeof( T ) ),
                       static_cast< GLsizeiptr >( data.size() * sizeof( T ) ),
                       data.data(), buffer );
}

template < typename T >
inline void
elObjectBuffer::InitialUpdateBuffer( const size_t            buffer,
                                     const uint64_t          dataDimension,
                                     const std::vector< T > &data ) noexcept
{
    if ( this->bufferDimension.size() <= buffer )
        this->bufferDimension.resize( buffer + 1 );
    this->bufferDimension[buffer] = dataDimension;

    this->UpdateBuffer( buffer, data );
}

template < typename T, typename... Targs >
inline void
elObjectBuffer::InitialUpdateBuffer( const size_t            buffer,
                                     const uint64_t          dataDimension,
                                     const std::vector< T > &data,
                                     const Targs &...dataList ) noexcept
{
    this->InitialUpdateBuffer( buffer, dataDimension, data );
    this->InitialUpdateBuffer( buffer + 1, dataList... );
}

} // namespace OpenGL
} // namespace el3D
