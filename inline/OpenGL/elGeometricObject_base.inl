namespace el3D
{
namespace OpenGL
{
template < typename... ObjectBufferArgs >
inline elGeometricObject_base::elGeometricObject_base(
    const DrawMode drawMode, const uint64_t dimension,
    const elGeometricObject_Type type, ObjectBufferArgs&&... args ) noexcept
    : objectBuffer( dimension, std::forward< ObjectBufferArgs >( args )... ),
      type( type ), drawMode( drawMode )
{
}

template < typename T, typename... Targs >
inline bb::product_ptr< T >
elGeometricObject_base::CreateTexture( const size_t       shaderId,
                                       const std::string& uniform,
                                       const GLenum       textureUnit,
                                       Targs&&... args ) noexcept
{
    auto texture = this->factory.CreateProductWithOnRemoveCallback< T >(
        [this]( auto ptr ) { this->RemoveTexture( ptr ); },
        std::forward< Targs >( args )... );

    texture.Extension().uniform     = uniform;
    texture.Extension().textureUnit = textureUnit;
    texture.Extension().shaderId    = shaderId;
    texture.Extension().attachment =
        this->AttachTexture( texture.Get(), shaderId, uniform, textureUnit );

    return std::move( texture );
}

template < typename T >
inline void
elGeometricObject_base::UpdateBuffer( const size_t            buffer,
                                      const std::vector< T >& data ) noexcept
{
    this->objectBuffer.UpdateBuffer( buffer, data );
}

template < typename T >
inline void
elGeometricObject_base::UpdateSubBuffer( const size_t            buffer,
                                         const std::vector< T >& data,
                                         const uint64_t elementOffset ) noexcept
{
    this->objectBuffer.UpdateSubBuffer( buffer, data, elementOffset );
}

} // namespace OpenGL
} // namespace el3D

