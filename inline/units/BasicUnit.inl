namespace el3D
{
namespace units
{
template < typename T >
inline std::string
BasicUnit< T >::ToString( const int precision ) const noexcept
{
    std::stringstream ss;
    ss << std::fixed << std::setprecision( precision )
       << *reinterpret_cast< const T* >( this );
    return ss.str();
}

template < typename T >
inline T
BasicUnit< T >::operator+( const T& rhs ) const noexcept
{
    T basicUnit;
    basicUnit.value = this->value + rhs.value;
    return basicUnit;
}

template < typename T >
inline T
BasicUnit< T >::operator-( const T& rhs ) const noexcept
{
    T basicUnit;
    basicUnit.value = this->value - rhs.value;
    return basicUnit;
}

template < typename T >
inline BasicUnit< T >&
BasicUnit< T >::operator+=( const T& rhs ) noexcept
{
    this->value += rhs.value;
    return *this;
}

template < typename T >
inline BasicUnit< T >&
BasicUnit< T >::operator-=( const T& rhs ) noexcept
{
    this->value -= rhs.value;
    return *this;
}

template < typename T >
inline bool
BasicUnit< T >::operator<( const T& rhs ) const noexcept
{
    return this->value < rhs.value;
}

template < typename T >
inline bool
BasicUnit< T >::operator<=( const T& rhs ) const noexcept
{
    return this->value <= rhs.value;
}

template < typename T >
inline bool
BasicUnit< T >::operator==( const T& rhs ) const noexcept
{
    return this->value == rhs.value;
}

template < typename T >
inline bool
BasicUnit< T >::operator!=( const T& rhs ) const noexcept
{
    return this->value != rhs.value;
}

template < typename T >
inline bool
BasicUnit< T >::operator>=( const T& rhs ) const noexcept
{
    return this->value >= rhs.value;
}

template < typename T >
inline bool
BasicUnit< T >::operator>( const T& rhs ) const noexcept
{
    return this->value > rhs.value;
}

template < typename T >
inline BasicUnit< T >::BasicUnit( const float_t value ) noexcept
    : value( value )
{
}

} // namespace units
} // namespace el3D
