namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
template < typename Key, typename Value >
class lua_map : public std::map< Key, Value >
{
  public:
    Value
    lua_at( const Key& key )
    {
        return this->at( key );
    }

    void
    lua_erase( const Key& key )
    {
        this->erase( key );
    }

    size_t
    lua_count( const Key& key )
    {
        return this->count( key );
    }

    bool
    lua_find( const Key& key )
    {
        return this->find( key ) != this->end();
    }

    // i know the _ at the end is dispensable but if we remove this the msvc
    // compiler gives some non sense errors
    // TODO: remove _ (when msvc can handle it)
    void
    lua_insert_( const Key& key, const Value& value )
    {
        this->insert( std::make_pair( key, value ) );
    }

    LuaTable< std::map< Key, Value > >
    lua_convert_to_table()
    {
        return LuaTable< std::map< Key, Value > >{*this};
    }

    static std::map< Key, Value >*
    lua_create_from_table( const LuaTable< std::map< Key, Value > >& luaTable )
    {
        auto retVal = new std::map< Key, Value >();
        *retVal     = luaTable.data;
        return retVal;
    }
};
} // namespace internal

template < typename Key, typename Value >
inline void
AddLuaMapBinding( const std::string& luaClassName,
                  const std::string& nameSpace )
{
    using stl_map = std::map< Key, Value >;
    using lua_map = internal::lua_map< Key, Value >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_map >();

    newClass.AddMethod< lua_map >( "at", &lua_map::lua_at );
    newClass.AddMethod< stl_map >( "clear", &stl_map::clear );
    newClass.AddMethod< lua_map >( "count", &lua_map::lua_count );
    newClass.AddMethod< stl_map >( "empty", &stl_map::empty );
    newClass.AddMethod< lua_map >( "erase", &lua_map::lua_erase );
    newClass.AddMethod< lua_map >( "find", &lua_map::lua_find );
    newClass.AddMethod< lua_map >( "insert", &lua_map::lua_insert_ );
    newClass.AddMethod< stl_map >( "max_size", &stl_map::max_size );
    newClass.AddMethod< stl_map >( "size", &stl_map::size );

    newClass.AddMethod< lua_map >( "convert_to_table",
                                   &lua_map::lua_convert_to_table );
    newClass.AddStaticMethod( "create_from_table",
                              &lua_map::lua_create_from_table );


    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace stl
} // namespace luaBindings
} // namespace el3D
