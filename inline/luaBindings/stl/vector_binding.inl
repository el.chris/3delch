namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
// we have to introduce this class since in C++ there seems to be no way on
// how to overcome the const overloaded function problem
template < typename T, typename Allocator >
class lua_vector : public std::vector< T, Allocator >
{
  public:
    T
    lua_at( const size_t pos )
    {
        return this->at( pos );
    }

    // i know the _ at the end is dispensable but if we remove this the msvc
    // compiler gives some non sense errors
    // TODO: remove _ (when msvc can handle it)
    void
    lua_insert_( const int64_t pos, const T& t )
    {
        this->insert( this->begin() + pos, t );
    }

    void
    lua_erase( const int64_t pos )
    {
        this->erase( this->begin() + pos );
    }

    void
    lua_push_back( const T& t )
    {
        this->push_back( t );
    }

    void
    lua_resize( const size_t size )
    {
        this->resize( size );
    }

    void
    lua_resize_with_default_value( const size_t size, const T& defaultValue )
    {
        this->resize( size, defaultValue );
    }

    LuaArray< std::vector< T, Allocator > >
    lua_convert_to_array()
    {
        return LuaArray< std::vector< T, Allocator > >{*this};
    }

    static std::vector< T, Allocator >*
    lua_create_from_array(
        const LuaArray< std::vector< T, Allocator > >& luaArray )
    {
        auto retVal = new std::vector< T, Allocator >();
        *retVal     = luaArray.data;
        return retVal;
    }
};
} // namespace internal

template < typename T, typename Allocator >
inline void
AddLuaVectorBinding( const std::string& luaClassName,
                     const std::string& nameSpace )
{
    using stl_vector = std::vector< T, Allocator >;
    using lua_vector = internal::lua_vector< T, Allocator >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_vector >();

    newClass.AddMethod< lua_vector >( "at", &lua_vector::lua_at );
    newClass.AddMethod< stl_vector >( "empty", &stl_vector::empty );
    newClass.AddMethod< stl_vector >( "size", &stl_vector::size );
    newClass.AddMethod< stl_vector >( "max_size", &stl_vector::max_size );
    newClass.AddMethod< stl_vector >( "reserve", &stl_vector::reserve );
    newClass.AddMethod< stl_vector >( "capacity", &stl_vector::capacity );
    newClass.AddMethod< stl_vector >( "shrink_to_fit",
                                      &stl_vector::shrink_to_fit );
    newClass.AddMethod< stl_vector >( "clear", &stl_vector::clear );
    newClass.AddMethod< lua_vector >( "insert", &lua_vector::lua_insert_ );
    newClass.AddMethod< lua_vector >( "erase", &lua_vector::lua_erase );
    newClass.AddMethod< lua_vector >( "push_back", &lua_vector::lua_push_back );
    newClass.AddMethod< stl_vector >( "pop_back", &stl_vector::pop_back );
    newClass.AddMethod< lua_vector >( "resize", &lua_vector::lua_resize );
    newClass.AddMethod< lua_vector >(
        "resize_with_default_value",
        &lua_vector::lua_resize_with_default_value );

    newClass.AddMethod< lua_vector >( "convert_to_array",
                                      &lua_vector::lua_convert_to_array );
    newClass.AddStaticMethod( "create_from_array",
                              &lua_vector::lua_create_from_array );


    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}

} // namespace stl
} // namespace luaBindings
} // namespace el3D
