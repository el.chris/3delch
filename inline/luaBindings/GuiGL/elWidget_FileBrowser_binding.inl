namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_FileBrowser, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetRootPath",
                                     &elWidget_FileBrowser::SetRootPath );
    newClass.AddMethod< ChildType >( "SetOpenButtonText",
                                     &elWidget_FileBrowser::SetOpenButtonText );
    newClass.AddMethod< ChildType >(
        "SetAbortButtonText", &elWidget_FileBrowser::SetAbortButtonText );
    newClass.AddMethod< ChildType >( "SetAbortCallback",
                                     &elWidget_FileBrowser::SetAbortCallback );
    newClass.AddMethod< ChildType >( "SetOpenCallback",
                                     &elWidget_FileBrowser::SetOpenCallback );
    newClass.AddMethod< ChildType >( "GetSelection",
                                     &elWidget_FileBrowser::GetSelection );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
