namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Window, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "ScrollTo", &elWidget_Window::ScrollTo );
    newClass.AddMethod< ChildType >( "GetScrollbarWidth",
                                     &elWidget_Window::GetScrollbarWidth );
    newClass.AddMethod< ChildType >( "SetScrollbarWidth",
                                     &elWidget_Window::SetScrollbarWidth );
    newClass.AddMethod< ChildType >( "GetHasNaturalScrolling",
                                     &elWidget_Window::GetHasNaturalScrolling );
    newClass.AddMethod< ChildType >( "SetHasNaturalScrolling",
                                     &elWidget_Window::SetHasNaturalScrolling );
    newClass.AddMethod< ChildType >( "SetIsScrollbarEnabled",
                                     &elWidget_Window::SetIsScrollbarEnabled );
    newClass.AddMethod< ChildType >( "GetIsScrollbarEnabled",
                                     &elWidget_Window::GetIsScrollbarEnabled );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
