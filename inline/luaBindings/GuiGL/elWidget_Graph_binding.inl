namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Graph, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_ShaderAnimation, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetUpGraphShader",
                                     &elWidget_Graph::SetUpGraphShader );
    newClass.AddMethod< ChildType >( "SetGraphDataCallback",
                                     &elWidget_Graph::SetGraphDataCallback );
    newClass.AddMethod< ChildType >( "SetGraphDataYDrawRange",
                                     &elWidget_Graph::SetGraphDataYDrawRange );
    newClass.AddMethod< ChildType >( "AddGraphDataValue",
                                     &elWidget_Graph::AddGraphDataValue );
    newClass.AddMethod< ChildType >( "ResetGraphData",
                                     &elWidget_Graph::ResetGraphData );
    newClass.AddMethod< ChildType >( "UpdateGraphData",
                                     &elWidget_Graph::UpdateGraphData );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
