namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Label, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetText", &elWidget_Label::SetText );
    newClass.AddMethod< ChildType >( "GetFontHeight",
                                     &elWidget_Label::GetFontHeight );
    newClass.AddMethod< ChildType >( "GetTextSize",
                                     &elWidget_Label::GetTextSize );
    newClass.AddMethod< ChildType >( "DoAdjustSizeToTextSize",
                                     &elWidget_Label::DoAdjustSizeToTextSize );
    newClass.AddMethod< ChildType >( "GetText", &elWidget_Label::GetText );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
