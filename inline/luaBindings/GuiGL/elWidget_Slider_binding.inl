namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Slider, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );
    newClass.AddMethod< ChildType >( "GetSliderPosition",
                                     &elWidget_Slider::GetSliderPosition );
    newClass.AddMethod< ChildType >( "SetSliderPosition",
                                     &elWidget_Slider::SetSliderPosition );
    newClass.AddMethod< ChildType >(
        "SetSliderPositionChangeCallback",
        &elWidget_Slider::SetSliderPositionChangeCallback );
    newClass.AddMethod< ChildType >(
        "SetRelativeSliderButtonSize",
        &elWidget_Slider::SetRelativeSliderButtonSize );
    newClass.AddMethod< ChildType >( "SetIsHorizontal",
                                     &elWidget_Slider::SetIsHorizontal );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
