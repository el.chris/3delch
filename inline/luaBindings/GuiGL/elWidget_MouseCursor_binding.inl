namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_MouseCursor, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_ShaderAnimation, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "GetCursorPosition",
                                     &elWidget_MouseCursor::GetCursorPosition );
    newClass.AddMethod< ChildType >(
        "GetRelativeCursorPosition",
        &elWidget_MouseCursor::GetRelativeCursorPosition );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
