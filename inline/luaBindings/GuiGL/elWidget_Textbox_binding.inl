namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Textbox, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_Window, ChildType >::Do( newClass );
    newClass.AddMethod< ChildType >( "GetText", &elWidget_Textbox::GetText );
    newClass.AddMethod< ChildType >( "GetNumberOfLines",
                                     &elWidget_Textbox::GetNumberOfLines );
    newClass.AddMethod< ChildType >( "ClearText",
                                     &elWidget_Textbox::ClearText );
    newClass.AddMethod< ChildType >( "GetLine", &elWidget_Textbox::GetLine );
    newClass.AddMethod< ChildType >( "GetLineWidget",
                                     &elWidget_Textbox::GetLineWidget );
    newClass.AddMethod< ChildType >( "InsertLine",
                                     &elWidget_Textbox::InsertLine );
    newClass.AddMethod< ChildType >( "ModifyLine",
                                     &elWidget_Textbox::ModifyLine );
    newClass.AddMethod< ChildType >( "RemoveLine",
                                     &elWidget_Textbox::RemoveLine );
    newClass.AddMethod< ChildType >( "SetIsEditable",
                                     &elWidget_Textbox::SetIsEditable );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
