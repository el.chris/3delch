namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Table, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_Window, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >(
        "ResetNumberOfColsAndRows", &elWidget_Table::ResetNumberOfColsAndRows );
    newClass.AddMethod< ChildType >(
        "GetElement_Label", &elWidget_Table::GetElement< elWidget_Label > );
    newClass.AddMethod< ChildType >(
        "GetElement_Button", &elWidget_Table::GetElement< elWidget_Button > );
    newClass.AddMethod< ChildType >( "SetColumnWidth",
                                     &elWidget_Table::SetColumnWidth );
    newClass.AddMethod< ChildType >( "SetRowHeight",
                                     &elWidget_Table::SetRowHeight );
    newClass.AddMethod< ChildType >( "SetHoveringMode",
                                     &elWidget_Table::SetHoveringMode );
    newClass.AddMethod< ChildType >( "SetSelectionMode",
                                     &elWidget_Table::SetSelectionMode );
    newClass.AddMethod< ChildType >( "SetOnSelectionCallback",
                                     &elWidget_Table::SetOnSelectionCallback );
    newClass.AddMethod< ChildType >( "GetSelection",
                                     &elWidget_Table::GetSelection );
    newClass.AddMethod< ChildType >( "SetHasTitleRow",
                                     &elWidget_Table::SetHasTitleRow );
    newClass.AddMethod< ChildType >( "GetHasTitleRow",
                                     &elWidget_Table::GetHasTitleRow );
    newClass.AddMethod< ChildType >( "SetTitleRowButtonText",
                                     &elWidget_Table::SetTitleRowButtonText );
    newClass.AddMethod< ChildType >( "SetElementBaseType",
                                     &elWidget_Table::SetElementBaseType );
    newClass.AddMethod< ChildType >(
        "SetElementDefaultDimensions",
        &elWidget_Table::SetElementDefaultDimensions );

    lua::elLuaScript::RegisterGlobal< int >(
        "tableModes",
        {{"TABLEMODE_ROW", elWidget_Table::TABLEMODE_ROW},
         {"TABLEMODE_COL", elWidget_Table::TABLEMODE_COL},
         {"TABLEMODE_ENTRY", elWidget_Table::TABLEMODE_ENTRY},
         {"TABLEMODE_NONE", elWidget_Table::TABLEMODE_NONE}},
        NAMESPACE_NAME );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
