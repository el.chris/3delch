namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaTextureBaseInterface( lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::OpenGL;
    newClass.AddConstructor< ChildType, GLuint, GLenum >();
    newClass.AddMethod< ChildType >( "GetSize", &elTexture_base::GetSize );
    newClass.AddMethod< ChildType >( "GetType", &elTexture_base::GetType );
    newClass.AddMethod< ChildType >( "GetInternalFormat",
                                     &elTexture_base::GetInternalFormat );
    newClass.AddMethod< ChildType >( "GetFormat", &elTexture_base::GetFormat );
    newClass.AddMethod< ChildType >( "GetID", &elTexture_base::GetID );
    newClass.AddMethod< ChildType >( "GetTarget", &elTexture_base::GetTarget );
}
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
