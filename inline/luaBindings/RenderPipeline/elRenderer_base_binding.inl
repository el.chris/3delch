namespace el3D
{
namespace luaBindings
{
namespace RenderPipeline
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddMethodsToLuaClass_elRenderer_base( lua::elLuaScript::class_t& newClass )
{
    newClass.AddMethod< ChildType >(
        "Render", &::el3D::RenderPipeline::elRenderer_base::Render );
    newClass.AddMethod< ChildType >(
        "Reshape", &::el3D::RenderPipeline::elRenderer_base::Reshape );
}
#endif
} // namespace RenderPipeline
} // namespace luaBindings
} // namespace el3D
