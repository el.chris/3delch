namespace el3D
{
namespace network
{
namespace etm
{
template < typename T >
inline uint64_t
Request_t< T >::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->id,
                                                       this->payload );
}

template < typename T >
inline uint64_t
Request_t< T >::Serialize( const utils::Endian  endianness,
                           utils::byteStream_t& byteStream,
                           const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->id, this->payload );
}

template < typename T >
inline bb::elExpected< utils::ByteStreamError >
Request_t< T >::Deserialize( const utils::Endian        endianness,
                             const utils::byteStream_t& byteStream,
                             const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->id, this->payload )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Request_t<T>", r, byteStream, byteStreamOffset );
        } );
}

} // namespace etm
} // namespace network
} // namespace el3D
