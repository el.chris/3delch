namespace el3D
{
namespace network
{
namespace etm
{
template < typename T >
inline uint64_t
TransportType_t< T >::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->transport_type_id,
                                                       this->payload );
}

template < typename T >
inline uint64_t
TransportType_t< T >::Serialize(
    const utils::Endian endianness, utils::byteStream_t& byteStream,
    const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->transport_type_id,
        this->payload );
}

template < typename T >
inline bb::elExpected< utils::ByteStreamError >
TransportType_t< T >::Deserialize( const utils::Endian        endianness,
                                   const utils::byteStream_t& byteStream,
                                   const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->transport_type_id, this->payload )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "TransportType_t<T>", r, byteStream, byteStreamOffset );
        } );
}

template < typename T >
inline Transmission_t< T >::Transmission_t(
    const uint64_t    transmission_id,
    const MessageType transport_type_id ) noexcept
    : transmission_id( transmission_id )
{
    this->payload.transport_type_id =
        static_cast< uint32_t >( transport_type_id );
}

template < typename T >
inline Transmission_t< T >::Transmission_t( const uint64_t    transmission_id,
                                            const MessageType transport_type_id,
                                            const T&          payload ) noexcept
    : Transmission_t( transmission_id, transport_type_id )
{
    this->payload.payload = payload;
}

template < typename T >
inline uint64_t
Transmission_t< T >::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->length, this->transmission_id, this->payload );
}

template < typename T >
inline uint64_t
Transmission_t< T >::Serialize( const utils::Endian  endianness,
                                utils::byteStream_t& byteStream,
                                const uint64_t byteStreamOffset ) const noexcept
{
    this->length = this->GetSerializedSize() - 8u;
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->length,
        this->transmission_id, this->payload );
}

template < typename T >
inline bb::elExpected< utils::ByteStreamError >
Transmission_t< T >::Deserialize( const utils::Endian        endianness,
                                  const utils::byteStream_t& byteStream,
                                  const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->length, this->transmission_id,
                  this->payload )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Transmission_t<T>", r, byteStream, byteStreamOffset );
        } );
}

} // namespace etm
} // namespace network
} // namespace el3D
