
namespace el3D
{
namespace network
{
template < typename T >
inline void
elEasyTCPMessaging_Server::SendBroadcastMessage(
    const uint64_t transmissionID, const etm::MessageType messageType,
    const T &payload ) noexcept
{
    utils::elByteSerializer serialized;
    serialized.Serialize(
        utils::Endian::Big,
        etm::Transmission_t< T >( transmissionID, messageType, payload ) );
    for ( size_t k = 0, limit = this->connectionVector.size(); k < limit; ++k )
        this->SendRawMessage( serialized.GetByteStream(), k );
}

template < typename T >
inline bool
elEasyTCPMessaging_Server::SendMessage( const uint64_t         transmissionID,
                                        const etm::MessageType messageType,
                                        const T &              payload,
                                        const size_t connectionID ) noexcept
{
    auto &c = this->connectionVector[connectionID];
    if ( !c->hasConnection ) return false;
    if ( c->connection->GetConnections().empty() )
    {
        this->Disconnect( connectionID );
        return false;
    }

    if ( c->connection
             ->Send( c->handle, utils::elByteSerializer()
                                    .Serialize( utils::Endian::Big,
                                                etm::Transmission_t< T >(
                                                    transmissionID, messageType,
                                                    payload ) )
                                    .GetByteStream() )
             .OrElse( [&] { this->Disconnect( connectionID ); } )
             .HasError() )
        return false;

    return true;
}
} // namespace network
} // namespace el3D
