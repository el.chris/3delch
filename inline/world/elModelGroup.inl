namespace el3D
{
namespace world
{
template < typename ShapeType, typename... ShapeTypeCTorArgs >
inline void
elModelGroup::SetPhysicalProperties(
    const physicalProperties_t physicalProperties,
    const ShapeTypeCTorArgs &...args ) noexcept
{
    this->physical = this->GetPhysics().AddCollisionShape< ShapeType >(
        physicalProperties,
        geometricProperties_t{ this->GetPosition(), this->GetScaling() },
        [this]( auto m )
        {
            bb::for_each( this->models,
                          [&]( auto &model ) { model->SetModelMatrix( m ); } );
        },
        args... );
}


} // namespace world
} // namespace el3D
