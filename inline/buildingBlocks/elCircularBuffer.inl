namespace el3D
{
namespace bb
{
template < typename T, uint64_t N >
inline elCircularBuffer< T, N >::elCircularBuffer() noexcept
{
    this->data = static_cast< T* >( malloc( sizeof( T ) * N ) );
}

template < typename T, uint64_t N >
inline elCircularBuffer< T, N >::elCircularBuffer(
    const elCircularBuffer& rhs ) noexcept
{
    *this = rhs;
}

template < typename T, uint64_t N >
inline elCircularBuffer< T, N >::elCircularBuffer(
    elCircularBuffer&& rhs ) noexcept
{
    *this = std::move( rhs );
}

template < typename T, uint64_t N >
inline elCircularBuffer< T, N >::~elCircularBuffer() noexcept
{
    this->Clear();
    this->Free();
}

template < typename T, uint64_t N >
inline void
elCircularBuffer< T, N >::Free() noexcept
{
    if ( this->data != nullptr )
    {
        free( this->data );
        this->data = nullptr;
    }
}

template < typename T, uint64_t N >
inline elCircularBuffer< T, N >&
elCircularBuffer< T, N >::operator=( const elCircularBuffer& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Clear();

        for ( index_t k = rhs.readIndex; k < rhs.writeIndex; ++k )
            new ( this->data + k ) T( rhs.data[k] );

        this->readIndex  = rhs.readIndex;
        this->writeIndex = rhs.writeIndex;
    }
    return *this;
}

template < typename T, uint64_t N >
inline elCircularBuffer< T, N >&
elCircularBuffer< T, N >::operator=( elCircularBuffer&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Clear();
        this->Free();

        this->data       = std::move( rhs.data );
        this->readIndex  = std::move( rhs.readIndex );
        this->writeIndex = std::move( rhs.writeIndex );
        rhs.data         = nullptr;
    }
    return *this;
}

template < typename T, uint64_t N >
template < typename... Targs >
inline std::optional< T >
elCircularBuffer< T, N >::Push( Targs&&... args ) noexcept
{
    std::optional< T > returnValue = std::nullopt;

    if ( this->IsFull() )
    {
        returnValue = this->Pop();
    }

    new ( this->data + this->writeIndex ) T( std::forward< Targs >( args )... );
    this->writeIndex++;

    return returnValue;
}

template < typename T, uint64_t N >
template < typename Allocator >
inline std::optional< std::vector< T > >
elCircularBuffer< T, N >::MultiPush(
    const std::vector< T, Allocator >& values ) noexcept
{
    std::optional< std::vector< T > > returnValue = std::nullopt;

    if ( this->Size() + values.size() > N )
    {
        returnValue = std::make_optional< std::vector< T > >(
            this->Size() + values.size() - N );
    }

    size_t k = 0;
    for ( auto& v : values )
    {
        auto latest = this->Push( v );
        if ( latest.has_value() ) ( *returnValue )[k++] = std::move( *latest );
    }

    return returnValue;
}


template < typename T, uint64_t N >
inline std::optional< T >
elCircularBuffer< T, N >::Pop() noexcept
{
    std::optional< T > returnValue = std::nullopt;

    if ( !this->IsEmpty() )
    {
        returnValue =
            std::make_optional( std::move( this->data[this->readIndex] ) );
        this->data[this->readIndex++].~T();
    }

    return returnValue;
}

template < typename T, uint64_t N >
template < typename OutputContainer >
inline OutputContainer
elCircularBuffer< T, N >::MultiGet(
    const size_t n, const bool doRemoveElementsAfterGet ) noexcept
{
    size_t limit = ( n == 0 ) ? this->Size() : n;

    OutputContainer returnValue( limit );
    if ( limit == 0 ) return returnValue;

    if ( std::is_trivially_copyable_v< T > )
    {
        uint64_t realReadIndex  = static_cast< uint64_t >( readIndex );
        uint64_t realWriteIndex = static_cast< uint64_t >( writeIndex );
        uint64_t fromReadToEnd  = N - realReadIndex;

        if ( realReadIndex >= realWriteIndex && limit > fromReadToEnd )
        {
            memcpy( returnValue.data(), this->data + realReadIndex,
                    sizeof( T ) * fromReadToEnd );
            memcpy( returnValue.data() + fromReadToEnd, this->data,
                    sizeof( T ) * ( limit - fromReadToEnd ) );
        }
        else
        {
            memcpy( returnValue.data(), this->data + realReadIndex,
                    sizeof( T ) * limit );
        }

        if ( doRemoveElementsAfterGet )
        {
            for ( size_t k = 0; k < limit; ++k )
                this->data[readIndex++].~T();
        }
    }
    else
    {
        for ( size_t k = 0; k < limit; ++k )
        {
            returnValue[k] = std::move( this->data[this->readIndex] );
            if ( doRemoveElementsAfterGet ) this->data[readIndex++].~T();
        }
    }

    return returnValue;
}

template < typename T, uint64_t N >
template < typename OutputContainer >
inline std::optional< OutputContainer >
elCircularBuffer< T, N >::MultiPop( const size_t n ) noexcept
{
    if ( n > this->Size() || this->IsEmpty() ) return std::nullopt;
    return std::make_optional(
        this->template MultiGet< OutputContainer >( n, true ) );
}

template < typename T, uint64_t N >
inline void
elCircularBuffer< T, N >::Clear() noexcept
{
    if ( this->data != nullptr )
        for ( ; this->readIndex != this->writeIndex; ++this->readIndex )
            this->data[this->readIndex].~T();
}

template < typename T, uint64_t N >
inline bool
elCircularBuffer< T, N >::IsEmpty() const noexcept
{
    return this->readIndex == this->writeIndex;
}

template < typename T, uint64_t N >
inline uint64_t
elCircularBuffer< T, N >::Size() const noexcept
{
    return static_cast< uint64_t >( this->writeIndex.GetValue() -
                                    this->readIndex.GetValue() );
}

template < typename T, uint64_t N >
inline bool
elCircularBuffer< T, N >::IsFull() const noexcept
{
    return this->Size() == N;
}

template < typename T, uint64_t N >
template < typename OutputContainer >
inline OutputContainer
elCircularBuffer< T, N >::GetContent() const noexcept
{
    return const_cast< elCircularBuffer< T, N >* >( this )
        ->template MultiGet< OutputContainer >( 0, false );
}

} // namespace bb
} // namespace el3D
