namespace el3D
{
namespace bb
{
template < typename integer_t >
integer_t
elUniqueIDHandler< integer_t >::RequestID() noexcept
{
    if ( !this->freeIDs.empty() )
    {
        auto retVal = this->freeIDs.back();
        this->freeIDs.pop_back();
        return retVal;
    }
    return this->nextFreeID++;
}

template < typename integer_t >
bool
elUniqueIDHandler< integer_t >::RequestCustomID( const integer_t id ) noexcept
{
    if ( this->nextFreeID == id )
    {
        this->nextFreeID++;
        return true;
    }
    else if ( this->nextFreeID > id )
    {
        auto iter = bb::find( freeIDs, id );
        if ( iter != freeIDs.end() )
        {
            this->freeIDs.erase( iter );
            return true;
        }
        else
        {
            return false;
        }
    }
    else if ( this->nextFreeID < id )
    {
        for ( integer_t k = this->nextFreeID; k < id; ++k )
            this->freeIDs.push_back( k );
        this->nextFreeID = id + 1;
        return true;
    }

    return true;
}

template < typename integer_t >
void
elUniqueIDHandler< integer_t >::FreeID( const integer_t i ) noexcept
{
    this->freeIDs.push_back( i );
}

template < typename integer_t >
void
elUniqueIDHandler< integer_t >::FreeAll() noexcept
{
    this->freeIDs.clear();
    this->nextFreeID = 0;
}
} // namespace bb
} // namespace el3D
