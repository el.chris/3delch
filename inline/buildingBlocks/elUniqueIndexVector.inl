namespace el3D
{
namespace bb
{

template < typename T >
inline elUniqueIndexVector< T >::elUniqueIndexVector(
    const std::initializer_list< T >& values ) noexcept
{
    for ( auto& v : values )
        this->emplace_back( v );
}

template < typename T >
template < typename... Targs >
inline typename elUniqueIndexVector< T >::element_t
elUniqueIndexVector< T >::emplace_back( Targs&&... t )
{
    auto id = idHandler.RequestID();
    this->emplace_at_position( id, std::forward< Targs >( t )... );
    return element_t{ id, this };
}

template < typename T >
template < typename... Targs >
inline void
elUniqueIndexVector< T >::emplace_at_position( const index_t id, Targs&&... t )
{
    auto pos = this->data.size();

    if ( id >= this->indexToData.size() ) this->indexToData.resize( id + 1 );

    this->indexToData[id] = pos;
    this->data.emplace_back( std::forward< Targs >( t )... );
    ++this->modificationCounter;
}

template < typename T >
template < typename... Targs >
inline bool
elUniqueIndexVector< T >::insert( const index_t id, Targs&&... t )
{
    if ( !idHandler.RequestCustomID( id ) ) return false;
    this->emplace_at_position( id, std::forward< Targs >( t )... );
    return true;
}

template < typename T >
inline const T&
elUniqueIndexVector< T >::operator[]( const index_t n ) const
{
    auto index = this->GetIndexToID( n );
    return this->data[index];
}

template < typename T >
inline T&
elUniqueIndexVector< T >::operator[]( const index_t n )
{
    return const_cast< T& >(
        ( *const_cast< const elUniqueIndexVector* >( this ) )[n] );
}

template < typename T >
inline void
elUniqueIndexVector< T >::erase( const index_t n )
{
    if ( n == INVALID_INDEX ) return;

    this->data.erase( this->data.begin() +
                      static_cast< int64_t >( this->GetIndexToID( n ) ) );
    this->DecrementIndex( n );
    this->idHandler.FreeID( n );
    this->indexToData[n] = INVALID_INDEX;
    ++this->modificationCounter;
}

template < typename T >
inline bool
elUniqueIndexVector< T >::has_index( const index_t n ) const noexcept
{
    if ( this->indexToData.size() <= n ) return false;
    if ( this->indexToData[n] == INVALID_INDEX ) return false;
    return true;
}

template < typename T >
inline typename elUniqueIndexVector< T >::index_t
elUniqueIndexVector< T >::size() const noexcept
{
    return this->data.size();
}

template < typename T >
inline bool
elUniqueIndexVector< T >::empty() const noexcept
{
    return this->data.empty();
}

template < typename T >
inline void
elUniqueIndexVector< T >::clear()
{
    this->data.clear();
    this->indexToData.clear();
    this->idHandler.FreeAll();
    ++this->modificationCounter;
}

template < typename T >
inline typename elUniqueIndexVector< T >::baseIter_t
elUniqueIndexVector< T >::begin()
{
    return this->data.begin();
}

template < typename T >
inline typename elUniqueIndexVector< T >::baseIter_t
elUniqueIndexVector< T >::end()
{
    return this->data.end();
}

template < typename T >
inline typename elUniqueIndexVector< T >::baseIterConst_t
elUniqueIndexVector< T >::begin() const
{
    return this->data.begin();
}

template < typename T >
inline typename elUniqueIndexVector< T >::baseIterConst_t
elUniqueIndexVector< T >::end() const
{
    return this->data.end();
}

template < typename T >
inline typename elUniqueIndexVector< T >::index_t
elUniqueIndexVector< T >::GetIndexToID( const index_t n ) const noexcept
{
    return this->indexToData[n];
}

template < typename T >
inline void
elUniqueIndexVector< T >::DecrementIndex( const index_t n ) noexcept
{
    index_t id = this->GetIndexToID( n );
    for ( auto& i : this->indexToData )
        if ( i > id ) i--;
}

template < typename T >
inline const typename elUniqueIndexVector< T >::indexVector_t&
elUniqueIndexVector< T >::get_index_vector() const noexcept
{
    if ( this->cache.modificationCounter == this->modificationCounter )
        return this->cache.indexToData;

    this->cache.indexToData.clear();

    for ( size_t k = 0, limit = this->indexToData.size(); k < limit; ++k )
        if ( this->indexToData[k] != INVALID_INDEX )
            this->cache.indexToData.emplace_back( k );

    this->cache.modificationCounter = this->modificationCounter;

    return this->cache.indexToData;
}

template < typename T >
inline const typename elUniqueIndexVector< T >::base_t&
elUniqueIndexVector< T >::get_const_contents() const noexcept
{
    return this->data;
}

template < typename T >
inline typename std::span< T >
elUniqueIndexVector< T >::get_contents() noexcept
{
    return std::span< T >{ this->data.begin(), this->data.end() };
}


/// element_t


template < typename T >
inline elUniqueIndexVector< T >::element_t::element_t(
    const index_t indexValue, elUniqueIndexVector< T >* const origin ) noexcept
    : indexValue( indexValue ), origin( origin )
{
}

template < typename T >
inline typename elUniqueIndexVector< T >::index_t
elUniqueIndexVector< T >::element_t::index() const noexcept
{
    return this->indexValue;
}

template < typename T >
inline T&
elUniqueIndexVector< T >::element_t::operator->() noexcept
{
    return ( *this->origin )[this->indexValue];
}

template < typename T >
inline const T&
elUniqueIndexVector< T >::element_t::operator->() const noexcept
{
    return ( *this->origin )[this->indexValue];
}

template < typename T >
inline T&
elUniqueIndexVector< T >::element_t::value() noexcept
{
    return ( *this->origin )[this->indexValue];
}

template < typename T >
inline const T&
elUniqueIndexVector< T >::element_t::value() const noexcept
{
    return ( *this->origin )[this->indexValue];
}

template < typename T >
template < typename F >
inline typename elUniqueIndexVector< T >::index_t
elUniqueIndexVector< T >::find_if( const F& compare ) const noexcept
{
    for ( auto index : this->indexToData )
        if ( index != INVALID_INDEX && compare( this->data[index] ) )
            return index;

    return INVALID_INDEX;
}


} // namespace bb
} // namespace el3D
