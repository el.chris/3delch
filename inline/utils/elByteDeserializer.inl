namespace el3D
{
namespace utils
{

inline elByteDeserializer::elByteDeserializer(
    const utils::byteStream_t& byteStream, const size_t byteStreamOffset )
    : byteStream( byteStream ), byteStreamSize( byteStream.stream.size() ),
      byteStreamOffset( byteStreamOffset )
{
}


template < typename T, typename... Targs >
inline bb::elExpected< utils::ByteStreamError >
elByteDeserializer::Extract( const Endian endianness, T& t,
                             Targs&... args ) noexcept
{
    return this->ExtractWithOffset( endianness, 0, t, args... );
}


template < typename T >
inline bb::elExpected< utils::ByteStreamError >
elByteDeserializer::ExtractWithOffset( const Endian endianness,
                                       const size_t offset, T& t ) noexcept
{
    return this->Get< T >( endianness, offset ).AndThen( [&]( const auto& r ) {
        t = r;
    } );
}


template < typename T, typename... Targs >
inline bb::elExpected< utils::ByteStreamError >
elByteDeserializer::ExtractWithOffset( const Endian endianness,
                                       const size_t offset, T& t,
                                       Targs&... args ) noexcept
{
    auto extractedValue = this->Get< T >( endianness, offset );
    if ( extractedValue.HasError() )
        return bb::Error( extractedValue.GetError() );

    t = extractedValue.GetValue();
    return this->ExtractWithOffset(
        endianness, offset + elByteSerializer::GetSerializedSize( t ),
        args... );
}

template < typename T >
inline std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    auto value = this->Get< typename units::BasicUnit< T >::float_t >(
        endianness, offset );
    if ( value.HasError() ) return bb::Error( value.GetError() );
    T returnValue;
    returnValue.value = value.GetValue();
    return bb::Success( returnValue );
}

template < typename T >
inline std::enable_if_t< std::is_floating_point_v< T >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    size_t absolutOffset = offset + this->byteStreamOffset;
    if ( absolutOffset + sizeof( T ) > this->byteStreamSize )
        return bb::Error( utils::ByteStreamError::TooShort );

    T value;
    for ( size_t k = 0; k < sizeof( T ); ++k )
        reinterpret_cast< byte_t* >( &value )[k] =
            this->byteStream
                .stream[absolutOffset + ( ( endianness == Endian::Big )
                                              ? sizeof( T ) - k - 1
                                              : k )];

    return bb::Success( value );
}


template < typename T >
inline std::enable_if_t< std::is_integral_v< T > && std::is_unsigned_v< T >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    size_t absolutOffset = offset + this->byteStreamOffset;
    if ( absolutOffset + sizeof( T ) > this->byteStreamSize )
        return bb::Error( utils::ByteStreamError::TooShort );

    T value;

    for ( size_t k = 0; k < sizeof( T ); ++k )
        reinterpret_cast< byte_t* >( &value )[k] =
            this->byteStream
                .stream[absolutOffset + ( ( endianness == Endian::Big )
                                              ? sizeof( T ) - k - 1
                                              : k )];

    return bb::Success( value );
}


template < typename T >
inline std::enable_if_t< std::is_integral_v< T > && !std::is_unsigned_v< T >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    auto value = this->Get< std::make_unsigned_t< T > >( endianness, offset );

    if ( value.HasError() )
        return bb::Error( value.GetError() );
    else
        return bb::Success( static_cast< T >( value.GetValue() ) );
}


template < typename T >
inline std::enable_if_t< bb::isVector< T >::value,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    auto vectorSize = this->Get< uint64_t >( endianness, offset + 0 );
    if ( vectorSize.HasError() )
        return bb::Error( utils::ByteStreamError::TooShort );

    T      value( *vectorSize );
    size_t currentPosition{ 8u };
    for ( size_t k = 0; k < *vectorSize; ++k )
    {
        auto element = this->Get< typename T::value_type >(
            endianness, offset + currentPosition );

        if ( element.HasError() )
            return bb::Error(
                utils::ByteStreamError::LengthInformationMismatch );

        currentPosition += elByteSerializer::GetSerializedSize( *element );
        value[k] = *element;
    }

    return bb::Success( value );
}

template < typename T >
inline std::enable_if_t< bb::isArray< T >::value,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    T      value;
    size_t arraySize = value.size();
    size_t currentPosition{ 0u };
    for ( size_t k = 0; k < arraySize; ++k )
    {
        auto element = this->Get< typename T::value_type >(
            endianness, offset + currentPosition );

        if ( element.HasError() )
            return bb::Error(
                utils::ByteStreamError::LengthInformationMismatch );

        currentPosition += elByteSerializer::GetSerializedSize( *element );
        value[k] = *element;
    }

    return bb::Success( value );
}

template < uint32_t N, typename Variant_T >
inline void
elByteDeserializer::SetVariantValue( const Endian endianness, Variant_T& v,
                                     const size_t offset,
                                     bool& setWasSuccessful ) const noexcept
{
    auto value = this->Get< typeAt< Variant_T, N > >( endianness, offset );
    if ( value.HasError() )
        setWasSuccessful = false;
    else
    {
        setWasSuccessful = true;
        v.template emplace< N >( *value );
    }
}


template < typename T >
inline std::enable_if_t< bb::isVariant< T >::value,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    auto variantIndex =
        this->Get< variantIndexType_t >( endianness, offset + 0u );
    if ( variantIndex.HasError() )
        return bb::Error( utils::ByteStreamError::TooShort );

    if ( *variantIndex > bb::NumberOfVariantArguments< T >::value )
        return bb::Error( utils::ByteStreamError::InvalidContent );

    bool setWasSuccessful{ false };
    T    returnValue;

    switch ( *variantIndex )
    {
        case 0:
            this->SetVariantValue<
                ( bb::NumberOfVariantArguments< T >::value > 0 ) ? 0 : 0 >(
                endianness, returnValue, offset + sizeof( variantIndexType_t ),
                setWasSuccessful );
            break;
        case 1:
            this->SetVariantValue<
                ( bb::NumberOfVariantArguments< T >::value > 1 ) ? 1 : 0 >(
                endianness, returnValue, offset + sizeof( variantIndexType_t ),
                setWasSuccessful );
            break;
        case 2:
            this->SetVariantValue<
                ( bb::NumberOfVariantArguments< T >::value > 2 ) ? 2 : 0 >(
                endianness, returnValue, offset + sizeof( variantIndexType_t ),
                setWasSuccessful );
            break;
        case 3:
            this->SetVariantValue<
                ( bb::NumberOfVariantArguments< T >::value > 3 ) ? 3 : 0 >(
                endianness, returnValue, offset + sizeof( variantIndexType_t ),
                setWasSuccessful );
            break;
        case 4:
            this->SetVariantValue<
                ( bb::NumberOfVariantArguments< T >::value > 4 ) ? 4 : 0 >(
                endianness, returnValue, offset + sizeof( variantIndexType_t ),
                setWasSuccessful );
            break;
        default:
            break;
    }

    if ( !setWasSuccessful )
        return bb::Error( utils::ByteStreamError::InvalidContent );

    return bb::Success( returnValue );
}


template < typename T >
inline std::enable_if_t< std::is_same_v< T, std::string >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    auto stringSize = this->Get< uint64_t >( endianness, offset + 0 );
    if ( stringSize.HasError() )
        return bb::Error( utils::ByteStreamError::TooShort );

    if ( this->byteStreamOffset + offset + 8u + *stringSize >
         this->byteStreamSize )
        return bb::Error( utils::ByteStreamError::TooShort );

    std::string value( *stringSize, '\0' );

    MemCpy::From( static_cast< void* >(
                      const_cast< byte_t* >( this->byteStream.stream.data() ) +
                      this->byteStreamOffset + offset + 8u ) )
        .To( static_cast< void* >( const_cast< char* >( value.data() ) ) )
        .Size( *stringSize );

    return bb::Success( value );
}

template < typename T >
inline std::enable_if_t< std::is_same_v< utils::byteStream_t, T >,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian, const size_t offset ) const noexcept
{
    size_t absolutOffset = offset + this->byteStreamOffset;
    if ( this->byteStreamSize < absolutOffset )
        return bb::Error( utils::ByteStreamError::TooShort );

    utils::byteStream_t newElement;
    newElement.stream.resize( this->byteStreamSize - absolutOffset );
    MemCpy::From( static_cast< void* >(
                      const_cast< byte_t* >( this->byteStream.stream.data() ) +
                      absolutOffset ) )
        .To( static_cast< void* >( newElement.stream.data() ) )
        .Size( this->byteStreamSize - absolutOffset );

    return bb::Success( newElement );
}


template < typename T >
inline std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                         bb::elExpected< T, utils::ByteStreamError > >
elByteDeserializer::Get( const Endian endianness,
                         const size_t offset ) const noexcept
{
    T t;
    if ( t.Deserialize( endianness, this->byteStream,
                        this->byteStreamOffset + offset ) )
        return bb::Success( std::move( t ) );
    else
        return bb::Error( utils::ByteStreamError::InvalidContent );
}

} // namespace utils
} // namespace el3D
