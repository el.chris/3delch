namespace el3D
{
namespace utils
{
template < typename T >
inline elThroughputMeasurement< T >::elThroughputMeasurement(
    const units::Time& interval, const T& v ) noexcept
    : interval( interval ), value( v ), startValue( v )
{
}

template < typename T >
inline void
elThroughputMeasurement< T >::IncrementWith( const T& v ) noexcept
{
    this->value += v;
}

template < typename T >
inline T
elThroughputMeasurement< T >::Get() const noexcept
{
    return this->value;
}

template < typename T >
inline elThroughputMeasurement< T >&
elThroughputMeasurement< T >::operator++() noexcept
{
    ++this->value;
    return *this;
}

template < typename T >
inline elThroughputMeasurement< T >&
elThroughputMeasurement< T >::operator+=( const T& rhs ) noexcept
{
    this->value += rhs;
    return *this;
}

template < typename T >
inline long double
elThroughputMeasurement< T >::GetThroughputPerSecond() const noexcept
{
    constexpr long double NANO_SECONDS = 1000000000.0;

    auto        now  = std::chrono::system_clock::now();
    long double diff = static_cast< long double >(
        std::chrono::duration_cast< std::chrono::nanoseconds >( now -
                                                                this->start )
            .count() );

    if ( diff > this->interval.GetNanoSeconds() )
    {
        this->throughPut = static_cast< T >(
            static_cast< long double >( this->value - this->startValue ) /
            ( diff / NANO_SECONDS ) );


        this->startValue = this->value;
        this->start      = now;
    }

    return this->throughPut;
}


} // namespace utils
} // namespace el3D
