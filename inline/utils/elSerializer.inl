namespace el3D
{
namespace utils
{
template < typename T >
elSerializer
elSerializer::Serialize( const T& t )
{
    return elSerializer().Add( ToString( t ) );
}

template < typename T, typename... Targs >
elSerializer
elSerializer::Serialize( const T& t, const Targs&... args )
{
    return Serialize( t ).Add( Serialize( args... ) );
}


template < typename T >
bool
elSerializer::DeserializeTypeAtIndex( const elSerializer& s, const size_t index,
                                      T& t ) const
{
    return FromString< T >( s.Get( index ), t );
}

template < typename T, typename... Targs >
bool
elSerializer::DeserializeTypeAtIndex( const elSerializer& s, const size_t index,
                                      T& t, Targs&... args ) const
{
    if ( !DeserializeTypeAtIndex( s, index, t ) ) return false;
    return DeserializeTypeAtIndex( s, index + 1, args... );
}

template < typename... Targs >
bool
elSerializer::Deserialize( Targs&... args ) const
{
    if ( this->isValid ) return DeserializeTypeAtIndex( *this, 0, args... );
    return false;
}

template < typename T >
typename std::enable_if< std::is_convertible< T, elSerializer >::value &&
                             !std::is_same< T, std::string >::value,
                         std::string >::type
elSerializer::ToString( const T& t )
{
    return static_cast< elSerializer >( t );
}

template < typename T >
typename std::enable_if<
    // all types which are convertible with stream operator
    std::is_arithmetic< T >::value ||
        // string
        std::is_same< T, std::string >::value ||
        // char[N]
        ( std::is_array< T >::value&& std::is_same<
            typename std::remove_extent< T >::type, char >::value ),
    std::string >::type
elSerializer::ToString( const T& t )
{
    std::stringstream converted, returnValue;
    converted << t;
    return converted.str();
}

template < typename T >
typename std::enable_if< !std::is_convertible< T, std::string >::value,
                         bool >::type
elSerializer::FromString( const std::string& value, T& t )
{
    return ConvertDirectlyFromString( value, t );
}

template < typename T >
typename std::enable_if< std::is_convertible< T, std::string >::value,
                         bool >::type
elSerializer::FromString( const std::string& value, T& t )
{
    t = static_cast< T >( value );
    return true;
}
} // namespace utils
} // namespace el3D
