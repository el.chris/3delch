namespace el3D
{
namespace utils
{
template < typename... Targs >
inline uint64_t
elByteSerializer::FillByteStreamWithSerialization(
    const Endian endianness, utils::byteStream_t& byteStream,
    const uint64_t byteStreamOffset, const Targs&... args ) noexcept
{
    return FillByteStreamWithSerializationImpl( endianness, byteStream,
                                                byteStreamOffset, 0, args... );
}

template < typename T >
inline uint64_t
elByteSerializer::FillByteStreamWithSerializationImpl(
    const Endian endianness, utils::byteStream_t& byteStream,
    const uint64_t byteStreamOffset, const uint64_t currentSize,
    const T& t ) noexcept
{
    return currentSize + Convert( endianness, byteStream, byteStreamOffset, t );
}

template < typename T, typename... Targs >
inline uint64_t
elByteSerializer::FillByteStreamWithSerializationImpl(
    const Endian endianness, utils::byteStream_t& byteStream,
    const uint64_t byteStreamOffset, const uint64_t currentSize, const T& t,
    const Targs&... args ) noexcept
{
    uint64_t elementSize =
        Convert( endianness, byteStream, byteStreamOffset, t );
    uint64_t newOffset = byteStreamOffset + elementSize;
    return FillByteStreamWithSerializationImpl(
        endianness, byteStream, newOffset, currentSize + elementSize, args... );
}

template < typename... Targs >
inline elByteSerializer&
elByteSerializer::Serialize( const Endian endianness,
                             const Targs&... args ) noexcept
{
    this->byteStream.stream.resize( GetSerializedSize( args... ) );
    FillByteStreamWithSerialization( endianness, this->byteStream, 0, args... );
    return *this;
}

template < typename T >
inline uint64_t
elByteSerializer::GetSerializedSize( const T& t ) noexcept
{
    return elByteSerializer::GetStreamSizeFor( t );
}

template < typename T, typename... Targs >
inline uint64_t
elByteSerializer::GetSerializedSize( const T& t, const Targs&... args ) noexcept
{
    return GetSerializedSize( t ) + GetSerializedSize( args... );
}


inline void
elByteSerializer::Clear() noexcept
{
    this->byteStream.stream.clear();
    this->currentOffset = 0;
}

template < typename T >
inline std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                         uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    return Convert( endianness, memory, offset, t.value );
}

template < typename T >
inline std::enable_if_t< std::is_floating_point_v< T >, uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    for ( uint64_t k = 0; k < sizeof( T ); ++k )
    {
        memory.stream[offset + ( ( endianness == Endian::Big )
                                     ? sizeof( T ) - k - 1
                                     : k )] =
            reinterpret_cast< const utils::byte_t* >( &t )[k];
    }
    return sizeof( T );
}

template < typename T >
inline std::enable_if_t< std::is_integral_v< T > && std::is_unsigned_v< T >,
                         uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    for ( uint64_t k = 0; k < sizeof( T ); ++k )
    {
        memory.stream[offset + ( ( endianness == Endian::Big )
                                     ? sizeof( T ) - k - 1
                                     : k )] =
            reinterpret_cast< const utils::byte_t* >( &t )[k];
    }
    return sizeof( T );
}

template < typename T >
inline std::enable_if_t< std::is_integral_v< T > && std::is_signed_v< T >,
                         uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    return elByteSerializer::Convert(
        endianness, memory, offset,
        static_cast< std::make_unsigned_t< T > >( t ) );
}

template < typename T >
inline std::enable_if_t< bb::isVector< T >::value, uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    uint64_t writtenBytes = elByteSerializer::Convert(
        endianness, memory, offset + 0u, static_cast< uint64_t >( t.size() ) );

    for ( uint64_t k = 0, limit = t.size(); k < limit; ++k )
    {
        writtenBytes += elByteSerializer::Convert(
            endianness, memory, offset + writtenBytes, t[k] );
    }

    return writtenBytes;
}

template < typename T >
inline std::enable_if_t< bb::isArray< T >::value, uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    uint64_t writtenBytes = 0u;

    for ( uint64_t k = 0, limit = t.size(); k < limit; ++k )
    {
        writtenBytes += elByteSerializer::Convert(
            endianness, memory, offset + writtenBytes, t[k] );
    }

    return writtenBytes;
}

template < typename T >
inline std::enable_if_t< std::is_same_v< T, std::string >, uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    uint64_t stringSize = static_cast< uint64_t >( t.size() );
    elByteSerializer::Convert( endianness, memory, offset + 0u, stringSize );
    MemCpy::From( static_cast< void* >( const_cast< char* >( t.data() ) ) )
        .To( static_cast< void* >( memory.stream.data() + offset + 8u ) )
        .Size( stringSize );

    return 8u + stringSize;
}

template < typename T >
inline std::enable_if_t< std::is_same_v< T, utils::byteStream_t >, uint64_t >
elByteSerializer::Convert( const Endian, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    uint64_t streamSize = t.stream.size();
    MemCpy::From( static_cast< void* >(
                      const_cast< utils::byte_t* >( t.stream.data() ) ) )
        .To( static_cast< void* >( memory.stream.data() + offset ) )
        .Size( streamSize );
    return static_cast< uint64_t >( streamSize );
}


template < typename T >
inline std::enable_if_t< bb::isVariant< T >::value, uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    uint64_t writtenBytes = elByteSerializer::Convert(
        endianness, memory, offset + 0u,
        static_cast< variantIndexType_t >( t.index() ) );
    writtenBytes += std::visit(
        ConvertVariantVisitor_t( endianness, memory,
                                 offset + sizeof( variantIndexType_t ) ),
        t );
    return writtenBytes;
}


template < typename T >
inline std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                         uint64_t >
elByteSerializer::Convert( const Endian endianness, utils::byteStream_t& memory,
                           const uint64_t offset, const T& t ) noexcept
{
    return t.Serialize( endianness, memory, offset );
}


inline const utils::byteStream_t&
elByteSerializer::GetByteStream() const noexcept
{
    return this->byteStream;
}

template < typename T >
inline std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                         uint64_t >
elByteSerializer::GetStreamSizeFor( const T& ) noexcept
{
    return sizeof( typename units::BasicUnit< T >::float_t );
}

template < typename T >
inline std::enable_if_t<
    std::is_integral_v< T > || std::is_floating_point_v< T >, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& ) noexcept
{
    return sizeof( T );
}

template < typename T >
inline std::enable_if_t< bb::isVector< T >::value, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    uint64_t size = 8u;
    for ( auto& e : t )
        size += GetStreamSizeFor( e );
    return size;
}

template < typename T >
inline std::enable_if_t< bb::isArray< T >::value, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    uint64_t size = 0u;
    for ( auto& e : t )
        size += GetStreamSizeFor( e );
    return size;
}

template < typename T >
inline std::enable_if_t< bb::isVariant< T >::value, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    return sizeof( variantIndexType_t ) +
           std::visit( StreamSizeVariantVisitor_t(), t );
}

template < typename T >
inline std::enable_if_t< std::is_same_v< std::string, T >, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    return 8u + t.size();
}


template < typename T >
inline std::enable_if_t< std::is_same_v< T, utils::byteStream_t >, uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    return t.stream.size();
}


template < typename T >
inline std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                         uint64_t >
elByteSerializer::GetStreamSizeFor( const T& t ) noexcept
{
    return t.GetSerializedSize();
}


template < typename T >
inline uint64_t
elByteSerializer::StreamSizeVariantVisitor_t::operator()( T& t )
{
    return elByteSerializer::GetStreamSizeFor( t );
}

template < typename T >
inline uint64_t
elByteSerializer::ConvertVariantVisitor_t::operator()( T& t )
{
    return elByteSerializer::Convert( endianness, this->memory, this->offset,
                                      t );
}

} // namespace utils
} // namespace el3D
