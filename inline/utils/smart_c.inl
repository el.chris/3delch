namespace el3D
{
namespace utils
{
template < typename Func, typename... Targs >
inline bb::elExpected< std::invoke_result_t< Func&, Targs... >, bb::errno_t >
__smart_c(
    const char* file, const int line, const char* func, const char* funcName,
    Func& f,
    const std::function< bool( std::invoke_result_t< Func&, Targs... > ) >&
        isError,
    Targs... args )
{
    using Return               = std::invoke_result_t< Func&, Targs... >;
    const size_t EINTR_RETRIES = 5;
    Return       returnValue;

    for ( size_t k = 0; k < EINTR_RETRIES; ++k )
    {
        errno       = 0;
        returnValue = f( args... );
        if ( errno != EINTR ) break;
    }

    if ( errno == EINTR )
    {
        logging::elLog::Log( logging::ERROR, file, line, func, 0 )
            << "call of function \"" << funcName
            << "\" was interrupted aggressively. Returning this behavior as an "
               "error! ["
            << strerror( errno ) << "]";
        return bb::Error( errno );
    }

    if ( isError( returnValue ) )
    {
        logging::elLog::Log( logging::ERROR, file, line, func, 0 )
            << "call of function \"" << funcName
            << "\" failed with error: " << strerror( errno );
        return bb::Error( errno );
    }

    return bb::Success( returnValue );
}
} // namespace utils
} // namespace el3D
