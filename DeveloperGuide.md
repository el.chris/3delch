## Howto add a new luabinding
1. create a new directory for the component (XYZ) if necessary
```
    mkdir include/luaBindings/XYZ
    mkdir inline/luaBindings/XYZ
    mkdir src/luaBindings/XYZ
```
* if you create a binding for a stl construct use the directory
```
    {include,inline,src}/luaBindings/stl_specific
```

2. Create header (.hpp) and implementation (.inl || .cpp) file. The files name should be identical to the file name of the corresponding cpp file with an additional "_binding" suffix
```
    include/XYZ/MyClass.hpp
    include/luaBindings/XYZ/MyClass_binding.hpp
```
3. implement lua binding
4. add header to include/luaBindings/luaBindings.hpp