//__default__
//!vertex
    #version 330 core

    in vec3 coordinates;
    uniform mat4 mvp;
    uniform mat4 m;

    void main(void) {
        gl_Position = mvp * m * vec4(coordinates, 1.0);
    }

//!fragment
    #version 330 core

    uniform vec4 color = vec4(0.3, 0.3, 0.3, 1.0);
    out vec4 fragcolor;

    void main(void) {
        fragcolor = color;
    }


//__texture__
//!vertex
    #version 330 core

    in vec3 coordinates;
    in vec2 textureCoordinates;

    out vec2 _textureCoordinates;
    out vec4 _color;

    uniform mat4 mvp;
    uniform mat4 m;
    uniform vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

    void main(void) {
        gl_Position = mvp * m *vec4(coordinates, 1.0);
        _textureCoordinates = textureCoordinates;
        _color = color;
    }

//!fragment
    #version 330 core

    in vec2 _textureCoordinates;
    uniform sampler2D textureMap;
    out vec4 fragcolor;
    in vec4 _color;

    void main(void) {
        fragcolor = texture(textureMap, _textureCoordinates) * _color;
    }

//__syntaxError__
//!vertex
    #version 330 core

    in vec3 coordinates;
    in vec2 textureCoordinates;

    out vec4 _color;

    uniform mat4 mvp;
    uniform vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

    void main(void) {
        gl_Position = mvp * m *vec4(coordinates, 1.0);
        _color = color;
    }

//!fragment
    #version 330 core

    in vec2 _textureCoordinates;
    uniform sampler2D textureMap;
    out vec4 fragcolor
    in vec4 _color;

    void main(void) {
        fragcolor = texture(textureMap, _textureCoordinates) * _color;
    }

//__group1,group2,group3__
//!vertex
    #version 330 core

    in vec3 coordinates;
    uniform mat4 mvp;
    uniform mat4 m;

    void main(void) {
        gl_Position = mvp * m * vec4(coordinates, 1.0);
    }

