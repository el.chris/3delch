#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <fstream>

#ifdef _WIN32
#define PATH_SEPARATOR std::string( "\\" )
#else
#define PATH_SEPARATOR std::string( "/" )
#endif

#define UNIT_TEST_DIR                                                          \
    std::string( "unitTest" ) + PATH_SEPARATOR + std::string( "data" ) +       \
        PATH_SEPARATOR

#include "logging/elLog.hpp"
