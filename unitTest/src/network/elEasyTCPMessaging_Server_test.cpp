#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/elNetworkSocket_Client.hpp"
#include "network/etm/Connect_t.hpp"
#include "network/etm/Identify_t.hpp"
#include "network/etm/Request_t.hpp"
#include "network/etm/Transmission_t.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::network;
using namespace ::el3D::network::etm;

class elEasyTCPMessaging_Server_test : public Test
{
  public:
    static constexpr char     SERVICE[] = "testService";
    static constexpr uint32_t VERSION   = 121;

    elEasyTCPMessaging_Server_test() : server( SERVICE, VERSION )
    {
    }

    void
    Listen( const uint16_t port )
    {
        ASSERT_THAT( server.Listen( port ).HasError(), Eq( false ) );
    }

    void
    ConnectTo( const uint16_t port )
    {
        auto result = client.Connect( "127.0.0.1", port ).get();
        ASSERT_THAT( result.HasError(), Eq( false ) );
    }

    template < typename T, typename Request >
    std::optional< T >
    SendRequest( const Request& request )
    {
        if ( client
                 .Send( utils::elByteSerializer()
                            .Serialize( utils::Endian::Big, request )
                            .GetByteStream() )
                 .HasError() )
            return std::nullopt;
        auto answer = client.TimedReceive( timeout );

        if ( !answer.has_value() ) return std::nullopt;
        if ( answer->stream.empty() ) return std::nullopt;

        T response;
        if ( utils::elByteDeserializer( *answer )
                 .Extract( utils::Endian::Big, response )
                 .HasError() )
            return std::nullopt;

        return response;
    }

    void
    EstablishETMConnection( const uint16_t          port,
                            elNetworkSocket_Client* connection )
    {
        ConnectTo( port );

        Transmission_t< Request_t< Connect_t::Request > > request(
            7951u, MessageType::Request );
        request.payload.payload.id =
            static_cast< uint32_t >( etm::RequestID::Connect );

        auto response =
            SendRequest< Transmission_t< Request_t< Connect_t::Response > > >(
                request );

        ASSERT_THAT( response.has_value(), Eq( true ) );

        ASSERT_THAT(
            connection
                ->Connect( "127.0.0.1", response->payload.payload.payload.port )
                .get()
                .HasError(),
            Eq( false ) );
    }

    units::Time               timeout = units::Time::MilliSeconds( 500 );
    elNetworkSocket_Client    client;
    elEasyTCPMessaging_Server server;
};

TEST_F( elEasyTCPMessaging_Server_test, ValidIdentifyRequest )
{
    Listen( 5567 );
    ConnectTo( 5567 );

    Transmission_t< Request_t< Identify_t::Request > > request;
    request.transmission_id = 4451u;
    request.payload.transport_type_id =
        static_cast< uint32_t >( MessageType::Request );

    auto response =
        SendRequest< Transmission_t< Request_t< Identify_t::Response > > >(
            request );

    ASSERT_THAT( response.has_value(), Eq( true ) );
    EXPECT_THAT( response->transmission_id, Eq( 4451u ) );
    EXPECT_THAT( response->payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Response ) ) );
    EXPECT_THAT( response->payload.payload.id,
                 Eq( static_cast< uint32_t >( RequestID::Identify ) ) );

    EXPECT_THAT( response->payload.payload.payload.id,
                 Eq( std::string( SERVICE ) ) );
    EXPECT_THAT( response->payload.payload.payload.service_protocol_version,
                 Eq( VERSION ) );
    EXPECT_THAT( response->payload.payload.payload.protocol_version,
                 Eq( etm::PROTOCOL_VERSION ) );
}

TEST_F( elEasyTCPMessaging_Server_test, InvalidManagementRequestID )
{
    Listen( 5567 );
    ConnectTo( 5567 );

    Transmission_t< Request_t< Identify_t::Request > > request;
    request.transmission_id = 441u;
    request.payload.transport_type_id =
        static_cast< uint32_t >( MessageType::Request );
    request.payload.payload.id = 891234;

    auto response = SendRequest< Transmission_t< void > >( request );
    ASSERT_THAT( response.has_value(), Eq( true ) );
    EXPECT_THAT( response->transmission_id, Eq( 441u ) );
    EXPECT_THAT( response->payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Error ) ) );
}

TEST_F( elEasyTCPMessaging_Server_test, InvalidMessageType )
{
    Listen( 5567 );
    ConnectTo( 5567 );

    Transmission_t< Request_t< Identify_t::Request > > request;
    request.transmission_id           = 4121u;
    request.payload.transport_type_id = 99128;
    request.payload.payload.id = static_cast< uint32_t >( RequestID::Identify );

    auto response = SendRequest< Transmission_t< void > >( request );
    ASSERT_THAT( response.has_value(), Eq( true ) );
    EXPECT_THAT( response->transmission_id, Eq( 4121u ) );
    EXPECT_THAT( response->payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Error ) ) );
}

TEST_F( elEasyTCPMessaging_Server_test, ValidConnectRequest )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );
    ConnectTo( 5567 );

    Transmission_t< Request_t< Connect_t::Request > > request(
        7751u, MessageType::Request );
    request.payload.payload.id =
        static_cast< uint32_t >( etm::RequestID::Connect );
    request.payload.payload.payload.connection_id = 7722;

    auto response =
        SendRequest< Transmission_t< Request_t< Connect_t::Response > > >(
            request );

    ASSERT_THAT( response.has_value(), Eq( true ) );
    EXPECT_THAT( response->transmission_id, Eq( 7751u ) );
    EXPECT_THAT( response->payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Response ) ) );
    EXPECT_THAT( response->payload.payload.id,
                 Eq( static_cast< uint32_t >( RequestID::Connect ) ) );

    EXPECT_THAT( response->payload.payload.payload.connection_id, Eq( 7722 ) );
    EXPECT_THAT( response->payload.payload.payload.port >= 1024, Eq( true ) );
}

TEST_F( elEasyTCPMessaging_Server_test, ConnectToStreamServer )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 400 ) );
    server.SetAsynchronousConnectionCallback(
        [&]( concurrent::elSmartLock< elNetworkSocket_Server >* s,
             socketHandleType_t                                 handle ) {
            Transmission_t< int > data( 991u, MessageType::Stream, 1337 );
            ( *s )->Send( handle, utils::elByteSerializer()
                                      .Serialize( utils::Endian::Big, data )
                                      .GetByteStream() );
        } );

    ConnectTo( 5567 );

    Transmission_t< Request_t< Connect_t::Request > > request(
        7951u, MessageType::Request );
    request.payload.payload.id =
        static_cast< uint32_t >( etm::RequestID::Connect );

    auto response =
        SendRequest< Transmission_t< Request_t< Connect_t::Response > > >(
            request );

    ASSERT_THAT( response.has_value(), Eq( true ) );

    elNetworkSocket_Client permanentConnection;
    ASSERT_THAT(
        permanentConnection
            .Connect( "127.0.0.1", response->payload.payload.payload.port )
            .get()
            .HasError(),
        Eq( false ) );

    auto answer = permanentConnection.BlockingReceive();
    ASSERT_THAT( answer.has_value(), Eq( true ) );
    Transmission_t< int > data;
    ASSERT_THAT( utils::elByteDeserializer( answer.value() )
                     .Extract( utils::Endian::Big, data )
                     .HasError(),
                 Eq( false ) );
    EXPECT_THAT( data.transmission_id, Eq( 991u ) );
    EXPECT_THAT( data.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Stream ) ) );
    EXPECT_THAT( data.payload.payload, Eq( 1337 ) );
}

TEST_F( elEasyTCPMessaging_Server_test, ConnectToRequestResponseServer )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );
    server.SetAsynchronousConnectionCallback(
        [&]( concurrent::elSmartLock< elNetworkSocket_Server >* s,
             socketHandleType_t                                 handle ) {
            Transmission_t< int > request;
            utils::elByteDeserializer(
                ( *s )->BlockingReceive( handle ).value() )
                .Extract( utils::Endian::Big, request );

            Transmission_t< int > data( request.transmission_id,
                                        MessageType::Response,
                                        request.payload.payload * 557 );
            ( *s )->Send( handle, utils::elByteSerializer()
                                      .Serialize( utils::Endian::Big, data )
                                      .GetByteStream() );
        } );


    elNetworkSocket_Client permanentConnection;
    EstablishETMConnection( 5567, &permanentConnection );

    permanentConnection.Send(
        utils::elByteSerializer()
            .Serialize(
                utils::Endian::Big,
                Transmission_t< int >( 91294, MessageType::Request, 789 ) )
            .GetByteStream() );
    auto answer = permanentConnection.BlockingReceive();
    ASSERT_THAT( answer.has_value(), Eq( true ) );
    Transmission_t< int > data;
    ASSERT_THAT( utils::elByteDeserializer( *answer )
                     .Extract( utils::Endian::Big, data )
                     .HasError(),
                 Eq( false ) );
    EXPECT_THAT( data.transmission_id, Eq( 91294u ) );
    EXPECT_THAT( data.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Response ) ) );
    EXPECT_THAT( data.payload.payload, Eq( 789 * 557 ) );
}

TEST_F( elEasyTCPMessaging_Server_test, BroadcastMessageToTwoClients )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );

    elNetworkSocket_Client client1, client2;
    EstablishETMConnection( 5567, &client1 );
    EstablishETMConnection( 5567, &client2 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    server.SendBroadcastMessage< int >( 321, MessageType::Stream, 121557 );

    auto answer1 = client1.BlockingReceive();
    auto answer2 = client2.BlockingReceive();

    ASSERT_THAT( answer1.has_value(), Eq( true ) );
    ASSERT_THAT( answer2.has_value(), Eq( true ) );

    Transmission_t< int > data1;
    Transmission_t< int > data2;
    elByteDeserializer( answer1.value() ).Extract( utils::Endian::Big, data1 );
    elByteDeserializer( answer2.value() ).Extract( utils::Endian::Big, data2 );

    EXPECT_THAT( data1.transmission_id, Eq( 321u ) );
    EXPECT_THAT( data2.transmission_id, Eq( 321u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Stream ) ) );
    EXPECT_THAT( data2.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Stream ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 121557 ) );
    EXPECT_THAT( data2.payload.payload, Eq( 121557 ) );
}

TEST_F( elEasyTCPMessaging_Server_test, SendMessageToTwoClients )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );

    elNetworkSocket_Client client1, client2;
    EstablishETMConnection( 5567, &client1 );
    EstablishETMConnection( 5567, &client2 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    auto conn = server.RefreshAndReturnConnections();
    ASSERT_THAT( conn.size(), Eq( 2 ) );
    server.SendMessage< int >( 8912, MessageType::Stream, 9912, conn[0] );
    server.SendMessage< int >( 78912, MessageType::Stream, 79912, conn[1] );

    auto answer1 = client1.BlockingReceive();
    auto answer2 = client2.BlockingReceive();

    ASSERT_THAT( answer1.has_value(), Eq( true ) );
    ASSERT_THAT( answer2.has_value(), Eq( true ) );

    Transmission_t< int > data1;
    Transmission_t< int > data2;
    utils::elByteDeserializer( answer1.value() )
        .Extract( utils::Endian::Big, data1 );
    utils::elByteDeserializer( answer2.value() )
        .Extract( utils::Endian::Big, data2 );

    EXPECT_THAT( data1.transmission_id, Eq( 8912u ) );
    EXPECT_THAT( data2.transmission_id, Eq( 78912u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Stream ) ) );
    EXPECT_THAT( data2.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Stream ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 9912 ) );
    EXPECT_THAT( data2.payload.payload, Eq( 79912 ) );
}

TEST_F( elEasyTCPMessaging_Server_test, ReceiveMessageFromTwoClients )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );

    elNetworkSocket_Client client1, client2;
    EstablishETMConnection( 5567, &client1 );
    EstablishETMConnection( 5567, &client2 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    client1.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      5567, MessageType::Request, 90338 ) )
                      .GetByteStream() );
    client2.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      95567, MessageType::Request, 9908 ) )
                      .GetByteStream() );

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    auto messages = server.TryReceiveMessages();
    ASSERT_THAT( messages.size(), Eq( 2 ) );

    Transmission_t< int > data1;
    Transmission_t< int > data2;

    ASSERT_THAT( utils::elByteDeserializer( messages[0].message )
                     .Extract( utils::Endian::Big, data1 )
                     .HasError(),
                 Eq( false ) );
    ASSERT_THAT( utils::elByteDeserializer( messages[1].message )
                     .Extract( utils::Endian::Big, data2 )
                     .HasError(),
                 Eq( false ) );

    EXPECT_THAT( data1.transmission_id, Eq( 5567u ) );
    EXPECT_THAT( data2.transmission_id, Eq( 95567u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Request ) ) );
    EXPECT_THAT( data2.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Request ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 90338 ) );
    EXPECT_THAT( data2.payload.payload, Eq( 9908 ) );
}

TEST_F( elEasyTCPMessaging_Server_test, RequestResponseWithTwoClients )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );

    elNetworkSocket_Client client1, client2;
    EstablishETMConnection( 5567, &client1 );
    EstablishETMConnection( 5567, &client2 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    /// send request
    client1.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      556, MessageType::Request, 9038 ) )
                      .GetByteStream() );
    client2.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      9556, MessageType::Request, 908 ) )
                      .GetByteStream() );

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    /// receive
    auto messages = server.TryReceiveMessages();
    ASSERT_THAT( messages.size(), Eq( 2 ) );

    Transmission_t< int > data1;
    Transmission_t< int > data2;
    utils::elByteDeserializer( messages[0].message )
        .Extract( utils::Endian::Big, data1 );
    utils::elByteDeserializer( messages[1].message )
        .Extract( utils::Endian::Big, data2 );

    /// send response
    server.SendMessage< int >( 57, MessageType::Response,
                               data1.payload.payload * 7,
                               messages[0].connectionID );
    server.SendMessage< int >( 967, MessageType::Response,
                               data2.payload.payload * 5,
                               messages[1].connectionID );

    /// receive response
    auto answer1 = client1.BlockingReceive();
    auto answer2 = client2.BlockingReceive();

    ASSERT_THAT( answer1.has_value(), Eq( true ) );
    ASSERT_THAT( answer2.has_value(), Eq( true ) );

    utils::elByteDeserializer( answer1.value() )
        .Extract( utils::Endian::Big, data1 );
    utils::elByteDeserializer( answer2.value() )
        .Extract( utils::Endian::Big, data2 );

    EXPECT_THAT( data1.transmission_id, Eq( 57u ) );
    EXPECT_THAT( data2.transmission_id, Eq( 967u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Response ) ) );
    EXPECT_THAT( data2.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Response ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 9038 * 7 ) );
    EXPECT_THAT( data2.payload.payload, Eq( 908 * 5 ) );
}

TEST_F( elEasyTCPMessaging_Server_test,
        BlockingReceiveIsBlockingTillMessageArrives )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );
    std::vector< elEasyTCPMessaging_Server::receivedMessage_t > messages;

    std::atomic_bool hasMessagesReceived = false;
    std::thread      t( [&] {
        messages            = server.BlockingReceiveMessages();
        hasMessagesReceived = true;
    } );

    elNetworkSocket_Client client1;
    EstablishETMConnection( 5567, &client1 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    ASSERT_THAT( hasMessagesReceived.load(), Eq( false ) );

    client1.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      5567, MessageType::Request, 90338 ) )
                      .GetByteStream() );

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );
    ASSERT_THAT( hasMessagesReceived.load(), Eq( true ) );

    ASSERT_THAT( messages.size(), Eq( 1 ) );

    Transmission_t< int > data1;

    ASSERT_THAT( utils::elByteDeserializer( messages[0].message )
                     .Extract( utils::Endian::Big, data1 )
                     .HasError(),
                 Eq( false ) );

    EXPECT_THAT( data1.transmission_id, Eq( 5567u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Request ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 90338 ) );
    t.join();
}

TEST_F( elEasyTCPMessaging_Server_test,
        BlockingReceiveReturnsImmediatelyWhenThereAreMessages )
{
    Listen( 5567 );
    server.SetTimeout( units::Time::MilliSeconds( 100 ) );
    std::vector< elEasyTCPMessaging_Server::receivedMessage_t > messages;


    elNetworkSocket_Client client1;
    EstablishETMConnection( 5567, &client1 );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    client1.Send( utils::elByteSerializer()
                      .Serialize( utils::Endian::Big,
                                  Transmission_t< int >(
                                      5567, MessageType::Request, 90338 ) )
                      .GetByteStream() );

    std::this_thread::sleep_for( std::chrono::milliseconds( 50 ) );

    std::atomic_bool hasMessagesReceived = false;
    std::thread      t( [&] {
        messages            = server.BlockingReceiveMessages();
        hasMessagesReceived = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    ASSERT_THAT( hasMessagesReceived.load(), Eq( true ) );
    ASSERT_THAT( messages.size(), Eq( 1 ) );

    Transmission_t< int > data1;

    ASSERT_THAT( utils::elByteDeserializer( messages[0].message )
                     .Extract( utils::Endian::Big, data1 )
                     .HasError(),
                 Eq( false ) );

    EXPECT_THAT( data1.transmission_id, Eq( 5567u ) );

    EXPECT_THAT( data1.payload.transport_type_id,
                 Eq( static_cast< uint32_t >( MessageType::Request ) ) );

    EXPECT_THAT( data1.payload.payload, Eq( 90338 ) );
    t.join();
}
