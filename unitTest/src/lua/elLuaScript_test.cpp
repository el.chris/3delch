#include "lua/elLuaScript.hpp"
#include "luaBindings/luaBindings.hpp"
#include "unitTest.hpp"
#include "utils/elFile.hpp"

using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::lua;
using namespace ::testing;

class elLuaScript_test : public ::testing::Test
{
  public:
    void
    SetUp()
    {
    }
};


TEST_F( elLuaScript_test, LuaFunctionCallsAndSource )
{
    testing::internal::CaptureStderr();
    elLuaScript file1 = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_TRUE( file1.Exec().HasError() );
    EXPECT_FALSE( file1.SetSource( "a=3; b=5; c=a+b" ).HasError() );
    EXPECT_FALSE( file1.Exec().HasError() );

    EXPECT_FALSE( file1.SetSource( "function bas() a=3+4 end\n" ).HasError() );
    EXPECT_FALSE( file1.CallVoid( "bas" ).HasError() );
    EXPECT_FALSE(
        file1.SetSource( "function bas(c, d) a=c+d end\n" ).HasError() );
    EXPECT_FALSE( file1.CallVoid( "bas", 1, 2 ).HasError() );

    EXPECT_TRUE( file1.CallVoid( "basf" ).HasError() );
    EXPECT_FALSE(
        file1.SetSource( "function abc(a, b) return a + b; end" ).HasError() );
    EXPECT_FALSE( file1.Call< lua_Integer >( "abc", 3, 1 ).HasError() );
    EXPECT_FALSE( file1.Call< lua_Integer >( "abc", 3, 1, 5, 2 ).HasError() );
    EXPECT_TRUE( file1.Call< lua_Integer >( "abc", 3 ).HasError() );
    EXPECT_TRUE( file1.Call< lua_Integer >( "abcf", 3, 4 ).HasError() );
    EXPECT_EQ( file1.Call< lua_Integer >( "abc", 3, 1 ).GetValue()[0], 4 );

    EXPECT_FALSE( file1.Call< lua_Number >( "abc", 3.5, 1.2 ).HasError() );
    EXPECT_EQ( file1.Call< lua_Number >( "abc", 3.5, 1.2 ).GetValue()[0], 4.7 );

    EXPECT_FALSE(
        file1.SetSource( "function abc() return 1, 2, 3; end" ).HasError() );
    EXPECT_FALSE( file1.Call< lua_Number >( "abc" ).HasError() );
    EXPECT_EQ( file1.Call< lua_Number >( "abc" ).GetValue(),
               std::vector< lua_Number >( {1, 2, 3} ) );

    EXPECT_TRUE(
        file1.SetSource( "function abca, b) return a + b; end" ).HasError() );

    EXPECT_FALSE(
        file1.SetSource( "function abc() return 'hello'; end" ).HasError() );
    EXPECT_FALSE( file1.Call< std::string >( "abc" ).HasError() );
    EXPECT_EQ( file1.Call< std::string >( "abc" ).GetValue()[0], "hello" );

    testing::internal::GetCapturedStderr();
}

TEST_F( elLuaScript_test, GetLuaVariables )
{
    testing::internal::CaptureStderr();
    elLuaScript file = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_FALSE(
        file.SetSource(
                "var1 = 12; var2 = 'hello'; var3 = {12.0, 122.0, 123.5}" )
            .HasError() );
    EXPECT_EQ( file.GetGlobal< lua_Integer >( {"var1"} )[0], 12 );
    EXPECT_EQ( file.GetGlobal< std::string >( {"var2"} )[0], "hello" );
    EXPECT_EQ( file.GetGlobal< std::string >( {"var2"} )[0], "hello" );
    EXPECT_EQ( file.GetGlobal< lua_Number >( {"var3"} ),
               std::vector< lua_Number >( {12.0, 122.0, 123.5} ) );

    EXPECT_FALSE(
        file.SetSource( "var2 = {'hello', 'bla', 'blubb'}" ).HasError() );
    EXPECT_EQ( file.GetGlobal< std::string >( {"var2"} ),
               std::vector< std::string >( {"hello", "bla", "blubb"} ) );
    EXPECT_EQ( file.GetGlobal< lua_Integer >( {"var2"} )[0], 0 );
    EXPECT_EQ( file.GetGlobal< lua_Integer >( {"var1XXX"} ).size(), 0 );

    testing::internal::GetCapturedStderr();
}

TEST_F( elLuaScript_test, GetLuaTables )
{
    testing::internal::CaptureStderr();
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_FALSE(
        script
            .SetSource(
                "tab1 = { ent1 = 2, ent2 = 3, ent4 = {sub1 = 5, sub2 = 6, "
                "sub3 = { a = {1.2, 2.2, 3.2}}}};"
                "tab2 = { a = 2, b = {2, 3}, {1,2,3}};"
                "fuu = 2; tab4 = { ent1 = 2, ent2 = 3, ent4 = {sub1 = 5, "
                "sub2 = 6, sub3 = { a = {8.2, 2.2, 3.2}}}}" )
            .HasError() );
    EXPECT_NO_THROW( script.GetGlobal< lua_Integer >( {"tab1", "ent1"} ) );
    EXPECT_EQ( script.GetGlobal< std::string >( {"tab2XX", "ent1"} ).size(),
               0 );
    EXPECT_EQ( script.GetGlobal< std::string >( {"tab1", "ent1XX"} ).size(),
               0 );
    EXPECT_EQ( script.GetGlobal< lua_Integer >( {"tab1", "ent1"} )[0], 2 );
    EXPECT_EQ( script.GetGlobal< lua_Integer >( {"tab1", "ent1"} )[0], 2 );
    EXPECT_EQ( script.GetGlobal< lua_Integer >( {"tab1", "ent4", "sub1"} )[0],
               5 );
    EXPECT_EQ( script.GetGlobal< lua_Number >( {"tab1", "ent4", "sub3", "a"} ),
               std::vector< lua_Number >( {1.2, 2.2, 3.2} ) );

    EXPECT_EQ(
        script.GetGlobal< lua_Number >( {"tab1", "ent4", "sub3", "a", "b"} )
            .size(),
        0 );
    EXPECT_EQ( script.GetGlobal< lua_Number >( {"tab2", "b", "1"} ).size(), 0 );
    EXPECT_EQ( script.GetGlobal< lua_Number >( {"tab2", "c", "2"} ).size(), 0 );
    EXPECT_EQ(
        script.GetGlobal< lua_Number >( {"tab4", "ent4", "sub3", "a", "1.0"} )
            .size(),
        0 );
    EXPECT_EQ( script.GetGlobal< lua_Number >( {"tab56", "tab4"} ).size(), 0 );
    EXPECT_EQ( script.GetGlobal< lua_Number >( {"tab56", "tab5"} ).size(), 0 );


    testing::internal::GetCapturedStderr();
}

TEST_F( elLuaScript_test, GetLuaTableEntries )
{
    testing::internal::CaptureStderr();
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );

    EXPECT_FALSE(
        script
            .SetSource(
                "fuu = 2; tab1 = { ent1 = 2, ent2 = 3, ent4 = {sub1 = 5, "
                "sub2 = 6, sub3 = { a = {8.2, 2.2, 3.2}}}}" )
            .HasError() );
    EXPECT_NO_THROW( script.GetGlobalTableEntries( {"tab1"} ) );
    EXPECT_EQ( script.GetGlobalTableEntries( {"tab1", "ent1"} ).size(), 0 );
    EXPECT_EQ( script.GetGlobalTableEntries( {"tab1", "ent1", "fuu"} ).size(),
               0 );
    EXPECT_EQ( script.GetGlobalTableEntries( {"fuu"} ).size(), 0 );

    auto t1 = script.GetGlobalTableEntries( {"tab1"} );
    std::sort( t1.begin(), t1.end() );
    EXPECT_EQ( t1, std::vector< std::string >( {"ent1", "ent2", "ent4"} ) );

    auto t2 = script.GetGlobalTableEntries( {"tab1", "ent4"} );
    std::sort( t2.begin(), t2.end() );
    EXPECT_EQ( t2, std::vector< std::string >( {"sub1", "sub2", "sub3"} ) );

    EXPECT_EQ(
        script.GetGlobalTableEntries( {"tab1", "ent4", "sub3", "b"} ).size(),
        0 );
    EXPECT_EQ(
        script.GetGlobalTableEntries( {"tab1", "ent4", "sub3", "a"} ).size(),
        3 );

    EXPECT_EQ( script.GetGlobalTableEntries( {"tab1", "ent4", "suXX"} ).size(),
               0 );
    EXPECT_EQ( script.GetGlobalTableEntries( {"tXXX"} ).size(), 0 );

    // TODO: memory corruption is here
    EXPECT_EQ(
        script.GetGlobalTableEntries( {"tab1", "ent4", "sub3", "a", "1.0"} )
            .size(),
        0 );
    testing::internal::GetCapturedStderr();
}

TEST_F( elLuaScript_test, ConfigFileTest )
{
    elLuaScript config = std::move( *elLuaScript::Create().GetValue() );
    elFile      configFile( std::string( UNIT_TEST_DIR ) + "config.lua" );

    EXPECT_TRUE( configFile.Read() );
    EXPECT_FALSE(
        config.SetSource( configFile.GetContentAsString() ).HasError() );
    EXPECT_EQ( config.GetGlobal< lua_Integer >( {"bla"} )[0], 123 );
    EXPECT_EQ( config.GetGlobal< lua_Number >( {"fuu"} )[0], 12.012 );
    EXPECT_EQ( config.GetGlobal< std::string >( {"var3"} )[0], "hello world" );
    EXPECT_EQ( config.GetGlobal< lua_Integer >( {"array1"} ),
               std::vector< lua_Integer >( {1, 2, 3} ) );

    EXPECT_EQ( config.GetGlobal< lua_Integer >( {"table1", "a"} ),
               std::vector< lua_Integer >( {1, 2, 3} ) );
}

int
sum( int a, int b )
{
    return a + b;
}

void
testFunc( int, int, int )
{
}

int
testFunc2()
{
    return 42;
}

TEST_F( elLuaScript_test, RegisterLocalFunction )
{
    //    testing::internal::CaptureStderr();
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.SetSource( "function bla() return sum(1, 2) end" );
    elLuaScript::RegisterFunction( "sum", sum, "ut1" );
    elLuaScript::RegisterFunction( "testFunc2", testFunc2, "ut1" );
    elLuaScript::RegisterFunction( "testFunc", testFunc, "ut1" );
    elLuaScript::RegisterFunction( "tf", testFunc, "ut1" );
    script.RequireNamespace( "ut1" );

    EXPECT_EQ( script.Call< lua_Integer >( "bla" ).GetValue()[0], 3 );

    script.SetSource( "function bla() return testFunc2() end" );
    EXPECT_EQ( script.Call< lua_Integer >( "bla" ).GetValue()[0], 42 );

    script.SetSource( "function bla() testFunc(1,2,3) end" );
    EXPECT_FALSE( script.Call< lua_Integer >( "bla" ).HasError() );

    script.SetSource( "function bla() testFunc(1) end" );
    EXPECT_THROW( script.Call< lua_Integer >( "bla" ), elLuaScript_Exception );
    script.SetSource( "function bla() testFunc() end" );
    EXPECT_THROW( script.Call< lua_Integer >( "bla" ), elLuaScript_Exception );
    script.SetSource( "function bla() testFunc(1,2,3,4,5) end" );
    EXPECT_THROW( script.Call< lua_Integer >( "bla" ), elLuaScript_Exception );


    script.SetSource( "function bla() tf(1,2,3) end" );
    EXPECT_NO_THROW( script.Call< lua_Integer >( "bla" ) );

    elLuaScript::RegisterFunction( "sum", sum, "math2" );

    elLuaScript script2 = std::move( *elLuaScript::Create().GetValue() );
    script2.RequireNamespace( "math2" );
    script2.SetSource( "function bla() return sum(5, 6) end" );
    EXPECT_EQ( script2.Call< lua_Integer >( "bla" ).GetValue()[0], 11 );
    //   testing::internal::GetCapturedStderr();
}


void
glbTestFunc1()
{
}
int
glbTestFunc2( int a )
{
    return 2 * a;
}

TEST_F( elLuaScript_test, RegisterGlobalFunc )
{
    elLuaScript::RegisterFunction( "glb1", glbTestFunc1, "glbTest" );
    elLuaScript::RegisterFunction( "glb2", glbTestFunc2, "glbTest" );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.SetSource(
        "function a() glb1() end function b() return glb2(4) end" ) );
    script.RequireNamespace( "glbTest" );
    EXPECT_NO_THROW( script.CallVoid( "a" ) );
    EXPECT_EQ( script.Call< lua_Integer >( "b" ).GetValue()[0], 8 );
}

bool
glbBoolFunction( int a )
{
    return a > 5;
}

TEST_F( elLuaScript_test, FunctionReturnsBool )
{
    elLuaScript::RegisterFunction( "glbBool", glbBoolFunction, "glbTest" );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.SetSource(
        "function a(val) return glbBool(val) end function b(val) if val > 5 "
        "then return true else return false end end" ) );
    script.RequireNamespace( "glbTest" );
    EXPECT_THAT( script.Call< bool >( "a", 7 ).GetValue()[0], Eq( true ) );
    EXPECT_THAT( script.Call< bool >( "a", 3 ).GetValue()[0], Eq( false ) );
    EXPECT_THAT( script.Call< bool >( "b", 7 ).GetValue()[0], Eq( true ) );
    EXPECT_THAT( script.Call< bool >( "b", 3 ).GetValue()[0], Eq( false ) );
}

#ifdef ADD_LUA_BINDINGS
LUA_F( elLuaScript_unittest )
{
    elLuaScript::RegisterFunction( "glbPre1", glbTestFunc1, "glbTest2" );
    elLuaScript::RegisterFunction( "glbPre2", glbTestFunc2, "glbTest2" );
}

LUA_R( elLuaScript_unittest );

TEST_F( elLuaScript_test, PredefinedGlobalFunc )
{
    LUA_INIT_PREDEF_FUNCTIONS;

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.SetSource(
        "function a() glbPre1() end; function b() return glbPre2(4) end" ) );
    script.RequireNamespace( "glbTest2" );
    EXPECT_NO_THROW( script.CallVoid( "a" ) );
    EXPECT_EQ( script.Call< lua_Integer >( "b" ).GetValue()[0], 8 );
}
#endif


class luaClassTest
{
  public:
    luaClassTest()
    {
    }
    luaClassTest( int a )
    {
        fuu = a;
    }
    ~luaClassTest()
    {
    }
    void
    func1() const
    {
    }
    int
    func2( int a, int b ) const
    {
        return fuu + a + b;
    }
    static int
    func3()
    {
        return 42;
    }
    void
    SetFuu( int _fuu )
    {
        fuu = _fuu;
    }
    int fuu;
};


class luaClassTestB
{
  public:
    luaClassTestB()
    {
    }
    luaClassTestB( std::string a )
    {
        fuu = a;
    }
    ~luaClassTestB()
    {
    }
    std::string
    func1()
    {
        return "hello " + fuu;
    }

  private:
    std::string fuu;
};

#ifdef ADD_LUA_BINDINGS
TEST_F( elLuaScript_test, LuaClassTest )
{
    testing::internal::CaptureStderr();
    elLuaScript::class_t newClass( "luaClassTest" );
    newClass.AddConstructor< luaClassTest, int >()
        .AddMethod< luaClassTest >( "func2", &luaClassTest::func2 )
        .AddMethod< luaClassTest >( "func1", &luaClassTest::func1 )
        .AddMethod< luaClassTest >( "SetFuu", &luaClassTest::SetFuu )
        .AddStaticMethod( "func3", &luaClassTest::func3 );

    elLuaScript::RegisterLuaClass( newClass );

    elLuaScript::class_t newClassB( "testB" );
    newClassB.AddConstructor< luaClassTestB, std::string >()
        .AddMethod< luaClassTestB >( "get", &luaClassTestB::func1 );
    elLuaScript::RegisterLuaClass( newClassB );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.RequireClass( "luaClassTest" ) );
    EXPECT_NO_THROW( script.RequireClass( "testB" ) );
    EXPECT_NO_THROW(
        script.SetSource( "a = luaClassTest.new(17); b = testB.new('zav'); "
                          "function f1() return a:func2(3, 4) end; "
                          "function f2() return luaClassTest.func3() end; "
                          "function f3() return b:get() end " ) );
    EXPECT_EQ( script.Call< lua_Integer >( "f1" ).GetValue()[0], 24 );
    EXPECT_EQ( script.Call< lua_Integer >( "f2" ).GetValue()[0], 42 );
    EXPECT_EQ( script.Call< std::string >( "f3" ).GetValue()[0], "hello zav" );

    EXPECT_TRUE( script.RequireClass( "Crap" ).HasError() );
    testing::internal::GetCapturedStderr();
}
#endif

TEST_F( elLuaScript_test, LuaSetGlobal )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.SetGlobal< int >( "glbA", 42 );
    script.SetGlobal< std::string >( "glbB", "HelloWorld" );
    script.SetSource(
        "function a() return glbA end; function b() return glbB end" );

    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 42 );
    EXPECT_EQ( script.Call< std::string >( "b" ).GetValue()[0], "HelloWorld" );
}

#ifdef ADD_LUA_BINDINGS
TEST_F( elLuaScript_test, GlobalThis )
{
    // testing::internal::CaptureStderr();
    elLuaScript* script =
        new elLuaScript( std::move( *elLuaScript::Create().GetValue() ) );

    script->RequireClass( "elLuaScript" );
    script->SetGlobal< elLuaScript* >( "this", script );
    script->SetSource(
        "this:RequireNamespace('ut1'); function a() return sum(3,77) end" );
    EXPECT_EQ( script->Call< lua_Integer >( "a" ).GetValue()[0], 80 );

    delete script;
    elLuaScript script2 = std::move( *elLuaScript::Create().GetValue() );
    script2.RequireClass( "elLuaScript" );
    script2.SetGlobal< elLuaScript* >( "this", &script2 );
    script2.SetSource(
        "this:RequireNamespace('ut1'); function a() return sum(3,77) end" );
    EXPECT_EQ( script2.Call< lua_Integer >( "a" ).GetValue()[0], 80 );


    // testing::internal::GetCapturedStderr();
}

TEST_F( elLuaScript_test, GlobalClassTest )
{
    luaClassTest fuu( 11 );
    elLuaScript  script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "luaClassTest" );
    script.SetGlobal< luaClassTest* >( "fuu", &fuu );
    script.SetSource( "function a() return fuu:func2(3, 4) end; function b() "
                      "fuu:SetFuu(1234) end" );

    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 18 );
    EXPECT_NO_THROW( script.CallVoid( "b" ) );
    EXPECT_EQ( fuu.fuu, 1234 );
}

luaClassTest
GetLuaClassTest( int a )
{
    return luaClassTest( a );
}

luaClassTest*
GetLuaClassTestPtr( int a )
{
    return new luaClassTest( a );
}

TEST_F( elLuaScript_test, ClassReturnTest )
{
    elLuaScript::RegisterFunction( "GetClass", GetLuaClassTest, "GCUT" );
    elLuaScript::RegisterFunction( "GetClassPtr", GetLuaClassTestPtr, "GCUT" );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "luaClassTest" );
    script.RequireNamespace( "GCUT" );
    script.SetSource(
        "function a() b = GetClass(5); return b:func2(3,5) end; function d() e "
        "= "
        "GetClassPtr(8); f = e:func2(3,4); e:delete(); return f end" );
    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 13 );
    EXPECT_EQ( script.Call< lua_Integer >( "d" ).GetValue()[0], 15 );
}

int
LuaParam( luaClassTest c )
{
    return c.func2( 11, 22 );
}

double
LuaParam2( luaClassTest* c )
{
    return c->func2( 5, 7 );
}

TEST_F( elLuaScript_test, ClassesAsParameter )
{
    elLuaScript::RegisterFunction( "LuaParam", LuaParam, "CAPUT" );
    elLuaScript::RegisterFunction( "LuaParam2", LuaParam2, "CAPUT" );

    luaClassTest fuu( 55 );
    elLuaScript  script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "luaClassTest" );
    script.SetGlobal< luaClassTest* >( "d", &fuu );
    script.RequireNamespace( "CAPUT" );
    script.SetSource( "a = luaClassTest.new(33); function b() return "
                      "LuaParam(a) end; function c() "
                      "return LuaParam2(d) end" );

    EXPECT_EQ( script.Call< lua_Integer >( "b" ).GetValue()[0], 66 );
    EXPECT_EQ( script.Call< lua_Integer >( "c" ).GetValue()[0], 67 );
}

enum TestEnum_t
{
    BLA,
    FUU,
    BLUH
};

int
LuaEnumArg( TestEnum_t test )
{
    switch ( test )
    {
        case TestEnum_t::BLA:
            return 42;
        case TestEnum_t::BLUH:
            return 100;
        default:
            return 0;
    }
}

TEST_F( elLuaScript_test, EnumAsParameter )
{
    elLuaScript::RegisterFunction( "LuaEnumArg", LuaEnumArg, "CAPUT" );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireNamespace( "CAPUT" );
    script.SetGlobal< int >( "BLUH", static_cast< int >( TestEnum_t::BLUH ) );
    script.SetSource(
        "function a() return LuaEnumArg(0) end; function b() return "
        "LuaEnumArg(BLUH) end" );

    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 42 );
    EXPECT_EQ( script.Call< lua_Integer >( "b" ).GetValue()[0], 100 );
}

TEST_F( elLuaScript_test, ExternalTypeDeclaration )
{
    elLuaScript::class_t newClass( "stdVec" );
    newClass.AddConstructor< std::vector< int >, size_t >()
        .AddMethod< std::vector< int > >( "size", &std::vector< int >::size )
        .AddMethod< std::vector< int > >( "clear", &std::vector< int >::clear );
    elLuaScript::RegisterLuaClass( newClass );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "stdVec" );
    script.SetSource(
        "a = stdVec.new(5); function b() return a:size() end; function c() "
        "a:clear(); return a:size() end" );

    EXPECT_EQ( script.Call< lua_Integer >( "b" ).GetValue()[0], 5 );
    EXPECT_EQ( script.Call< lua_Integer >( "c" ).GetValue()[0], 0 );
}

class GetSetTest
{
  public:
    std::string   a;
    int           b;
    double        c;
    luaClassTest* d;
    luaClassTest  e;
    static int    f;
};
int GetSetTest::f;

TEST_F( elLuaScript_test, GetSetClassFunction )
{
    elLuaScript::class_t newClass( "GetSetTest" );
    newClass.AddConstructor< GetSetTest >()
        .AddVariable( "a", &GetSetTest::a )
        .AddVariable( "b", &GetSetTest::b )
        .AddVariable( "c", &GetSetTest::c )
        .AddVariable( "d", &GetSetTest::d )
        .AddVariable( "e", &GetSetTest::e )
        .AddStaticVariable( "f", &GetSetTest::f );
    elLuaScript::RegisterLuaClass( newClass );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "GetSetTest" );
    script.RequireClass( "luaClassTest" );

    GetSetTest bla;
    bla.d = new luaClassTest( 100 );
    script.SetGlobal< GetSetTest* >( "GST", &bla );

    script.SetSource(
        "GST:Set_a('hello'); GST:Set_b(55); GST:Set_c(12.2); a = {}; a[#a + 1] "
        "= 55; a[#a + 1] = 66; b ={'hello', 'fuu'};" );
    script.Exec();

    EXPECT_EQ( bla.a, "hello" );
    EXPECT_EQ( bla.b, 55 );
    EXPECT_EQ( bla.c, 12.2 );

    bla.a = "fuu";
    bla.b = 100;
    bla.c = 42.42;

    script.SetSource(
        "function a() return GST:Get_a() end; function b() return "
        "GST:Get_b() end; function c() return GST:Get_c() end; function "
        "d() return GST:Get_d():func2(10, 10) end" );

    EXPECT_EQ( script.Call< std::string >( "a" ).GetValue()[0], "fuu" );
    EXPECT_EQ( script.Call< int >( "b" ).GetValue()[0], 100 );
    EXPECT_EQ( script.Call< double >( "c" ).GetValue()[0], 42.42 );
    EXPECT_EQ( script.Call< int >( "d" ).GetValue()[0], 120 );

    delete bla.d;

    script.SetSource( "a = luaClassTest.new(42); GST:Set_e(a)" );
    script.Exec();

    EXPECT_EQ( bla.e.fuu, 42 );

    GetSetTest::f = 100;
    script.SetSource(
        "function a() return GST.Get_f() end; function b() GST.Set_f(42) end" );
    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 100 );
    script.CallVoid( "b" );
    EXPECT_EQ( GetSetTest::f, 42 );
}
#endif


TEST_F( elLuaScript_test, SetGlobalsWithNamespaces )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );

    elLuaScript::SetGlobalInNamespace( "BLA", 123, "CATOR" );
    script.SetSource( "function a() return BLA; end" );
    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 0 );
    script.RequireNamespace( "CATOR" );
    EXPECT_EQ( script.Call< lua_Integer >( "a" ).GetValue()[0], 123 );
}

TEST_F( elLuaScript_test, GetGlobalNames )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );

    std::set< std::string > test1 = {"bla", "fuu", "blub", "arr1"};
    script.SetSource( "bla = 123; fuu = 456; blub = 'dudu'; arr1 = {1,2,3}" );
    auto v = script.GetGlobalNames();

    for ( auto e : v )
        EXPECT_NE( test1.find( e ), test1.end() );


    std::set< std::string > test2 = {"abla", "fsuu", "ablub", "aarr1"};
    script.SetSource(
        "abla = 123; fsuu = 456; ablub = 'dudu'; aarr1 = {1,2,3}" );
    auto v2 = script.GetGlobalNames();

    for ( auto e : v2 )
        EXPECT_NE( test2.find( e ), test2.end() );
}

TEST_F( elLuaScript_test, GetGlobalVariablesInEmptyScript )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );

    script.SetSource( "" );
    auto v = script.GetGlobalNames();

    EXPECT_EQ( script.GetGlobal< lua_Integer >( {} ).size(), 0 );
    EXPECT_EQ( script.GetGlobal< lua_Integer >( {"var2"} ).size(), 0 );
    EXPECT_EQ(
        script.GetGlobal< lua_Integer >( {"var2", "asd", "assa", "xx"} ).size(),
        0 );
    EXPECT_EQ(
        script.GetGlobal< std::string >( {"var21", "a1sd", "assa", "xx"} )
            .size(),
        0 );
    EXPECT_EQ( script.GetGlobal< std::string >( {"1var2"} ).size(), 0 );


    EXPECT_EQ( script.GetGlobalTableEntries( {} ).size(), 0 );
    EXPECT_EQ( script.GetGlobalTableEntries( {"var2"} ).size(), 0 );
    EXPECT_EQ( script.GetGlobalTableEntries( {"1var2", "AddShader"} ).size(),
               0 );
    EXPECT_EQ(
        script.GetGlobalTableEntries( {"var22", "asd", "Xxx", "Asas"} ).size(),
        0 );


    EXPECT_EQ( script.GetGlobalNames().size(), 0 );


    EXPECT_EQ( script.GetGlobalType( "asd" ), 0 );
    EXPECT_EQ( script.GetGlobalType( "" ), 0 );
}

TEST_F( elLuaScript_test, HasGlobal )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );

    script.SetSource( "bla = 123; fuu = 456; blub = 'dudu'; arr1 = {1,2,3}; "
                      "arr2 = { arr3 = {789, 112}, fuu = 123, blah = { argh = "
                      "'asd', f = 123}}" );

    EXPECT_EQ( script.HasGlobal( {"bla"} ), true );
    EXPECT_EQ( script.HasGlobal( {"bla2"} ), false );
    EXPECT_EQ( script.HasGlobal( {"arr1"} ), true );
    EXPECT_EQ( script.HasGlobal( {"arr2"} ), true );
    EXPECT_EQ( script.HasGlobal( {"arr2", "arr3"} ), true );
    EXPECT_EQ( script.HasGlobal( {"arr2", "arr666"} ), false );
    EXPECT_EQ( script.HasGlobal( {"arr2", "blah", "argh"} ), true );
    EXPECT_EQ( script.HasGlobal( {"arr2", "blah", "argh", "schmu"} ), false );
    EXPECT_EQ( script.HasGlobal( {"arr2", "blah", "schmu"} ), false );
    EXPECT_EQ( script.HasGlobal( {"nother", "schamr"} ), false );
}

#ifdef ADD_LUA_BINDINGS
void
PointerArg( GetSetTest* arg1 )
{
    arg1->b++;
    arg1->a = "juhu";
}

int
ConstPointerArg( const GetSetTest* const arg1 )
{
    return arg1->b + 44;
}

TEST_F( elLuaScript_test, UsingPointerAsFunctionArgument )
{
    elLuaScript::RegisterFunction( "PointerArg", PointerArg, "SUT" );
    elLuaScript::RegisterFunction( "ConstPointerArg", ConstPointerArg, "SUT" );
    elLuaScript::class_t newClass( "GetSetTest" );
    newClass.AddConstructor< GetSetTest >()
        .AddVariable( "a", &GetSetTest::a )
        .AddVariable( "b", &GetSetTest::b )
        .AddVariable( "c", &GetSetTest::c )
        .AddVariable( "d", &GetSetTest::d )
        .AddVariable( "e", &GetSetTest::e )
        .AddStaticVariable( "f", &GetSetTest::f );
    elLuaScript::RegisterLuaClass( newClass );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    script.RequireClass( "GetSetTest" );
    script.RequireNamespace( "SUT" );
    script.SetSource( "function f1() a = GetSetTest.new(); a:Set_b(66); "
                      "PointerArg(a:ThisPointer()); "
                      "return a:Get_a() end; function f2() "
                      "local b = ConstPointerArg(a:ThisPointer()); return "
                      "b end " );

    EXPECT_EQ( script.Call< std::string >( "f1" ).GetValue()[0], "juhu" );
    EXPECT_EQ( script.Call< lua_Integer >( "f2" ).GetValue()[0], 111 );
}

TEST_F( elLuaScript_test, ThisPointer )
{
    elLuaScript::class_t newClass( "luaClassTest" );
    newClass.AddConstructor< luaClassTest, int >()
        .AddMethod< luaClassTest >( "func2", &luaClassTest::func2 )
        .AddMethod< luaClassTest >( "func1", &luaClassTest::func1 )
        .AddMethod< luaClassTest >( "SetFuu", &luaClassTest::SetFuu )
        .AddStaticMethod( "func3", &luaClassTest::func3 );
    elLuaScript::RegisterLuaClass( newClass );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.RequireClass( "luaClassTest" ) );
    EXPECT_NO_THROW(
        script.SetSource( "a = luaClassTest.new(17); b = a:ThisPointer(); "
                          "function f1() return b.func3() end;"
                          "function f2() return b:func2(1, 2) end" ) );
    EXPECT_EQ( script.Call< lua_Integer >( "f1" ).GetValue()[0], 42 );
    EXPECT_EQ( script.Call< lua_Integer >( "f2" ).GetValue()[0], 20 );
}

TEST_F( elLuaScript_test, LoadClassesWithRequireNamespace )
{
    elLuaScript::class_t newClass1( "luaClassTest" );
    newClass1.AddConstructor< luaClassTest, int >()
        .AddMethod< luaClassTest >( "func2", &luaClassTest::func2 )
        .AddMethod< luaClassTest >( "func1", &luaClassTest::func1 )
        .AddMethod< luaClassTest >( "SetFuu", &luaClassTest::SetFuu )
        .AddStaticMethod( "func3", &luaClassTest::func3 );
    elLuaScript::RegisterLuaClass( newClass1, "luaTest" );

    elLuaScript::class_t newClass2( "luaClassTestB" );
    newClass2.AddConstructor< luaClassTestB, std::string >()
        .AddMethod< luaClassTestB >( "func1", &luaClassTestB::func1 );
    elLuaScript::RegisterLuaClass( newClass2, "luaTest" );

    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.RequireNamespace( "luaTest" ) );
    EXPECT_NO_THROW(
        script.SetSource( "a = luaClassTest.new(17); b = a:ThisPointer(); "
                          "function f1() return b.func3() end;"
                          "function f2() return b:func2(1, 2) end;"
                          "function f3() d = luaClassTestB.new(\"bla\"); "
                          "return d:func1() end;" ) );
    EXPECT_EQ( script.Call< lua_Integer >( "f1" ).GetValue()[0], 42 );
    EXPECT_EQ( script.Call< lua_Integer >( "f2" ).GetValue()[0], 20 );
    EXPECT_EQ( script.Call< std::string >( "f3" ).GetValue()[0], "hello bla" );
}

TEST_F( elLuaScript_test, LoadConstantsWithRequireNamespace )
{
    elLuaScript::RegisterGlobal< int >(
        "namespaceTest", {{"NS_1", 445}, {"asd_NS2", 1337}}, "namespaceTest" );
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    EXPECT_NO_THROW( script.RequireNamespace( "namespaceTest" ) );
    EXPECT_NO_THROW( script.SetSource( "function f1() return NS_1 end;"
                                       "function f2() return asd_NS2 end;" ) );
    EXPECT_EQ( script.Call< lua_Integer >( "f1" ).GetValue()[0], 445 );
    EXPECT_EQ( script.Call< lua_Integer >( "f2" ).GetValue()[0], 1337 );
}

TEST_F( elLuaScript_test, VectorHandling )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    luaBindings::stl::AddLuaVectorBinding< int >( "vec_t", "namespaceTest" );
    EXPECT_NO_THROW( script.RequireNamespace( "namespaceTest" ) );
    EXPECT_NO_THROW( script.SetSource(
        "function f1(vector) return vector:size() end;"
        "function f2() a = {4,5,6}; return vec_t.create_from_array( a ) end; "
        "function f3(vector2) b = vector2:convert_to_array(); return b[1]; "
        "end" ) );

    std::vector< int > input{1, 2, 3, 4};
    EXPECT_EQ( script.Call< lua_Integer >( "f1", &input ).GetValue()[0], 4 );

    std::vector< int > result{4, 5, 6};
    auto               constructedValue =
        script.Call< std::vector< int >* >( "f2" ).GetValue()[0];
    EXPECT_EQ( *constructedValue, result );
    delete constructedValue;

    EXPECT_EQ( script.Call< lua_Integer >( "f3", &result ).GetValue()[0], 5 );
}

TEST_F( elLuaScript_test, MapHandling )
{
    elLuaScript script = std::move( *elLuaScript::Create().GetValue() );
    luaBindings::stl::AddLuaMapBinding< std::string, std::string >(
        "map_t", "namespaceTest" );
    EXPECT_NO_THROW( script.RequireNamespace( "namespaceTest" ) );
    EXPECT_NO_THROW( script.SetSource(
        "function f1(map) return map:size() end;"
        "function f2() a = {abc = \"def\", fuu = \"bar\"}; return "
        "map_t.create_from_table( a ) end;"
        "function f3(map2) b = map2:convert_to_table(); return b[\"fuu\"]; "
        "end" ) );

    std::map< std::string, std::string > input{
        {"hello", "world"}, {"fuu", "bar"}, {"di", "du"}};

    EXPECT_EQ( script.Call< lua_Integer >( "f1", &input ).GetValue()[0], 3 );

    std::map< std::string, std::string > result{{"abc", "def"}, {"fuu", "bar"}};
    auto                                 constructedValue =
        script.Call< std::map< std::string, std::string >* >( "f2" )
            .GetValue()[0];
    EXPECT_EQ( *constructedValue, result );
    delete constructedValue;

    EXPECT_EQ( script.Call< std::string >( "f3", &result ).GetValue()[0],
               "bar" );
}
#endif
