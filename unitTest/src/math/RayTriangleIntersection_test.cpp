#include "math/RayTriangleIntersection.hpp"
#include "unitTest.hpp"

using namespace ::el3D;
using namespace ::el3D::math;
using namespace ::testing;

TEST( math_RayTriangleIntersection_test, RayParallelToTriangle )
{
    EXPECT_FALSE( DoesRayIntersectTriangle( { 1.0f, 2.0f, 3.0f },
                                            { 4.0f, 5.0f, 0.0f },
                                            { { { 1.0f, 1.0f, 1.0f },
                                                { 3.0f, 3.0f, 1.0f },
                                                { -2.0f, 1.0f, 1.0f } } } ) );

    EXPECT_FALSE( DoesRayIntersectTriangle( { 1.0f, 1.0f, 0.0f },
                                            { 4.0f, 5.0f, 0.0f },
                                            { { { 1.0f, 1.0f, 0.0f },
                                                { 3.0f, 3.0f, 0.0f },
                                                { -2.0f, 1.0f, 0.0f } } } ) );
}

TEST( math_RayTriangleIntersection_test, NonParallelNonIntersecting )
{
    EXPECT_FALSE( DoesRayIntersectTriangle( { 1.0f, 2.0f, 3.0f },
                                            { 4.0f, 0.0f, 0.0f },
                                            { { { 1.0f, 1.0f, 1.0f },
                                                { 3.0f, 3.0f, 1.0f },
                                                { -2.0f, 1.0f, 1.0f } } } ) );

    EXPECT_FALSE( DoesRayIntersectTriangle( { 1.0f, 1.0f, 0.0f },
                                            { 4.0f, 0.0f, 0.0f },
                                            { { { 1.0f, 1.0f, 0.0f },
                                                { 3.0f, 3.0f, 0.0f },
                                                { -2.0f, 1.0f, 0.0f } } } ) );
}

TEST( math_RayTriangleIntersection_test, Intersecting )
{
    EXPECT_TRUE( DoesRayIntersectTriangle( { 0.5f, 0.5f, 1.0f },
                                           { 0.0f, 0.0f, 1.0f },
                                           { { { 0.0f, 0.0f, 0.0f },
                                               { 1.0f, 0.0f, 0.0f },
                                               { 0.5f, 1.0f, 0.0f } } } ) );

    EXPECT_TRUE( DoesRayIntersectTriangle( { 0.5f, 0.5f, 1.0f },
                                           { 0.1f, 0.1f, 1.0f },
                                           { { { 0.0f, 0.0f, 0.0f },
                                               { 1.0f, 0.0f, 0.0f },
                                               { 0.5f, 1.0f, 0.0f } } } ) );
}
