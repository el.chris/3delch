#include "OpenGL/elGLSLParser.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::OpenGL;

class elGLSLParser_test : public Test
{
  public:
    elGLSLParser_test()
        : parser( *elGLSLParser::Create(
                       std::string( UNIT_TEST_DIR ) + "/glslTest.glsl", 130 )
                       .OrElse( [] { ASSERT_TRUE( false ); } )
                       .GetValue() )
    {
    }
    virtual void
    SetUp()
    {
    }
    elGLSLParser parser;
};

TEST_F( elGLSLParser_test, OpenFileAndParse )
{
    EXPECT_NE( parser.GetContent().find( "default" ),
               parser.GetContent().end() );
    EXPECT_NE( parser.GetContent().find( "texture" ),
               parser.GetContent().end() );
    EXPECT_NE( parser.GetContent().find( "syntaxError" ),
               parser.GetContent().end() );

    EXPECT_NE(
        parser.GetContent().find( "default" )->second.find( GL_VERTEX_SHADER ),
        parser.GetContent().find( "default" )->second.end() );
    EXPECT_NE(
        parser.GetContent().find( "texture" )->second.find( GL_VERTEX_SHADER ),
        parser.GetContent().find( "texture" )->second.end() );
    EXPECT_NE( parser.GetContent()
                   .find( "texture" )
                   ->second.find( GL_FRAGMENT_SHADER ),
               parser.GetContent().find( "texture" )->second.end() );
}

TEST_F( elGLSLParser_test, Groups )
{
    EXPECT_THAT( parser.GetContent().find( "group1" ),
                 Ne( parser.GetContent().end() ) );
    EXPECT_THAT( parser.GetContent().find( "group2" ),
                 Ne( parser.GetContent().end() ) );
    EXPECT_THAT( parser.GetContent().find( "group3" ),
                 Ne( parser.GetContent().end() ) );
}
