#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "unitTest.hpp"

using namespace ::el3D::bb;
using namespace ::testing;

class elUniqueIndexVector_test : public Test
{
  public:
    virtual void
    SetUp()
    {
    }
    elUniqueIndexVector< size_t > v;
};

TEST_F( elUniqueIndexVector_test, emplace_back_simple )
{
    auto i = v.emplace_back( 123 ).index();
    EXPECT_THAT( v[i], Eq( 123 ) );
}

TEST_F( elUniqueIndexVector_test, emplace_back_with_access )
{
    auto a1 = v.emplace_back( 123 ).index();
    auto a2 = v.emplace_back( 456 ).index();

    v[a1] = 42;
    v[a2] = 63;

    EXPECT_THAT( v[a1], Eq( 42 ) );
    EXPECT_THAT( v[a2], Eq( 63 ) );
}

TEST_F( elUniqueIndexVector_test, emplace_back_verify )
{
    v.emplace_back( 1 );
    v.emplace_back( 2 );
    v.emplace_back( 3 );

    auto iter = v.begin();

    EXPECT_THAT( *iter, Eq( 1 ) );
    ++iter;
    EXPECT_THAT( *iter, Eq( 2 ) );
    ++iter;
    EXPECT_THAT( *iter, Eq( 3 ) );
    ++iter;
}

TEST_F( elUniqueIndexVector_test, size )
{
    for ( size_t i = 0; i < 10; ++i )
    {
        EXPECT_THAT( v.size(), Eq( i ) );
        v.emplace_back( i );
    }
}

TEST_F( elUniqueIndexVector_test, erase_simple )
{
    auto a1 = v.emplace_back( 11 ).index();
    auto a2 = v.emplace_back( 22 ).index();
    auto a3 = v.emplace_back( 33 ).index();
    auto a4 = v.emplace_back( 34 ).index();

    v.erase( a2 );

    EXPECT_THAT( v[a1], Eq( 11 ) );
    EXPECT_THAT( v[a3], Eq( 33 ) );
    EXPECT_THAT( v[a4], Eq( 34 ) );

    v.erase( a1 );

    EXPECT_THAT( v[a3], Eq( 33 ) );
    EXPECT_THAT( v[a4], Eq( 34 ) );

    v.erase( a4 );

    EXPECT_THAT( v[a3], Eq( 33 ) );

    EXPECT_THAT( v.size(), Eq( 1 ) );
}

TEST_F( elUniqueIndexVector_test, erase_backward )
{
    size_t                limit = 10;
    std::vector< size_t > id;
    for ( size_t i = 0; i < limit; ++i )
        id.emplace_back( v.emplace_back( i ).index() );

    int i = static_cast< int >( limit );
    for ( auto iter = id.rbegin(); iter != id.rend(); ++iter )
    {
        --i;
        v.erase( *iter );
        EXPECT_THAT( v.size(), Eq( i ) );

        for ( size_t m = 0; static_cast< int >( m ) < i; ++m )
        {
            EXPECT_THAT( v[id[m]], Eq( m ) );
        }
    }
}

TEST_F( elUniqueIndexVector_test, erase_forward )
{
    auto a1 = v.emplace_back( 123 ).index();
    auto a2 = v.emplace_back( 2123 ).index();
    auto a3 = v.emplace_back( 3123 ).index();
    auto a4 = v.emplace_back( 4123 ).index();

    v.erase( a1 );

    EXPECT_THAT( v[a2], Eq( 2123 ) );
    EXPECT_THAT( v[a3], Eq( 3123 ) );
    EXPECT_THAT( v[a4], Eq( 4123 ) );

    v.erase( a2 );
    EXPECT_THAT( v[a3], Eq( 3123 ) );
    EXPECT_THAT( v[a4], Eq( 4123 ) );

    v.erase( a3 );
    EXPECT_THAT( v[a4], Eq( 4123 ) );

    EXPECT_THAT( v.size(), Eq( 1 ) );
}

TEST_F( elUniqueIndexVector_test, clear )
{
    v.emplace_back( 123 );
    v.emplace_back( 123 );
    v.emplace_back( 123 );
    v.emplace_back( 123 );

    EXPECT_THAT( v.size(), Eq( 4 ) );

    v.clear();
    EXPECT_THAT( v.size(), Eq( 0 ) );
}

TEST_F( elUniqueIndexVector_test, IterateAfterErase )
{
    for ( size_t i = 0; i < 10; ++i )
        EXPECT_THAT( v.emplace_back( i ).index(), Eq( i ) );

    v.erase( 1 );
    v.erase( 3 );
    v.erase( 5 );
    v.erase( 7 );
    v.erase( 9 );

    size_t i = 0;
    for ( auto e : v )
    {
        EXPECT_THAT( e, Eq( i ) );
        i += 2;
    }
}

TEST_F( elUniqueIndexVector_test, IterateAfterReverseErase )
{
    for ( size_t i = 0; i < 10; ++i )
        EXPECT_THAT( v.emplace_back( i ).index(), Eq( i ) );

    v.erase( 9 );
    v.erase( 7 );
    v.erase( 5 );
    v.erase( 3 );
    v.erase( 1 );

    size_t i = 0;
    for ( auto e : v )
    {
        EXPECT_THAT( e, Eq( i ) );
        i += 2;
    }
}

TEST_F( elUniqueIndexVector_test, InsertEraseMix )
{
    for ( size_t i = 0; i < 10; ++i )
        EXPECT_THAT( v.emplace_back( i ).index(), Eq( i ) );

    v.erase( 9 );
    v.erase( 2 );

    v.emplace_back( 55 );
    v.emplace_back( 155 );
    auto v255 = v.emplace_back( 255 ).index();
    auto v355 = v.emplace_back( 355 ).index();
    auto v455 = v.emplace_back( 455 ).index();

    v.erase( 8 );
    v.erase( 5 );
    v.erase( 2 );
    v.erase( 1 );
    v.erase( 0 );
    v.erase( 9 );

    EXPECT_THAT( v[3], Eq( 3 ) );
    EXPECT_THAT( v[v255], Eq( 255 ) );
    EXPECT_THAT( v[v355], Eq( 355 ) );
    EXPECT_THAT( v[v455], Eq( 455 ) );
}

TEST_F( elUniqueIndexVector_test, InsertAtCustomPosition )
{
    ASSERT_THAT( v.insert( 13, 123 ), Eq( true ) );
    EXPECT_THAT( v[13], Eq( 123 ) );
}

TEST_F( elUniqueIndexVector_test, InsertAtCustomPositionAndErase )
{
    ASSERT_THAT( v.insert( 31, 123 ), Eq( true ) );
    v.erase( 31 );
}

TEST_F( elUniqueIndexVector_test, InsertCustomPositionBackwards )
{
    ASSERT_THAT( v.insert( 5, 1337 ), Eq( true ) );
    ASSERT_THAT( v.insert( 2, 42 ), Eq( true ) );
    ASSERT_THAT( v.insert( 0, 3537 ), Eq( true ) );
    EXPECT_THAT( v[0], Eq( 3537 ) );
    EXPECT_THAT( v[2], Eq( 42 ) );
    EXPECT_THAT( v[5], Eq( 1337 ) );
}

TEST_F( elUniqueIndexVector_test, InsertAtCustomTakenPosition )
{
    auto i = v.emplace_back( 232 ).index();
    ASSERT_THAT( v.insert( i, 909 ), Eq( false ) );
    EXPECT_THAT( v[i], Eq( 232 ) );
}

TEST_F( elUniqueIndexVector_test, InsertAtCustomTakenFreedPosition )
{
    auto i = v.emplace_back( 232 ).index();
    v.erase( i );
    ASSERT_THAT( v.insert( i, 909 ), Eq( true ) );
    EXPECT_THAT( v[i], Eq( 909 ) );
}

TEST_F( elUniqueIndexVector_test, InsertAtCustomPositionEraseNonCustomPosition )
{
    auto i = v.emplace_back( 232 ).index();
    ASSERT_THAT( v.insert( i + 5, 1909 ), Eq( true ) );
    v.erase( i );
    EXPECT_THAT( v[i + 5], Eq( 1909 ) );
}

TEST_F( elUniqueIndexVector_test, FindIfExistingElement )
{
    v.emplace_back( 232 );
    v.emplace_back( 131 );
    v.emplace_back( 3211 );

    auto idx = v.find_if( []( auto n ) { return n == 131; } );
    EXPECT_THAT( idx, Eq( 1 ) );
}

TEST_F( elUniqueIndexVector_test, FindIfNonExistingElement )
{
    v.emplace_back( 232 );

    auto idx = v.find_if( []( auto n ) { return n == 131; } );
    EXPECT_THAT( idx, Eq( decltype( v )::INVALID_INDEX ) );
}

