#include "buildingBlocks/elExpected.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::bb;

class elExpected_test : public Test
{
  public:
    struct Test
    {
        Test( int a, int b ) : a( a ), b( b )
        {
        }

        int
        Gimme()
        {
            return a + b;
        }
        int a;
        int b;
    };
};

TEST_F( elExpected_test, ErrorOr )
{
    auto sut = elExpected< int >::CreateError( 123 );
    EXPECT_THAT( sut.GetErrorOr( 555 ), Eq( 123 ) );

    sut = elExpected< int >::CreateValue();
    EXPECT_THAT( sut.GetErrorOr( 891 ), Eq( 891 ) );

    auto sut2 = elExpected< int, int >::CreateError( 123 );
    EXPECT_THAT( sut2.GetErrorOr( 555 ), Eq( 123 ) );

    sut2 = elExpected< int, int >::CreateValue( 910 );
    EXPECT_THAT( sut2.GetErrorOr( 891 ), Eq( 891 ) );
}

TEST_F( elExpected_test, BoolOperatorOfErrorOnly )
{
    auto sut = elExpected< int >::CreateError( 123 );
    EXPECT_FALSE( static_cast< bool >( sut ) );

    sut = elExpected< int >::CreateValue();
    EXPECT_TRUE( static_cast< bool >( sut ) );
}

TEST_F( elExpected_test, BoolOperatorOfValueError )
{
    auto sut = elExpected< int, int >::CreateError( 123 );
    EXPECT_FALSE( static_cast< bool >( sut ) );

    sut = elExpected< int, int >::CreateValue( 456 );
    EXPECT_TRUE( static_cast< bool >( sut ) );
}

TEST_F( elExpected_test, CreateWithValue )
{
    auto sut = elExpected< int, float >::CreateValue( 123 );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( sut.GetValue(), Eq( 123 ) );
}

TEST_F( elExpected_test, CreateWithError )
{
    auto sut = elExpected< int, float >::CreateError( 123.12f );
    ASSERT_THAT( sut.HasError(), Eq( true ) );
    EXPECT_THAT( sut.GetError(), Eq( 123.12f ) );
}

TEST_F( elExpected_test, CreateValue )
{
    auto sut = elExpected< Test, int >::CreateValue( 12, 222 );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( sut.GetValue().a, Eq( 12 ) );
}

TEST_F( elExpected_test, CreateError )
{
    auto sut = elExpected< int, Test >::CreateError( 313, 212 );
    ASSERT_THAT( sut.HasError(), Eq( true ) );
    EXPECT_THAT( sut.GetError().b, Eq( 212 ) );
}

TEST_F( elExpected_test, GetValueOr )
{
    auto sut = elExpected< int, float >::CreateError( 16523.12f );
    EXPECT_THAT( sut.GetValueOr( 90 ), Eq( 90 ) );
}

TEST_F( elExpected_test, ConstGetValueOr )
{
    auto sut = elExpected< int, float >::CreateError( 1652.12f );
    EXPECT_THAT( sut.GetValueOr( 15 ), Eq( 15 ) );
}

TEST_F( elExpected_test, ArrowOperator )
{
    // auto sut = elExpected< Test, float >::CreateValue( 55, 81 );
    // ASSERT_THAT( sut.HasError(), Eq( false ) );
    // EXPECT_THAT( sut->Gimme(), Eq( 136 ) );
}

TEST_F( elExpected_test, Dereferencing )
{
    auto sut = elExpected< int, float >::CreateValue( 1652 );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( *sut, Eq( 1652 ) );
}

TEST_F( elExpected_test, VoidCreateValue )
{
    auto sut = elExpected< float >::CreateValue();
    ASSERT_THAT( sut.HasError(), Eq( false ) );
}

TEST_F( elExpected_test, VoidCreateError )
{
    auto sut = elExpected< float >::CreateError( 12.2f );
    ASSERT_THAT( sut.HasError(), Eq( true ) );
    ASSERT_THAT( sut.GetError(), Eq( 12.2f ) );
}

TEST_F( elExpected_test, VoidCreateFromSuccessType )
{
    elExpected< float > sut{ Success<>() };
    ASSERT_THAT( sut.HasError(), Eq( false ) );
}

TEST_F( elExpected_test, CreateFromSuccessType )
{
    elExpected< int, float > sut{ Success( 55 ) };
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( sut.GetValue(), Eq( 55 ) );
}

TEST_F( elExpected_test, VoidCreateFromErrorType )
{
    elExpected< float > sut{ Error( 12.1f ) };
    ASSERT_THAT( sut.HasError(), Eq( true ) );
    EXPECT_THAT( sut.GetError(), Eq( 12.1f ) );
}

TEST_F( elExpected_test, CreateFromErrorType )
{
    elExpected< int, float > sut{ Error( 112.1f ) };
    ASSERT_THAT( sut.HasError(), Eq( true ) );
    EXPECT_THAT( sut.GetError(), Eq( 112.1f ) );
}

TEST_F( elExpected_test, OnErrorWhenHavingError )
{
    elExpected< int, float > sut{ Error( 112.1f ) };
    int                      a = 0;
    sut.OrElse( [&]( const auto& ) { a = 1; } ).AndThen( [&]( const auto& ) {
        a = 2;
    } );

    EXPECT_THAT( a, Eq( 1 ) );
}

TEST_F( elExpected_test, OnSuccessWhenHavingSuccess )
{
    elExpected< int, float > sut{ Success( 112 ) };
    int                      a = 0;
    sut.OrElse( [&]( const auto& ) { a = 1; } ).AndThen( [&]( const auto& ) {
        a = 2;
    } );

    EXPECT_THAT( a, Eq( 2 ) );
}

TEST_F( elExpected_test, VoidOnErrorWhenHavingError )
{
    elExpected< float > sut{ Error( 112.1f ) };
    int                 a = 0;
    sut.OrElse( [&]( const auto& ) { a = 1; } ).AndThen( [&] { a = 2; } );

    EXPECT_THAT( a, Eq( 1 ) );
}

TEST_F( elExpected_test, VoidOnSuccessWhenHavingSuccess )
{
    elExpected< float > sut{ Success<>() };
    int                 a = 0;
    sut.OrElse( [&]( const auto& ) { a = 1; } ).AndThen( [&] { a = 2; } );

    EXPECT_THAT( a, Eq( 2 ) );
}

TEST_F( elExpected_test, ConvertNonVoidSuccessResultToVoidResult )
{
    elExpected< int, float > sut{ Success( 123 ) };
    elExpected< float >      sut2 = sut;
    EXPECT_THAT( sut2.HasError(), Eq( false ) );
}

TEST_F( elExpected_test, ConvertNonVoidErrorResultToVoidResult )
{
    elExpected< int, float > sut{ Error( 1.23f ) };
    elExpected< float >      sut2 = sut;
    EXPECT_THAT( sut2.HasError(), Eq( true ) );
    EXPECT_THAT( sut2.GetError(), Eq( 1.23f ) );
}

TEST_F( elExpected_test, AndThenChainWithoutError )
{
    elExpected< int, int > sut{ Success( 1 ) };
    auto result = sut.AndThen( []( const auto& ) { return Success( 2 ); } )
                      .AndThen( []( const auto ) { return Success( 42 ); } );

    EXPECT_THAT( result.HasError(), Eq( false ) );
    EXPECT_THAT( result.GetValue(), Eq( 42 ) );
}

TEST_F( elExpected_test, AndThenChainWithError )
{
    elExpected< int, int > sut{ Success( 1 ) };
    auto result = sut.AndThen( []( const auto& ) { return Success( 2 ); } )
                      .AndThen( []( const auto& ) { return Error( 5 ); } )
                      .AndThen( []( const auto& ) { return Success( 42 ); } )
                      .AndThen( []( const auto& ) { return Error( 787 ); } );

    EXPECT_THAT( result.HasError(), Eq( true ) );
    EXPECT_THAT( result.GetError(), Eq( 5 ) );
}

TEST_F( elExpected_test, RValueAndThenChainWithoutError )
{
    auto sut = elExpected< int, int >( Success( 1 ) )
                   .AndThen( []( const auto& ) { return Success( 2 ); } )
                   .AndThen( []( const auto& ) { return Success( 3 ); } );
    EXPECT_THAT( sut.GetValue(), Eq( 3 ) );
}
