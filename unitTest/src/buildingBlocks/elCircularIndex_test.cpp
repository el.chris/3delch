#include "buildingBlocks/elCircularIndex.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::bb;

TEST( elCircularIndex_test, Assignment )
{
    elCircularIndex< 10 > sut( 5u );
    EXPECT_THAT( 5u, Eq( static_cast< uint64_t >( sut ) ) );
}


TEST( elCircularIndex_test, Assignment_Overflow )
{
    elCircularIndex< 11 > sut( 13u );
    EXPECT_THAT( 2u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PreIncrement )
{
    elCircularIndex< 12 > sut( 3u );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( ++sut ) ) );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PreIncrement_Overflow )
{
    elCircularIndex< 13 > sut( 12u );
    EXPECT_THAT( 0u, Eq( static_cast< uint64_t >( ++sut ) ) );
    EXPECT_THAT( 0u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PreDecrement )
{
    elCircularIndex< 12 > sut( 5u );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( --sut ) ) );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PostIncrement )
{
    elCircularIndex< 12 > sut( 3u );
    EXPECT_THAT( 3u, Eq( static_cast< uint64_t >( sut++ ) ) );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PostIncrement_Overflow )
{
    elCircularIndex< 12 > sut( 11u );
    EXPECT_THAT( 11u, Eq( static_cast< uint64_t >( sut++ ) ) );
    EXPECT_THAT( 0u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, PostDecrement )
{
    elCircularIndex< 12 > sut( 5u );
    EXPECT_THAT( 5u, Eq( static_cast< uint64_t >( sut-- ) ) );
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, Assign )
{
    elCircularIndex< 5 > sut;
    sut = 2;
    EXPECT_THAT( 2u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, Assign_Overflow )
{
    elCircularIndex< 5 > sut;
    sut = 8;
    EXPECT_THAT( 3u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, Compare )
{
    elCircularIndex< 6 > sut( 4 ), sut2( 3 ), sut3( 4 );
    EXPECT_THAT( sut == sut3, Eq( true ) );
    EXPECT_THAT( sut == sut2, Eq( false ) );
}

TEST( elCircularIndex_test, NotEqual )
{
    elCircularIndex< 6 > sut( 4 ), sut2( 3 ), sut3( 4 );
    EXPECT_THAT( sut != sut3, Eq( false ) );
    EXPECT_THAT( sut != sut2, Eq( true ) );
}

TEST( elCircularIndex_test, AddAndAssignNonOverflow )
{
    elCircularIndex< 7 > sut( 2 );
    sut += 3;
    EXPECT_THAT( 5u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, AddAndAssignOverflow )
{
    elCircularIndex< 8 > sut( 6 );
    sut += 4;
    EXPECT_THAT( 2u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, SubAndAssignNonOverflow )
{
    elCircularIndex< 9 > sut( 7 );
    sut -= 3;
    EXPECT_THAT( 4u, Eq( static_cast< uint64_t >( sut ) ) );
}

TEST( elCircularIndex_test, SubAndAssignOverflow )
{
    elCircularIndex< 4 > sut( 9 );
    sut -= 2;
    EXPECT_THAT( 3u, Eq( static_cast< uint64_t >( sut ) ) );
}
