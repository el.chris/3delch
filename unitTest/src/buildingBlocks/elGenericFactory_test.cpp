#include "buildingBlocks/elGenericFactory.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::bb;

class elGenericFactory_test : public Test
{
  public:
    static int baseDtorCounter;
    static int childDtorCounter;

    struct Base
    {
        Base( int a ) : a( a )
        {
        }
        virtual ~Base()
        {
            ++baseDtorCounter;
        }

        int a;
    };

    struct Child : public Base
    {
        Child( int a, int b ) : Base( a ), b( b )
        {
        }

        ~Child()
        {
            ++childDtorCounter;
        }

        int b;
    };

    struct Base2
    {
        Base2( int a ) : a( a )
        {
        }
        virtual ~Base2()
        {
            ++baseDtorCounter;
        }

        int a;
    };

    struct Child2 : public Base2
    {
        Child2( int a, int b ) : Base2( a ), b( b )
        {
        }

        ~Child2()
        {
            ++childDtorCounter;
        }

        int b;
    };

    struct SomeClass
    {
        int a;
        int b;
        int c;
    };

    void
    SetUp()
    {
        baseDtorCounter  = 0;
        childDtorCounter = 0;
    }

    using SutType_t = elGenericFactory< int, Base, float, double,
                                        CompositeProduct< Base2, SomeClass > >;
    SutType_t sut;
};

int elGenericFactory_test::baseDtorCounter;
int elGenericFactory_test::childDtorCounter;

TEST_F( elGenericFactory_test, CreatingProductWorks )
{
    auto product = sut.CreateProduct< int >( 1 );

    EXPECT_THAT( *product, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, CreatingBaseProductWorks )
{
    auto product = sut.CreateProduct< Base >( 2 );

    EXPECT_THAT( product->a, Eq( 2 ) );
}

TEST_F( elGenericFactory_test, CreatingChildProductWorks )
{
    auto product = sut.CreateProduct< Child >( 3, 4 );

    EXPECT_THAT( product->a, Eq( 3 ) );
    EXPECT_THAT( product->b, Eq( 4 ) );
}

TEST_F( elGenericFactory_test, BaseDTorCalledWhenOutOfScope )
{
    {
        auto product = sut.CreateProduct< Base >( 1 );
    }

    EXPECT_THAT( baseDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, BaseAndChildDTorCalledWhenOutOfScope )
{
    {
        auto product = sut.CreateProduct< Child >( 1, 2 );
    }

    EXPECT_THAT( baseDtorCounter, Eq( 1 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, AfterMoveBaseDTorCalledWhenOutOfScope )
{
    decltype( sut ) sut2;
    {
        auto product = sut.CreateProduct< Base >( 1 );

        sut2 = std::move( sut );
    }

    EXPECT_THAT( baseDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, AfterMoveBaseAndChildDTorCalledWhenOutOfScope )
{
    decltype( sut ) sut2;
    {
        auto product = sut.CreateProduct< Child >( 1, 2 );

        sut2 = std::move( sut );
    }

    EXPECT_THAT( baseDtorCounter, Eq( 1 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, AcquireProducts )
{

    auto product1 = sut.CreateProduct< int >( 1 );
    auto product2 = sut.CreateProduct< int >( 2 );

    auto& products = sut.GetProducts< int >();

    ASSERT_THAT( products.size(), Eq( 2 ) );

    EXPECT_THAT( *products[0], Eq( 1 ) );
    EXPECT_THAT( *products[1], Eq( 2 ) );
}

TEST_F( elGenericFactory_test, CreateBaseProductWithExtension )
{
    auto product1 = sut.CreateProduct< Base2 >( 8 );
    EXPECT_THAT( product1->a, Eq( 8 ) );
}

TEST_F( elGenericFactory_test, CreateChildProductWithExtension )
{
    auto product1 = sut.CreateProduct< Child2 >( 1, 3 );
    EXPECT_THAT( product1->a, Eq( 1 ) );
    EXPECT_THAT( product1->b, Eq( 3 ) );
}

TEST_F( elGenericFactory_test, AcquireCompositeProducts )
{
    auto  product1    = sut.CreateProduct< Child2 >( 81, 12 );
    auto& productVec1 = sut.GetProducts< Child2 >();
    EXPECT_THAT( productVec1[0]->a, Eq( 81 ) );
}

TEST_F( elGenericFactory_test, AcquireConstCompositeProducts )
{
    auto  product1 = sut.CreateProduct< Child2 >( 81, 12 );
    auto& productVec1 =
        const_cast< const decltype( sut )& >( sut ).GetProducts< Child2 >();
    EXPECT_THAT( productVec1[0]->a, Eq( 81 ) );
}

TEST_F( elGenericFactory_test, CreateBaseAndModifyExtension )
{
    auto product1          = sut.CreateProduct< Base2 >( 8 );
    product1.Extension().b = 123;
    EXPECT_THAT( product1.Extension().b, Eq( 123 ) );
}

TEST_F( elGenericFactory_test, CreateChildAndModifyExtension )
{
    auto product1          = sut.CreateProduct< Child2 >( 8, 901 );
    product1.Extension().c = 1231;
    EXPECT_THAT( product1.Extension().c, Eq( 1231 ) );
}

TEST_F( elGenericFactory_test, AcquireNonCompositePtrFromCompositeProduct )
{
    {
        product_ptr< Child2 > product1 = sut.CreateProduct< Child2 >( 8, 901 );
        product1->b                    = 1231;
        EXPECT_THAT( product1->b, Eq( 1231 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, ProductGuardReleasesCompositeObject )
{
    {
        product_guard< Child2 > product1 =
            sut.CreateProduct< Child2 >( 8, 901 );
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, ProductGuardOnRemoveCallbackIsCalled )
{
    int a = 0;
    {
        product_guard< Child2 > product1 =
            sut.CreateProductWithOnRemoveCallback< Child2 >(
                [&]( auto ) { a = 1231; }, 8, 901 );
    }
    EXPECT_THAT( a, Eq( 1231 ) );
}

TEST_F( elGenericFactory_test, SharedProductPtrSimpleElementAccessible )
{
    shared_product_ptr< int > product = sut.CreateSharedProduct< int >( 191 );

    EXPECT_THAT( *product, Eq( 191 ) );
    EXPECT_THAT( *product.Get(), Eq( 191 ) );
    EXPECT_THAT( *product.operator->(), Eq( 191 ) );
}

TEST_F( elGenericFactory_test, SharedProductPtrCompositeElementAccessible )
{
    auto p = sut.CreateSharedProduct< Child2 >( 191, 1201 );
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        std::move( p );

    EXPECT_THAT( ( *product ).a, Eq( 191 ) );
    EXPECT_THAT( ( *product ).b, Eq( 1201 ) );
    EXPECT_THAT( product->a, Eq( 191 ) );
    EXPECT_THAT( product->b, Eq( 1201 ) );
    EXPECT_THAT( product.Get()->a, Eq( 191 ) );
    EXPECT_THAT( product.Get()->b, Eq( 1201 ) );

    product.Extension().a = 123;
    product.Extension().b = 1231;
    product.Extension().c = 31;

    EXPECT_THAT( product.Extension().a, Eq( 123 ) );
    EXPECT_THAT( product.Extension().b, Eq( 1231 ) );
    EXPECT_THAT( product.Extension().c, Eq( 31 ) );
}

TEST_F( elGenericFactory_test, SharedProductPtrSimpleElementCallsDtor )
{
    {
        shared_product_ptr< Child > product =
            sut.CreateSharedProduct< Child >( 191, 11 );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, SharedProductPtrCompositeElementCallsDtor )
{
    {
        shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
            sut.CreateSharedProduct< Child2 >( 191, 1201 );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrSimpleElementMoveAssignmentCallsDtor )
{
    shared_product_ptr< Child > product =
        sut.CreateSharedProduct< Child >( 191, 11 );
    {
        shared_product_ptr< Child > product2 =
            sut.CreateSharedProduct< Child >( 191, 11 );
        product2 = std::move( product );
        EXPECT_THAT( childDtorCounter, Eq( 1 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 2 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrCompositeElementMoveAssignmentCallsDtor )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 191, 1201 );
    {
        shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2 =
            sut.CreateSharedProduct< Child2 >( 191, 1201 );
        product2 = std::move( product );
        EXPECT_THAT( childDtorCounter, Eq( 1 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 2 ) );
}

TEST_F( elGenericFactory_test, SharedProductPtrSimpleElementMoveCTorCallsDtor )
{
    shared_product_ptr< Child > product =
        sut.CreateSharedProduct< Child >( 191, 11 );
    {
        shared_product_ptr< Child > product2( std::move( product ) );
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrCompositeElementMoveCTorCallsDtor )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 191, 1201 );
    {
        shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2(
            std::move( product ) );
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrSimpleElementLastCopyAssignmentCallsDtor )
{
    {
        shared_product_ptr< Child > product =
            sut.CreateSharedProduct< Child >( 191, 11 );
        {
            shared_product_ptr< Child > product2 =
                sut.CreateSharedProduct< Child >( 191, 11 );
            product2 = product;
            EXPECT_THAT( childDtorCounter, Eq( 1 ) );
        }
        EXPECT_THAT( childDtorCounter, Eq( 1 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 2 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrCompositeElementLastCopyAssignmentCallsDtor )
{
    {
        shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
            sut.CreateSharedProduct< Child2 >( 191, 1201 );
        {
            shared_product_ptr< CompositeProduct< Child2, SomeClass > >
                product2 = sut.CreateSharedProduct< Child2 >( 191, 1201 );
            product2     = product;
            EXPECT_THAT( childDtorCounter, Eq( 1 ) );
        }
        EXPECT_THAT( childDtorCounter, Eq( 1 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 2 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrSimpleElementLastCopyCTorCallsDtor )
{
    {
        shared_product_ptr< Child > product =
            sut.CreateSharedProduct< Child >( 191, 11 );
        {
            shared_product_ptr< Child > product2( product );
        }
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrCompositeElementLastCopyCTorCallsDtor )
{
    {
        shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
            sut.CreateSharedProduct< Child2 >( 191, 1201 );
        {
            shared_product_ptr< CompositeProduct< Child2, SomeClass > >
                product2( product );
        }
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrExtensionAccessWorksAfterMoveCtor )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product.Extension().a = 901;
    product.Extension().b = 9301;
    product.Extension().c = 9021;

    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2(
        std::move( product ) );

    EXPECT_THAT( product2.Extension().a, Eq( 901 ) );
    EXPECT_THAT( product2.Extension().b, Eq( 9301 ) );
    EXPECT_THAT( product2.Extension().c, Eq( 9021 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrExtensionAccessWorksAfterCopyCtor )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product.Extension().a = 2901;
    product.Extension().b = 29301;
    product.Extension().c = 29021;

    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2(
        product );

    EXPECT_THAT( product2.Extension().a, Eq( 2901 ) );
    EXPECT_THAT( product2.Extension().b, Eq( 29301 ) );
    EXPECT_THAT( product2.Extension().c, Eq( 29021 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrExtensionAccessWorksAfterMoveAssignment )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product.Extension().a = 901;
    product.Extension().b = 9301;
    product.Extension().c = 9021;

    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2 =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product2 = std::move( product );

    EXPECT_THAT( product2.Extension().a, Eq( 901 ) );
    EXPECT_THAT( product2.Extension().b, Eq( 9301 ) );
    EXPECT_THAT( product2.Extension().c, Eq( 9021 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrExtensionAccessWorksAfterCopyAssignment )
{
    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product.Extension().a = 2901;
    product.Extension().b = 29301;
    product.Extension().c = 29021;

    shared_product_ptr< CompositeProduct< Child2, SomeClass > > product2 =
        sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product2 = product;

    EXPECT_THAT( product2.Extension().a, Eq( 2901 ) );
    EXPECT_THAT( product2.Extension().b, Eq( 29301 ) );
    EXPECT_THAT( product2.Extension().c, Eq( 29021 ) );
}

TEST_F( elGenericFactory_test, CreateSharedProductWorks )
{
    auto product = sut.CreateSharedProduct< Child >( 120, 121 );
    EXPECT_THAT( product->a, Eq( 120 ) );
    EXPECT_THAT( product->b, Eq( 121 ) );
}

TEST_F( elGenericFactory_test, CreateSharedCompositeProductWorks )
{
    auto product = sut.CreateSharedProduct< Child2 >( 1201, 1121 );
    EXPECT_THAT( product->a, Eq( 1201 ) );
    EXPECT_THAT( product->b, Eq( 1121 ) );

    product.Extension().b = 912;
    EXPECT_THAT( product.Extension().b, Eq( 912 ) );
}

TEST_F( elGenericFactory_test, CreatedSharedProductReleaseWorks )
{
    {
        auto product = sut.CreateSharedProduct< Child >( 120, 121 );
    }
    EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 0 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, CreatedSharedCompositeProductReleaseWorks )
{
    {
        auto product = sut.CreateSharedProduct< Child >( 120, 121 );
    }
    EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 0 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        CreatedSharedProductWhenLastGoesOutOfscopeReleaseWorks )
{
    {
        auto product = sut.CreateSharedProduct< Child >( 120, 121 );
        {
            auto product2 = product;
        }
        EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 1 ) );
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 0 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        CreatedSharedCompositeProductWhenLastGoesOutOfScopeReleaseWorks )
{
    {
        auto product = sut.CreateSharedProduct< Child >( 120, 121 );
        {
            auto product2 = product;
        }
        EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 1 ) );
        EXPECT_THAT( childDtorCounter, Eq( 0 ) );
    }
    EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 0 ) );
    EXPECT_THAT( childDtorCounter, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, CreateSharedCompositeProductExtensionIsShared )
{
    auto product = sut.CreateSharedProduct< Child2 >( 120, 121 );
    {
        auto product2          = product;
        product2.Extension().a = 123;
        product2.Extension().b = 456;
        product2.Extension().c = 789;
    }

    EXPECT_THAT( product.Extension().a, Eq( 123 ) );
    EXPECT_THAT( product.Extension().b, Eq( 456 ) );
    EXPECT_THAT( product.Extension().c, Eq( 789 ) );
}

TEST_F( elGenericFactory_test, AcquireSharedProductWorks )
{
    auto product  = sut.CreateSharedProduct< Child >( 1205, 1215 );
    auto product2 = sut.AcquireSharedProduct< Child >( *product.Get() );

    ASSERT_TRUE( static_cast< bool >( product2 ) );
    EXPECT_THAT( product2->a, Eq( 1205 ) );
    EXPECT_THAT( product2->b, Eq( 1215 ) );
}

TEST_F( elGenericFactory_test, AcquireSharedCompositeProductWorks )
{
    auto product          = sut.CreateSharedProduct< Child2 >( 41205, 41215 );
    product.Extension().a = 901;
    product.Extension().b = 9301;
    product.Extension().c = 9021;

    auto product2 = sut.AcquireSharedProduct< Child2 >( *product.Get() );

    ASSERT_TRUE( static_cast< bool >( product2 ) );
    EXPECT_THAT( product2->a, Eq( 41205 ) );
    EXPECT_THAT( product2->b, Eq( 41215 ) );

    EXPECT_THAT( product2.Extension().a, Eq( 901 ) );
    EXPECT_THAT( product2.Extension().b, Eq( 9301 ) );
    EXPECT_THAT( product2.Extension().c, Eq( 9021 ) );
}

TEST_F( elGenericFactory_test, AcquireSharedProductWithMultipleInstancesWorks )
{
    auto product           = sut.CreateSharedProduct< Child2 >( 1205, 1215 );
    auto product2          = product;
    product2.Extension().a = 90121;

    auto product3 = sut.AcquireSharedProduct< Child2 >( *product.Get() );
    auto product4 = product3;

    ASSERT_TRUE( static_cast< bool >( product4 ) );
    EXPECT_THAT( product4.Extension().a, Eq( 90121 ) );
}

TEST_F( elGenericFactory_test,
        CompositeProductExtensionAccessibleWhileIterating )
{
    auto product          = sut.CreateProduct< Base2 >( 1205 );
    product.Extension().a = 1234;
    product.Extension().b = 4567;
    product.Extension().c = 8901;

    auto& productVector = sut.GetProducts< Base2 >();

    ASSERT_THAT( productVector.size(), Eq( 1 ) );
    EXPECT_THAT( productVector[0].Extension().a, Eq( 1234 ) );
    EXPECT_THAT( productVector[0].Extension().b, Eq( 4567 ) );
    EXPECT_THAT( productVector[0].Extension().c, Eq( 8901 ) );
}

TEST_F( elGenericFactory_test,
        SharedCompositeProductExtensionAccessibleWhileIterating )
{
    auto product          = sut.CreateSharedProduct< Base2 >( 1205 );
    product.Extension().a = 51234;
    product.Extension().b = 54567;
    product.Extension().c = 58901;

    auto& productVector = sut.GetProducts< Base2 >();

    ASSERT_THAT( productVector.size(), Eq( 1 ) );
    EXPECT_THAT( productVector[0].Extension().a, Eq( 51234 ) );
    EXPECT_THAT( productVector[0].Extension().b, Eq( 54567 ) );
    EXPECT_THAT( productVector[0].Extension().c, Eq( 58901 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductCallsOnRemoveCallbackWhenOutOfScope )
{
    int    hasCallbackCalled = 0;
    Child* ptr               = nullptr;
    Base*  ptrArgument       = nullptr;
    {
        auto product = sut.CreateSharedProductWithOnRemoveCallback< Child >(
            [&]( auto v )
            {
                ++hasCallbackCalled;
                ptrArgument = v;
            },
            1205, 1215 );
        ptr = product.Get();
    }

    EXPECT_THAT( hasCallbackCalled, Eq( 1 ) );
    EXPECT_EQ( ptr, ptrArgument );
}

TEST_F( elGenericFactory_test,
        SharedProductCopyCallsOnRemoveCallbackOnceWhenOutOfScope )
{
    int hasCallbackCalled = 0;
    {
        auto product = sut.CreateSharedProductWithOnRemoveCallback< Child >(
            [&]( auto ) { hasCallbackCalled = true; }, 1205, 1215 );

        auto product2 = product;
    }

    EXPECT_THAT( hasCallbackCalled, Eq( 1 ) );
}

TEST_F( elGenericFactory_test,
        AcquiredSharedProductCallsOnRemoveCallbackOnceWhenOutOfScope )
{
    std::optional< shared_product_ptr< Child > > product;
    int                                          hasCallbackCalled = 0;
    product = sut.CreateSharedProductWithOnRemoveCallback< Child >(
        [&]( auto ) { hasCallbackCalled = true; }, 1205, 1215 );

    {
        auto product2 = sut.AcquireSharedProduct( **product );

        product.reset();
        EXPECT_THAT( hasCallbackCalled, Eq( 0 ) );
    }
    EXPECT_THAT( hasCallbackCalled, Eq( 1 ) );
}

TEST_F( elGenericFactory_test, ProductPtrAndThenCallbackCalledWhenHasValue )
{
    int  value   = 0;
    auto product = sut.CreateProduct< int >( 1205 );
    product.AndThen( [&]( auto& v ) { value = v; } );
    EXPECT_THAT( value, Eq( 1205 ) );
}

TEST_F( elGenericFactory_test, ProductPtrAndThenCallbackNotCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateProduct< int >( 1205 );
    auto product2          = std::move( product );

    product.AndThen( [&]( auto& ) { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test, ProductPtrOrElseCallbackCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateProduct< int >( 1205 );
    auto product2          = std::move( product );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_TRUE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test, ProductPtrOrElseCallbackNotCalledWhenHasValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateProduct< int >( 1205 );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrAndThenCallbackCalledWhenHasValue )
{
    int  value   = 0;
    auto product = sut.CreateSharedProduct< int >( 1205 );
    product.AndThen( [&]( auto& v ) { value = v; } );
    EXPECT_THAT( value, Eq( 1205 ) );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrAndThenCallbackNotCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< int >( 1205 );
    auto product2          = std::move( product );

    product.AndThen( [&]( auto& ) { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test, SharedProductPtrOrElseCallbackCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< int >( 1205 );
    auto product2          = std::move( product );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_TRUE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test,
        SharedProductPtrOrElseCallbackNotCalledWhenHasValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< int >( 1205 );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test,
        SharedCompositeProductPtrAndThenCallbackCalledWhenHasValue )
{
    int  value   = 0;
    auto product = sut.CreateSharedProduct< Base2 >( 1205 );
    product.AndThen( [&]( auto& v ) { value = v.a; } );
    EXPECT_THAT( value, Eq( 1205 ) );
}

TEST_F( elGenericFactory_test,
        SharedCompositeProductPtrAndThenCallbackNotCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< Base2 >( 1205 );
    auto product2          = std::move( product );

    product.AndThen( [&]( auto& ) { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test,
        SharedCompositeProductPtrOrElseCallbackCalledWhenNoValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< Base2 >( 1205 );
    auto product2          = std::move( product );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_TRUE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test,
        SharedCompositeProductPtrOrElseCallbackNotCalledWhenHasValue )
{
    bool hasCallbackCalled = false;
    auto product           = sut.CreateSharedProduct< Base2 >( 1205 );

    product.OrElse( [&]() { hasCallbackCalled = true; } );
    EXPECT_FALSE( hasCallbackCalled );
}

TEST_F( elGenericFactory_test, CreateManyProductsAndRemoveThemInRandomOrder )
{
    {
        std::vector< product_ptr< Child > > products;
        for ( uint64_t i = 0; i < 100; ++i )
            products.emplace_back( sut.CreateProduct< Child >(
                static_cast< int >( i ), static_cast< int >( i * 2 ) ) );

        for ( uint64_t i = 0; i < 100; i += 2 )
            products[i].Reset();
    }
    EXPECT_THAT( sut.GetProducts< Child >().size(), Eq( 0 ) );
}

TEST_F( elGenericFactory_test, ProductClassWithProductMembers )
{
    struct ProductClass
    {
        ProductClass( elGenericFactory< ProductClass, Base >& sut )
        {
            this->a = sut.CreateProduct< Child >( 1, 2 );
            this->b = sut.CreateProduct< Child >( 4, 3 );
            this->c = sut.CreateProduct< Child >( 6, 5 );
        }
        product_ptr< Child > a;
        product_ptr< Child > b;
        product_ptr< Child > c;
    };

    elGenericFactory< ProductClass, Base > sut2;

    {
        std::vector< product_ptr< ProductClass > > products;
        for ( uint64_t i = 0; i < 100; ++i )
            products.emplace_back( sut2.CreateProduct< ProductClass >( sut2 ) );

        for ( uint64_t i = 0; i < 100; i += 2 )
            products[i].Reset();
    }

    EXPECT_THAT( sut2.GetProducts< ProductClass >().size(), Eq( 0 ) );
}

TEST_F( elGenericFactory_test, ProductClassWithLateProductMembers )
{
    struct ProductClass
    {
        ProductClass( elGenericFactory< ProductClass, Base >& sut )
            : sut( &sut )
        {
        }

        void
        Init()
        {
            this->a = sut->CreateProduct< Child >( 1, 2 );
            this->b = sut->CreateProduct< Child >( 4, 3 );
            this->c = sut->CreateProduct< Child >( 6, 5 );
        }

        elGenericFactory< ProductClass, Base >* sut;
        product_ptr< Child >                    a;
        product_ptr< Child >                    b;
        product_ptr< Child >                    c;
    };

    elGenericFactory< ProductClass, Base > sut2;

    {
        std::vector< product_ptr< ProductClass > > products;
        for ( uint64_t i = 0; i < 100; ++i )
            products.emplace_back( sut2.CreateProduct< ProductClass >( sut2 ) );

        for ( uint64_t i = 0; i < 100; ++i )
            products[i]->Init();

        for ( uint64_t i = 0; i < 100; i += 2 )
            products[i].Reset();
    }


    EXPECT_THAT( sut2.GetProducts< ProductClass >().size(), Eq( 0 ) );
}
