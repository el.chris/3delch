#include "unitTest.hpp"
#include "utils/elFile.hpp"

using namespace ::el3D::utils;
using namespace ::testing;

class elFile_test : public ::testing::Test
{
  public:
    virtual void
    SetUp()
    {
    }
};

TEST_F( elFile_test, ReadWrite )
{
    std::string stdOutput;
    elFile      file1( "/some/crappy/file" );

    testing::internal::CaptureStderr();
    EXPECT_THAT( file1.Read(), Eq( false ) );
    stdOutput = testing::internal::GetCapturedStderr();

    std::remove(
        std::string( std::string( UNIT_TEST_DIR ) + "elFile.io" ).c_str() );
    elFile file2( std::string( UNIT_TEST_DIR ) + "elFile.io", true );
    for ( char i = 12; i < 127; ++i )
        file2.content.push_back( i );

    EXPECT_THAT( file2.Write(), Eq( true ) );

    elFile file3( std::string( UNIT_TEST_DIR ) + "elFile.io", true );

    EXPECT_THAT( file3.Read(), Eq( true ) );
    for ( char i = 12; i < 127; ++i )
        EXPECT_EQ( file3.content[static_cast< size_t >( i ) - 12], i );

    file1.content.push_back( 38 );

    testing::internal::CaptureStderr();
    EXPECT_THAT( file1.Write(), Eq( false ) );
    stdOutput = testing::internal::GetCapturedStderr();
}

TEST_F( elFile_test, StdGetter )
{
    elFile file1( "fuuBar" );
    EXPECT_EQ( file1.GetPath(), "fuuBar" );
}

TEST_F( elFile_test, GetContent )
{
    std::remove(
        std::string( std::string( UNIT_TEST_DIR ) + "elFile2.io" ).c_str() );
    elFile file1( std::string( UNIT_TEST_DIR ) + "elFile2,io", false );

    for ( size_t i = 0; i < 10; ++i )
        file1.content.push_back( 'h' );

    EXPECT_EQ( file1.GetContentAsString(), "hhhhhhhhhh" );

    // test2
    for ( size_t i = 0; i < 10; ++i )
    {
        file1.content.push_back( '\n' );
        for ( size_t k = 0; k < 10; ++k )
            file1.content.push_back( 'x' );
    }
    ASSERT_EQ( file1.GetContentAsVector().size(), 11 );
    EXPECT_EQ( file1.GetContentAsVector()[0], "hhhhhhhhhh" );
    for ( size_t i = 1; i < 10; ++i )
        EXPECT_EQ( file1.GetContentAsVector()[i], "xxxxxxxxxx" );

    // test3
    file1.content.clear();
    EXPECT_EQ( file1.GetContentAsVector().size(), 0 );
    file1.content.push_back( 'a' );
    ASSERT_EQ( file1.GetContentAsVector().size(), 1 );
    EXPECT_EQ( file1.GetContentAsVector()[0], "a" );
}

TEST_F( elFile_test, SetContent )
{
    std::remove(
        std::string( std::string( UNIT_TEST_DIR ) + "elFile3.io" ).c_str() );
    elFile file1( std::string( UNIT_TEST_DIR ) + "elFile3,io", false );

    file1.SetContentFromString( "jjjjjj" );
    ASSERT_EQ( file1.content.size(), 6 );

    for ( size_t i = 0; i < 6; ++i )
        EXPECT_EQ( file1.content[i], 'j' );

    file1.SetContentFromString( "" );
    EXPECT_EQ( file1.content.size(), 0 );

    std::vector< std::string > cont;
    cont.push_back( "XXX" );

    file1.SetContentFromVector( cont );
    ASSERT_EQ( file1.content.size(), 3 );
    for ( size_t i = 0; i < 3; ++i )
        EXPECT_EQ( file1.content[i], 'X' );

    cont.push_back( "XXX" );
    cont.push_back( "XXX" );

    file1.SetContentFromVector( cont );
    ASSERT_EQ( file1.content.size(), 11 );
    for ( size_t i = 0; i < 11; ++i )
        if ( ( i + 1 ) % 4 == 0 )
            EXPECT_EQ( file1.content[i], '\n' );
        else
            EXPECT_EQ( file1.content[i], 'X' );

    cont.clear();


    file1.SetContentFromVector( cont );
    EXPECT_EQ( file1.content.size(), 0 );
}
