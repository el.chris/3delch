#include "unitTest.hpp"
#include "units/Time.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"
#include "utils/serializerTypes.hpp"

using namespace ::testing;
using namespace ::el3D::utils;
using namespace ::el3D;
using namespace ::el3D::units;

class elByteSerializer_test : public ::testing::Test
{
  public:
    class SerializableClass : public Serializable
    {
      public:
        uint64_t
        GetSerializedSize() const noexcept override
        {
            return elByteSerializer::GetSerializedSize( a, b, c, d );
        }

        uint64_t
        Serialize( const Endian endianness, byteStream_t& byteStream,
                   const uint64_t byteStreamOffset ) const noexcept override
        {
            return elByteSerializer::FillByteStreamWithSerialization(
                endianness, byteStream, byteStreamOffset, a, b, c, d );
        }

        bb::elExpected< ByteStreamError >
        Deserialize( const Endian endianness, const byteStream_t& byteStream,
                     const uint64_t byteStreamOffset ) noexcept override
        {
            return elByteDeserializer( byteStream, byteStreamOffset )
                .Extract( endianness, a, b, c, d );
        }

        int         a;
        float       b;
        double      c;
        std::string d;
    };

    void
    SetUp()
    {
    }
};

TEST_F( elByteSerializer_test, ConvertDouble_BigEndian )
{
    double           value = 123.456;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x40 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x5e ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xdd ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x2f ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x1a ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0x9f ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0xbe ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x77 ) );
}

TEST_F( elByteSerializer_test, ConvertDouble_LittleEndian )
{
    double           value = 123.456;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x40 ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0x5e ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0xdd ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x2f ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x1a ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0x9f ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xbe ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x77 ) );
}

TEST_F( elByteSerializer_test, ConvertUint8_BigEndian )
{
    uint8_t          value = 0xab;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 1 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xab ) );
}

TEST_F( elByteSerializer_test, ConvertUint8_LittleEndian )
{
    uint8_t          value = 0xab;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 1 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xab ) );
}

TEST_F( elByteSerializer_test, ConvertUint16_BigEndian )
{
    uint16_t         value = 0xabcd;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 2 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xab ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xcd ) );
}

TEST_F( elByteSerializer_test, ConvertUint16_LittleEndian )
{
    uint16_t         value = 0xabcd;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 2 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xcd ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xab ) );
}

TEST_F( elByteSerializer_test, ConvertUint32_BigEndian )
{
    uint32_t         value = 0x12345678;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 4 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x12 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0x56 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x78 ) );
}

TEST_F( elByteSerializer_test, ConvertUint32_LittleEndian )
{
    uint32_t         value = 0x12345678;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 4 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x78 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x56 ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x12 ) );
}

TEST_F( elByteSerializer_test, ConvertUint64_BigEndian )
{
    uint64_t         value = 0x1234567890abcdef;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x12 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0x56 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x78 ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x90 ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0xab ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0xcd ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0xef ) );
}

TEST_F( elByteSerializer_test, ConvertUint64_LittleEndian )
{
    uint64_t         value = 0x1234567890abcdef;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xef ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xcd ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xab ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x90 ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x78 ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0x56 ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x12 ) );
}

TEST_F( elByteSerializer_test, ConvertInt8_BigEndian )
{
    int8_t           value = -123;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 1 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x85 ) );
}

TEST_F( elByteSerializer_test, ConvertInt8_LittleEndian )
{
    int8_t           value = -123;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 1 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x85 ) );
}

TEST_F( elByteSerializer_test, ConvertInt16_BigEndian )
{
    int16_t          value = -0x1234;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 2 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xed ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xcc ) );
}

TEST_F( elByteSerializer_test, ConvertInt16_LittleEndian )
{
    int16_t          value = -0x1234;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 2 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xcc ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xed ) );
}


TEST_F( elByteSerializer_test, ConvertInt32_BigEndian )
{
    int32_t          value = -0x12345678;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 4 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xed ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xcb ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xa9 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x88 ) );
}

TEST_F( elByteSerializer_test, ConvertInt32_LittleEndian )
{
    int32_t          value = -0x12345678;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 4 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x88 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xa9 ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xcb ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0xed ) );
}

TEST_F( elByteSerializer_test, ConvertInt64_BigEndian )
{
    int64_t          value = -0x1234567890abcdef;
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xed ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xcb ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xa9 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x87 ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x6f ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0x54 ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0x32 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x11 ) );
}

TEST_F( elByteSerializer_test, ConvertInt64_LittleEndian )
{
    int64_t          value = -0x1234567890abcdef;
    elByteSerializer sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0xed ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0xcb ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0xa9 ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x87 ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x6f ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0x54 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x32 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x11 ) );
}

TEST_F( elByteSerializer_test, ConvertUint16Vector_BigEndian )
{
    std::vector< uint16_t > value{ 0x1234, 0x2345, 0x3456, 0x4567 };
    elByteSerializer        sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 16 ) );
    for ( size_t k = 0; k < 7; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x00 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x04 ) );

    EXPECT_THAT( sut.GetByteStream().stream[8], Eq( 0x12 ) );
    EXPECT_THAT( sut.GetByteStream().stream[9], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[10], Eq( 0x23 ) );
    EXPECT_THAT( sut.GetByteStream().stream[11], Eq( 0x45 ) );
    EXPECT_THAT( sut.GetByteStream().stream[12], Eq( 0x34 ) );
    EXPECT_THAT( sut.GetByteStream().stream[13], Eq( 0x56 ) );
    EXPECT_THAT( sut.GetByteStream().stream[14], Eq( 0x45 ) );
    EXPECT_THAT( sut.GetByteStream().stream[15], Eq( 0x67 ) );
}

TEST_F( elByteSerializer_test, ConvertInt32Vector_LittleEndian )
{
    std::vector< int32_t > value{ -0x123456, -0x234567 };
    elByteSerializer       sut;
    sut.Serialize( Endian::Little, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 16 ) );
    for ( size_t k = 1; k < 8; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x00 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x02 ) );

    EXPECT_THAT( sut.GetByteStream().stream[8], Eq( 0xaa ) );
    EXPECT_THAT( sut.GetByteStream().stream[9], Eq( 0xcb ) );
    EXPECT_THAT( sut.GetByteStream().stream[10], Eq( 0xed ) );
    EXPECT_THAT( sut.GetByteStream().stream[11], Eq( 0xff ) );

    EXPECT_THAT( sut.GetByteStream().stream[12], Eq( 0x99 ) );
    EXPECT_THAT( sut.GetByteStream().stream[13], Eq( 0xba ) );
    EXPECT_THAT( sut.GetByteStream().stream[14], Eq( 0xdc ) );
    EXPECT_THAT( sut.GetByteStream().stream[15], Eq( 0xff ) );
}

TEST_F( elByteSerializer_test, ConvertString )
{
    std::string      value{ "Hello" };
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( value.size() + 8u ) );
    for ( size_t k = 0; k < 7; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x00 ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x05 ) );

    EXPECT_THAT( sut.GetByteStream().stream[8], Eq( 0x48 ) );
    EXPECT_THAT( sut.GetByteStream().stream[9], Eq( 0x65 ) );
    EXPECT_THAT( sut.GetByteStream().stream[10], Eq( 0x6c ) );
    EXPECT_THAT( sut.GetByteStream().stream[11], Eq( 0x6c ) );
    EXPECT_THAT( sut.GetByteStream().stream[12], Eq( 0x6f ) );
}

TEST_F( elByteSerializer_test, ConvertMultipleValues )
{
    elByteSerializer sut;
    uint8_t          a{ 0xa }, b{ 0xb }, c{ 0xc };
    std::string      d{ "abc" };
    sut.Serialize( Endian::Big, a, b, c, d );

    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 14 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0xa ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0xb ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xc ) );
    for ( size_t k = 3; k < 10; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x0 ) );

    EXPECT_THAT( sut.GetByteStream().stream[10], Eq( 0x3 ) );
    EXPECT_THAT( sut.GetByteStream().stream[11], Eq( 0x61 ) );
    EXPECT_THAT( sut.GetByteStream().stream[12], Eq( 0x62 ) );
    EXPECT_THAT( sut.GetByteStream().stream[13], Eq( 0x63 ) );
}

TEST_F( elByteSerializer_test, ConvertVariantWithString )
{
    std::variant< uint32_t, std::string, uint8_t > testData{ "bonjour" };
    elByteSerializer                               sut;
    sut.Serialize( Endian::Big, testData );

    ASSERT_THAT( sut.GetByteStream().stream.size(),
                 Eq( sizeof( variantIndexType_t ) + 8 +
                     std::get< 1 >( testData ).size() ) );
    for ( size_t k = 0; k < sizeof( variantIndexType_t ) - 1; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x0 ) );

    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) - 1],
                 Eq( 0x1 ) );

    for ( size_t k = sizeof( variantIndexType_t );
          k < sizeof( variantIndexType_t ) + 7; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x0 ) );

    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 7],
                 Eq( 0x7 ) );

    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 8],
                 Eq( 0x62 ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 9],
                 Eq( 0x6f ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 10],
                 Eq( 0x6e ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 11],
                 Eq( 0x6a ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 12],
                 Eq( 0x6f ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 13],
                 Eq( 0x75 ) );
    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) + 14],
                 Eq( 0x72 ) );
}

TEST_F( elByteSerializer_test, ConvertVariantWithInt )
{
    std::variant< uint32_t, std::string, uint8_t > testData{
        static_cast< uint8_t >( 0xab ) };
    elByteSerializer sut;
    sut.Serialize( Endian::Big, testData );

    ASSERT_THAT( sut.GetByteStream().stream.size(),
                 Eq( sizeof( variantIndexType_t ) + 1 ) );
    for ( size_t k = 0; k < sizeof( variantIndexType_t ) - 1; ++k )
        EXPECT_THAT( sut.GetByteStream().stream[k], Eq( 0x0 ) );

    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t ) - 1],
                 Eq( 0x2 ) );

    EXPECT_THAT( sut.GetByteStream().stream[sizeof( variantIndexType_t )],
                 Eq( 0xab ) );
}

TEST_F( elByteSerializer_test, ConvertEmptyString )
{
    std::string      emptyString = "";
    elByteSerializer sut;
    sut.Serialize( Endian::Big, emptyString );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8u ) );
}

TEST_F( elByteSerializer_test, ConvertEmptyStringVector )
{
    std::vector< std::string > emptyVector;
    elByteSerializer           sut;
    sut.Serialize( Endian::Big, emptyVector );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8u ) );
}

TEST_F( elByteSerializer_test, ConvertVectorOfPartiallyEmptyStrings )
{
    std::vector< std::string > emptyVector{ "", "data", "", "daat", "" };
    elByteSerializer           sut;
    sut.Serialize( Endian::Big, emptyVector );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 6u * 8u + 8u ) );

    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 5 ) );  // size of vector
    EXPECT_THAT( sut.GetByteStream().stream[15], Eq( 0 ) ); // size of 1st str
    EXPECT_THAT( sut.GetByteStream().stream[23], Eq( 4 ) ); // size of 2st str
    EXPECT_THAT( sut.GetByteStream().stream[35], Eq( 0 ) ); // size of 3st str
    EXPECT_THAT( sut.GetByteStream().stream[43], Eq( 4 ) ); // size of 4st str
    EXPECT_THAT( sut.GetByteStream().stream[55], Eq( 0 ) ); // size of 4st str
}

TEST_F( elByteSerializer_test, ConvertArray )
{
    std::array< uint8_t, 2 > myArray{ 0xaf, 0xfe };
    elByteSerializer         sut;
    sut.Serialize( Endian::Big, myArray );
    auto byteStream = sut.GetByteStream().stream;
    ASSERT_THAT( byteStream.size(), Eq( 2u ) );
    EXPECT_THAT( byteStream[0], Eq( 0xaf ) );
    EXPECT_THAT( byteStream[1], Eq( 0xfe ) );
}

TEST_F( elByteSerializer_test, SerializeSerializableClass )
{
    elByteSerializer  sut;
    SerializableClass a;
    a.a = 123;
    a.b = 22.1f;
    a.c = 123.123;
    a.d = "hello world";

    sut.Serialize( Endian::Big, a );
    ASSERT_THAT( sut.GetByteStream().stream.size(),
                 Eq( 4u + 4u + 8u + 8u + 11u ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 123 ) );

    SerializableClass b;
    b.Deserialize( Endian::Big, sut.GetByteStream(), 0 );
    EXPECT_THAT( b.a, Eq( 123 ) );
    EXPECT_THAT( b.b, Eq( 22.1f ) );
    EXPECT_THAT( b.c, Eq( 123.123 ) );
    EXPECT_THAT( b.d, Eq( "hello world" ) );
}

TEST_F( elByteSerializer_test, ComplexVariant )
{
    std::variant< SerializableClass, std::vector< SerializableClass > > data,
        extractedData;
    elByteSerializer  sut;
    SerializableClass sdata;
    sdata.a = 123;
    sdata.b = 44.5f;
    sdata.c = 123.456;
    sdata.d = "hello world";

    data = sdata;
    sut.Serialize( Endian::Big, data );
    ASSERT_THAT( sut.GetByteStream().stream.size(),
                 Eq( sizeof( variantIndexType_t ) + sizeof( int ) +
                     sizeof( float ) + sizeof( double ) + 8u + 11u ) );
    EXPECT_THAT( sut.GetSerializedSize( data ),
                 sut.GetByteStream().stream.size() );

    elByteDeserializer( sut.GetByteStream() )
        .Extract( Endian::Big, extractedData );
    ASSERT_THAT( extractedData.index(), Eq( 0 ) );
    EXPECT_THAT( std::get< 0 >( extractedData ).a, Eq( 123 ) );
    EXPECT_THAT( std::get< 0 >( extractedData ).b, Eq( 44.5f ) );
    EXPECT_THAT( std::get< 0 >( extractedData ).c, Eq( 123.456 ) );
    EXPECT_THAT( std::get< 0 >( extractedData ).d, Eq( "hello world" ) );
}

TEST_F( elByteSerializer_test, ComplexVariantWithVector )
{
    std::variant< SerializableClass, std::vector< SerializableClass > > data,
        extractedData;
    elByteSerializer  sut;
    SerializableClass sdata1;
    sdata1.a = 9123;
    sdata1.b = 944.5f;
    sdata1.c = 9123.456;
    sdata1.d = "blah";

    SerializableClass sdata2;
    sdata2.a = 7123;
    sdata2.b = 744.5f;
    sdata2.c = 7123.456;
    sdata2.d = "fuhh";

    data = std::vector< SerializableClass >( { sdata1, sdata2, sdata1 } );
    sut.Serialize( Endian::Big, data );
    EXPECT_THAT( sut.GetSerializedSize( data ),
                 sut.GetByteStream().stream.size() );
    ASSERT_THAT( sut.GetByteStream().stream.size(),
                 Eq( sizeof( variantIndexType_t ) +
                     3 * ( sizeof( int ) + sizeof( float ) + sizeof( double ) +
                           8u + 4u ) +
                     8u ) );

    elByteDeserializer( sut.GetByteStream() )
        .Extract( Endian::Big, extractedData );
    ASSERT_THAT( extractedData.index(), Eq( 1 ) );
    ASSERT_THAT( std::get< 1 >( extractedData ).size(), Eq( 3 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[0].a, Eq( 9123 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[0].b, Eq( 944.5f ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[0].c, Eq( 9123.456 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[0].d, Eq( "blah" ) );

    EXPECT_THAT( std::get< 1 >( extractedData )[1].a, Eq( 7123 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[1].b, Eq( 744.5f ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[1].c, Eq( 7123.456 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[1].d, Eq( "fuhh" ) );

    EXPECT_THAT( std::get< 1 >( extractedData )[2].a, Eq( 9123 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[2].b, Eq( 944.5f ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[2].c, Eq( 9123.456 ) );
    EXPECT_THAT( std::get< 1 >( extractedData )[2].d, Eq( "blah" ) );
}


TEST_F( elByteSerializer_test, BasicUnitSerialization )
{
    units::Time      value = units::Time::Seconds( 123.456 );
    elByteSerializer sut;
    sut.Serialize( Endian::Big, value );
    ASSERT_THAT( sut.GetByteStream().stream.size(), Eq( 8 ) );
    EXPECT_THAT( sut.GetByteStream().stream[0], Eq( 0x40 ) );
    EXPECT_THAT( sut.GetByteStream().stream[1], Eq( 0x5e ) );
    EXPECT_THAT( sut.GetByteStream().stream[2], Eq( 0xdd ) );
    EXPECT_THAT( sut.GetByteStream().stream[3], Eq( 0x2f ) );
    EXPECT_THAT( sut.GetByteStream().stream[4], Eq( 0x1a ) );
    EXPECT_THAT( sut.GetByteStream().stream[5], Eq( 0x9f ) );
    EXPECT_THAT( sut.GetByteStream().stream[6], Eq( 0xbe ) );
    EXPECT_THAT( sut.GetByteStream().stream[7], Eq( 0x77 ) );
}
