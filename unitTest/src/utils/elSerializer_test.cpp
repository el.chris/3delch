#include "unitTest.hpp"
#include "utils/elSerializer.hpp"

using namespace ::testing;
using namespace ::el3D::utils;

namespace elSerialize_test
{
struct Test
{
    Test( std::string value1, std::string value2 )
        : value1( value1 ), value2( value2 )
    {
    }

    Test( const elSerializer& s )
    {
        s.Deserialize( value1, value2 );
    }

    operator elSerializer() const
    {
        return elSerializer::Serialize( value1, value2 );
    }

    std::string value1, value2;
};
} // namespace elSerialize_test


TEST( elSerialize_test, SimpleString )
{
    EXPECT_THAT( elSerializer::Serialize( "hello" ).GetRaw(), Eq( "5:hello" ) );
    EXPECT_THAT( elSerializer::Serialize( "hello", "world" ).GetRaw(),
                 Eq( "5:hello5:world" ) );
    EXPECT_THAT( elSerializer::Serialize( std::string( "foobar" ) ).GetRaw(),
                 Eq( "6:foobar" ) );
    EXPECT_THAT(
        elSerializer::Serialize( std::string( "foobar" ), "dombooo" ).GetRaw(),
        Eq( "6:foobar7:dombooo" ) );
}

TEST( elSerialize_test, MixedTypes )
{
    EXPECT_THAT( elSerializer::Serialize( 1, 23.1f, 1.12 ).GetRaw(),
                 Eq( "1:14:23.14:1.12" ) );
}

TEST( elSerialize_test, AdvancedTypes )
{
    elSerialize_test::Test bla( "asd", "fge" );
    EXPECT_THAT( elSerializer::Serialize( bla ).GetRaw(),
                 Eq( "10:3:asd3:fge" ) );
}

TEST( elSerialize_test, DeserializeSimpleString )
{
    std::string a, b, c;

    elSerializer( std::string( "3:asd" ) ).Deserialize( a );
    EXPECT_THAT( a, Eq( "asd" ) );

    elSerializer( std::string( "3:fuu5:barla" ) ).Deserialize( a, b );
    EXPECT_THAT( a, Eq( "fuu" ) );
    EXPECT_THAT( b, Eq( "barla" ) );

    elSerializer( std::string( "0:5:fufuu2:al" ) ).Deserialize( a, b, c );

    EXPECT_THAT( a, Eq( "" ) );
    EXPECT_THAT( b, Eq( "fufuu" ) );
    EXPECT_THAT( c, Eq( "al" ) );
}

TEST( elSerialize_test, DeserializeMultipleTypes )
{
    long int a;
    float    b;
    double   c;
    char     d;

    elSerializer( std::string( "3:1234:0.125:1.2341:V" ) )
        .Deserialize( a, b, c, d );
    EXPECT_THAT( a, Eq( 123 ) );
    EXPECT_THAT( b, Eq( 0.12f ) );
    EXPECT_THAT( c, Eq( 1.234 ) );
    EXPECT_THAT( d, Eq( 'V' ) );
}

TEST( elSerialize_test, InvalidSerializationString )
{
    EXPECT_THAT( elSerializer( std::string( "4:123" ) ), Eq( false ) );
    EXPECT_THAT( elSerializer( std::string( "4:" ) ), Eq( false ) );
    EXPECT_THAT( elSerializer( std::string( "4:XXXX2:A" ) ), Eq( false ) );
}
