#include "unitTest.hpp"
#include "utils/elReferenceCounter.hpp"

using namespace ::testing;
using namespace ::el3D::utils;

class elReferenceCounter_test : public Test
{
  public:
    using ref_t = elReferenceCounter< uint64_t >;
    ref_t sut1;
    ref_t sut2;
};

TEST_F( elReferenceCounter_test, CTorIncrements )
{
    EXPECT_THAT( sut1.GetValue(), Eq( 1 ) );
}

TEST_F( elReferenceCounter_test, CopyCTor )
{
    ref_t sut3( sut1 );
    EXPECT_THAT( sut3.GetValue(), Eq( 2 ) );
}

TEST_F( elReferenceCounter_test, DestructorAfterCopyCTor )
{
    {
        ref_t sut3;
        {
            ref_t sut4( sut3 );
            EXPECT_THAT( sut3.GetValue(), Eq( 2 ) );
        }
        EXPECT_THAT( sut3.GetValue(), Eq( 1 ) );
    }
}

TEST_F( elReferenceCounter_test, MoveCTor )
{
    ref_t sut3( std::move( sut1 ) );
    EXPECT_THAT( sut3.GetValue(), Eq( 1 ) );
}

TEST_F( elReferenceCounter_test, CopyAssignment )
{
    sut1 = sut2;
    EXPECT_THAT( sut1.GetValue(), Eq( 2 ) );
    EXPECT_THAT( sut2.GetValue(), Eq( 2 ) );
}

TEST_F( elReferenceCounter_test, MoveAssignment )
{
    sut1 = std::move( sut2 );
    EXPECT_THAT( sut1.GetValue(), Eq( 1 ) );
}
