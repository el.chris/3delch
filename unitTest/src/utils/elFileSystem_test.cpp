#include "unitTest.hpp"
#include "utils/elFileSystem.hpp"

using namespace ::testing;
using namespace ::el3D::utils;

TEST( elFileSystem_test, ConvertStringToVectorPath_StdPath )
{
    auto vecPath = elFileSystem::ConvertStringToVectorPath(
        std::string( "fuu" ) + elFileSystem::PATH_DELIMITER + "bar" +
        elFileSystem::PATH_DELIMITER + "bla" );

    ASSERT_THAT( vecPath.size(), Eq( 3 ) );

    EXPECT_THAT( vecPath[0], Eq( "fuu" ) );
    EXPECT_THAT( vecPath[1], Eq( "bar" ) );
    EXPECT_THAT( vecPath[2], Eq( "bla" ) );
}

TEST( elFileSystem_test, ConvertStringToVectorPath_StdPathWithPrePathDelimiter )
{
    std::string delimiter;
    delimiter.push_back( elFileSystem::PATH_DELIMITER );
    auto vecPath = elFileSystem::ConvertStringToVectorPath(
        delimiter + "fuu" + elFileSystem::PATH_DELIMITER + "bar" +
        elFileSystem::PATH_DELIMITER + "bla" );

    ASSERT_THAT( vecPath.size(), Eq( 3 ) );

    EXPECT_THAT( vecPath[0], Eq( "fuu" ) );
    EXPECT_THAT( vecPath[1], Eq( "bar" ) );
    EXPECT_THAT( vecPath[2], Eq( "bla" ) );
}

TEST( elFileSystem_test,
      ConvertStringToVectorPath_StdPathWithPostPathDelimiter )
{
    std::string delimiter;
    delimiter.push_back( elFileSystem::PATH_DELIMITER );
    auto vecPath = elFileSystem::ConvertStringToVectorPath(
        std::string( "fuu" ) + elFileSystem::PATH_DELIMITER + "bar" +
        elFileSystem::PATH_DELIMITER + "bla" + elFileSystem::PATH_DELIMITER );

    ASSERT_THAT( vecPath.size(), Eq( 3 ) );

    EXPECT_THAT( vecPath[0], Eq( "fuu" ) );
    EXPECT_THAT( vecPath[1], Eq( "bar" ) );
    EXPECT_THAT( vecPath[2], Eq( "bla" ) );
}

TEST( elFileSystem_test,
      ConvertStringToVectorPath_StdPathWithMultiPathDelimiter )
{
    std::string delimiter;
    delimiter.push_back( elFileSystem::PATH_DELIMITER );
    auto vecPath = elFileSystem::ConvertStringToVectorPath(
        std::string( "fuu" ) + elFileSystem::PATH_DELIMITER + delimiter +
        delimiter + "bar" + elFileSystem::PATH_DELIMITER + "bla" +
        elFileSystem::PATH_DELIMITER );

    ASSERT_THAT( vecPath.size(), Eq( 3 ) );

    EXPECT_THAT( vecPath[0], Eq( "fuu" ) );
    EXPECT_THAT( vecPath[1], Eq( "bar" ) );
    EXPECT_THAT( vecPath[2], Eq( "bla" ) );
}


TEST( elFileSystem_test, ConvertStringToVectorPath_EmptyString )
{
    auto vecPath = elFileSystem::ConvertStringToVectorPath( "" );

    ASSERT_THAT( vecPath.size(), Eq( 0 ) );
}

TEST( elFileSystem_test, ConvertStringToVectorPath_SinglePathElement )
{
    auto vecPath = elFileSystem::ConvertStringToVectorPath( "bla" );

    ASSERT_THAT( vecPath.size(), Eq( 1 ) );
    EXPECT_THAT( vecPath[0], Eq( "bla" ) );
}

TEST( elFileSystem_test, ConvertStringToVectorPath_RootPath )
{
    auto vecPath = elFileSystem::ConvertStringToVectorPath(
        std::string( elFileSystem::ROOT_PATH ) );

#ifndef _WIN32
    ASSERT_THAT( vecPath.size(), Eq( 0 ) );
#else
    /// in windows we have drive letters
    ASSERT_THAT( vecPath.size(), Eq( 1 ) );
#endif
}
