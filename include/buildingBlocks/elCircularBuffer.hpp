#pragma once

#include "buildingBlocks/elCircularIndex.hpp"

#include <array>
#include <cstring>
#include <memory>
#include <optional>
#include <vector>

namespace el3D
{
namespace bb
{
template < typename T, uint64_t N >
class elCircularBuffer
{
  public:
    static constexpr uint64_t SIZE = N;
    using ValueType                = T;
    using index_t                  = elCircularIndex< N >;

    elCircularBuffer() noexcept;
    elCircularBuffer( const elCircularBuffer& ) noexcept;
    elCircularBuffer( elCircularBuffer&& ) noexcept;
    ~elCircularBuffer() noexcept;

    elCircularBuffer&
    operator=( const elCircularBuffer& ) noexcept;
    elCircularBuffer&
    operator=( elCircularBuffer&& ) noexcept;

    template < typename... Targs >
    std::optional< T >
    Push( Targs&&... args ) noexcept;
    template < typename Allocator >
    std::optional< std::vector< T > >
    MultiPush( const std::vector< T, Allocator >& values ) noexcept;
    std::optional< T >
    Pop() noexcept;
    template < typename OutputContainer >
    std::optional< OutputContainer >
    MultiPop( const size_t n = 0 ) noexcept;

    template < typename OutputContainer >
    OutputContainer
    GetContent() const noexcept;

    void
    Clear() noexcept;
    bool
    IsEmpty() const noexcept;
    uint64_t
    Size() const noexcept;
    bool
    IsFull() const noexcept;

  private:
    template < typename OutputContainer >
    OutputContainer
    MultiGet( const size_t n, const bool doRemoveElementsAfterGet ) noexcept;
    void
    Free() noexcept;

  private:
    T*      data = nullptr;
    index_t readIndex{ 0 };
    index_t writeIndex{ 0 };
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/elCircularBuffer.inl"
