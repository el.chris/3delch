#pragma once

#include <cstdint>
#include <optional>
#include <queue>
#include <vector>

namespace el3D
{
namespace bb
{
template < typename T >
class elFiFo
{
  public:
    using ValueType = T;

    template < typename... Targs >
    void
    Push( Targs&&... args ) noexcept;
    template < typename Allocator >
    void
    MultiPush( const std::vector< T, Allocator >& values ) noexcept;
    std::optional< T >
    Pop() noexcept;
    template < typename OutputContainer >
    std::optional< OutputContainer >
    MultiPop( const size_t n = 0 ) noexcept;

    void
    Clear() noexcept;
    bool
    IsEmpty() const noexcept;
    uint64_t
    Size() const noexcept;

  private:
    std::queue< T > data;
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/elFiFo.inl"
