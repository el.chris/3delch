#pragma once

#include "buildingBlocks/algorithm_extended.hpp"

#include <list>

namespace el3D
{
namespace bb
{
template < typename integer_t >
class elUniqueIDHandler
{
  public:
    elUniqueIDHandler()                           = default;
    elUniqueIDHandler( const elUniqueIDHandler& ) = default;
    elUniqueIDHandler( elUniqueIDHandler&& )      = default;
    ~elUniqueIDHandler()                          = default;

    elUniqueIDHandler&
    operator=( const elUniqueIDHandler& ) = default;
    elUniqueIDHandler&
    operator=( elUniqueIDHandler&& ) = default;

    integer_t
    RequestID() noexcept;
    void
    FreeID( const integer_t ) noexcept;
    void
    FreeAll() noexcept;
    bool
    RequestCustomID( const integer_t ) noexcept;

  private:
    std::list< integer_t > freeIDs;
    integer_t              nextFreeID = 0;
};
} // namespace bb
} // namespace el3D
#include "buildingBlocks/elUniqueIDHandler.inl"
