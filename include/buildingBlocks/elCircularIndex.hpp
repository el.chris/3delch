#pragma once
#include <cstdint>
#include <cstdlib>

namespace el3D
{
namespace bb
{
template < uint64_t N >
class elCircularIndex
{
  public:
    elCircularIndex()                         = default;
    elCircularIndex( const elCircularIndex& ) = default;
    elCircularIndex( elCircularIndex&& )      = default;
    ~elCircularIndex()                        = default;

    elCircularIndex&
    operator=( const elCircularIndex& ) = default;
    elCircularIndex&
    operator=( elCircularIndex&& ) = default;

    elCircularIndex( const int64_t value );

    operator uint64_t() const noexcept;

    int64_t
    GetValue() const noexcept;

    // pre
    elCircularIndex< N >
    operator++() noexcept;
    elCircularIndex< N >
    operator--() noexcept;

    // post
    elCircularIndex< N >
    operator++( int ) noexcept;
    elCircularIndex< N >
    operator--( int ) noexcept;

    elCircularIndex< N >&
    operator+=( const int64_t rhs ) noexcept;
    elCircularIndex< N >&
    operator-=( const int64_t rhs ) noexcept;

    bool
    operator==( const elCircularIndex< N > v ) const noexcept;

    bool
    operator!=( const elCircularIndex< N > v ) const noexcept;

  private:
    int64_t value{0};
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/elCircularIndex.inl"
