#pragma once

#include "buildingBlocks/elUniqueIDHandler.hpp"

#include <iostream>
#include <memory>
#include <span>
#include <vector>

namespace el3D
{
namespace bb
{
template < typename T >
class elUniqueIndexVector
{
  public:
    using base_t          = std::vector< T >;
    using baseIter_t      = typename base_t::iterator;
    using baseIterConst_t = typename base_t::const_iterator;

    using index_t       = uint64_t;
    using indexVector_t = std::vector< index_t >;

    static constexpr index_t INVALID_INDEX =
        std::numeric_limits< index_t >::max();

    class element_t
    {
      public:
        element_t( const index_t                   indexValue,
                   elUniqueIndexVector< T >* const origin ) noexcept;

        index_t
        index() const noexcept;

        T&
        value() noexcept;
        const T&
        value() const noexcept;

        // return reference here so that it is recursively called when for
        // instance something link std::shared_ptr is stored in T
        T&
        operator->() noexcept;
        const T&
        operator->() const noexcept;

      private:
        index_t indexValue{ std::numeric_limits< index_t >::max() };
        elUniqueIndexVector< T >* origin{ nullptr };
    };

    elUniqueIndexVector() = default;
    elUniqueIndexVector( const std::initializer_list< T >& values ) noexcept;
    elUniqueIndexVector( const elUniqueIndexVector& ) = default;
    elUniqueIndexVector( elUniqueIndexVector&& )      = default;
    ~elUniqueIndexVector()                            = default;

    elUniqueIndexVector&
    operator=( const elUniqueIndexVector& ) = default;
    elUniqueIndexVector&
    operator=( elUniqueIndexVector&& ) = default;

    baseIter_t
    begin();
    baseIter_t
    end();

    baseIterConst_t
    begin() const;
    baseIterConst_t
    end() const;

    template < typename... Targs >
    element_t
    emplace_back( Targs&&... t );

    const T&
    operator[]( const index_t n ) const;
    T&
    operator[]( const index_t n );

    void
    erase( const index_t n );

    template < typename... Targs >
    bool
    insert( const index_t id, Targs&&... t );

    index_t
    size() const noexcept;

    bool
    empty() const noexcept;

    void
    clear();

    const indexVector_t&
    get_index_vector() const noexcept;

    const base_t&
    get_const_contents() const noexcept;
    std::span< T >
    get_contents() noexcept;

    bool
    has_index( const index_t n ) const noexcept;

    template < typename F >
    index_t
    find_if( const F& compare ) const noexcept;

  private:
    index_t
    GetIndexToID( const index_t n ) const noexcept;
    void
    DecrementIndex( const index_t n ) noexcept;

    template < typename... Targs >
    void
    emplace_at_position( const index_t id, Targs&&... t );

  private:
    elUniqueIDHandler< index_t > idHandler;
    base_t                       data;
    indexVector_t                indexToData;
    uint64_t                     modificationCounter = 0;

    struct cache_t
    {
        indexVector_t indexToData;
        uint64_t      modificationCounter = 0;
    };

    mutable cache_t cache;
};
} // namespace bb
} // namespace el3D
#include "buildingBlocks/elUniqueIndexVector.inl"
