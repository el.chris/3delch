#pragma once

namespace el3D
{
namespace bb
{
template < typename T, unsigned int N >
struct dimension_t;

template < typename T >
struct dimension_t< T, 1 >
{
    T width;
};

template < typename T >
struct dimension_t< T, 2 >
{
    T width;
    T height;
};

template < typename T >
struct dimension_t< T, 3 >
{
    T width;
    T height;
    T length;
};

} // namespace bb
} // namespace el3D
