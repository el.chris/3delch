#pragma once

#include <array>
#include <list>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

namespace el3D
{
namespace bb
{
namespace internal
{
template < typename >
std::false_type
isDereferencable( unsigned long );
template < typename T >
auto
isDereferencable( int ) -> decltype( *std::declval< T >(), std::true_type{} );
template < typename T >
auto
isDereferencable( long ) -> decltype( std::declval< T >()
                                          .
                                          operator->(),
                                      std::true_type{} );
} // namespace internal

template < typename >
struct GetMethodInfo;

template < typename Return, typename Class, typename... Args >
struct GetMethodInfo< Return ( Class::* )( Args... ) >
{
    using ReturnType                     = Return;
    using ClassType                      = Class;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename Return, typename Class, typename... Args >
struct GetMethodInfo< Return ( Class::* )( Args... ) const >
{
    using ReturnType                     = Return;
    using ClassType                      = Class;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename Return, typename Class, typename... Args >
struct GetMethodInfo< Return ( Class::* )( Args... ) const noexcept >
{
    using ReturnType                     = Return;
    using ClassType                      = Class;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename Return, typename Class, typename... Args >
struct GetMethodInfo< Return ( Class::* )( Args... ) noexcept >
{
    using ReturnType                     = Return;
    using ClassType                      = Class;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename >
struct GetStaticMethodInfo;

template < typename Return, typename... Args >
struct GetStaticMethodInfo< Return ( * )( Args... ) >
{
    using ReturnType                     = Return;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename Return, typename... Args >
struct GetStaticMethodInfo< Return ( * )( Args... ) noexcept >
{
    using ReturnType                     = Return;
    using ArgumentTuple                  = std::tuple< typename std::remove_cv<
        typename std::remove_reference< Args >::type >::type... >;
    static constexpr size_t ArgumentSize = sizeof...( Args );
};

template < typename >
struct GetMemberVariableInfo;

template < typename Class, typename Var >
struct GetMemberVariableInfo< Var Class::* >
{
    using ClassType = Class;
    using VarType   = Var;
};

template < typename T >
static constexpr bool isDereferencable =
    decltype( internal::isDereferencable< T >( 0 ) )::value;

template < typename... args >
struct NumberOfVariantArguments;
template < typename... args >
struct NumberOfVariantArguments< std::variant< args... > >
{
    static constexpr size_t value = sizeof...( args );
};


template < typename... T >
struct isVariant
{
    static const bool value = false;
};
template < typename... T >
struct isVariant< std::variant< T... > >
{
    static const bool value = true;
};

template < typename T >
struct isVector
{
    static const bool value = false;
};
template < typename T >
struct isVector< std::vector< T > >
{
    static const bool value = true;
};

template < typename T >
struct isSet
{
    static const bool value = false;
};
template < typename T >
struct isSet< std::set< T > >
{
    static const bool value = true;
};

template < typename T >
struct isMultiSet
{
    static const bool value = false;
};
template < typename T >
struct isMultiSet< std::multiset< T > >
{
    static const bool value = true;
};

template < typename T >
struct isUnorderedSet
{
    static const bool value = false;
};
template < typename T >
struct isUnorderedSet< std::unordered_set< T > >
{
    static const bool value = true;
};

template < typename T >
struct isUnorderedMultiSet
{
    static const bool value = false;
};
template < typename T >
struct isUnorderedMultiSet< std::unordered_multiset< T > >
{
    static const bool value = true;
};

template < typename T >
struct isList
{
    static const bool value = false;
};
template < typename T >
struct isList< std::list< T > >
{
    static const bool value = true;
};

template < typename T >
struct isArray
{
    static const bool value = false;
};
template < typename K, size_t V >
struct isArray< std::array< K, V > >
{
    static const bool value = true;
};

template < typename T >
struct isMap
{
    static const bool value = false;
};
template < typename K, typename V >
struct isMap< std::map< K, V > >
{
    static const bool value = true;
};

template < typename T >
struct isMultiMap
{
    static const bool value = false;
};
template < typename K, typename V >
struct isMultiMap< std::multimap< K, V > >
{
    static const bool value = true;
};

template < typename T >
struct isUnorderedMap
{
    static const bool value = false;
};
template < typename K, typename V >
struct isUnorderedMap< std::unordered_map< K, V > >
{
    static const bool value = true;
};

template < typename T >
struct isUnorderedMultiMap
{
    static const bool value = false;
};
template < typename K, typename V >
struct isUnorderedMultiMap< std::unordered_multimap< K, V > >
{
    static const bool value = true;
};
} // namespace bb
} // namespace el3D
