#pragma once

#include "HighGL/elCameraController_base.hpp"
#include "OpenGL/elCamera_Perspective.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elCameraController_Orbit : public elCameraController_base
{
  public:
    enum class Zoom : int64_t
    {
        In   = -1,
        Out  = 1,
        Stop = 0
    };

    struct settings_t
    {
        glm::vec3 center      = glm::vec3( 0.0f );
        glm::vec2 angle       = glm::vec2( 0.0f );
        float     distance    = 1.0f;
        float     sensitivity = 0.1f;
        float     velocity    = 0.25f;
    };

    elCameraController_Orbit(
        OpenGL::elCamera_Perspective&         camera,
        const std::function< units::Time() >& getDeltaTime,
        const settings_t&                     s ) noexcept;

    glm::vec3
    GetCenter() const noexcept;
    glm::vec2
    GetAngle() const noexcept;
    float
    GetDistance() const noexcept;

    void
    SetCenter( const glm::vec3& v ) noexcept;
    void
    SetAngle( const glm::vec2& v ) noexcept;
    void
    SetDistance( const float v ) noexcept;

    void
    SetZoom( const Zoom v ) noexcept;

  private:
    void
    CalculateState() noexcept override;

  private:
    settings_t settings;

    Zoom                           zoom = Zoom::Stop;
    std::function< units::Time() > getDeltaTime;
};
} // namespace HighGL
} // namespace el3D

