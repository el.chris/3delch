#pragma once

#include <cstdint>
#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elEventColor
{
  public:
    elEventColor() noexcept = default;
    elEventColor( const uint32_t& colorIndex ) noexcept;
    elEventColor( const glm::vec4& color ) noexcept;

    uint32_t
    GetIndex() const noexcept;
    glm::vec4
    GetRGBAColor() const noexcept;

    friend class elEventColorRange;

  private:
    static glm::vec4
    ConvertINT2RGBA( const uint32_t ) noexcept;
    static uint32_t
    ConvertRGBA2INT( const glm::vec4& ) noexcept;

  private:
    uint32_t  colorIndex = 0;
    glm::vec4 color      = glm::vec4{ 0.0f, 0.0f, 0.0f, 0.0f };
};


} // namespace HighGL
} // namespace el3D
