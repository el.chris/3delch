#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>

namespace el3D
{
namespace HighGL
{
class elBoundingBoxGenerator
{
  public:
    struct objectGeometry_t
    {
        std::vector< GLfloat > cornerLines;
        std::vector< GLfloat > borderLines;
    };

    elBoundingBoxGenerator(
        const std::vector< float >& vertices,
        const float                 relativeCornerLineLength = 0.2f ) noexcept;

    const objectGeometry_t&
    GetGeometry() const noexcept;

  private:
    void
    SetMinMax( const std::vector< float >& vertices ) noexcept;
    void
    SetBorderLines() noexcept;
    void
    AddLine( std::vector< GLfloat >& lineVertices, const glm::vec3& start,
             const glm::vec3& end ) noexcept;

  private:
    float relativeCornerLineLength = 0.25f;
    float absoluteCornerLineLength = 0.0f;

    objectGeometry_t geometry;
    glm::vec3        min{ 0.0, 0.0, 0.0 };
    glm::vec3        max{ 0.0, 0.0, 0.0 };
};
} // namespace HighGL
} // namespace el3D
