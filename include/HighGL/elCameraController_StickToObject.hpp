#pragma once

#include "HighGL/elCameraController_base.hpp"
#include "OpenGL/elCamera_Perspective.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elCameraController_StickToObject : public elCameraController_base
{
  public:
    template < typename T >
    elCameraController_StickToObject( OpenGL::elCamera_Perspective& camera,
                                      T&                            object,
                                      const glm::vec3& positionOffset,
                                      const glm::vec3& lookAtOffset ) noexcept;
    elCameraController_StickToObject(
        OpenGL::elCamera_Perspective&       camera,
        const std::function< glm::vec3() >& getPosition,
        const std::function< glm::quat() >& getRotationQuaternion,
        const glm::vec3&                    positionOffset,
        const glm::vec3&                    lookAtOffset ) noexcept;

    glm::vec3
    GetPositionOffset() const noexcept;
    glm::vec3
    GetLookAtOffset() const noexcept;

    void
    SetPositionOffset( const glm::vec3& v ) noexcept;
    void
    SetLookAtOffset( const glm::vec3& v ) noexcept;

  private:
    void
    CalculateState() noexcept override;

  private:
    glm::vec3 positionOffset = { 0.0f, 0.0f, 0.0f };
    glm::vec3 lookAtOffset   = { 0.0f, 0.0f, 0.0f };

    std::function< glm::vec3() > getPosition;
    std::function< glm::quat() > getRotationQuaternion;
};
} // namespace HighGL
} // namespace el3D

#include "HighGL/elCameraController_StickToObject.inl"
