#pragma once

#include "HighGL/elLight_base.hpp"
#include "OpenGL/elBufferObject.hpp"
#include "OpenGL/elCamera_Orthographic.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elVertexArrayObject.hpp"
#include "animation/elAnimatable.hpp"

namespace el3D
{
namespace HighGL
{
class elLight_DirectionalLight : public elLight_base
{
  public:
    elLight_DirectionalLight();
    elLight_DirectionalLight( const glm::vec3 &color,
                              const glm::vec3 &direction,
                              const GLfloat    diffuseIntensity,
                              const GLfloat    ambientIntensity );
    elLight_DirectionalLight( const elLight_DirectionalLight & ) = delete;
    elLight_DirectionalLight( elLight_DirectionalLight && )      = default;
    virtual ~elLight_DirectionalLight(){};

    elLight_DirectionalLight &
    operator=( const elLight_DirectionalLight & ) = delete;
    elLight_DirectionalLight &
    operator=( elLight_DirectionalLight && ) = default;


    static void
    Bind( const OpenGL::elShaderProgram::shaderAttribute_t &a );
    static void
    Unbind( const OpenGL::elShaderProgram::shaderAttribute_t &a );

    void
    SetDirection( const glm::vec3 &v );
    glm::vec3
    GetDirection() const noexcept;
    void
    UpdateCameraParameters( const glm::vec3 &position,
                            const float      zFar ) noexcept;

    OpenGL::elCamera_Orthographic
    GetOrthographicCamera() const noexcept;

    /// @brief Defines the scaling of camera frustum in relation to zFar
    void
    SetFrustumRangeScaling( const float v ) noexcept;

  private:
    void
    UpdateGeometricObjectPosition() noexcept override;

    OpenGL::elCamera_Orthographic::frustum_t
    GetFrustum() const noexcept;

    OpenGL::elCamera::zFactor_t
    GetZFactor() const noexcept;

    OpenGL::elCamera_Orthographic::lookAt_t
    GetLookAt() const noexcept;


  private:
    glm::vec3 direction;
    float     zFar                = 100.0f;
    float     frustumRangeScaling = 1.0f / 2.0f;
    glm::vec3 position            = glm::vec3{ 0.0f };
};
} // namespace HighGL
} // namespace el3D
