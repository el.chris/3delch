#pragma once

#include "utils/byteStream_t.hpp"

namespace el3D
{
namespace HighGL
{
struct texture_t
{
    uint64_t            width  = 0;
    uint64_t            height = 0;
    utils::byteStream_t data;
};

enum class TextureFormat : uint64_t
{
    RGB  = 3,
    RGBA = 4
};

texture_t
ConvertHeightMapToNormalMap(
    const uint64_t x, const uint64_t y, const utils::byteStream_t& heightmap,
    const float         scale     = 1.0f,
    const TextureFormat outFormat = TextureFormat::RGBA,
    const TextureFormat inFormat  = TextureFormat::RGBA ) noexcept;

texture_t
ConvertHeightMapToNormalMap(
    const std::string& imageFile, const float scale = 1.0f,
    const TextureFormat outFormat = TextureFormat::RGBA,
    const TextureFormat inFormat  = TextureFormat::RGBA ) noexcept;
} // namespace HighGL
} // namespace el3D
