#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <vector>

namespace el3D
{
namespace HighGL
{
std::vector< GLuint >
EliminateCloseVerticesInElements( const std::vector< GLfloat > &vertices,
                                  const std::vector< GLuint > & elements,
                                  const GLfloat minimumDistance ) noexcept;

std::vector< GLuint >
GenerateAdjacencyList( const std::vector< GLuint > &elements ) noexcept;

} // namespace HighGL
} // namespace el3D
