#pragma once

#include "HighGL/elCameraController_base.hpp"
#include "OpenGL/elCamera_Perspective.hpp"
#include "units/Time.hpp"

namespace el3D
{
namespace HighGL
{
class elCameraController_FreeFly : public elCameraController_base
{
  public:
    struct settings_t
    {
        glm::vec3 movementDirection = glm::vec3( 0.0f );
        glm::vec3 rotationDirection = glm::vec3( 0.0f );
        float     sensitivity       = 0.1f;
        float     velocity          = 0.25f;
    };

    elCameraController_FreeFly(
        OpenGL::elCamera_Perspective&         camera,
        const std::function< units::Time() >& getDeltaTime,
        const settings_t&                     s ) noexcept;

    const settings_t&
    GetSettings() const noexcept;

    void
    SetRotationDirection( const glm::vec3& v ) noexcept;
    void
    SetRotationStep( const glm::vec3& v ) noexcept;
    void
    SetMovementDirection( const glm::vec3& v ) noexcept;
    void
    SetSensitivity( const float v ) noexcept;
    void
    SetVelocity( const float v ) noexcept;

  private:
    void
    CalculateState() noexcept override;

  private:
    settings_t                     settings;
    glm::vec3                      rotation     = glm::vec3( 0.0f );
    glm::vec3                      rotationStep = glm::vec3( 0.0f );
    glm::vec3                      right        = glm::vec3( 1.0f );
    std::function< units::Time() > getDeltaTime;
};
} // namespace HighGL
} // namespace el3D
