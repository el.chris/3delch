#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>

namespace el3D
{
namespace OpenGL
{
class elObjectRegistry
{
  public:
    struct texture_t
    {
        bool
        operator==( const texture_t& rhs ) const noexcept;

        std::string identifier;
        GLuint      texture;
        GLenum      target;
    };
    using textureRegistry_t = std::vector< texture_t >;

    ~elObjectRegistry();

    static elObjectRegistry*
    GetInstance();

    uint64_t
    GetLastUpdateToken() const noexcept;
    void
    RegisterTexture( const std::string& identifier, const GLuint texture,
                     const GLenum target );
    void
    UnregisterTexture( const GLuint texture );
    textureRegistry_t
    GetRegisteredTextures() const;

  private:
    elObjectRegistry();

  private:
    textureRegistry_t textureRegistry;
    uint64_t          lastUpdateToken{ 0u };
};
} // namespace OpenGL
} // namespace el3D
