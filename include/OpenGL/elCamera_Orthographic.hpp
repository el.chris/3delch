#pragma once

#include "OpenGL/elCamera.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace el3D
{
namespace OpenGL
{
class elCamera_Orthographic : public elCamera
{
  public:
    struct frustum_t
    {
        float left   = 0.0f;
        float right  = 0.0f;
        float top    = 0.0f;
        float bottom = 0.0f;
    };

    struct lookAt_t
    {
        glm::vec3 eye    = glm::vec3( 0.0 );
        glm::vec3 center = glm::vec3( 0.0 );
        glm::vec3 up     = glm::vec3( 0.0 );
    };

    elCamera_Orthographic() noexcept;
    elCamera_Orthographic( const frustum_t& frustum, const zFactor_t& zFactor,
                           const lookAt_t& lookAt ) noexcept;
    elCamera_Orthographic( const elCamera_Orthographic& rhs ) = default;
    elCamera_Orthographic( elCamera_Orthographic&& rhs )      = default;
    ~elCamera_Orthographic()                                  = default;

    elCamera_Orthographic&
    operator=( const elCamera_Orthographic& rhs ) = default;
    elCamera_Orthographic&
    operator=( elCamera_Orthographic&& rhs ) = default;

    void
    SetLookAt( const lookAt_t& value ) noexcept;
    void
    SetFrustum( const frustum_t& value ) noexcept;

  private:
    void
    Update() const noexcept override;

  private:
    frustum_t    frustum;
    lookAt_t     lookAt;
    mutable bool hasUpdate{ true };
};
} // namespace OpenGL
} // namespace el3D
