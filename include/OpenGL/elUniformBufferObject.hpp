#pragma once

#include "DesignPattern/Creation.hpp"
#include "OpenGL/elBufferObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "utils/elGenericRAII.hpp"

namespace el3D
{
namespace OpenGL
{
enum class elUniformBufferObject_Error
{
    UnableToFindUniformBlockIndex,
    UnableToFindUniformIndex
};

class elUniformBufferObject
    : public elBufferObject,
      public DesignPattern::Creation< elUniformBufferObject,
                                      elUniformBufferObject_Error >
{
  public:
    elUniformBufferObject( const elUniformBufferObject & ) = delete;
    elUniformBufferObject( elUniformBufferObject && )      = default;
    ~elUniformBufferObject();

    elUniformBufferObject &
    operator=( elUniformBufferObject && ) = default;
    elUniformBufferObject &
    operator=( const elUniformBufferObject & ) = delete;

    void
    BindToShader( const size_t shaderId = 0,
                  const size_t n        = 0 ) const noexcept;
    void
    UnbindFromShader( const size_t shaderId = 0,
                      const size_t n        = 0 ) const noexcept;
    void
    SetVariableData( const size_t arrayIndex, const size_t varIndex,
                     const void *varData, const size_t dataSize );

    bb::elExpected< size_t, elUniformBufferObject_Error >
    AddShader( const elShaderProgram *const ) noexcept;
    void
    RemoveShader( const size_t shaderId ) noexcept;

    friend struct DesignPattern::Creation< elUniformBufferObject,
                                           elUniformBufferObject_Error >;

  private:
    using elBufferObject::Bind;
    using elBufferObject::Unbind;

    elUniformBufferObject( const std::string &               name,
                           const std::vector< std::string > &varNames,
                           const elShaderProgram *           shader );

  private:
    std::string            name;
    size_t                 bufferIndex{ 1 };
    GLint                  size;
    std::vector< GLubyte > data;

    std::vector< GLint >              varOffsets;
    std::vector< GLint >              varStride;
    bb::elUniqueIndexVector< GLuint > indices;
};
} // namespace OpenGL
} // namespace el3D
