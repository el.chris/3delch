#pragma once

#include "OpenGL/elBufferObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elVertexArrayObject.hpp"

#include <initializer_list>

namespace el3D
{
namespace OpenGL
{
class elObjectBuffer
{
  public:
    elObjectBuffer( const uint64_t dimension, const uint64_t numberOfBuffers,
                    const uint64_t elementBufferNumber ) noexcept;

    /// @brief Targs... two combined parameters:
    ///     first parameter unsigned int = data dimension
    ///     second parameter std::vector<T> = data
    /// @code
    ///   elObjectBuffer(3, myVertices, myElements, 2u, textureCoordinates, 3u,
    ///   normals);
    /// @endcode
    template < typename... Targs >
    elObjectBuffer( const uint64_t                dimension,
                    const std::vector< GLfloat > &vertices,
                    const std::vector< GLuint > & elements,
                    const Targs &...buffer ) noexcept;

    elObjectBuffer( const elObjectBuffer &rhs ) = delete;
    elObjectBuffer( elObjectBuffer &&rhs )      = default;

    elObjectBuffer &
    operator=( const elObjectBuffer &rhs ) = delete;
    elObjectBuffer &
    operator=( elObjectBuffer &&rhs ) = default;
    ~elObjectBuffer()                 = default;

    uint64_t
    GetNumberOfElements() const noexcept;
    void
    SetNumberOfElements( const uint64_t v ) noexcept;

    uint64_t
    GetNumberOfVertices() const noexcept;

    void
    Bind() const noexcept;

    void
    Enable(
        const std::vector< size_t > &             buffer,
        const elShaderProgram::shaderAttribute_t &attribute ) const noexcept;

    void
    Disable( const std::vector< elShaderProgram::shaderAttribute_t >
                 &attribute ) const noexcept;

    elBufferObject &
    GetBuffer() noexcept;
    const elBufferObject &
    GetBuffer() const noexcept;

    template < typename T >
    void
    UpdateBuffer( const size_t buffer, const std::vector< T > &data ) noexcept;
    template < typename T >
    void
    UpdateSubBuffer( const size_t buffer, const std::vector< T > &data,
                     const uint64_t elementOffset ) noexcept;

  private:
    void
    InitialUpdateBuffer( const size_t ) noexcept;

    template < typename T >
    void
    InitialUpdateBuffer( const size_t buffer, const uint64_t dataDimension,
                         const std::vector< T > &data ) noexcept;


    template < typename T, typename... Targs >
    void
    InitialUpdateBuffer( const size_t buffer, const uint64_t dataDimension,
                         const std::vector< T > &data,
                         const Targs &...dataList ) noexcept;
    void
    UpdateVertexBuffer( const std::vector< GLfloat > &vertices ) noexcept;

  private:
    elVertexArrayObject     vao;
    elBufferObject          vbo;
    uint64_t                numberOfElements{ 0 };
    uint64_t                dimension{ 3 };
    uint64_t                numberOfVertices{ 0 };
    std::vector< uint64_t > bufferDimension;
}; // namespace OpenGL
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elObjectBuffer.inl"
