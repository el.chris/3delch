#pragma once

#include "animation/Types.hpp"
#include "animation/elAnimatable.hpp"
#include "animation/elAnimate_SourceDestination.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace el3D
{
namespace OpenGL
{
template < typename Child, template < typename > class Interpolation >
class elModelMatrix : public animation::elAnimatable< Child >
{
  public:
    elModelMatrix() noexcept = default;
    virtual ~elModelMatrix() = default;

    virtual void
    SetPosition( const glm::vec3& ) noexcept = 0;
    virtual void
    SetScaling( const glm::vec3& ) noexcept = 0;
    virtual void
    SetRotation( const glm::vec4& rotation ) noexcept = 0;
    virtual void
    SetModelMatrix( const glm::mat4& matrix ) noexcept = 0;

    virtual glm::vec3
    GetPosition() const noexcept = 0;
    virtual glm::vec3
    GetScaling() const noexcept = 0;
    virtual glm::vec3
    GetRotationAxis() const noexcept = 0;
    virtual float
    GetRotationDegree() const noexcept = 0;

    glm::quat
    GetRotation() const noexcept;


    virtual void
    MoveTo( const glm::vec3& destination, const animation::velocity_t velocity,
            const animation::AnimationVelocity velocityType =
                animation::AnimationVelocity::Absolute,
            const animation::onArrivalCallback_t& onArrivalCallback =
                animation::onArrivalCallback_t() ) noexcept;
    virtual void
    ResizeTo( const glm::vec3& size, const animation::velocity_t velocity,
              const animation::AnimationVelocity velocityType =
                  animation::AnimationVelocity::Absolute,
              const animation::onArrivalCallback_t& onArrivalCallback =
                  animation::onArrivalCallback_t() ) noexcept;
    virtual void
    RotateTo( const glm::vec4& rotation, const animation::velocity_t velocity,
              const animation::AnimationVelocity velocityType =
                  animation::AnimationVelocity::Absolute,
              const animation::onArrivalCallback_t& onArrivalCallback =
                  animation::onArrivalCallback_t() ) noexcept;

    virtual void
    Move( const glm::vec3&            direction,
          const animation::velocity_t velocity ) noexcept;
    virtual void
    Resize( const glm::vec3&            direction,
            const animation::velocity_t velocity ) noexcept;
    virtual void
    Rotate( const glm::vec3&            axis,
            const animation::velocity_t velocity ) noexcept;

    virtual void
    StopMoving() noexcept;
    virtual void
    StopResizing() noexcept;
    virtual void
    StopRotating() noexcept;
};
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elModelMatrix.inl"
