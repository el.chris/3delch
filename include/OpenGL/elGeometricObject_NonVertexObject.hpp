#pragma once
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace el3D
{
namespace OpenGL
{
class elGeometricObject_NonVertexObject : public elGeometricObject_base,
                                          public elGeometricObject_ModelMatrix
{
  public:
    enum buffer_t
    {
        BUFFER_VERTEX = 0,
        BUFFER_ELEMENT,
        BUFFER_COLOR,
        BUFFER_END
    };

    static constexpr char MVP_ATTRIBUTE_NAME[] = "mvp";

    elGeometricObject_NonVertexObject( const DrawMode                drawMode,
                                       const std::vector< GLfloat > &vertices );
    elGeometricObject_NonVertexObject(
        const elGeometricObject_NonVertexObject & ) = delete;
    elGeometricObject_NonVertexObject( elGeometricObject_NonVertexObject && ) =
        default;
    ~elGeometricObject_NonVertexObject() = default;

    elGeometricObject_NonVertexObject &
    operator=( const elGeometricObject_NonVertexObject & ) = delete;
    elGeometricObject_NonVertexObject &
    operator=( elGeometricObject_NonVertexObject && ) = default;

    void
    SetDrawSize( const float size ) noexcept;
    uint64_t
    GetNumberOfVertices() const noexcept;

  protected:
    virtual void
    DrawImplementation( const uint64_t  shaderID,
                        const elCamera &camera ) const noexcept override;

  private:
    virtual bb::elExpected< size_t, elShaderProgram_Error >
    AttachShader( const uint64_t shaderId ) noexcept override;

    void
    Bind( const size_t     shaderID,
          const glm::mat4 &viewProjection ) const noexcept;
    void
    Unbind( const size_t shaderID ) const noexcept;
    void
    UpdateDrawSize() const noexcept;

  private:
    enum Uniform
    {
        UNIFORM_MVP = 0,
    };

    static const std::vector< elShaderProgram::shaderAttribute_t > ATTRIBUTES;

    mutable glm::mat4 vp               = glm::mat4( 1.0 );
    uint64_t          numberOfVertices = 0u;
    GLfloat           drawSize         = 1.0f;
};
} // namespace OpenGL
} // namespace el3D
