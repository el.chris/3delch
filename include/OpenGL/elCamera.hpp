#pragma once

#include "units/Length.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace OpenGL
{
enum class elCameraType
{
    base,
    Perspective,
    Orthographic
};

class elCamera
{
  public:
    struct matrix_t
    {
        glm::mat4 projection     = glm::mat4( 1.0 );
        glm::mat4 view           = glm::mat4( 1.0 );
        glm::mat4 viewProjection = glm::mat4( 1.0 );
    };

    struct zFactor_t
    {
        units::Length zNear = units::Length::Meter( 0.1f );
        units::Length zFar  = units::Length::Meter( 100.0f );
    };

    virtual ~elCamera() = default;

    const matrix_t&
    GetMatrices() const noexcept;

    elCameraType
    GetType() const noexcept;

    zFactor_t
    GetZFactor() const noexcept;

    void
    SetZFactor( const units::Length zNear, const units::Length zFar ) noexcept;

    bool
    HasUpdatedState() const noexcept;

  protected:
    elCamera( const elCameraType type ) noexcept;
    virtual void
    Update() const noexcept = 0;

  protected:
    mutable matrix_t matrix;
    zFactor_t        zFactor;
    mutable bool     hasUpdate           = true;
    bool             hasContinuousUpdate = false;

  private:
    elCameraType type = elCameraType::base;
};

} // namespace OpenGL
} // namespace el3D
