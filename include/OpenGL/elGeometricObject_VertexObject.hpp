#pragma once
#include "OpenGL/elBufferObject.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <initializer_list>

namespace el3D
{
namespace HighGL
{
class elLight_DirectionalLight;
class elLight_PointLight;
} // namespace HighGL

namespace OpenGL
{
class elGeometricObject_VertexObject : public elGeometricObject_base,
                                       public elGeometricObject_ModelMatrix
{
  public:
    enum buffer_t : uint64_t
    {
        BUFFER_VERTEX         = 0U,
        BUFFER_ELEMENT        = 1U,
        BUFFER_TEXTURE_COORD  = 2U,
        BUFFER_NORMAL         = 3U,
        BUFFER_TANGENTS       = 4U,
        BUFFER_BITANGENTS     = 5U,
        BUFFER_DIFFUSE        = 6U,
        BUFFER_EVENT_COLOR    = 7U,
        BUFFER_EMISSION_COLOR = 8U,
        BUFFER_END
    };

    static constexpr char MODEL_MATRIX_ATTRIBUTE_NAME[]    = "modelMatrix";
    static constexpr char VIEW_PROJECTION_ATTRIBUTE_NAME[] = "viewProjection";

    elGeometricObject_VertexObject(
        const DrawMode drawMode, const OpenGL::objectGeometry_t &objectGeometry,
        const std::vector< GLfloat > &diffuse       = {},
        const std::vector< GLfloat > &eventColor    = {},
        const std::vector< GLfloat > &emissionColor = {} );
    elGeometricObject_VertexObject( const elGeometricObject_VertexObject & ) =
        delete;
    elGeometricObject_VertexObject( elGeometricObject_VertexObject && ) =
        default;
    ~elGeometricObject_VertexObject() = default;

    elGeometricObject_VertexObject &
    operator=( const elGeometricObject_VertexObject & ) = delete;
    elGeometricObject_VertexObject &
    operator=( elGeometricObject_VertexObject && ) = default;

  protected:
    virtual void
    DrawImplementation( const uint64_t  shaderID,
                        const elCamera &camera ) const noexcept override;

    friend class HighGL::elLight_PointLight;

  private:
    size_t
    GetNumberOfElements() const noexcept;
    void
    Bind( const size_t     shaderID,
          const glm::mat4 &viewProjection ) const noexcept;
    void
    Unbind( const size_t shaderID ) const noexcept;
    virtual bb::elExpected< size_t, elShaderProgram_Error >
    AttachShader( const uint64_t shaderId ) noexcept override;

  private:
    enum Uniform
    {
        UNIFORM_MODEL_MATRIX    = 0,
        UNIFORM_VIEW_PROJECTION = 1,
    };

    static const std::vector< elShaderProgram::shaderAttribute_t > ATTRIBUTES;

    mutable glm::mat4 vp = glm::mat4( 1.0 );
};
} // namespace OpenGL
} // namespace el3D
