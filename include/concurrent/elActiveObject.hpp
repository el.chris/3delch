#pragma once

#include "concurrent/elThreadPool.hpp"

#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <optional>

namespace el3D
{
namespace concurrent
{

/// @brief to use it thread safe please add something like this in the
///         destructor of every class in which activeObject is a member of.
/// @code
///   this->activeObject.AllowToAddNewTasks( false );
///   this->activeObject.WaitForTasksToFinish();
/// @endcode
class elActiveObject
{
  public:
    elActiveObject( elThreadPool& threadPool ) noexcept;
    ~elActiveObject();

    void
    AddTask( const std::function< void() >& f ) noexcept;
    template < typename T >
    std::optional< std::future< T > >
    AddTaskWithReturn( const std::function< T() >& f ) noexcept;

    void
    AllowToAddNewTasks( const bool ) noexcept;
    void
    WaitForTasksToFinish() noexcept;

  private:
    elThreadPool&           threadPool;
    bool                    addingTasksIsAllowed{true};
    uint64_t                numberOfActiveTasks{0};
    std::mutex              mtx;
    std::condition_variable waitForTasksToFinish;
};
} // namespace concurrent
} // namespace el3D

#include "concurrent/elActiveObject.inl"
