#pragma once

#include "concurrent/elActiveObject.hpp"
#include "concurrent/elSmartLock.hpp"
#include "concurrent/elTrigger.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "network/etm/Transmission_t.hpp"
#include "network/etm/etm.hpp"

#include <cstdint>
#include <memory>
#include <string>


namespace el3D
{
namespace network
{
class elEasyTCPMessaging_Server
{
  public:
    struct receivedMessage_t
    {
        size_t              connectionID;
        utils::byteStream_t message;
    };

    using asyncConnectionCallback_t = std::function< void(
        concurrent::elSmartLock< elNetworkSocket_Server > *,
        socketHandleType_t ) >;

    elEasyTCPMessaging_Server( const std::string &serviceID,
                               const uint32_t     serviceVersion ) noexcept;
    elEasyTCPMessaging_Server( const elEasyTCPMessaging_Server & ) = delete;
    elEasyTCPMessaging_Server( elEasyTCPMessaging_Server && )      = delete;
    ~elEasyTCPMessaging_Server()                                   = default;

    elEasyTCPMessaging_Server &
    operator=( const elEasyTCPMessaging_Server &rhs ) = delete;
    elEasyTCPMessaging_Server &
    operator=( elEasyTCPMessaging_Server &&rhs ) = delete;

    bb::elExpected< elNetworkSocket_Error >
    Listen( const uint16_t port ) noexcept;
    void
    StopListen() noexcept;

    units::Time
    GetTimeout() const noexcept;
    void
    SetTimeout( const units::Time &timout ) noexcept;

    template < typename T >
    void
    SendBroadcastMessage( const uint64_t         transmissionID,
                          const etm::MessageType messageType,
                          const T &              payload ) noexcept;
    template < typename T >
    bool
    SendMessage( const uint64_t         transmissionID,
                 const etm::MessageType messageType, const T &payload,
                 const size_t connectionID ) noexcept;

    std::vector< receivedMessage_t >
    TryReceiveMessages() noexcept;
    std::vector< receivedMessage_t >
    BlockingReceiveMessages() noexcept;


    std::vector< size_t >
    RefreshAndReturnConnections() noexcept;
    void
    Disconnect( const size_t connectionID ) noexcept;

    void
    SetAsynchronousConnectionCallback(
        const asyncConnectionCallback_t & ) noexcept;

  private:
    struct connection_t;

    void
    ManagementConnectionCallback(
        const network::socketHandleType_t handle ) noexcept;
    void
    ManagementPortThread( const network::socketHandleType_t handle ) noexcept;
    void
    HandleManagementRequests( const network::socketHandleType_t handle,
                              const uint32_t                    requestID,
                              const utils::byteStream_t &       byteStream,
                              const uint64_t transmissionID ) noexcept;
    bool
    SendError( const network::socketHandleType_t handle,
               const uint64_t                    transmissionID ) noexcept;
    bool
    SendInternalMessage( const network::socketHandleType_t handle,
                         const utils::byteStream_t &       message,
                         const std::string &messageType ) noexcept;
    bool
    SendRawMessage( const utils::byteStream_t &message,
                    const size_t               connectionID ) noexcept;
    void
    RespondToIdentifyRequest( const network::socketHandleType_t handle,
                              const utils::byteStream_t &       byteStream,
                              const uint64_t transmissionID ) noexcept;
    void
    RespondToConnectRequest( const network::socketHandleType_t handle,
                             const utils::byteStream_t &       byteStream,
                             const uint64_t transmissionID ) noexcept;
    std::optional< uint16_t >
    OpenPortForIncomingConnection() noexcept;
    void
    ServiceConnectionCallback( const network::socketHandleType_t handle,
                               const size_t connectionID ) noexcept;
    void
    CloseConnectionAfterTimeoutThread( connection_t *const c,
                                       const size_t        connectionID,
                                       const uint16_t      port ) noexcept;

  private:
    struct connection_t
    {
        concurrent::elSmartLock< elNetworkSocket_Server > connection;
        bool                                              hasConnection = false;
        std::condition_variable                           isConnectedTrigger;
        std::mutex                                        predicateMutex;
        network::socketHandleType_t                       handle;
    };

    // the member order is extremely important, since the backgroundTasks
    // are working on some members (server) when the dtor is called. remember
    // the dtors are called backwards

    // connectionVector has a constant order since there are no erases, an
    // erase is performed with freeConnectionIDs and stopListening
    std::vector< std::unique_ptr< connection_t > >  connectionVector;
    concurrent::elSmartLock< bb::elFiFo< size_t > > freeConnectionIDs;
    concurrent::elTrigger< uint64_t >               messageReceivedTrigger;
    elNetworkSocket_Server                          server;
    concurrent::elActiveObject                      activeObject;
    asyncConnectionCallback_t                       asyncConnectionCallback;

    units::Time timeout{ units::Time::Seconds( 2 ) };
    std::string serviceID{ "" };
    uint32_t    serviceVersion{ 0 };
};
} // namespace network
} // namespace el3D

#include "network/elEasyTCPMessaging_Server.inl"
