#pragma once

#include "format.hpp"
#include "logging/common.hpp"
#include "threading.hpp"

#include <cstdio>
#include <fstream>

namespace el3D
{
namespace logging
{
/**
 * @brief Containes all log output methods which can be set
 *        via elLog::AddOutput(**)
 */
namespace output
{
using format_t = std::function< std::string(
    const std::string &, const logLevel_t, const std::string &,
    const std::string &, const std::string &, const std::string & ) >;

/**
 * @brief If you want to write a new output method your new class
 *        needs to be a child of Base
 *
 *        The standard log level is logging::INFO and the standard formatter
 *        is logging::format::Default.
 */
class Base
{
  public:
    /**
     * @brief This method formats all arguments to gain a single log
     *        message which is then printed with Print() if the logLevel
     *        is equal or higher.
     *
     * @param[in] channelName
     * @param[in] logLevel
     * @param[in] file
     * @param[in] line
     * @param[in] func
     * @param[in] msg
     */
    void
    Out( const std::string &channelName, const logLevel_t logLevel,
         const std::string &file, const std::string &line,
         const std::string &func, const std::string &msg ) noexcept;

    /**
     * @brief Sets the loglevel of the output.
     *
     * @param[in] logLevel
     */
    void
    SetLogLevel( const logLevel_t logLevel ) noexcept;

    /**
     * @brief Sets the formatting of the generated output.
     *        See logging::format::**
     *
     * @param[in] format_t
     */
    void
    SetFormatting( const format_t & ) noexcept;

  protected:
    virtual void
    Print( const std::string &msg ) noexcept = 0;

    format_t formatMessage = format::Default;

  private:
    logLevel_t logLevel = INFO;
};

/**
 * @brief Log output to text console
 *
 * @tparam Sets the thread policy, default is threading::Disable
 *        For more information see logging::threading::**
 */
template < typename THREAD_POLICY = threading::Disable >
class ToTerminal : public Base, public THREAD_POLICY
{
  private:
    void
    Print( const std::string &msg ) noexcept override;
};

class Exception_FileOpen;
/**
 * @brief Log output to file
 *
 * @tparam Sets the thread policy, default is threading::Disable
 *        For more information see logging::threading::**
 */
template < typename THREAD_POLICY = threading::Disable >
class ToFile : public Base, public THREAD_POLICY
{
  public:
    ToFile() = delete;
    /**
     * @brief The log file will be open on construction and closed on
     *        destruction. If the file cannot be opened a
     *        logging::output::Exception_FileOpen is thrown.
     *
     * @param fileName
     */
    ToFile( const std::string &fileName );
    ToFile( const ToFile & ) = delete;
    ToFile( ToFile && )      = default;
    ~ToFile();
    ToFile &
    operator=( const ToFile & ) = delete;
    ToFile &
    operator=( ToFile && ) = default;

  private:
    void
    Print( const std::string &msg ) noexcept override;

    const std::string fileName;
    std::ofstream     file;
};
} // namespace output
} // namespace logging
} // namespace el3D

#include "logging/policies/output.inl"
