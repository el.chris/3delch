#pragma once

#include <type_traits>

namespace el3D
{
namespace animation
{
namespace interpolation
{
using factor_t                          = float;
static constexpr factor_t ERROR_EPSILON = static_cast< factor_t >( 0.001 );

template < typename T >
class Linear
{
  public:
    T
    Interpolate( const factor_t factor, const T& source,
                 const T& destination ) noexcept;
};

template < typename T >
class Hermite
{
  public:
    T
    Interpolate( const factor_t factor, const T& source,
                 const T& destination ) noexcept;
};

template < typename T, template < typename > class Interpolation >
static constexpr bool IsInterpolationType =
    std::is_same_v< Interpolation< T >, Linear< T > > ||
    std::is_same_v< Interpolation< T >, Hermite< T > >;

} // namespace interpolation
} // namespace animation
} // namespace el3D

#include "animation/Interpolation.inl"
