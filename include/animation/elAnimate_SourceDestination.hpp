#pragma once

#include "animation/Interpolation.hpp"
#include "animation/Types.hpp"
#include "animation/elAnimate_base.hpp"
#include "math/math.hpp"

namespace el3D
{
namespace animation
{
template < typename T,
           template < typename > class Interpolation = interpolation::Hermite >
class elAnimate_SourceDestination : public elAnimate_base,
                                    protected Interpolation< T >
{
  public:
    static_assert( interpolation::IsInterpolationType< T, Interpolation >,
                   "Interpolation must be an interpolation type" );

    elAnimate_SourceDestination() = default;

    elAnimate_SourceDestination&
    SetVelocity( const velocity_t ) noexcept;
    elAnimate_SourceDestination&
    SetVelocityType( const AnimationVelocity ) noexcept;
    elAnimate_SourceDestination&
    SetSource( const T& ) noexcept;
    elAnimate_SourceDestination&
    SetDestination( const T& ) noexcept;
    elAnimate_SourceDestination&
    SetDirection( const T& ) noexcept;
    elAnimate_SourceDestination&
    JumpTo( const T& ) noexcept;
    elAnimate_SourceDestination&
    SetLoop( const bool ) noexcept;

    T
    GetSource() const noexcept;
    bool
    HasDestination() const noexcept;
    bool
    HasDirection() const noexcept;
    T
    GetDestination() const noexcept;
    T
    GetDirection() const noexcept;
    T
    GetCurrentPosition() const noexcept;
    velocity_t
    GetVelocity() const noexcept;
    bool
    HasLoop() const noexcept;

  protected:
    void
    PerformUpdate() noexcept override;
    void
    Set() noexcept override;
    void
    Reset() noexcept override;

    enum class PathType
    {
        Destination,
        Direction,
        Undefined,
    };


  private:
    AnimationVelocity velocityType = AnimationVelocity::Relative;
    PathType          pathType     = PathType::Undefined;

    velocity_t              velocity      = 0.0f;
    interpolation::factor_t currentFactor = 0.0f;
    bool                    hasLoop       = false;

    T source;
    T currentPosition;
    T destination;
    T direction;
};
} // namespace animation
} // namespace el3D

#include "animation/elAnimate_SourceDestination.inl"
