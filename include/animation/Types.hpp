#pragma once

#include "buildingBlocks/type_traits_extended.hpp"

#include <functional>
#include <glm/glm.hpp>

namespace el3D
{
namespace units
{
class Time;
}
namespace animation
{
class elAnimate_base;

using getTimeCallback_t   = std::function< units::Time() >;
using onUpdateCallback_t  = std::function< void() >;
using onArrivalCallback_t = std::function< void() >;

enum class AnimationVelocity
{
    Relative,
    Absolute
};

using velocity_t = float;

using animationSetCallback_t = std::function< void( const void* const ) >;

template < typename T >
using ArgumentType = typename std::tuple_element<
    0, typename bb::GetMethodInfo< T >::ArgumentTuple >::type;

template < typename T >
static constexpr bool HasOneArgument =
    ( std::tuple_size_v< typename bb::GetMethodInfo< T >::ArgumentTuple > ==
      1 );

template < typename T >
static constexpr bool HasReturnTypeVoid =
    std::is_same_v< typename bb::GetMethodInfo< T >::ReturnType, void >;

} // namespace animation
} // namespace el3D
