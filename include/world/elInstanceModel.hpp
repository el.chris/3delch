#pragma once

#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "utils/byteStream_t.hpp"
#include "world/elObject.hpp"

#include <cstdint>

namespace el3D
{
namespace world
{
class elInstanceModel : public elObject
{
  public:
    elInstanceModel( const objectSettings_t          settings,
                     const OpenGL::objectGeometry_t& objectGeometry ) noexcept;

    void
    SetGeometry(
        const std::vector< OpenGL::instanceGeometry_t >& geometry ) noexcept;

  private:
    bb::product_ptr< OpenGL::elGeometricObject_InstancedObject > object;
    bb::product_ptr< OpenGL::elTexture_2D >                      diffuseTex;
};
} // namespace world
} // namespace el3D
