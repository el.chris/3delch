#pragma once

#include "HighGL/elCameraController_StickToObject.hpp"
#include "world/elModel.hpp"

namespace el3D
{
namespace world
{
class elModelGroup
    : public OpenGL::elModelMatrix< elModelGroup,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    elModelGroup( const objectSettings_t settings, const std::string& fileName,
                  const float scaling = 1.0f ) noexcept;
    elModelGroup( const objectSettings_t settings ) noexcept;
    ~elModelGroup() = default;

    virtual void
    SetPosition( const glm::vec3& ) noexcept override;
    virtual void
    SetScaling( const glm::vec3& ) noexcept override;
    virtual void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    virtual void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    virtual glm::vec3
    GetPosition() const noexcept override;
    virtual glm::vec3
    GetScaling() const noexcept override;
    virtual glm::vec3
    GetRotationAxis() const noexcept override;
    virtual float
    GetRotationDegree() const noexcept override;

    void
    SetDiffuseColor( const color4_t& color ) noexcept;
    void
    SetEmissionColor( const color4_t& color ) noexcept;
    void
    SetSpecularColor( const color3_t&     color,
                      const utils::byte_t metallic ) noexcept;
    void
    SetShininessAndRoughness( const uint8_t shininess,
                              const uint8_t roughness ) noexcept;

    std::vector< GLfloat >
    GetVertices() const noexcept;
    std::vector< GLuint >
    GetElements() const noexcept;

    template < typename ShapeType, typename... ShapeTypeCTorArgs >
    void
    SetPhysicalProperties( const physicalProperties_t physicalProperties,
                           const ShapeTypeCTorArgs&... args ) noexcept;

    glm::mat4
    GetModelMatrix() const noexcept;

    glm::vec3
    GetMin() const noexcept;
    glm::vec3
    GetMax() const noexcept;
    glm::vec3
    GetDimension() const noexcept;

    bb::product_ptr< elModelGroup >
    Clone() const noexcept;

    bool
    AttachCamera( OpenGL::elCamera_Perspective& camera,
                  const glm::vec3&              positionOffset,
                  const glm::vec3               lookAtOffset ) noexcept;


    std::vector< bb::product_ptr< elModel > > models;

  protected:
    friend class elVehicle;
    friend class elInteractiveSet;

    void
    SetCustomPhysicalTransformGetter(
        const std::function< const btTransform&() >& v ) noexcept;

    bb::product_ptr< elPhysics::Object >                      physical;
    std::optional< HighGL::elCameraController_StickToObject > cameraAttachment;
};
} // namespace world
} // namespace el3D

#include "world/elModelGroup.inl"
