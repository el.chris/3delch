#pragma once

#include "animation/elAnimatable.hpp"
#include "world/elObject.hpp"

namespace el3D
{
namespace world
{
struct pointLightProperties_t
{
    glm::vec3 color            = { 0.0f, 0.0f, 0.0f };
    glm::vec3 attenuation      = { 0.0f, 0.0f, 0.0f };
    glm::vec3 position         = { 0.0f, 0.0f, 0.0f };
    float     diffuseIntensity = 0.0f;
    float     ambientIntensity = 0.0f;
    bool      doCastShadow     = false;
};

class elPointLight : public animation::elAnimatable< elPointLight >,
                     public elObject
{
  public:
    elPointLight( const objectSettings_t        settings,
                  const pointLightProperties_t& properties ) noexcept;

    void
    SetAttenuation( const glm::vec3& ) noexcept;
    void
    SetPosition( const glm::vec3& ) noexcept;

    void
    SetColor( const glm::vec3& ) noexcept;
    void
    SetDiffuseIntensity( const float ) noexcept;
    void
    SetAmbientIntensity( const float ) noexcept;

    void
    Move( const glm::vec3& source, const glm::vec3& direction,
          const float speed ) noexcept;

    void
    MoveTo( const glm::vec3& source, const glm::vec3& destination,
            const float speed, const bool isInfiniteLoop ) noexcept;

    const pointLightProperties_t&
    GetProperties() const noexcept;

  private:
    pointLightProperties_t                        properties;
    bb::product_ptr< HighGL::elLight_PointLight > light;
};

} // namespace world
} // namespace el3D
