#pragma once

#include "world/elBoundingBox.hpp"
#include "world/elInteractiveMesh.hpp"
#include "world/elModel.hpp"
#include "world/elModelEffect.hpp"
#include "world/elModelGroup.hpp"
#include "world/elObject.hpp"
#include "world/elWorld.hpp"

#include <map>
#include <vector>

namespace el3D
{
namespace world
{
template < typename T >
struct interactiveCallbacks_t
{
    std::function< void( T* ) > select;
    std::function< void( T* ) > deselect;
};

struct interactiveMeshCallbacks_t
{
    std::function< void( glm::vec3 ) > click;
};

struct interactiveSetProperties_t
{
    color4_t        hoverDiffuseColor{ 0xff, 0x99, 0x33, 0xff };
    color4_t        hoverGlowColor{ 0xff, 0x99, 0x33, 0x00 };
    ModelEffectType hoverEffectType = ModelEffectType::Wireframe;

    color4_t        selectDiffuseColor{ 0x33, 0x99, 0xff, 0xff };
    color4_t        selectGlowColor{ 0x33, 0x99, 0xff, 0x00 };
    ModelEffectType selectEffectType = ModelEffectType::Silhouette;

    color4_t selectBoxCornerColor{ 0xff, 0xff, 0xff, 0x99 };
    color4_t selectBoxLineColor{ 0xff, 0xff, 0xff, 0x66 };
};

class elInteractiveSet : public elObject
{
  public:
    elInteractiveSet( const objectSettings_t            settings,
                      const interactiveSetProperties_t& properties =
                          interactiveSetProperties_t() ) noexcept;
    elInteractiveSet( const elInteractiveSet& ) noexcept = delete;
    elInteractiveSet( elInteractiveSet&& ) noexcept      = delete;
    ~elInteractiveSet()                                  = default;

    elInteractiveSet&
    operator=( const elInteractiveSet& ) noexcept = delete;
    elInteractiveSet&
    operator=( elInteractiveSet&& ) noexcept = delete;

    void
    Attach( elModel&                                 model,
            const interactiveCallbacks_t< elModel >& callbacks ) noexcept;
    void
    Attach( elModelGroup&                                 modelGroup,
            const interactiveCallbacks_t< elModelGroup >& callbacks ) noexcept;

    void
    AttachMembers(
        elModelGroup&                            modelGroup,
        const interactiveCallbacks_t< elModel >& callbacks ) noexcept;

    void
    Detach( elModel& model ) noexcept;
    void
    Detach( elModelGroup& modelGroup ) noexcept;
    void
    DetachMembers( elModelGroup& modelGroup ) noexcept;

    template < typename... Targs >
    void
    SetupInteractiveMesh( interactiveMeshCallbacks_t callbacks,
                          Targs&&... args ) noexcept;

    elInteractiveMesh&
    GetInteractiveMesh() noexcept;

    const elInteractiveMesh&
    GetInteractiveMesh() const noexcept;

  private:
    template < typename T >
    struct ObjectDetails_t
    {
        T*                          object;
        interactiveCallbacks_t< T > callbacks;
    };

    using objectVariant_t = std::variant< ObjectDetails_t< elModel >,
                                          ObjectDetails_t< elModelGroup > >;
    void
    EventCallback( const SDL_Event e ) noexcept;
    void
    MainLoopCallback() noexcept;
    void
    Color2ModelCallback() noexcept;
    template < typename T >
    void
    RemoveFromColor2Object( const T& model ) noexcept;
    void
    ResetModelHover() noexcept;
    void
    ResetModelSelection() noexcept;
    void
    ActivateModelHover( const uint32_t colorIndex ) noexcept;
    void
    ActivateModelSelection( const uint32_t colorIndex ) noexcept;

  private:
    template < typename T >
    struct EventEntry_t
    {
        T*                                      object;
        bb::product_ptr< HighGL::elEventColor > eventColor;
    };

    enum ObjectIndices
    {
        MODEL       = 0,
        MODEL_GROUP = 1
    };

    static constexpr uint32_t INVALID_COLOR =
        std::numeric_limits< uint32_t >::max();

    bb::product_ptr< elInteractiveMesh >        interactiveMesh;
    interactiveMeshCallbacks_t                  interactiveMeshCallbacks;
    std::vector< EventEntry_t< elModel > >      models;
    std::vector< EventEntry_t< elModelGroup > > modelGroups;

    bool     isMouseHoveringInteractiveMesh = false;
    uint32_t previousColorIndex             = INVALID_COLOR;
    uint32_t hoverColorIndex                = INVALID_COLOR;
    uint32_t selectColorIndex               = INVALID_COLOR;
    std::map< uint32_t, objectVariant_t > color2Object;

    utils::CallbackGuard              eventColorCallback;
    bb::product_ptr< GLAPI::elEvent > event;

    bb::product_ptr< world::elModelEffect > hoverObject;
    ModelEffectProperties                   hoverObjectProperties;

    bb::product_ptr< world::elModelEffect > selectObject;
    ModelEffectProperties                   selectObjectProperties;

    bb::product_ptr< world::elBoundingBox > selectBoxObject;
    BoundingBoxProperties                   selectBoxProperties;
};
} // namespace world
} // namespace el3D

#include "world/elInteractiveSet.inl"
