#pragma once

#include "HighGL/elTerrainGenerator.hpp"
#include "world/elObject.hpp"

#include <cstdint>
#include <glm/glm.hpp>

namespace el3D
{
namespace world
{
struct terrainProperties_t
{
    float      frequency       = 5.0f;
    uint64_t   octaves         = 1;
    uint32_t   seed            = 91231;
    glm::uvec2 dimension       = { 128, 128 };
    float      widthScaling    = 0.1f;
    float      heightScaling   = 0.003f;
    uint64_t   normalGridRange = 5;
};

class elTerrain : public elObject
{
  public:
    elTerrain( const objectSettings_t settings,
               terrainProperties_t    properties ) noexcept;

    std::vector< GLfloat >
    GetVertices() const noexcept;
    std::vector< GLuint >
    GetElements() const noexcept;

  private:
    bb::product_ptr< HighGL::UnifiedVertexObject::Object > object;
    std::optional< HighGL::elTerrainGenerator >            generator;
};
} // namespace world
} // namespace el3D
