#pragma once

#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "world/elModel.hpp"
#include "world/elModelGroup.hpp"
#include "world/elObject.hpp"

namespace el3D
{
namespace world
{
struct BoundingBoxProperties
{
    float    borderLineDrawSize = 1.0f;
    color4_t borderLineColor{ 0xff, 0xff, 0xff, 0x88 };

    float    cornerLineLength   = 0.2f;
    float    cornerLineDrawSize = 2.0f;
    color4_t cornerLineColor{ 0xff, 0xff, 0xff, 0xce };
};

class elBoundingBox
    : public OpenGL::elModelMatrix< elBoundingBox,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    using ModelMatrixGetter_t = std::function< glm::mat4() >;
    elBoundingBox(
        const objectSettings_t settings, const std::vector< GLfloat >& vertices,
        const ModelMatrixGetter_t& modelMatrixGetter = []
        { return glm::mat4( 1.0 ); },
        const BoundingBoxProperties& properties =
            BoundingBoxProperties() ) noexcept;
    elBoundingBox( objectSettings_t settings, const elModel& model,
                   const BoundingBoxProperties& properties =
                       BoundingBoxProperties() ) noexcept;
    elBoundingBox( objectSettings_t settings, const elModelGroup& modelGroup,
                   const BoundingBoxProperties& properties =
                       BoundingBoxProperties() ) noexcept;

    void
    SetBorderLineDrawSize( const float size ) noexcept;
    void
    SetBorderLineColor( const color4_t& color ) noexcept;

    void
    SetCornerLineDrawSize( const float size ) noexcept;
    void
    SetCornerLineColor( const color4_t& color ) noexcept;

    void
    SetPosition( const glm::vec3& position ) noexcept override;
    void
    SetScaling( const glm::vec3& scaling ) noexcept override;
    void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    glm::vec3
    GetPosition() const noexcept override;
    glm::vec3
    GetScaling() const noexcept override;
    glm::vec3
    GetRotationAxis() const noexcept override;
    float
    GetRotationDegree() const noexcept override;

  private:
    enum objectTypes_t
    {
        BORDER_LINES = 0,
        CORNER_LINES = 1,
        OBJECT_TYPES_END
    };

    std::array< bb::product_ptr< OpenGL::elGeometricObject_NonVertexObject >,
                OBJECT_TYPES_END >
                        objects;
    ModelMatrixGetter_t modelMatrixGetter;
};
} // namespace world
} // namespace el3D
