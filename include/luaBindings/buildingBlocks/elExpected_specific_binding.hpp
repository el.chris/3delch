#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace bb
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elExpected_specific );
#endif
} // namespace bb
} // namespace luaBindings
} // namespace el3D
