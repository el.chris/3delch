#pragma once

#include "OpenGL/elTexture_base.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elTexture_base );

template < typename ChildType >
static void
AddLuaTextureBaseInterface( lua::elLuaScript::class_t& newClass );
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/OpenGL/elTexture_base_binding.inl"
