#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
#ifdef ADD_LUA_BINDINGS
LUA_R( byteStream_t );
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
