#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "utils/elRawPointer.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
template < typename T >
void
AddLuaRawPointerBinding( const std::string& luaClassName,
                         const std::string& nameSpace = "" );
} // namespace utils
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/utils/elRawPointer_binding.inl"
#endif
