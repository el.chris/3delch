#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elNetworkSocket_internal );
#endif
} // namespace network
} // namespace luaBindings
} // namespace el3D
