#pragma once

#include "lua/elLuaScript.hpp"
#include "network/elNetworkSocket_Base.hpp"

namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
void
AddLuaNetworkSocket_baseInterface( lua::elLuaScript::class_t& newClass );
#endif
} // namespace network
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/network/elNetworkSocket_base_binding.inl"
