#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace units
{
#ifdef ADD_LUA_BINDINGS
LUA_R( Time );
#endif
} // namespace units
} // namespace luaBindings
} // namespace el3D
