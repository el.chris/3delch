#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"

#include <optional>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename T >
void
AddLuaOptionalBinding( const std::string& luaClassName,
                       const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/optional_binding.inl"
#endif
