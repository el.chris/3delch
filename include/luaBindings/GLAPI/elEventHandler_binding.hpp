#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GLAPI
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elEventHandler );
#endif
} // namespace GLAPI
} // namespace luaBindings
} // namespace el3D
