#pragma once

namespace el3D
{
namespace luaBindings
{
template < typename T >
struct LuaArray
{
    using value_type = typename T::value_type;
    T data;
};

template < typename T >
struct LuaTable
{
    using key_type    = typename T::key_type;
    using mapped_type = typename T::mapped_type;
    T data;
};

template < typename T >
struct isLuaArray
{
    static const bool value = false;
};
template < typename T >
struct isLuaArray< LuaArray< T > >
{
    static const bool value = true;
};

template < typename T >
struct isLuaTable
{
    static const bool value = false;
};
template < typename T >
struct isLuaTable< LuaTable< T > >
{
    static const bool value = true;
};

} // namespace luaBindings
} // namespace el3D
