#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>

namespace el3D
{
namespace math
{
template < typename T >
constexpr T
Fract( const T t ) noexcept;

template < typename T >
constexpr T
MapTo( const T t, const std::array< T, 2 >& oldRange,
       const std::array< T, 2 >& newRange ) noexcept;

template < typename T >
constexpr T
Fade( const T t ) noexcept;

template < typename T >
constexpr T
Lerp( const T t, const T a, const T b ) noexcept;

template < typename T >
constexpr typename std::make_unsigned< T >::type
NextPowerOfTwo( const T t ) noexcept;

} // namespace math
} // namespace el3D

#include "math/functions.inl"
