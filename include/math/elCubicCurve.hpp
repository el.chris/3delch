#pragma once

#include "math/math.hpp"

#include <cstdint>

namespace el3D
{
namespace math
{
template < typename T >
class elCubicCurve
{
  public:
    elCubicCurve( const T& a, const T& b, const T& c, const T& d ) noexcept;
    T
    operator()( const float t ) const noexcept;

    T
    GetValueOfDerivate_1st( const float t ) const noexcept;
    T
    GetValueOfDerivate_2nd( const float t ) const noexcept;

    float
    Length( const uint64_t resolution ) const noexcept;

  private:
    T a;
    T b;
    T c;
    T d;
};
} // namespace math
} // namespace el3D

#include "math/elCubicCurve.inl"
