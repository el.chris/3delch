#pragma once

#include "GuiGL/elWidget_Table.hpp"

#include <functional>

namespace el3D
{
namespace GuiGL
{
class elWidget_Dropbutton;
class elWidget_Listbox : public elWidget_Table
{
  public:
    elWidget_Listbox() = delete;
    elWidget_Listbox( const constructor_t& constructor );
    virtual ~elWidget_Listbox() = default;

    void
    AddElement( const std::string&, const size_t position );
    void
    AddElements( const std::vector< std::string >& elements,
                 const size_t                      position = 0 ) noexcept;
    void
    RemoveElements( const size_t position, const size_t length ) noexcept;
    void
    Clear() noexcept;
    virtual void
    Reshape() override;
    std::vector< size_t >
    GetSelectedWidgets() const noexcept;
    std::vector< std::string >
    GetSelectedWidgetText() const noexcept;
    void
    AdjustHeightToFitElements() noexcept;
    void
    SetTextAlignment( const positionCorner_t position,
                      const glm::vec2& offset = glm::vec2( 0.0 ) ) noexcept;

    void
    SetElementSelectionCallback(
        const std::function< void() >& callback ) noexcept;

    template < typename T = elWidget_base >
    T*
    GetElementAtIndex( const size_t index ) noexcept;

    friend class elWidget_Dropbutton;

  private:
    std::vector< std::string >           listboxLabels;
    std::vector< size_t >                selection;
    bb::product_ptr< handler::Callable > elementSelectionCallback;
    positionCorner_t                     textAlignment = POSITION_CENTER;
    glm::vec2                            offset        = glm::vec2( 0.0 );

  private:
    void
    SetLabelText() noexcept;
    void
    ResetListSelection() noexcept;

    virtual void
    ResetNumberOfColsAndRows( const size_t rows,
                              const size_t cols ) noexcept override;
    template < typename T = elWidget_base >
    T*
    GetElement( const size_t col, const size_t row ) noexcept;
    virtual void
    SetColumnWidth( const size_t col, const float width ) noexcept override;
    virtual void
    SetRowHeight( const size_t row, const float height ) noexcept override;
    virtual void
    SetElementDefaultDimensions( const size_t width,
                                 const size_t height ) noexcept override;
};
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/elWidget_Listbox.inl"
