#pragma once

#include "GuiGL/common.hpp"

#include <functional>

namespace el3D
{
namespace GLAPI
{
class elEventHandler;
class elEvent;
class elWindow;
class elFontCache;
} // namespace GLAPI

namespace OpenGL
{
class elShaderProgram;
} // namespace OpenGL

namespace HighGL
{
class elShaderTexture;
class elEventTexture;
} // namespace HighGL

namespace lua
{
class elConfigHandler;
}

namespace GuiGL
{
class elWidget_base;
class elWidget_MouseCursor;

struct constructor_t
{
    constructor_t(
        elWidget_base* parent, const OpenGL::elShaderProgram* shader,
        const OpenGL::elShaderProgram*     glowShader,
        HighGL::elEventTexture* const      eventTexture,
        GLAPI::elEventHandler* const       eventHandler,
        GLAPI::elEvent* const              stickyExclusiveEvent,
        const GLAPI::elWindow* const       window,
        GLAPI::elFontCache* const          fontCache,
        GuiGL::elWidget_MouseCursor* const mouseCursor,
        const std::function< size_t( HighGL::elShaderTexture* ) >&
                                               shaderTextureRegistrator,
        const std::function< void( size_t ) >& shaderTextureDeregistrator,
        const int shaderVersion, const lua::elConfigHandler* theme,
        const widgetType_t type, const size_t* screenWidth,
        const size_t* screenHeight, const std::string& className,
        const widgetType_t enforcedThemeParent, const float glowScaling,
        CallbackContainer_t& callbackContainer ) noexcept;

    elWidget_base*                 parent;
    const OpenGL::elShaderProgram* shader;
    const OpenGL::elShaderProgram* glowShader;
    HighGL::elEventTexture*        eventTexture;
    GLAPI::elEventHandler*         eventHandler;
    GLAPI::elEvent*                stickyExclusiveEvent;
    const GLAPI::elWindow*         window;
    GLAPI::elFontCache*            fontCache;
    GuiGL::elWidget_MouseCursor*   mouseCursor;
    std::function< size_t( HighGL::elShaderTexture* ) >
                                    shaderTextureRegistrator;
    std::function< void( size_t ) > shaderTextureDeregistrator;
    int                             shaderVersion;
    const lua::elConfigHandler*     theme;
    widgetType_t                    type;
    const size_t*                   screenWidth;
    const size_t*                   screenHeight;
    std::string                     className;
    widgetType_t                    enforcedThemeParent;
    float                           glowScaling;
    CallbackContainer_t*            callbackContainer;
};
} // namespace GuiGL
} // namespace el3D
