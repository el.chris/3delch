#pragma once

#include "GuiGL/common.hpp"

#include <functional>
#include <utility>

namespace el3D
{
namespace GuiGL
{
class elWidget_base;
template < typename T, typename Factory >
class widget_ptr
{
  public:
    widget_ptr() = default;
    ~widget_ptr();
    widget_ptr( const widget_ptr& ) = delete;
    template < typename Child >
    widget_ptr( widget_ptr< Child, Factory >&& rhs ) noexcept;

    widget_ptr&
    operator=( const widget_ptr& ) = delete;
    template < typename Child >
    widget_ptr&
    operator=( widget_ptr< Child, Factory >&& rhs ) noexcept;

    explicit operator bool() const noexcept;

    T*
    operator->() noexcept;
    const T*
    operator->() const noexcept;
    T&
    operator*() noexcept;
    const T&
    operator*() const noexcept;

    T*
    Get() noexcept;
    const T*
    Get() const noexcept;
    void
    Reset() noexcept;

    template < typename, typename >
    friend class widget_ptr;
    friend Factory;

  private:
    widget_ptr(
        T* const                                       ptr,
        const std::function< void( elWidget_base* ) >& deleter ) noexcept;

  private:
    T*                                      ptr = nullptr;
    std::function< void( elWidget_base* ) > deleter;
};
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/widget_ptr.inl"
