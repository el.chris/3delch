#pragma once

#include "GuiGL/elWidget_Button.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Listbox;
class elWidget_Dropbutton : public elWidget_Button
{
  public:
    elWidget_Dropbutton() = delete;
    elWidget_Dropbutton( const constructor_t &constructor );
    virtual ~elWidget_Dropbutton() = default;

    virtual void
    Reshape() override;

    void
    AddElement( const std::string &element, const size_t position ) noexcept;
    void
    AddElements( const std::vector< std::string > &elements,
                 const size_t                      position = 0 ) noexcept;
    void
    RemoveElements( const size_t position, const size_t length ) noexcept;
    uint64_t
    GetSelection() noexcept;

    void
    SetMaxListHeight( const float ) noexcept;

    void
    SetCallbackOnSelection( const std::function< void() > & ) noexcept;

    virtual void
    SetPositionPointOfOrigin(
        const positionCorner_t,
        const bool doKeepPosition = false ) noexcept override;

  private:
    void
    ToggleList() noexcept;
    void
    CloseList() noexcept;
    void
    OpenList() noexcept;

  private:
    elWidget_Listbox_ptr              list;
    glm::vec2                         listSize;
    uint64_t                          selected      = 0u;
    float                             maxListHeight = 150.0f;
    std::function< void() >           selectionCallback;
    bb::product_ptr< GLAPI::elEvent > closeListOnClick;
};
} // namespace GuiGL
} // namespace el3D
