#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_KeyValuePair : public elWidget_base
{
  public:
    elWidget_KeyValuePair( const constructor_t &constructor ) noexcept;
    virtual ~elWidget_KeyValuePair() = default;

    void
    SetKeyText( const std::string &value ) noexcept;
    void
    SetValueText( const std::string &value ) noexcept;
    void
    SetValueOffset( const glm::vec2 &offset ) noexcept;
    uint64_t
    GetFontHeight() const noexcept;

    virtual void
    Reshape() override;

  private:
    elWidget_Label_ptr key;
    elWidget_Label_ptr value;
};
} // namespace GuiGL
} // namespace el3D
