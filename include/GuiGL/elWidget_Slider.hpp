#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Window;
class elWidget_Slider : public elWidget_base
{
  public:
    elWidget_Slider() = delete;
    elWidget_Slider( const constructor_t& constructor );
    virtual ~elWidget_Slider() = default;

    void
    Reshape() override;
    float
    GetSliderPosition() const noexcept;
    void
    SetSliderPosition( const float ) noexcept;
    void
    SetSliderPositionChangeCallback(
        const std::function< void( const float ) >& ) noexcept;
    void
    SetShowValueOnButton( const bool v, const float scaling = 1.0f,
                          const uint64_t precision = 2U ) noexcept;
    void
    SetRelativeSliderButtonSize( const float ) noexcept;
    void
    SetIsHorizontal( const bool ) noexcept;
    void
    SetDescriptionText( const std::string& text ) noexcept;

    friend class elWidget_Window;

  private:
    void
    UpdateSliderButtonText() noexcept;
    void
    SetSliderPositionWithCallback( const float,
                                   const bool doCallback ) noexcept;
    void
    SliderCallback();
    void
    MoveSliderButton();
    void
    MoveSliderToClickPosition();
    void
    CreateMovingSliderEvent();
    void
    CreateMoveSliderToMouseClickEvent();
    glm::vec2
    GetMaxPosition();
    glm::vec2
    GetDescriptionTextPosition() const noexcept;

  private:
    float                                relativeSliderButtonSize{ 0.3f };
    std::function< void( const float ) > sliderPositionChangeCallback;
    bb::product_ptr< GLAPI::elEvent >    moveSliderToMouseClickEvent;
    float                                lastSliderPosition{ -1.0f };
    float                                sliderPosition{ 0.0f };
    bool                                 isHorizontal{ true };

    struct
    {
        elWidget_Label_ptr text;
        bool               isOnLeftSide = false;
    } description;

    struct
    {
        elWidget_Button_ptr widget;
        bool                doShowValue   = false;
        float               scalingFactor = 1.0f;
        uint64_t            precision     = 2U;
    } button;
};
} // namespace GuiGL
} // namespace el3D
