#pragma once
#include "GuiGL/GuiGL.hpp"
#include "GuiGL/common.hpp"
#include "GuiGL/constructor_t.hpp"
#include "GuiGL/widget_ptr.hpp"
#include "HighGL/elEventTexture.hpp"

#include <functional>
#include <memory>
#include <vector>

namespace el3D
{
namespace GLAPI
{
class elEventHandler;
class elWindow;
class elFontCache;
} // namespace GLAPI

namespace OpenGL
{
class elShaderProgram;
} // namespace OpenGL

namespace HighGL
{
class elShaderTexture;
}

namespace lua
{
class elConfigHandler;
}

namespace GuiGL
{
namespace handler
{
class widgetFactory
{
  protected:
    widgetFactory() = delete;
    widgetFactory(
        elWidget_base* me, elWidget_base* parent,
        GLAPI::elEvent* const              stickyExclusiveEvent,
        HighGL::elEventTexture* const      eventTexture,
        GLAPI::elEventHandler* const       eventHandler,
        const GLAPI::elWindow* const       window,
        GLAPI::elFontCache* const          fontCache,
        GuiGL::elWidget_MouseCursor* const mouseCursor,
        CallbackContainer_t* const         callbackContainer,
        const std::function< size_t( HighGL::elShaderTexture* ) >&
                                               shaderTextureRegistrator,
        const std::function< void( size_t ) >& shaderTextureDeregistrator,
        const int shaderVersion, const lua::elConfigHandler* theme,
        const OpenGL::elShaderProgram* const shader,
        const OpenGL::elShaderProgram* const glowShader,
        const size_t* screenWidth, const size_t* screenHeight,
        const float glowScaling, const widgetStates_t* widgetStatePointer );
    virtual ~widgetFactory();

  public:
    template < typename T >
    std::enable_if_t< !std::is_same_v< T, elWidget_Titlebar >, widget_t< T > >
    Create( const std::string&                             className = "",
            const std::function< void( elWidget_base* ) >& customDeleter =
                std::function< void( elWidget_base* ) >() ) noexcept;


    const lua::elConfigHandler*
    GetTheme() noexcept;
    void
    RemoveThis() noexcept;

    elWidget_Titlebar*
    GetTitlebar() noexcept;
    void
    MoveWidgetToTop() noexcept;

    template < typename, typename >
    friend class ::el3D::GuiGL::widget_ptr;

  protected:
    const std::vector< elWidget_base* >&
    GetChildren() const noexcept;
    size_t
    GetScreenWidth() const noexcept;
    size_t
    GetScreenHeight() const noexcept;
    const OpenGL::elShaderProgram*
    GetShader() const noexcept;
    const OpenGL::elShaderProgram*
    GetGlowShader() const noexcept;
    void
    MoveMyWidgetToTop( elWidget_base* const widget ) noexcept;
    elWidget_base*
    CreateTitlebar() noexcept;

  private:
    void
    Remove( elWidget_base* widget ) noexcept;
    void
    PrepareWidget( elWidget_base* ) noexcept;
    void
    PrepareTitlebar() noexcept;
    template < typename T >
    widgetType_t
    GetType();
    static void
    RemoveFromParent( elWidget_base* const widget ) noexcept;
    static void
    DeleteWidget( elWidget_base* const widget ) noexcept;

  private:
    elWidget_base*                me                   = nullptr;
    elWidget_base*                parent               = nullptr;
    GLAPI::elEvent*               stickyExclusiveEvent = nullptr;
    HighGL::elEventTexture*       eventTexture         = nullptr;
    GLAPI::elEventHandler*        eventHandler         = nullptr;
    const GLAPI::elWindow*        window               = nullptr;
    GLAPI::elFontCache*           fontCache            = nullptr;
    elWidget_MouseCursor*         mouseCursor          = nullptr;
    CallbackContainer_t*          callbackContainer    = nullptr;
    std::vector< elWidget_base* > widgets;
    bool                          removeNullptrFromWidgets = false;

    std::function< size_t( HighGL::elShaderTexture* ) >
                                    shaderTextureRegistrator;
    std::function< void( size_t ) > shaderTextureDeregistrator;
    int                             shaderVersion;
    elWidget_Titlebar*              titleBar           = nullptr;
    const widgetStates_t*           widgetStatePointer = nullptr;
    const lua::elConfigHandler*     theme;
    const size_t*                   screenWidth  = nullptr;
    const size_t*                   screenHeight = nullptr;
    const OpenGL::elShaderProgram*  shader       = nullptr;
    const OpenGL::elShaderProgram*  glowShader   = nullptr;
    float                           glowScaling;
};
} // namespace handler
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/handler/widgetFactory.inl"
