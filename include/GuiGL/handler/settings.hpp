#pragma once
#include "GuiGL/common.hpp"
#include "animation/elAnimatable.hpp"
#include "animation/elAnimate_SourceDestination.hpp"
#include "logging/elLog.hpp"
#include "utils/elClassSettings.hpp"

#include <map>
#include <string>

namespace el3D
{
namespace lua
{
class elConfigHandler;
}

namespace GuiGL
{
class elWidget_Window;
class elWidget_base;
class elWidget_ColorPicker;
namespace handler
{
class settings : public animation::elAnimatable< settings >
{
  protected:
    settings() = delete;
    settings( elWidget_base* me, const lua::elConfigHandler* theme,
              const std::string& className,
              const widgetType_t enforcedThemeParent );
    virtual ~settings();

  public:
    bool
    GetDoInvertTextureCoordinates() const noexcept;
    void
    SetDoInvertTextureCoordinates( const bool v ) noexcept;

    glm::vec2
    GetAbsolutPosition() const noexcept;

    void
    MoveTo( const glm::vec2&               position,
            const std::function< void() >& onArrivalCallback =
                std::function< void() >() ) noexcept;

    void
    ResizeTo( const glm::vec2&               size,
              const std::function< void() >& onArrivalCallback =
                  std::function< void() >() ) noexcept;

    /*!
       \brief Set the border size of the window
       \param .x = top; .y = bottom, .z = left, .w = right
    */
    void
    SetBorderSize( const glm::vec4& border );
    void
    SetPosition( const glm::vec2& pos );
    virtual void
    SetPositionPointOfOrigin( const positionCorner_t,
                              const bool doKeepPosition = false ) noexcept;
    void
    SetPositionPointOfOriginForChilds(
        const positionCorner_t, const bool doKeepPosition = false ) noexcept;
    positionCorner_t
    GetPositionPointOfOrigin() const noexcept;
    positionCorner_t
    GetPositionPointOfOriginForChilds() const noexcept;
    void
    SetIsAlwaysOnTop( const bool ) noexcept;
    void
    SetIsAlwaysOnBottom( const bool ) noexcept;
    void
    SetHasVerticalScrolling( const bool ) noexcept;
    void
    SetHasHorizontalScrolling( const bool ) noexcept;
    void
    SetSize( const glm::vec2& size );

    void
    SetHasTitlebar( const bool ) noexcept;
    bool
    GetHasTitlebar() const noexcept;

    void
    SetHasMultiSelection( const bool ) noexcept;
    bool
    GetHasMultiSelection() const noexcept;

    void
    SetIsUnselectable( const bool ) noexcept;
    bool
    GetIsUnselectable() const noexcept;

    /*!
        \brief Sets if this widget is effected by scrolling. If its
        false the widget scrolls up and down in the parent widget
        otherwise it sticks at his position.
    */
    void
    SetHasFixedPosition( const bool xAxis, const bool yAxis ) noexcept;

    /*!
        \brief Let the widget stick to parent size.
        \param[in] size Sticks to "widgetSize = parentSize - size"
        a negative value means do not stick to parent size
    */
    void
    SetStickToParentSize( const glm::vec2& size ) noexcept;
    void
    SetBaseColorForState( const glm::vec4& color, const widgetStates_t );
    void
    SetAccentColorForState( const glm::vec4& color, const widgetStates_t );
    glm::vec4
    GetBaseColorToState( const widgetStates_t );
    glm::vec4
    GetAccentColorToState( const widgetStates_t );
    void
    SetBaseGlowColorForState( const glm::vec4& color, const widgetStates_t );
    void
    SetAccentGlowColorForState( const glm::vec4& color, const widgetStates_t );
    glm::vec4
    GetBaseGlowColorToState( const widgetStates_t );
    glm::vec4
    GetAccentGlowColorToState( const widgetStates_t );
    void
    SetIsMovable( const bool );
    void
    SetIsResizable( const bool );
    void
    SetDoRenderWidget( const bool ) noexcept;
    bool
    DoRenderWidget() const noexcept;
    void
    SetDisableEventHandling( const bool ) noexcept;
    bool
    GetHasDisabledEventHandling() noexcept;
    virtual void
    SetHasVerticalAlignment( const bool ) noexcept;
    bool
    GetHasVerticalAlignment() const noexcept;

    bool
    GetIsMovable() const noexcept;
    bool
    GetIsResizable() const noexcept;
    glm::vec2
    GetSize() const noexcept;
    glm::vec4
    GetBorderSize() const noexcept;
    glm::vec2
    GetContentSize() const noexcept;
    glm::vec2
    GetContentDimension() const noexcept;
    glm::vec2
    GetPosition() const noexcept;

    utils::elClassSettings
    GetSettingsHandle() const noexcept;

    template < typename Exception >
    void
    ThrowIfPropertyIsUnset( const size_t property ) const;

    void
    SetAlignedPositionWithWidget( const elWidget_base* widget, const bool xPos,
                                  const bool yPos ) noexcept;

    void
    SetButtonAlignment( const position_t ) noexcept;
    position_t
    GetButtonAlignment() const noexcept;
    glm::vec4
    GetWindowDimensions() const noexcept;

    std::string
    GetClassName() const noexcept;

    glm::ivec2
    GetAdjustSizeToFitChildren() const noexcept;
    void
    SetAdjustSizeToFitChildren( const bool xSize, const bool ySize ) noexcept;

    void
    SetClassName( const std::string& className,
                  const bool         updateGeometry ) noexcept;

    glm::vec4
    GetBorderEventIncrease() const noexcept;
    void
    SetBorderEventIncrease( const glm::vec4& ) noexcept;

    glm::vec2
    GetAbsolutPositionOffset() const noexcept;
    void
    SetAbsolutPositionOffset( const glm::vec2& ) noexcept;

    // is not drawn but it is still there and considered when calculating
    // content size etc
    void
    SetIsGhostWidget( const bool ) noexcept;
    bool
    IsGhostWidget() const noexcept;
    void
    SetDoDrawColors( const bool v ) noexcept;
    void
    SetStickToWidget( elWidget_base* const       widget,
                      const verticalPosition_t   vPosition,
                      const horizontalPosition_t hPosition ) noexcept;
    void
    UnsetStickToWidget() noexcept;

  protected:
    template < typename T >
    using AnimationInterpolation = animation::interpolation::Hermite< T >;
    template < typename T >
    using ColorTransitionInterpolation = animation::interpolation::Linear< T >;

    struct alignedPosition_t
    {
        const elWidget_base* widget = nullptr;
        bool                 xPos   = false;
        bool                 yPos   = false;
    };

    struct stickyWidget_t
    {
        elWidget_base*       widget;
        verticalPosition_t   vPosition;
        horizontalPosition_t hPosition;
    };

    std::vector< stickyWidget_t > stickyWidgetsSubscriber;
    elWidget_base*                stickyWidgetSubcribedTo = nullptr;
    utils::elClassSettings        widgetSettings;
    bool                          doInheritStateFromParent{ false };
    std::vector< elWidget_base* > stateInheritanceVector;

  protected:
    template < typename T >
    T
    RoundVector( const T& value ) const noexcept;
    void
    SetInheritStateFromParent( const bool& ) noexcept;
    std::vector< elWidget_base* >
    GetStateInheritanceVector() const noexcept;
    std::string
    GetAnimationShaderFile() const noexcept;
    float
    GetAnimationSpeed() const noexcept;
    bool
    GetCopySelectedTextToClipboard() const noexcept;
    void
    SetNonConformTextureSize( const glm::vec2& ) noexcept;
    bool
    HasNonConformTextureSize() const noexcept;
    glm::vec2
    GetNonConformTextureSize() const noexcept;
    void
    EnforceThemeParentWidget( const widgetType_t enforcedType,
                              const bool         updateGeometry ) noexcept;
    void
    SetForwardMoveEventToParent( const bool ) noexcept;
    bool
    GetForwardMoveEventToParent() const noexcept;
    alignedPosition_t
    GetAlignedPositionWithWidget() noexcept;

    glm::vec4
    GetAccentColorSwapAdjusted() const noexcept;
    glm::vec4
    GetBaseColorSwapAdjusted() const noexcept;
    glm::vec4
    GetAccentGlowColorSwapAdjusted() const noexcept;
    glm::vec4
    GetBaseGlowColorSwapAdjusted() const noexcept;

    glm::vec4
    GetAccentColor() const noexcept;
    glm::vec4
    GetBaseColor() const noexcept;
    glm::vec4
    GetAccentGlowColor() const noexcept;
    glm::vec4
    GetBaseGlowColor() const noexcept;

    void
    SetAccentColor( const glm::vec4& ) noexcept;
    void
    SetBaseColor( const glm::vec4& ) noexcept;
    void
    SetAccentGlowColor( const glm::vec4& ) noexcept;
    void
    SetBaseGlowColor( const glm::vec4& ) noexcept;

    glm::ivec2
    GetHasFixedPosition() const noexcept;
    bool
    GetHasVerticalScrolling() const noexcept;
    bool
    GetHasHorizontalScrolling() const noexcept;
    position_t
    GetHorizontalPointOfOrigin() const noexcept;
    bool
    GetIsAlwaysOnTop() const noexcept;
    bool
    GetIsAlwaysOnBottom() const noexcept;
    const widgetStates_t&
    GetState() const noexcept;
    const glm::vec2
    GetStickToParentSize() const noexcept;
    position_t
    GetVerticalPointOfOrigin() const noexcept;

    virtual void
    UpdateWindowDimensions() noexcept;
    void
    ReshapeAbsolutPosition() noexcept;
    void
    LoadDefaultsFromConfig( const bool doUpdatePositionAndSize = true );
    void
    ResetWindowDimensions() noexcept;
    void
    StateChangeTo( const widgetStates_t ) noexcept;
    void
    UpdateState( const widgetStates_t ) noexcept;
    bool
    UpdateColorTransition();
    void
    SetDoReadTexture( const bool ) noexcept;

    void
    SetSwapColor( const bool ) noexcept;
    bool
    GetSwapColor() const noexcept;

    glm::vec2
    GetTransformedSize() const noexcept;
    glm::vec2
    GetTransformedAbsolutPosition() const noexcept;
    glm::vec4
    GetTransformedBorderEventIncrease() const noexcept;
    glm::vec2
    GetTransformedNonConformTextureSize() const noexcept;
    glm::vec4
    GetTransformedBorderSize() const noexcept;
    void
    SetPositionOfStickyWidgets() noexcept;

  private:
    std::vector< std::string >
    GetWidgetLine( const size_t maxDepth = 0 ) const noexcept;

    template < typename T, typename getter_t >
    std::pair< std::vector< std::string >, std::vector< T > >
    GetSettingsEntry( const size_t property, getter_t Get,
                      const std::vector< T >& defaultValue ) noexcept;

    std::string
    UnsetPropertyErrorMessage( const size_t property ) const noexcept;
    float
    GetTitlebarAdjustmentHeight( const elWidget_base* widget ) const noexcept;
    glm::vec2
    GetPositionFromAbsolutPosition() const noexcept;

  private:
    struct
    {
        glm::vec4 accentColor     = glm::vec4{ 0.0f };
        glm::vec4 baseColor       = glm::vec4{ 0.0f };
        glm::vec4 accentGlowColor = glm::vec4{ 0.0f };
        glm::vec4 baseGlowColor   = glm::vec4{ 0.0f };
        bool      doUpdate        = true;
        uint64_t  numberOfColorTransitions{ 0u };
    } currentColor;

    elWidget_base*              me;
    std::string                 className;
    const lua::elConfigHandler* theme;
    widgetType_t                enforcedThemeParent;

    bool              doInvertTextureCoordinates = false;
    bool              doRenderWidget             = true;
    bool              hasVerticalScrolling       = false;
    bool              hasHorizontalScrolling     = false;
    bool              isGhostWidget              = false;
    glm::vec4         dimension{ 0.0f };
    glm::vec2         nonConformTextureSize{ 0.0f };
    widgetStates_t    widgetState         = STATE_DEFAULT;
    widgetStates_t    previousWidgetState = STATE_END;
    glm::vec2         absolutPosition{ 0.0f, 0.0f };
    size_t            settingsLookupDepth = 3;
    alignedPosition_t alignedPosition;
};

} // namespace handler
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/handler/settings.inl"
