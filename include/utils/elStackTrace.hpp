#pragma once

#include <string>
#include <vector>

namespace el3D
{
namespace utils
{
class elStackTrace
{
  public:
    struct stackEntry_t
    {
        stackEntry_t( const std::string &symbol, const std::string &function,
                      const std::string &offset, const std::string &address );
        std::string
        GetBinaryName() const noexcept;

        std::string symbol;
        std::string function;
        std::string offset;
        std::string address;
        std::string internalSymbol;
        std::string file;
    };

    elStackTrace();

    std::vector< stackEntry_t >
    Get() const noexcept;
    void
    Print() const noexcept;

    static void
    RegisterInSignalHandler() noexcept;

  private:
    struct stackAddressList_t
    {
        stackAddressList_t();
        static constexpr size_t MAX_STACK_DEPTH = 128;
        void *                  addresses[MAX_STACK_DEPTH];
        int                     depth;
    };


    std::vector< stackEntry_t > stackTrace;

    void
    AddRawEntryToStackTrace( const std::string &rawEntry );
    void
    GenerateStackTrace( const stackAddressList_t &addressList );
};
} // namespace utils
} // namespace el3D
