#pragma once
#include <climits>
#include <sstream>
#include <string>
#include <vector>

/**
 * @brief
 * @code{.cpp}
    // simple example
    elSerialize stuff = elSerializer::Serialize(
        1, 12.3f, 12.001, "hello" );

    //print serialized raw string
    std::cout << stuff.GetRaw();

    int a;
    float b;
    double c;
    std::string d;

    stuff.Deserialize( a, b, c, d);
    std::cout << a << b << c << d;

    // classes need to be convertable to elSerializer therefore
    // a elSerializer CTor must be defined and the elSerializer
    // conversion operator
    struct SomeClass {
        SomeClass(const elSerializer & s) {
            s.Deserialize(a, b, c, serialClass);
        }

        operator elSerializer() const {
            return elSerializer::Serialize(a, b, c, serialClass);
        }

        int                         a;
        std::string                 b;
        char                        c;
        OtherSerializeableClass     serialClass;
    };
 * @endcode
 */
namespace el3D
{
namespace utils
{
class elSerializer
{
  public:
    elSerializer();

    /**
     * @brief Gets the raw serialized string as argument. If the
     *          serialized string as an invalid syntax/format then
     *          the bool operator returns false
     @code{.cpp}
        elSerializer good("3:asd5:12345");
        if ( good ) { ... }

        // has an invalid serialization string and is therefore invalid.
        elSerializer bad("4:ab");
        bad.Deserialize(someString); //someString is untouched since
                                // elSerializer is invalid
     @endcode
     * @param value
     */
    explicit elSerializer( const std::string& value );

    bool
    operator==( const elSerializer& rhs ) const;

    /**
     * @brief Was the object created
     *          successfully.
     *
     * @return true = valid serializationstring in ctor
     *          false = invalid serializationstring
     */
    operator bool() const;
    operator std::string() const;

    /**
     * @return The serialized string
     */
    std::string
    GetRaw() const;

    /**
     * @brief Gets the serialization of the element at index
     *
     * @param index
     *
     * @return serialized string
     */
    std::string
    Get( const size_t index ) const;

    /**
     * @brief Factory method which creates an elSerializer object
     *      and serializes an arbitrary amount of argument. These
     *      arguments must be either of a type which is convertable
     *      to a string via stringstream (int, float, std::string ...)
     *      or they need to be convertable to elSerializer via a
     *          operator elSerializer();
     *      method.
     * @return The created elSerializer object which contains the
     *      serialization.
     */
    template < typename T, typename... Targs >
    static elSerializer
    Serialize( const T& t, const Targs&... args );

    template < typename T >
    static elSerializer
    Serialize( const T& t );

    /**
     * @brief Deserializes the elSerializer object and writes the value
     *          into the given arguments.
     *          These types must be either (int's, float's, char or string)
     *          or must be constructable from a elSerializer object via the
     *          CTor
     */
    template < typename... Targs >
    bool
    Deserialize( Targs&... args ) const;

  private:
    static constexpr char      delimiter = ':';
    std::vector< std::string > entry;
    bool                       isValid = true;

    elSerializer&
    Add( const std::string& value );

    elSerializer&
    Add( const elSerializer& value );

    template < typename T >
    bool
    DeserializeTypeAtIndex( const elSerializer& s, const size_t index,
                            T& t ) const;

    template < typename T, typename... Targs >
    bool
    DeserializeTypeAtIndex( const elSerializer& s, const size_t index, T& t,
                            Targs&... args ) const;

    template < typename T >
    static typename std::enable_if<
        std::is_convertible< T, elSerializer >::value &&
            !std::is_same< T, std::string >::value,
        std::string >::type
    ToString( const T& t );

    template < typename T >
    static typename std::enable_if<
        // all types which are convertible with stream operator
        std::is_arithmetic< T >::value ||
            // string
            std::is_same< T, std::string >::value ||
            // char[N]
            ( std::is_array< T >::value&& std::is_same<
                typename std::remove_extent< T >::type, char >::value ),
        std::string >::type
    ToString( const T& t );

    template < typename T >
    static
        typename std::enable_if< std::is_convertible< T, std::string >::value,
                                 bool >::type
        FromString( const std::string& value, T& t );

    template < typename T >
    static
        typename std::enable_if< !std::is_convertible< T, std::string >::value,
                                 bool >::type
        FromString( const std::string& value, T& t );

    static bool
    ConvertDirectlyFromString( const std::string& value, int& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, long int& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, long long int& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, unsigned int& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, unsigned long int& t );
    static bool
    ConvertDirectlyFromString( const std::string&      value,
                               unsigned long long int& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, float& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, double& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, long double& t );
    static bool
    ConvertDirectlyFromString( const std::string& value, char& t );
};
} // namespace utils
} // namespace el3D

#include "utils/elSerializer.inl"
