#pragma once
#include <filesystem>
#include <string>
#include <vector>

namespace el3D
{
namespace utils
{
class elFileSystem
{
  public:
    struct file_t
    {
        std::filesystem::path      path;
        std::filesystem::file_type type;
        size_t                     size;
        std::string                sizeHumanReadable;
    };

    static std::string
    FileSizeHumanReadable( const size_t size ) noexcept;

    static std::vector< file_t >
    GetDirectoryContents( const std::string& path ) noexcept;

    static std::vector< std::string >
    ConvertStringToVectorPath( const std::string& v ) noexcept;

    static std::string
    ConvertVectorPathToString( const std::vector< std::string >& v ) noexcept;

    static std::string
    FileTypeToString( const std::filesystem::file_type type ) noexcept;

    static std::string
    RemoveDoubleDelimiterFromStringPath( const std::string& path ) noexcept;

    static std::string
    GetCurrentPath() noexcept;

    static std::string
    GetAbsolutPath( const std::string& path ) noexcept;

    static std::string
    GetParentPath( const std::string& path ) noexcept;

    static std::string
    GetFileName( const std::string& path ) noexcept;

    static bool
    IsDirectory( const std::string& path ) noexcept;

    static bool
    IsRegularFile( const std::string& path ) noexcept;
#ifndef _WIN32
    static constexpr char PATH_DELIMITER = '/';
    static constexpr char ROOT_PATH[]    = "/";
#else
    static constexpr char PATH_DELIMITER = '\\';
    static constexpr char ROOT_PATH[]    = "C:\\";
#endif
};
} // namespace utils
} // namespace el3D
