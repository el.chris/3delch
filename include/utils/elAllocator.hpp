#pragma once

#include <cstdint>
#include <functional>

namespace el3D
{
namespace utils
{
template < typename T >
class elAllocator
{
  public:
    using value_type = T;

    using allocator_t   = std::function< T*( uint64_t ) >;
    using deallocator_t = std::function< void( T*, uint64_t ) >;

    elAllocator() noexcept;
    elAllocator( const allocator_t&   allocator,
                 const deallocator_t& deallocator ) noexcept;

    T*
    allocate( const uint64_t numberOfObjects ) noexcept;

    void
    deallocate( T* memory, const uint64_t numberOfObjects ) noexcept;

    bool
    operator==( const elAllocator& rhs ) const noexcept;
    bool
    operator!=( const elAllocator& rhs ) const noexcept;

  private:
    bool          isDefaultConstructed;
    allocator_t   allocator;
    deallocator_t deallocator;
};
} // namespace utils
} // namespace el3D

#include "utils/elAllocator.inl"
