#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/serializerTypes.hpp"

namespace el3D
{
namespace utils
{
class Serializable
{
  public:
    virtual ~Serializable() = default;

    virtual uint64_t
    GetSerializedSize() const noexcept = 0;
    virtual uint64_t
    Serialize( const Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept = 0;
    virtual bb::elExpected< ByteStreamError >
    Deserialize( const Endian endianness, const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept = 0;
};
} // namespace utils
} // namespace el3D
