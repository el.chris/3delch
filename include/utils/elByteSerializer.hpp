#pragma once

#include "buildingBlocks/type_traits_extended.hpp"
#include "units/BasicUnit.hpp"
#include "utils/Serializable.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/memcpy_pp.hpp"
#include "utils/serializerTypes.hpp"

#include <cstdint>
#include <string>
#include <vector>


namespace el3D
{
namespace utils
{
namespace internal
{
template < typename T >
struct IsNotDirectlyConvertable
{
    static constexpr bool value =
        !std::is_base_of_v< units::BasicUnit< T >, T > &&
        !std::is_same_v< T, utils::byteStream_t > &&
        !std::is_same_v< std::string, T > && !bb::isVariant< T >::value &&
        !bb::isVector< T >::value && !bb::isArray< T >::value &&
        !std::is_integral_v< T > && !std::is_floating_point_v< T >;
};
} // namespace internal

class elByteSerializer
{
  public:
    void
    Clear() noexcept;

    const utils::byteStream_t&
    GetByteStream() const noexcept;

    template < typename... Targs >
    elByteSerializer&
    Serialize( const Endian endianness, const Targs&... args ) noexcept;

    template < typename... Targs >
    static uint64_t
    FillByteStreamWithSerialization( const Endian         endianness,
                                     utils::byteStream_t& byteStream,
                                     const uint64_t       byteStreamOffset,
                                     const Targs&... args ) noexcept;

    template < typename T >
    static uint64_t
    GetSerializedSize( const T& t ) noexcept;

    template < typename T, typename... Targs >
    static uint64_t
    GetSerializedSize( const T& t, const Targs&... args ) noexcept;

    friend struct ConvertVariantVisitor_t;

  private:
    template < typename T >
    static uint64_t
    FillByteStreamWithSerializationImpl( const Endian         endianness,
                                         utils::byteStream_t& byteStream,
                                         const uint64_t       byteStreamOffset,
                                         const uint64_t       currentSize,
                                         const T&             t ) noexcept;

    template < typename T, typename... Targs >
    static uint64_t
    FillByteStreamWithSerializationImpl( const Endian         endianness,
                                         utils::byteStream_t& byteStream,
                                         const uint64_t       byteStreamOffset,
                                         const uint64_t currentSize, const T& t,
                                         const Targs&... args ) noexcept;


    template < typename T >
    static std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                             uint64_t >
    GetStreamSizeFor( const T& ) noexcept;
    template < typename T >
    static std::enable_if_t<
        std::is_integral_v< T > || std::is_floating_point_v< T >, uint64_t >
    GetStreamSizeFor( const T& ) noexcept;

    template < typename T >
    static std::enable_if_t< bb::isVector< T >::value, uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    static std::enable_if_t< bb::isArray< T >::value, uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    static std::enable_if_t< bb::isVariant< T >::value, uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    static std::enable_if_t< std::is_same_v< std::string, T >, uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    static std::enable_if_t< std::is_same_v< T, utils::byteStream_t >,
                             uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    static std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                             uint64_t >
    GetStreamSizeFor( const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_floating_point_v< T >, uint64_t > static Convert(
        const Endian endianness, utils::byteStream_t& memory,
        const uint64_t offset, const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_integral_v< T > && std::is_unsigned_v< T >,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_integral_v< T > && std::is_signed_v< T >,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< bb::isVector< T >::value, uint64_t > static Convert(
        const Endian endianness, utils::byteStream_t& memory,
        const uint64_t offset, const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< bb::isArray< T >::value, uint64_t > static Convert(
        const Endian endianness, utils::byteStream_t& memory,
        const uint64_t offset, const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< bb::isVariant< T >::value, uint64_t > static Convert(
        const Endian endianness, utils::byteStream_t& memory,
        const uint64_t offset, const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_same_v< T, std::string >,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_same_v< T, utils::byteStream_t >,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    template < typename T >
    std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                      uint64_t > static Convert( const Endian endianness,
                                                 utils::byteStream_t& memory,
                                                 const uint64_t       offset,
                                                 const T& t ) noexcept;

    struct StreamSizeVariantVisitor_t
    {
        template < typename T >
        uint64_t
        operator()( T& t );
    };

    struct ConvertVariantVisitor_t
    {
        ConvertVariantVisitor_t( const Endian         endianness,
                                 utils::byteStream_t& memory,
                                 const uint64_t       offset )
            : endianness( endianness ), memory( memory ), offset( offset )
        {
        }
        template < typename T >
        uint64_t
        operator()( T& t );

        Endian        endianness;
        byteStream_t& memory;
        uint64_t      offset;
    };


    uint64_t            currentOffset{ 0 };
    utils::byteStream_t byteStream;
};
} // namespace utils
} // namespace el3D

#include "utils/elByteSerializer.inl"
