#pragma once

#include <atomic>
#include <functional>

namespace el3D
{
namespace utils
{
template < typename T >
class elReferenceCounter
{
  public:
    elReferenceCounter(
        const std::function< void() >& callWhenNoMoreReferencesExist =
            std::function< void() >() ) noexcept;
    elReferenceCounter( const elReferenceCounter& rhs ) noexcept;
    elReferenceCounter( elReferenceCounter&& rhs ) noexcept;
    ~elReferenceCounter();

    elReferenceCounter&
    operator=( const elReferenceCounter& rhs ) noexcept;
    elReferenceCounter&
    operator=( elReferenceCounter&& rhs ) noexcept;

    T
    GetValue() const noexcept;

  private:
    void
    IncrementReferenceCounter() noexcept;
    void
    DecrementReferenceCounter() noexcept;

  private:
    std::atomic< T >*       value;
    std::function< void() > callWhenNoMoreReferencesExist;
};
} // namespace utils
} // namespace el3D

#include "utils/elReferenceCounter.inl"
