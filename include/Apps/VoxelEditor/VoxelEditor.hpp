#pragma once

#include "Apps/VoxelEditor/Coordinates.hpp"
#include "Apps/VoxelEditor/VoxelObject.hpp"
#include "GLAPI/elEvent.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
struct voxelEditorProperties_t
{
    SDL_KeyCode highlightToggleKey        = SDLK_x;
    SDL_KeyCode increaseHighlightLevelKey = SDLK_r;
    SDL_KeyCode decreaseHighlightLevelKey = SDLK_f;

    CoordinatesProperties coordinates;
};

class VoxelEditor : public world::elWorld
{
  public:
    VoxelEditor( _3Delch::elScene&              scene,
                 const voxelEditorProperties_t& p ) noexcept;

    const Coordinates&
    GetCoordinates() const noexcept;

  private:
    void
    KeyboardCallback( const SDL_Event e ) noexcept;
    void
    PerformVoxelBuildInteraction() noexcept;
    void
    LeftClickCoordinatesCallback( const glm::uvec3 p ) noexcept;

  private:
    voxelEditorProperties_t           properties;
    VoxelObject                       voxelObject;
    bb::product_ptr< world::elSun >   sun;
    bb::product_ptr< world::elSky >   sky;
    Coordinates                       coordinates;
    bb::product_ptr< GLAPI::elEvent > keyboardEvents;
};
} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D
