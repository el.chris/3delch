#pragma once

#include "HighGL/elEventTexture.hpp"
#include "HighGL/elLight_DirectionalLight.hpp"
#include "HighGL/elLight_PointLight.hpp"
#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elFrameBufferObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "RenderPipeline/elRenderer_Cleanup.hpp"
#include "RenderPipeline/elRenderer_Deferred.hpp"
#include "RenderPipeline/elRenderer_Final.hpp"
#include "RenderPipeline/elRenderer_Forward.hpp"
#include "RenderPipeline/elRenderer_GUI.hpp"
#include "RenderPipeline/elRenderer_PostProcessing.hpp"
#include "RenderPipeline/elRenderer_ShaderTexture.hpp"
#include "RenderPipeline/internal.hpp"
#include "buildingBlocks/elGenericFactory.hpp"
#include "units/Time.hpp"

namespace el3D
{
namespace lua
{
class elConfigHandler;
}

namespace RenderPipeline
{


class elRenderPipeline
{
  public:
    elRenderPipeline( const std::string                &sceneName,
                      const lua::elConfigHandler *const config,
                      HighGL::elEventTexture *const     eventTexture,
                      const GLAPI::elWindow *const      window,
                      GLAPI::elFontCache *const         fontCache,
                      GLAPI::elEventHandler *const      eventHandler );
    elRenderPipeline( const elRenderPipeline & ) = delete;
    elRenderPipeline( elRenderPipeline && )      = delete;
    ~elRenderPipeline();

    elRenderPipeline &
    operator=( const elRenderPipeline & ) = delete;
    elRenderPipeline &
    operator=( elRenderPipeline && ) = delete;

    void
    Render() noexcept;

    const OpenGL::elCamera_Perspective &
    GetCamera() const noexcept;
    OpenGL::elCamera_Perspective &
    GetCamera() noexcept;

    bb::product_ptr< HighGL::elLight_PointLight >
    CreatePointLight( const glm::vec3 &color, const glm::vec3 &attenuation,
                      const glm::vec3 &position, const float diffuseIntensity,
                      const float ambientIntensity ) noexcept;

    bb::product_ptr< HighGL::elLight_DirectionalLight >
    CreateDirectionalLight( const glm::vec3 &color, const glm::vec3 &direction,
                            const GLfloat diffuseIntensity,
                            const GLfloat ambientIntensity ) noexcept;

    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept;

    void
    DrawOutputToScreen( const GLint x, const GLint y ) const noexcept;

    glm::vec4
    GetEventColorAtPosition( const glm::ivec2 position ) const noexcept;

    size_t
    RegisterShaderTexture( const HighGL::elShaderTexture *const v ) noexcept;

    void
    DeregisterShaderTexture( const size_t id ) noexcept;

    elRenderer_GUI *
    GetGUIRenderer() noexcept;

    elRenderer_Deferred *
    GetDeferredRenderer() noexcept;

    elRenderer_Forward *
    GetForwardRenderer() noexcept;

    void
    UseSkyboxForReflection( const bool v ) noexcept;

    void
    RegisterSkyboxTexture( const OpenGL::elTexture_CubeMap &texture ) noexcept;

    void
    ResetSkyboxTexture() noexcept;

  private:
    template < typename render_T, typename... Targs >
    render_T *
    AddRenderer( const GLAPI::elWindow *const window, const Targs &...t );

    enum fullResTextures_t
    {
        TEX_FINAL = 0,
        TEX_GLOW,
        TEX_MATERIAL,
        TEX_EVENT,
        TEX_DEPTH,
    };

    struct renderPipe_t
    {
        void
        emplace_back( elRenderer_base *const renderer ) noexcept;
        bool
        erase( const elRenderer_base *const renderer ) noexcept;

        std::vector< elRenderer_base * > all;
        std::vector< elRenderer_base * > top;
        std::vector< elRenderer_base * > scene;
    };

    std::vector< std::unique_ptr< OpenGL::elTexture_2D > > outputTextures;
    struct renderer_t
    {
        std::unique_ptr< elRenderer_Cleanup >        cleanupTextures;
        std::unique_ptr< elRenderer_ShaderTexture >  shaderTexture;
        std::unique_ptr< elRenderer_GUI >            gui;
        std::unique_ptr< elRenderer_Deferred >       deferred;
        std::unique_ptr< elRenderer_Forward >        forward;
        std::unique_ptr< elRenderer_PostProcessing > postProcessing;
        std::unique_ptr< elRenderer_Final >          final;
    } renderer;

    std::string                      sceneName;
    const lua::elConfigHandler      *config;
    HighGL::elEventTexture          *eventTexture{ nullptr };
    const OpenGL::elTexture_CubeMap *optionalSkyboxTexture{ nullptr };
    uint64_t                         maxDirectionalLights = 0u;
    OpenGL::elCamera_Perspective     camera;
    renderPipe_t                     renderPipe;

    // should be always last member / cleanup can access other members
    bb::elGenericFactory< HighGL::elLight_PointLight,
                          HighGL::elLight_DirectionalLight >
        factory;
};
} // namespace RenderPipeline
} // namespace el3D

#include "RenderPipeline/elRenderPipeline.inl"
