#pragma once

#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "buildingBlocks/elGenericFactory.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "lua/elConfigHandler.hpp"

#include <GL/glew.h>

namespace el3D
{
namespace HighGL
{
class elLight_DirectionalLight;
class elLight_PointLight;
class elEventTexture;
} // namespace HighGL

namespace RenderPipeline
{
namespace internal
{
struct rendererShared_t
{
    std::string                       sceneName;
    const lua::elConfigHandler*       config{ nullptr };
    const GLAPI::elWindow*            window{ nullptr };
    OpenGL::elTexture_2D*             finalTexture;
    OpenGL::elTexture_2D*             depthTexture;
    OpenGL::elTexture_2D*             eventTexture;
    OpenGL::elTexture_2D*             glowTexture;
    OpenGL::elTexture_2D*             materialTexture;
    const OpenGL::elTexture_CubeMap** optionalSkyboxTexture;
    HighGL::elEventTexture*           eventManager;
    std::vector< bb::product_ptr< HighGL::elLight_DirectionalLight > >*
        directionalLights;
    std::vector< bb::product_ptr< HighGL::elLight_PointLight > >* pointLights;
};

} // namespace internal
} // namespace RenderPipeline
} // namespace el3D
