#pragma once

#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "RenderPipeline/elPostEffect_base.hpp"

namespace el3D
{
namespace lua
{
class elConfigHandler;
}

namespace RenderPipeline
{
class elPostEffect_Glow : public elPostEffect_base
{
  public:
    elPostEffect_Glow( const std::string&                sceneName,
                       const std::string&                blurType,
                       const lua::elConfigHandler* const config,
                       const OpenGL::elTexture_2D* const rawBlur ) noexcept;
    void
    Render( const OpenGL::elCamera_Perspective& ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;
    OpenGL::elTexture_2D*
    GetOutput() noexcept override;
    void
    SetIntensity( const float intensity ) noexcept;

  private:
    struct shader_t
    {
        std::string file;
        std::string group;
    };

    shader_t
    GetShaderFileAndGroup( const lua::elConfigHandler* const config,
                           const std::string&                sceneName,
                           const std::string&                blurType,
                           const std::string&                shaderEntry,
                           const std::string&                defaultShader,
                           const std::string& defaultGroup ) const noexcept;

    void
    CreateBlurShader( const std::string& sceneName, const std::string& blurType,
                      const lua::elConfigHandler* const config ) noexcept;

  private:
    static constexpr glm::vec4 GAUSS_5BI_OFFSET{ 0.0, 1.3846153846,
                                                 3.2307692308, 0.0 };
    static constexpr glm::vec4 GAUSS_5BI_WEIGHT{ 0.2270270270, 0.3162162162,
                                                 0.0702702703, 0.0 };
    static constexpr GLint     GAUSS_5BI_ITERATIONS = 3;

    float scaling         = 0.25f;
    float intensity       = 1.175f;
    float stepWidthFactor = 1.0f;

    std::optional< HighGL::elShaderTexture > blurShader;
    const OpenGL::elCamera_Perspective*      camera{ nullptr };
    const OpenGL::elTexture_2D*              rawBlur{ nullptr };
};
} // namespace RenderPipeline
} // namespace el3D
