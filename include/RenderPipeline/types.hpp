#pragma once

#include "OpenGL/types.hpp"

namespace el3D
{
namespace RenderPipeline
{
class elRenderer_Forward;
class elRenderer_Deferred;

template < OpenGL::RenderTarget >
struct TargetToRenderer_t;

template <>
struct TargetToRenderer_t< OpenGL::RenderTarget::Forward >
{
    using type = elRenderer_Forward;
};

template <>
struct TargetToRenderer_t< OpenGL::RenderTarget::Deferred >
{
    using type = elRenderer_Deferred;
};

template < OpenGL::RenderTarget Target >
using TargetToRenderer = typename TargetToRenderer_t< Target >::type;

enum class ObjectType
{
    VertexObject,
    NonVertexObject,
    UnifiedVertexObject,
    InstancedObject
};

constexpr const char *OBJECT_TYPE_NAME[] = { "vertexObject", "nonVertexObject",
                                             "unifiedVertexObject",
                                             "instancedObject" };

} // namespace RenderPipeline
} // namespace el3D
