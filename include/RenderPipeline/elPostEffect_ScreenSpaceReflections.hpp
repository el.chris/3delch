#pragma once

#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "RenderPipeline/elPostEffect_base.hpp"
#include "lua/elConfigHandler.hpp"

namespace el3D
{
namespace RenderPipeline
{
class elPostEffect_ScreenSpaceReflections : public elPostEffect_base
{
  public:
    elPostEffect_ScreenSpaceReflections(
        const std::string& sceneName, const lua::elConfigHandler& config,
        const OpenGL::elTexture_2D&       positionTexture,
        const OpenGL::elTexture_2D&       normalTexture,
        const OpenGL::elTexture_2D&       materialTexture,
        const OpenGL::elTexture_2D&       finalTexture,
        const OpenGL::elTexture_CubeMap** optionalSkyboxTexture ) noexcept;

    void
    Render( const OpenGL::elCamera_Perspective& ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;
    OpenGL::elTexture_2D*
    GetOutput() noexcept override;

    void
    UseSkyboxForReflection( const bool v ) noexcept;

  private:
    static constexpr char SSR_SHADER_FILE[] =
        "glsl/PostProcessing/screenSpaceReflection.glsl";
    static constexpr char SSR_SHADER_GROUP[] = "default";

    float                                      scaling = 1.0f;
    std::unique_ptr< HighGL::elShaderTexture > ssrUVShader;
    const OpenGL::elCamera_Perspective*        camera                = nullptr;
    const OpenGL::elTexture_CubeMap**          optionalSkyboxTexture = nullptr;
    const OpenGL::elTexture_CubeMap*           skyboxTexture         = nullptr;
    OpenGL::elTexture_CubeMap                  skyboxPlaceholder;
    bool                                       isOptionalSkyboxAttached = false;
    bool                                       useSkyboxForReflection   = true;
};
} // namespace RenderPipeline
} // namespace el3D
