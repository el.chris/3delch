#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class AngularVelocity : public BasicUnit< AngularVelocity >
{
  public:
    AngularVelocity() noexcept;

    static AngularVelocity
    DegreePerSecond( const float_t value ) noexcept;
    static AngularVelocity
    RadiansPerSecond( const float_t value ) noexcept;

    float_t
    GetRadiansPerSecond() const noexcept;
    float_t
    GetDegreePerSecond() const noexcept;

  private:
    AngularVelocity( const float_t radiansPerSeconds ) noexcept;
};

std::ostream&
operator<<( std::ostream&                       stream,
            const el3D::units::AngularVelocity& v ) noexcept;
} // namespace units
} // namespace el3D
