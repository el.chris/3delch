#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class AngularAcceleration : public BasicUnit< AngularAcceleration >
{
  public:
    AngularAcceleration() noexcept;

    static AngularAcceleration
    RadiansPerSecondSquare( const float_t value ) noexcept;

    float_t
    GetRadiansPerSecondSquare() const noexcept;

  private:
    AngularAcceleration( const float_t radiansPerSecondSquare ) noexcept;
};

std::ostream&
operator<<( std::ostream&                           stream,
            const el3D::units::AngularAcceleration& a ) noexcept;
} // namespace units
} // namespace el3D
