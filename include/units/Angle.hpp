#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Angle : public BasicUnit< Angle >
{
  public:
    Angle() noexcept;

    static Angle
    Degree( const float_t value ) noexcept;
    static Angle
    Radian( const float_t value ) noexcept;

    float_t
    GetDegree() const noexcept;
    float_t
    GetRadian() const noexcept;

  private:
    Angle( const float_t degree ) noexcept;
};

namespace literals
{
Angle operator""_deg( unsigned long long ) noexcept;
Angle operator""_deg( long double ) noexcept;
Angle operator""_rad( unsigned long long ) noexcept;
Angle operator""_rad( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Angle& v ) noexcept;
} // namespace units
} // namespace el3D
