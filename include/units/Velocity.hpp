#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Velocity : public BasicUnit< Velocity >
{
  public:
    Velocity() noexcept;

    static Velocity
    MeterPerSecond( const float_t value ) noexcept;
    static Velocity
    KilometerPerHour( const float_t value ) noexcept;

    float_t
    GetMeterPerSecond() const noexcept;
    float_t
    GetKilometerPerHour() const noexcept;

  private:
    Velocity( const float_t meterPerSeconds ) noexcept;
};
namespace literals
{
Velocity operator""_m_s( unsigned long long ) noexcept;
Velocity operator""_m_s( long double ) noexcept;
Velocity operator""_km_h( unsigned long long ) noexcept;
Velocity operator""_km_h( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Velocity& v ) noexcept;
} // namespace units
} // namespace el3D
