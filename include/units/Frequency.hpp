#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Frequency : public BasicUnit< Frequency >
{
  public:
    Frequency() noexcept;

    static Frequency
    Hz( const float_t value ) noexcept;
    static Frequency
    KHz( const float_t value ) noexcept;
    static Frequency
    MHz( const float_t value ) noexcept;
    static Frequency
    GHz( const float_t value ) noexcept;

    float_t
    GetHz() const noexcept;
    float_t
    GetKHz() const noexcept;
    float_t
    GetMHz() const noexcept;
    float_t
    GetGHz() const noexcept;

  private:
    Frequency( const float_t hertz ) noexcept;
};
namespace literals
{
Frequency operator""_Hz( unsigned long long ) noexcept;
Frequency operator""_Hz( long double ) noexcept;
Frequency operator""_KHz( unsigned long long ) noexcept;
Frequency operator""_KHz( long double ) noexcept;
Frequency operator""_MHz( unsigned long long ) noexcept;
Frequency operator""_MHz( long double ) noexcept;
Frequency operator""_GHz( unsigned long long ) noexcept;
Frequency operator""_GHz( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Frequency& l ) noexcept;
} // namespace units
} // namespace el3D
