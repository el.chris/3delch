#pragma once

#include <SDL.h>
#include <string>

namespace el3D
{
namespace GLAPI
{
// at the moment only a-zA-Z0-9
SDL_Keycode
StringToSDLKeyCode( const std::string& key ) noexcept;

enum class KeyState : uint32_t
{
    DOWN = SDL_KEYDOWN,
    UP   = SDL_KEYUP
};
} // namespace GLAPI
} // namespace el3D
