#pragma once

#define SDL_MAIN_HANDLED
#include "DesignPattern/Creation.hpp"
#include "GLAPI/common/helperTypes.hpp"
#include "GLAPI/elEventHandler.hpp"
#include "logging/elLog.hpp"
#include "units/Time.hpp"
#include "utils/elGenericRAII.hpp"
#include "utils/elThroughputMeasurement.hpp"
#include "utils/macros.hpp"

#include <GL/glew.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <cstdint>
#include <functional>
#include <iostream>
#include <string>
#include <vector>


namespace el3D
{
namespace _3Delch
{
class _3Delch;
}

namespace GLAPI
{
enum class elWindow_Error
{
    UnableToInitSDL2,
    OpenGLCoreProfileNotSupported,
    OpenGLCompatibilityProfileNotSupported,
    OpenGLVersionNotSupported,
    UnableToCreateWindow,
    UnableToInitializeGLContext,
    UnableToInitializeTTFfonts,
    CaptureMouse,
    RelativeMouseMode,
    ShowCursor,
    UnableToSetHintPriority,
    InternalError
};

class elWindow : public DesignPattern::Creation< elWindow, elWindow_Error >
{
  public:
    using reshapeCallback_t      = std::function< void( int, int ) >;
    elWindow( const elWindow & ) = delete;
    elWindow( elWindow && )      = delete;

    elWindow &
    operator=( const elWindow & ) = delete;
    elWindow &
    operator=( elWindow && ) = delete;
    ~elWindow();

    elEventHandler *
    GetEventHandler() noexcept;

    bb::elExpected< elWindow_Error >
    SetCaptureMouse( const bool ) const;
    bb::elExpected< elWindow_Error >
    SetRelativeMouseMode( const bool ) const;
    bb::elExpected< elWindow_Error >
    SetShowCursor( const bool ) const;
    void
    SetWindowGrab( const bool ) const noexcept;
    void
    SetWindowSize( const int x, const int y ) noexcept;
    void
    SetWindowIsResizable( const bool ) noexcept;

    units::Time
    GetRuntime() const noexcept;
    units::Time
    GetDeltaTime() const noexcept;
    windowSize_t
    GetWindowSize() const noexcept;
    glVersion_t
    GetOpenGLVersion() const noexcept;
    int
    GetShaderVersion() const noexcept;
    size_t
    GetFrameCounter() const noexcept;
    float
    GetFramesPerSecond() const noexcept;
    float
    GetDisplayDPI() const noexcept;

    friend class _3Delch::_3Delch;
    friend class elSDL2Image;
    friend struct DesignPattern::Creation< elWindow, elWindow_Error >;

  private:
    elWindow( const int32_t x, const int32_t y, const std::string &name,
              const int glMajor, const int glMinor, const bool useCoreProfile,
              const uint32_t flags, const bool enableVSync );

    bb::elExpected< elWindow_Error >
    SetHintWithPriority( const char *name, const char *value,
                         const SDL_HintPriority priority ) const;
    void
    PrintInfo() const noexcept;
    void
    SetShaderVersion() noexcept;
    void
    CalculateDeltaTime() noexcept;
    void
    Reshape() noexcept;
    void
    MainLoop() noexcept;
    void
    ExitMainLoop( const std::function< void() > &afterExitCallback =
                      std::function< void() >() ) noexcept;
    void
    SetMainLoopCallback( const std::function< uint32_t() > & ) noexcept;
    void
    SetReshapeCallback( const reshapeCallback_t & ) noexcept;
    void
    UpdateFramesPerSecond() noexcept;

  private:
    int32_t                                                   x;
    int32_t                                                   y;
    int                                                       glMajor;
    int                                                       glMinor;
    int                                                       shaderVersion;
    std::string                                               name;
    std::unique_ptr< elEventHandler >                         eventHandler;
    bb::product_ptr< elEvent >                                windowEvent;
    std::shared_ptr< std::remove_pointer_t< SDL_GLContext > > glContext;
    bool                          exitMainLoop = false;
    std::shared_ptr< SDL_Window > window;
    std::function< uint32_t() >   mainLoopCallback;
    reshapeCallback_t             reshapeCallback;
    units::Time                   runtime;
    units::Time                   deltaTime;
    utils::elGenericRAII          sdlQuit;
    utils::elGenericRAII          ttfQuit;


    utils::elThroughputMeasurement< uint64_t > frameCounter{
        units::Time::MilliSeconds( 500.0f ), 0u };

    uint64_t lastTick{ 0 };
    uint64_t firstTick = SDL_GetPerformanceCounter();
    float    performanceFrequency =
        static_cast< float >( SDL_GetPerformanceFrequency() );
    std::function< void() > afterExitCallback;
};
} // namespace GLAPI
} // namespace el3D
