if ( WIN32 )
    include( ${CMAKE_CURRENT_LIST_DIR}/win32.cmake)
elseif ( UNIX )
    include( ${CMAKE_CURRENT_LIST_DIR}/unix.cmake)
else ()
    message ( FATAL_ERROR "Platform not supported, aborting" )
endif ()

