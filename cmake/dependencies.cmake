if(STATIC_LINKAGE)
    set(BUILD_SHARED_LIBS OFF)
    set(BUILD_STATIC_LIBS ON)
else()
    set(BUILD_SHARED_LIBS ON)
    set(BUILD_STATIC_LIBS OFF)
endif()

#################
### add glm
#################
FetchContent_Declare(
    glm
    GIT_REPOSITORY https://github.com/g-truc/glm.git
    GIT_TAG 0.9.9.8
)
FetchContent_GetProperties(glm)
if (NOT glm_POPULATED AND NOT TARGET glm)
    message(STATUS "updating: glm" )
    FetchContent_Populate(glm)
    if(STATIC_LINKAGE)
        set(BUILD_STATIC_LIBS 1)
    endif()
    add_subdirectory(${glm_SOURCE_DIR} ${glm_BINARY_DIR})
endif()
#add_library(LIB_GLM ALIAS glm::glm)

#################
### add sdl2
#################
FetchContent_Declare(
    sdl2
    GIT_REPOSITORY https://github.com/SDL-mirror/SDL
    GIT_TAG release-2.0.12
)
FetchContent_GetProperties(sdl2)
if (NOT sdl2_POPULATED)
    message(STATUS "updating: sdl2" )
    FetchContent_Populate(sdl2)
    add_subdirectory(${sdl2_SOURCE_DIR} ${sdl2_BINARY_DIR})
endif()
set(SDL2_DIR ${sdl2_SOURCE_DIR})
if (STATIC_LINKAGE)
    if ( WIN32 )
        target_link_libraries(SDL2-static vcruntime)
    endif()
    add_library(LIB_SDL2 ALIAS SDL2-static) 
else()
    if ( WIN32 )
        target_link_libraries(SDL2 vcruntime)
    endif()
    add_library(LIB_SDL2 ALIAS SDL2)
endif()

#################
### add glew
#################
FetchContent_Declare(
    glew
    GIT_REPOSITORY https://github.com/Perlmint/glew-cmake.git
    GIT_TAG glew-cmake-2.1.0
)
FetchContent_GetProperties(glew)
if (NOT glew_POPULATED)
    message(STATUS "updating: glew" )
    FetchContent_Populate(glew)
    add_subdirectory(${glew_SOURCE_DIR} ${glew_BINARY_DIR})
endif()   
if (STATIC_LINKAGE)
    add_library(LIB_GLEW ALIAS libglew_static)
else()
    add_library(LIB_GLEW ALIAS libglew_shared)
endif()

#################
### add lua
#################
FetchContent_Declare(
    lua
    GIT_REPOSITORY https://github.com/lua/lua.git
    GIT_TAG v5.3.5
)
FetchContent_GetProperties(lua)
if ( NOT lua_POPULATED ) 
    message(STATUS "updating: lua")
    FetchContent_Populate(lua)
    FILE(GLOB LUA_SOURCES ${lua_SOURCE_DIR}/*.c ${lua_SOURCE_DIR}/*.h)
    if (STATIC_LINKAGE)
        add_library(LIB_LUA STATIC ${LUA_SOURCES})
    else()
        add_library(LIB_LUA SHARED ${LUA_SOURCES})
    endif()

    target_include_directories(LIB_LUA
      PUBLIC
      ${LUA_SOURCE_DIR}
    )
    set(LUA_INCLUDE_DIR ${LUA_SOURCE_DIR})
    set_target_properties(LIB_LUA PROPERTIES COMPILE_FLAGS "${C_FLAGS} ${CXX_DISABLE_WARNINGS}")
endif()

#################
### add zlib
#################
FetchContent_Declare(
    zlib
    GIT_REPOSITORY https://github.com/madler/zlib
    GIT_TAG v1.2.11
)
FetchContent_GetProperties(zlib)
if (NOT zlib_POPULATED)
    message(STATUS "updating: zlib" )
    FetchContent_Populate(zlib)
    add_subdirectory(${zlib_SOURCE_DIR} ${zlib_BINARY_DIR})
    set(ZLIB_INCLUDE_DIR ${zlib_SOURCE_DIR} ${zlib_BINARY_DIR})
endif()

#################
### add libjpeg
#################
FetchContent_Declare(
    libjpeg
    GIT_REPOSITORY https://github.com/csparker247/jpeg-cmake
    GIT_TAG v1.1.0
)
FetchContent_GetProperties(libjpeg)
if (NOT libjpeg_POPULATED)
    message(STATUS "updating: libjpeg" )
    FetchContent_Populate(libjpeg)
    add_subdirectory(${libjpeg_SOURCE_DIR} ${libjpeg_BINARY_DIR})
    set(LIBJPEG_INCLUDE_DIR ${libjpeg_SOURCE_DIR}/libjpeg)
endif()

#################
### add libpng
#################
FetchContent_Declare(
    libpng
    GIT_REPOSITORY https://github.com/glennrp/libpng
    GIT_TAG v1.6.35
)
FetchContent_GetProperties(libpng)
if (NOT libpng_POPULATED)
    message(STATUS "updating: libpng" )
    set(PNG_BUILD_ZLIB ON)
    set(PNG_SHARED OFF)
    set(PNG_TESTS OFF)
    FetchContent_Populate(libpng)
    add_subdirectory(${libpng_SOURCE_DIR} ${libpng_BINARY_DIR})
    set(LIBPNG_INCLUDE_DIR ${libpng_SOURCE_DIR} ${libpng_BINARY_DIR})
    include_directories(${ZLIB_INCLUDE_DIR})
endif()



#################
### add sdl2image
#################
FetchContent_Declare(
    sdl2image
    URL https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.5.tar.gz
)
FetchContent_GetProperties(sdl2image)
if (NOT sdl2image_POPULATED)
    message(STATUS "updating: sdl2image" )
    FetchContent_Populate(sdl2image)
    FILE(GLOB SDL2IMAGE_SOURCES ${sdl2image_SOURCE_DIR}/*.c)
    if(STATIC_LINKAGE)
        add_library(LIB_SDL2IMAGE STATIC ${SDL2IMAGE_SOURCES} )
    else()
        add_library(LIB_SDL2IMAGE SHARED ${SDL2IMAGE_SOURCES} )
    endif()
    target_compile_definitions(LIB_SDL2IMAGE
        PRIVATE 
            -DLOAD_BMP 
            -DLOAD_GIF 
            -DLOAD_LBM 
            -DLOAD_PCX 
            -DLOAD_PNM
            -DLOAD_TGA 
            -DLOAD_XCF 
            -DLOAD_XPM 
            -DLOAD_XV 
            -DLOAD_XPM
            -DLOAD_JPG
            -DLOAD_PNG
            #-DLOAD_WEBP
    )
    target_include_directories(LIB_SDL2IMAGE PUBLIC ${sdl2image_SOURCE_DIR} ${LIBJPEG_INCLUDE_DIR} ${LIBPNG_INCLUDE_DIR})
    target_link_libraries(LIB_SDL2IMAGE LIB_SDL2 jpeg_static png_static)
endif()

#################
### add freetype
#################
FetchContent_Declare(
    freetype2
    GIT_REPOSITORY https://github.com/aseprite/freetype2
    GIT_TAG VER-2-10-0
)
FetchContent_GetProperties(freetype2)
if (NOT freetype2_POPULATED)
    message(STATUS "updating: freetype2" )
    FetchContent_Populate(freetype2)
   add_subdirectory(${freetype2_SOURCE_DIR} ${freetype2_BINARY_DIR})
endif()

#################
### add sdl2ttf
#################
FetchContent_Declare(
    sdl2ttf
    URL https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.15.tar.gz
)
FetchContent_GetProperties(SDL2TTF)
if (NOT sdl2ttf_POPULATED)
    message(STATUS "updating: sdl2ttf" )
    FetchContent_Populate(sdl2ttf)

    if(STATIC_LINKAGE)
        add_library(LIB_SDL2TTF STATIC ${sdl2ttf_SOURCE_DIR}/SDL_ttf.c)
    else()
        add_library(LIB_SDL2TTF STATIC ${sdl2ttf_SOURCE_DIR}/SDL_ttf.c)
    endif()

    target_link_libraries(LIB_SDL2TTF LIB_SDL2 freetype)
    target_include_directories(LIB_SDL2TTF
        PUBLIC 
        ${sdl2ttf_SOURCE_DIR}
    )
endif()

#################
### add assimp
#################
FetchContent_Declare(
    ASSIMP
    GIT_REPOSITORY https://github.com/assimp/assimp.git
    GIT_TAG v5.0.0
)
FetchContent_GetProperties(ASSIMP)
if (NOT assimp_POPULATED)
    message(STATUS "updating: assimp" )
    FetchContent_Populate(ASSIMP)
    set(ASSIMP_BUILD_TESTS OFF)
    set(ASSIMP_BUILD_ASSIMP_TOOLS OFF)

    if(STATIC_LINKAGE)
        set(BUILD_SHARED_LIBS OFF)
    else()
        set(BUILD_SHARED_LIBS ON)
    endif()
    add_subdirectory(${assimp_SOURCE_DIR} ${assimp_BINARY_DIR})
    set_target_properties(assimp PROPERTIES COMPILE_FLAGS "${CXX_FLAGS} ${CXX_DISABLE_WARNINGS}")
endif()
add_library(LIB_ASSIMP ALIAS assimp)

#################
### add bullet
#################
FetchContent_Declare(
    BULLET
    GIT_REPOSITORY https://github.com/bulletphysics/bullet3
    GIT_TAG 3.09
)
FetchContent_GetProperties(BULLET)
if (NOT bullet_POPULATED)
    message(STATUS "updating: bullet" )
    FetchContent_Populate(BULLET)

    set(BUILD_EXTRAS OFF)
    set(BUILD_BULLET2_DEMOS OFF)
    set(BUILD_OPENGL3_DEMOS OFF)
    set(BUILD_UNIT_TESTS OFF)
    set(BUILD_PYBULLET OFF)
    set(BUILD_CPU_DEMOS OFF)

    add_subdirectory(${bullet_SOURCE_DIR} ${bullet_BINARY_DIR})
endif()
target_include_directories(Bullet3Collision PUBLIC ${bullet_SOURCE_DIR}/src)
target_link_libraries(Bullet3Collision PUBLIC LinearMath Bullet3Dynamics Bullet3Common Bullet3Geometry BulletSoftBody BulletInverseDynamics BulletDynamics)
add_library(LIB_BULLET ALIAS Bullet3Collision)
