if ( UNIX )
    include( FindPkgConfig )
    find_package( PkgConfig REQUIRED )
    find_package(Threads)
endif(UNIX)

function(my_add_library)
    if(NOT "${LIBRARY_HEADER}" STREQUAL "ON") 
        message("")
        message("       Libraries")
    endif()
    set(LIBRARY_HEADER "ON" PARENT_SCOPE )

    cmake_parse_arguments(f "" "TARGET" "DEPENDS;LINK" ${ARGN} )

    file ( GLOB_RECURSE LIBRARY_SRC
            "${CMAKE_CURRENT_LIST_DIR}/src/${f_TARGET}/*.cpp"
         )
    if ( STATIC_LINKAGE )
        add_library( ${f_TARGET} STATIC ${LIBRARY_SRC} )
    else()
        add_library( ${f_TARGET} SHARED ${LIBRARY_SRC} )
    endif()

    target_include_directories( ${f_TARGET} 
      PUBLIC 
        ${CMAKE_CURRENT_LIST_DIR}/include/
        ${CMAKE_CURRENT_LIST_DIR}/inline/
    )

    set_property(TARGET ${f_TARGET} PROPERTY CXX_STANDARD 20)

    if ( f_DEPENDS )
        add_dependencies(${f_TARGET} ${f_DEPENDS})
        target_link_libraries( ${f_TARGET} PUBLIC ${f_DEPENDS} )
    endif ( f_DEPENDS )

    if ( f_LINK )
        target_link_libraries( ${f_TARGET} PUBLIC ${f_LINK} )
    endif(f_LINK)

    if(CLANG_TIDY_EXE)
        set_target_properties(
            ${f_TARGET} PROPERTIES CXX_CLANG_TIDY "${PERFORM_CLANG_TIDY}"
            )
    endif(CLANG_TIDY_EXE)

    message("           ${f_TARGET}")
endfunction(my_add_library)

function(my_add_app)
    if(NOT "${APPS_HEADER}" STREQUAL "ON") 
        message("")
        message("       Apps")
    endif()
    set(APPS_HEADER "ON" PARENT_SCOPE )

    cmake_parse_arguments(f "" "TARGET" "DEPENDS;LINK" ${ARGN} )

    file ( GLOB_RECURSE APP_SRC
        "${CMAKE_CURRENT_LIST_DIR}/src/Apps/${f_TARGET}/*.cpp"
         )
    add_executable( ${f_TARGET} ${APP_SRC} )

    target_include_directories( ${f_TARGET} 
      PUBLIC 
        ${CMAKE_CURRENT_LIST_DIR}/include/
        ${CMAKE_CURRENT_LIST_DIR}/inline/
    )

    set_property(TARGET ${f_TARGET} PROPERTY CXX_STANDARD 20)

    if ( f_DEPENDS )
        add_dependencies(${f_TARGET} ${f_DEPENDS})
        target_link_libraries( ${f_TARGET} PUBLIC ${f_DEPENDS} )
    endif ( f_DEPENDS )

    if ( f_LINK )
        target_link_libraries( ${f_TARGET} PUBLIC ${f_LINK} )
    endif(f_LINK)

    if(CLANG_TIDY_EXE)
        set_target_properties(
            ${f_TARGET} PROPERTIES CXX_CLANG_TIDY "${PERFORM_CLANG_TIDY}"
            )
    endif(CLANG_TIDY_EXE)

    message("           ${f_TARGET}")
endfunction(my_add_app)



function(add_system_dependency)
    cmake_parse_arguments(f "" "TARGET;LIBRARY" "" ${ARGN} )
    message(STATUS "adding system dependency: ${f_LIBRARY}")

    pkg_search_module ( ${f_TARGET} QUIET ${f_LIBRARY} )
    if ( ${f_TARGET}_FOUND )
        add_library(${f_TARGET} IMPORTED INTERFACE)
        target_link_libraries(${f_TARGET} INTERFACE ${${f_TARGET}_LIBRARIES})
        target_include_directories(${f_TARGET} INTERFACE ${${f_TARGET}_INCLUDE_DIRS})
    else()
        find_package(${f_LIBRARY} QUIET)
        STRING(TOUPPER ${f_LIBRARY} HINT)
        if( ${HINT}_FOUND )
            add_library(${f_TARGET} IMPORTED INTERFACE)
            target_link_libraries(${f_TARGET} INTERFACE ${${HINT}_LIBRARIES})
            target_include_directories(${f_TARGET} INTERFACE ${${HINT}_INCLUDE_DIRS} ${${HINT}_INCLUDE_DIR})
        else()
            message(FATAL_ERROR "could not find required library ${f_LIBRARY}")
        endif(${HINT}_FOUND)
    endif()
endfunction(add_system_dependency)

function(add_option)
    cmake_parse_arguments(f "" "NAME;DESCRIPTION;VALUE" "" ${ARGN} )

    if(NOT "${OPTION_HEADER}" STREQUAL "ON") 
        message("")
        message("       Options")
    endif()
    set(OPTION_HEADER "ON" PARENT_SCOPE )

    if(NOT DEFINED ${f_NAME})
        set(SET_VARIABLE_AFTER_OPTION ON)
    else()
        set(SET_VARIABLE_AFTER_OPTION OFF)
    endif()
    option(${f_NAME} ${f_DESCRIPTION} ${f_VALUE} )

    if(SET_VARIABLE_AFTER_OPTION)
        set(${f_NAME} ${f_VALUE} CACHE INTERNAL ${f_NAME})
    endif()

    string(LENGTH ${f_NAME} OPT_LENGTH)

    set(NAME ${f_NAME})
    foreach(LOOP_VAR RANGE ${OPT_LENGTH} 25 1)
        string(APPEND NAME ".")
    endforeach(LOOP_VAR)

    message( "          ${NAME}: ${${f_NAME}}")
endfunction(add_option)

function(show_build_properties)
    message("")
    message("       Build Properties")
    message("          type......................: " ${CMAKE_BUILD_TYPE})
    message("          project name..............: " ${CMAKE_PROJECT_NAME})
    message("          c compiler................: " ${CMAKE_C_COMPILER})
    message("          c++ compiler..............: " ${CMAKE_CXX_COMPILER})
    message("          c++ flags.................: " ${CMAKE_CXX_FLAGS})
    message("          clang-tidy................: " ${CLANG_TIDY_EXE})
endfunction(show_build_properties)
