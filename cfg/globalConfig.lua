TRUE = 1
FALSE = 0

global = {
    log = {
        file = "logfile.log",
        level = 2, -- 3 = error
    },

    window = {
        name = "3Delch",
        size = {1900, 1000},
        openGLVersion = {4, 4},
        useOpenGLCoreProfile = TRUE,
        showMouseCursor = FALSE,
        relativeMouseMode = TRUE,
        windowGrabMouse = TRUE,
        enableVSync = FALSE,
        -- for 4k display use either 2.0 dpiScaling or multiSamplingFactor = 0.5
        -- if no dpiScaling is set the autodetected dpi scaling is used
        dpiScaling = 2.0,           -- for 4k display use either 2.0 dpiScaling or multiSamplingFactor = 0.5
        multiSamplingFactor = 1.0,  -- 0.5 double pixel size
    },

    integratedConsole = {
        key = "F12",
    },

    camera = {
        sensitivity = 0.1,
        speedInMeterPerSecond = 0.25,
        freeFlyKeys = {
            forward     = "w",
            backward    = "s",
            left        = "a",
            right       = "d",
            up          = "r",
            down        = "f",
            rollLeft    = "q",
            rollRight   = "e",
        },
        orbitKeys = {
            toggleRotate = "g",
            zoomIn       = "q",
            zoomOut      = "e",
        },
    },

    scenes = {
        main = {
            camera = {
                zNearInMeter                    = 0.1,
                zFarInMeter                     = 300.0,
            },
            light = {
                maxDirectionalLights            = 2,
                -- shadowMapSize_DirectionalLight  = {32768, 32768},
                -- shadowMapSize_PointLight        = {2048, 2048},
                shadowMapSize_DirectionalLight  = {8192, 8192},
                shadowMapSize_PointLight        = {512, 512},
                shaderFile_deferred             = "glsl/Light/deferredRenderer.glsl"
            },

            nonVertexObject = {
                shaderFile_forward      = "glsl/NonVertexObject/forwardRenderer.glsl",
            },

            vertexObject = {
                shaderFile_forward      = "glsl/VertexObject/forwardRenderer.glsl",
                shaderFile_deferred     = "glsl/VertexObject/deferredRenderer.glsl"
            },

            unifiedVertexObject = {
                shaderFile_deferred     = "glsl/UnifiedVertexObject/deferredRenderer.glsl",
                shaderFile_forward      = "glsl/UnifiedVertexObject/forwardRenderer.glsl",
                shaderFile_silhouette   = "glsl/forwardRenderer_Silhouette.glsl",
                shaderFile_wireframe    = "glsl/forwardRenderer_WireFrame.glsl",
                textureSize             = {1024, 1024, 64},
            },

            instancedObject = {
                shaderFile_deferred     = "glsl/InstancedObject/deferredRenderer.glsl",
            },


            passThroughShader = {
                shaderFile_deferred     = "glsl/passThrough.glsl",
            },

            final = {
                shaderFile  = "glsl/finalRenderer.glsl",
            },

            forward = {
                defaultShader = {
                    file    = "glsl/forwardRenderer_VertexObject.glsl",
                    group   = "default",
                },
            },

            skybox = {
                shader = {
                    file    = "glsl/forwardRenderer_Skybox.glsl",
                    group   = "default",
                },
                textures = {
                    "data/textures/sb1-5.jpg", "data/textures/sb1-1.jpg",
                    "data/textures/sb1-2.jpg", "data/textures/sb1-4.jpg",
                    "data/textures/sb1-3.jpg", "data/textures/sb1-6.jpg"
                },
                color = { 0.2, 0.2, 0.2, 1.0 }
            },

            gui = {
                shaderFile_gui  = "glsl/gui/standard.glsl",
                theme           = "cfg/guiTheme.lua",
            },
            postEffects = {
                ambientOcclusion = {
                    gtao = {
                        scaling = 1.0,
                        lengthOfOneMeter = 1.0,
                        gtaoShader = {
                            file    = "glsl/PostProcessing/gtao.glsl",
                            group   = "default",
                        },
                        blurShader = {
                            file    = "glsl/PostProcessing/gtaoSpatialBlur.glsl",
                            group   = "default",
                        },
                        finalShader = {
                            file    = "glsl/PostProcessing/gtaoFinal.glsl",
                            group   = "default",
                        },
 
                    },
                },
                screenSpaceReflection = {
                    scaling = 1.0;
                    screenSpaceReflectionShader = {
                        file    = "glsl/PostProcessing/screenSpaceReflection.glsl",
                        group   = "default",
                    },
                },
                glow = {
                    gui = {
                        scaling = 0.25,
                        intensity = 0.6,
                        stepWidthFactor = 1.0,
                        shader = {
                            file    = "glsl/PostProcessing/gaussianBlur.glsl",
                            group   = "default",
                        },
                     },
                    light = {
                        scaling = 0.5,
                        intensity = 1.0,
                        stepWidthFactor = 1.5,
                        shader = {
                            file    = "glsl/PostProcessing/gaussianBlur.glsl",
                            group   = "default",
                        },
                    },
                },
            },
        },
    },
}
