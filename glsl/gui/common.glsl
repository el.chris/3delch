uniform settings_t
{
    vec4  color;
    vec4  colorAccent;
    vec4  glowColor;
    vec4  glowColorAccent;
    float glowScaling;
    vec4  borders;
    vec4  borderEventIncrease;
    vec2  resolution;
    vec2  size;
    vec2  textureSize;
    vec2  position;
    vec2  mousePosition;
    float time;
    vec4  viewPort;
    int   doDrawEventColor;
    int   doReadTexture;
    int   doDrawColors;
    int   doInvertTextureCoordinates;
    int   isEmpty;
    vec4  eventColorTop;
    vec4  eventColorBottom;
    vec4  eventColorLeft;
    vec4  eventColorRight;
    vec4  eventColorTopLeft;
    vec4  eventColorTopRight;
    vec4  eventColorBottomLeft;
    vec4  eventColorBottomRight;
    vec4  eventColorInner;
}
setting;
