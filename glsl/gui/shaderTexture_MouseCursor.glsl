//__default__
//!fragment
#version 440

//<<common.glsl

in vec2 _texCoord;
layout (location=0) out vec4 fragColor;

vec2  uv            = _texCoord;

void
main()
{
    float smoothness = 0.1;
    float color = smoothstep(0.0, smoothness, 1.0 - uv.x) 
                    - smoothstep(0.0, smoothness, 1.0 - uv.x - uv.y) 
                    - smoothstep(0.0, smoothness, uv.y + 0.45 * (1.0 - uv.x) - 0.8);

    float accentColor = smoothstep(0.9, 1.0, color);

    fragColor = setting.color * (color - accentColor) + setting.colorAccent * accentColor;
}
