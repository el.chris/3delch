//__geometry__
//!fragment
#version 440

uniform sampler2D colorTexture;
uniform sampler2D specularColorTexture;
uniform sampler2D shininessTexture;
uniform sampler2D normalTexture;
uniform sampler2D emissionTexture;
uniform sampler2D roughnessTexture;

in vec3 _position;
in vec3 _normal;
in vec3 _tangent;
in vec3 _bitangent;
in vec2 _texCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec3 position;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec4 material;
layout(location = 4) out vec4 glow;

vec3 GenerateNormalFromMap() {
    // normal maps store value from -1.0 - 1.0 mapped to a range from 0.0 - 1.0
    // restore the normal value
    vec3 normalValue = texture(normalTexture, _texCoord).xyz;
    if ( normalValue == vec3(0.0) ) return _normal;

    normalValue = normalValue * 2.0 - vec3(1.0);
    vec3 bitangent = cross(_tangent, _normal);

    // tbn matrix * normal
    return normalize(mat3(_tangent, bitangent, _normal) * normalValue);
}

void main()
{
    fragColor   = texture(colorTexture, _texCoord);
    position    = _position;
    normal      = GenerateNormalFromMap();
    glow        = texture(emissionTexture, _texCoord);

    float metallic      = texture(specularColorTexture, _texCoord).w;
    float roughness     = texture(roughnessTexture, _texCoord).x;
    material = vec4(metallic, roughness, 0.0, 1.0);
}

//!vertex
#version 440

uniform mat4 modelMatrix;
uniform mat4 viewProjection;
layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out vec3 _position;
out vec3 _normal;
out vec2 _texCoord;
out vec3 _tangent;
out vec3 _bitangent;

void main()
{
    _position   = (modelMatrix * vec4(coord, 1.0)).xyz;
    _texCoord   = texCoord;
    _normal     = normalize((vec4(normal, 1.0) * inverse(modelMatrix)).xyz);
    _tangent    = normalize((vec4(tangent, 1.0) * inverse(modelMatrix)).xyz);
    _bitangent  = normalize((vec4(bitangent, 1.0) * inverse(modelMatrix)).xyz);

    gl_Position = viewProjection * modelMatrix * vec4(coord, 1.0);
}


//__directionalLightShadow__
//!fragment
#version 440

out vec4 directionalLight;

void main() {
    directionalLight = vec4( gl_FragCoord.z, 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform mat4 modelMatrix;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;

void main() {
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}


//__pointLightShadow__
//!fragment
#version 440

uniform vec3 lightPosition;

in vec3 positionInWorld;
out vec4 pointLight;

void main() {
    pointLight = vec4(length(positionInWorld - lightPosition), 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform mat4 modelMatrix;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;

out vec3 positionInWorld;

void main() {
    positionInWorld     = (modelMatrix * vec4(coord, 1.0)).xyz;
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}
