//__geometry__
//!fragment
#version 440

uniform sampler2D colorTexture;
uniform sampler2D specularColorTexture;
uniform sampler2D shininessTexture;
uniform sampler2D normalTexture;
uniform sampler2D emissionTexture;
uniform sampler2D roughnessTexture;

in VSData {
    vec3 position;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
    vec4 diffuse;
    vec4 eventColor;
    vec4 emissionColor;
} inData;

#define MAX_LIGHTS 10
layout(std140, binding = 0) uniform scene_t {
    vec3    cameraPosition;
} scene;

layout(std140, binding = 1) uniform light_t {
    vec3    color[MAX_LIGHTS];
    vec3    direction[MAX_LIGHTS];
    float   ambientIntensity[MAX_LIGHTS];
    float   diffuseIntensity[MAX_LIGHTS];
    int     numberOfLights;
} light;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;
layout(location = 2) out vec4 normal;
layout(location = 3) out vec4 position;
layout(location = 4) out vec4 material;
layout(location = 5) out vec4 event;

vec3 GenerateNormalFromMap() {
    // normal maps store value from -1.0 - 1.0 mapped to a range from 0.0 - 1.0
    // restore the normal value
    vec3 normalValue = texture(normalTexture, inData.texCoord).xyz;
    if ( normalValue == vec3(0.0) ) return inData.normal;

    normalValue = normalValue * 2.0 - vec3(1.0);
    vec3 bitangent = cross(inData.tangent, inData.normal);

    // tbn matrix * normal
    return normalize(mat3(inData.tangent, bitangent, inData.normal) * normalValue);
}

void main() {
    float shininess         = texture(shininessTexture, inData.texCoord).x;
    float metallic          = texture(specularColorTexture, inData.texCoord).w;
    vec3  specularColor     = texture(specularColorTexture, inData.texCoord).xyz;
    vec3  normalFromMap     = GenerateNormalFromMap();
    normal                  = vec4(normalFromMap, (normalFromMap == vec3(0.0)) ? 0.0 : 1.0);
    vec4 lightFactor        = vec4(1.0);

    if ( normal != vec4(0.0) ) {
        vec3 ambient = vec3(0.0), lighting = vec3(0.0), specularLighting = vec3(0.0);
        vec3 viewDirection = normalize(scene.cameraPosition - inData.position);

        for(int n = 0; n < light.numberOfLights; ++n) {
            ambient        += light.ambientIntensity[n] * light.color[n];

            float lightingFactor = max(dot(normalFromMap, -light.direction[n]), 0.0);
            lighting       += lightingFactor * light.color[n];

            if ( lightingFactor > 0.0 ) {
                vec3 reflectDirection = reflect(light.direction[n], normalFromMap);
                float specularFactor  = pow(max(dot(viewDirection, reflectDirection), 0.0), shininess);

                specularLighting         += metallic * specularFactor * light.color[n] * specularColor;
            }
        }
        lightFactor = vec4((ambient + lighting + specularLighting), 1.0);
    }

    fragColor       = lightFactor * ( texture(colorTexture, inData.texCoord) + inData.diffuse);

    glow            = texture(emissionTexture, inData.texCoord) + inData.emissionColor;

    position        = (fragColor == vec4(0.0)) ? vec4(0.0) : vec4(inData.position, 1.0);

    float roughness = texture(roughnessTexture, inData.texCoord).x;
    material        = (roughness == 0.0 && metallic == 0.0) ? vec4(0.0) 
                            : vec4(metallic, roughness, 0.0, 1.0);
    event           = inData.eventColor;
}

//!vertex
#version 440

uniform mat4 modelMatrix;
uniform mat4 viewProjection;
layout(location = 0) in vec3 coord;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;
layout(location = 5) in vec4 diffuse;
layout(location = 6) in vec4 eventColor;
layout(location = 7) in vec4 emissionColor;

out VSData {
    vec3 position;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
    vec4 diffuse;
    vec4 eventColor;
    vec4 emissionColor;
} outData;

void main() {
    mat4 invModelMatrix = inverse(modelMatrix);

    gl_Position           = viewProjection * modelMatrix * vec4(coord, 1.0);
    outData.position      = (modelMatrix * vec4(coord, 1.0)).xyz;
    outData.texCoord      = texCoord;
    outData.normal        = (normal == vec3(0.0)) ? vec3(0.0) : normalize((vec4(normal, 1.0) * invModelMatrix).xyz);
    outData.tangent       = (tangent == vec3(0.0)) ? vec3(0.0) : normalize((vec4(tangent, 1.0) * invModelMatrix).xyz);
    outData.bitangent     = bitangent;
    outData.diffuse       = diffuse;
    outData.eventColor    = eventColor;
    outData.emissionColor = emissionColor;
}


