//__default__
//!fragment
#version 440

uniform samplerCube diffuse;
in vec3 _texCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;

void main() {
    fragColor = texture(diffuse, _texCoord);
    glow = vec4(0.0);
}

//!vertex
#version 440

uniform mat4 modelMatrix;
uniform mat4 viewProjection;
layout(location = 0) in vec3 coord;

out vec3 _texCoord;

void main() {
    vec4 worldPos = viewProjection * modelMatrix * vec4(coord, 1.0);

    // override z component with w component, => perspective divide by w results
    // in 1.0 and therefore skybox will always fail depth test since 1.0 is mapped
    // to z far and consequently the skybox is always behind any other scene object
    gl_Position = worldPos.xyww; 
    _texCoord = coord;
}
