//__geometry__
//!fragment
#version 440

in vec4 _color;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;

void main() {
    fragColor = _color;
    glow      = vec4(0.0);
}

//!vertex
#version 440

uniform mat4 mvp;
layout(location = 0) in vec3 coord;
layout(location = 1) in vec4 color;

out vec4 _color;

void main() {
    gl_Position = mvp * vec4(coord, 1.0);
    _color = color;
}
