//__default__
//!fragment
#version 440

uniform sampler2D diffuse;
uniform vec2 invertedResolution;
uniform float intensity;
uniform float stepWidthFactor;

uniform int iterations;
uniform vec4 offset;
uniform vec4 weight;

out vec4 fragColor;
in vec2 _texCoord;
vec2 uv = (vec2(1.0) - _texCoord);

void main() 
{
    fragColor = vec4(0.0);
    vec2 stepWidth = invertedResolution;
    float stepIntensity = intensity;
    int passes = 7;

    for(int pass = 0; pass < passes; ++pass) {
        fragColor += stepIntensity * weight[0] * textureLod(diffuse, uv, pass);

        for(int i = 1; i < iterations; ++i) {
            vec2 xDir = offset[i] * vec2(1.0, 0.0) * stepWidth;
            vec2 yDir = offset[i] * vec2(0.0, 1.0) * stepWidth;

            fragColor += stepIntensity * weight[i] * textureLod(diffuse, uv + xDir, pass);
            fragColor += stepIntensity * weight[i] * textureLod(diffuse, uv - xDir, pass);
            fragColor += stepIntensity * weight[i] * textureLod(diffuse, uv + yDir, pass);
            fragColor += stepIntensity * weight[i] * textureLod(diffuse, uv - yDir, pass);
        }
        stepWidth *= stepWidthFactor;
        stepIntensity *= intensity;
    }
}
