#include "logging/policies/output.hpp"

namespace el3D
{
namespace logging
{
namespace output
{
void
Base::Out( const std::string &channelName, const logLevel_t logLevel,
           const std::string &file, const std::string &line,
           const std::string &func, const std::string &msg ) noexcept
{
    if ( this->logLevel <= logLevel )
    {
        this->Print( this->formatMessage( channelName, logLevel, file, line,
                                          func, msg ) );
    }
}

void
Base::SetLogLevel( const logLevel_t logLevel ) noexcept
{
    this->logLevel = logLevel;
}

void
Base::SetFormatting( const format_t &f ) noexcept
{
    this->formatMessage = f;
}
} // namespace output
} // namespace logging
} // namespace el3D
