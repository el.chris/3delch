#include "units/Frequency.hpp"

namespace el3D
{
namespace units
{
Frequency::Frequency() noexcept : Frequency( 0.0 )
{
}

Frequency::Frequency( const float_t hertz ) noexcept : BasicUnit( hertz )
{
}

Frequency
Frequency::Hz( const float_t value ) noexcept
{
    return Frequency( value );
}

Frequency
Frequency::KHz( const float_t value ) noexcept
{
    return Frequency( value * 1000. );
}

Frequency
Frequency::MHz( const float_t value ) noexcept
{
    return Frequency( value * 1000000. );
}

Frequency
Frequency::GHz( const float_t value ) noexcept
{
    return Frequency( value * 1000000000. );
}

BasicUnit< Frequency >::float_t
Frequency::GetHz() const noexcept
{
    return this->value;
}

BasicUnit< Frequency >::float_t
Frequency::GetKHz() const noexcept
{
    return this->value / 1000;
}

BasicUnit< Frequency >::float_t
Frequency::GetMHz() const noexcept
{
    return this->value / 1000000;
}

BasicUnit< Frequency >::float_t
Frequency::GetGHz() const noexcept
{
    return this->value / 1000000000;
}

namespace literals
{
Frequency operator"" _Hz( unsigned long long value ) noexcept
{
    return Frequency::Hz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _Hz( long double value ) noexcept
{
    return Frequency::Hz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _KHz( unsigned long long value ) noexcept
{
    return Frequency::KHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _KHz( long double value ) noexcept
{
    return Frequency::KHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _MHz( unsigned long long value ) noexcept
{
    return Frequency::MHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _MHz( long double value ) noexcept
{
    return Frequency::MHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _GHz( unsigned long long value ) noexcept
{
    return Frequency::GHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}

Frequency operator"" _GHz( long double value ) noexcept
{
    return Frequency::GHz(
        static_cast< BasicUnit< Frequency >::float_t >( value ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Frequency& t ) noexcept
{
    auto                  bytes      = t.GetHz();
    static constexpr char units[][4] = { "Hz", "KHz", "MHz", "GHz" };

    for ( uint64_t i = 0, limit = sizeof( units ) / sizeof( char[3] );
          i < limit; ++i )
    {
        if ( bytes < 1000. || i + 1 == limit )
        {
            stream << bytes << units[i];
            return stream;
        }

        bytes /= 1000;
    }

    return stream;
}
} // namespace units
} // namespace el3D
