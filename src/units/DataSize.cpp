#include "units/DataSize.hpp"

namespace el3D
{
namespace units
{
DataSize::DataSize() noexcept : DataSize( 0.0 )
{
}

DataSize
DataSize::Bytes( const float_t value ) noexcept
{
    return DataSize( value );
}

DataSize
DataSize::KiloBytes( const float_t value ) noexcept
{
    return DataSize( value * 1024u );
}

DataSize
DataSize::MegaBytes( const float_t value ) noexcept
{
    return DataSize( value * 1024u * 1024u );
}

DataSize
DataSize::GigaBytes( const float_t value ) noexcept
{
    return DataSize( value * 1024u * 1024u * 1024u );
}

BasicUnit< DataSize >::float_t
DataSize::GetBytes() const noexcept
{
    return this->value;
}

BasicUnit< DataSize >::float_t
DataSize::GetKiloBytes() const noexcept
{
    return this->value / 1024u;
}

BasicUnit< DataSize >::float_t
DataSize::GetMegaBytes() const noexcept
{
    return this->value / ( 1024u * 1024u );
}

BasicUnit< DataSize >::float_t
DataSize::GetGigaBytes() const noexcept
{
    return this->value / ( 1024u * 1024u * 1024u );
}

DataSize::DataSize( const float_t length ) noexcept : BasicUnit( length )
{
}

namespace literals
{
DataSize operator"" _b( unsigned long long value ) noexcept
{
    return DataSize::Bytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _b( long double value ) noexcept
{
    return DataSize::Bytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _kb( unsigned long long value ) noexcept
{
    return DataSize::KiloBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _kb( long double value ) noexcept
{
    return DataSize::KiloBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _mb( unsigned long long value ) noexcept
{
    return DataSize::MegaBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _mb( long double value ) noexcept
{
    return DataSize::MegaBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _gb( unsigned long long value ) noexcept
{
    return DataSize::GigaBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}

DataSize operator"" _gb( long double value ) noexcept
{
    return DataSize::GigaBytes(
        static_cast< BasicUnit< DataSize >::float_t >( value ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::DataSize& t ) noexcept
{
    auto                  bytes      = t.GetBytes();
    static constexpr char units[][3] = { "b", "kb", "mb", "gb" };

    for ( uint64_t i = 0, limit = sizeof( units ) / sizeof( char[3] );
          i < limit; ++i )
    {
        if ( bytes < 1024. || i + 1 == limit )
        {
            if ( i == 0 )
                stream << static_cast< int64_t >( bytes ) << units[i];
            else
                stream << bytes << units[i];
            return stream;
        }

        bytes = static_cast< BasicUnit< DataSize >::float_t >(
                    static_cast< uint64_t >( bytes / 1024. * 100 ) ) /
                100.;
    }

    return stream;
}
} // namespace units
} // namespace el3D
