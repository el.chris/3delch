#include "units/Acceleration.hpp"

namespace el3D
{
namespace units
{
Acceleration::Acceleration() noexcept : Acceleration( 0.0 )
{
}

Acceleration::Acceleration( const float_t acceleration ) noexcept
    : BasicUnit( acceleration )
{
}

BasicUnit< Acceleration >::float_t
Acceleration::GetMeterPerSecondSquare() const noexcept
{
    return this->value;
}

Acceleration
Acceleration::MeterPerSecondSquare( const float_t value ) noexcept
{
    return Acceleration( value );
}

namespace literals
{
Acceleration operator""_m_s2( long double value ) noexcept
{
    return Acceleration::MeterPerSecondSquare(
        static_cast< BasicUnit< Acceleration >::float_t >( value ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Acceleration& t ) noexcept
{
    stream << t.GetMeterPerSecondSquare() << "m/(s^2)";
    return stream;
}
} // namespace units
} // namespace el3D
