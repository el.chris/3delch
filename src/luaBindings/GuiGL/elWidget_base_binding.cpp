#include "luaBindings/GuiGL/elWidget_base_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elWidget_base )
{
    using namespace ::el3D::GuiGL;
    AddLuaWidgetBinding< elWidget_base >( "elWidget_base" );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
