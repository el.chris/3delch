#include "luaBindings/OpenGL/elTexture_base_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elTexture_base.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elTexture_base )
{
    using namespace ::el3D::OpenGL;
    lua::elLuaScript::RegisterGlobal< int >(
        "textureTargets",
        {
            {"GL_TEXTURE_1D", GL_TEXTURE_1D},
            {"GL_TEXTURE_2D", GL_TEXTURE_2D},
            {"GL_TEXTURE_3D", GL_TEXTURE_3D},
            {"GL_TEXTURE_CUBE_MAP", GL_TEXTURE_CUBE_MAP},
            {"GL_TEXTURE_RECTANGLE", GL_TEXTURE_RECTANGLE},
            {"GL_TEXTURE_BUFFER", GL_TEXTURE_BUFFER},
            {"GL_TEXTURE_1D_ARRAY", GL_TEXTURE_1D_ARRAY},
            {"GL_TEXTURE_2D_ARRAY", GL_TEXTURE_2D_ARRAY},
            {"GL_TEXTURE_CUBE_MAP_ARRAY", GL_TEXTURE_CUBE_MAP_ARRAY},
            {"GL_TEXTURE_2D_MULTISAMPLE", GL_TEXTURE_2D_MULTISAMPLE},
            {"GL_TEXTURE_2D_MULTISAMPLE_ARRAY",
             GL_TEXTURE_2D_MULTISAMPLE_ARRAY},
        },
        NAMESPACE_NAME );

    lua::elLuaScript::class_t newClass( "elTexture_base" );
    AddLuaTextureBaseInterface< elTexture_base >( newClass );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
