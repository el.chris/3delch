#include "luaBindings/OpenGL/elObjectRegistry_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elObjectRegistry.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elObjectRegistry )
{
    lua::elLuaScript::class_t newStruct( "elObjectRegistry_texture_t" );
    using namespace ::el3D::OpenGL;

    newStruct.AddConstructor< elObjectRegistry::texture_t >()
        .AddVariable( "identifier", &elObjectRegistry::texture_t::identifier )
        .AddVariable( "texture", &elObjectRegistry::texture_t::texture )
        .AddVariable( "target", &elObjectRegistry::texture_t::target );
    lua::elLuaScript::RegisterLuaClass( newStruct, NAMESPACE_NAME );

    lua::elLuaScript::class_t newClass( "elObjectRegistry" );
    newClass.AddPrivateConstructor< elObjectRegistry >()
        .AddMethod< elObjectRegistry >( "RegisterTexture",
                                        &elObjectRegistry::RegisterTexture )
        .AddMethod< elObjectRegistry >( "UnregisterTexture",
                                        &elObjectRegistry::UnregisterTexture )
        .AddMethod< elObjectRegistry >(
            "GetRegisteredTextures", &elObjectRegistry::GetRegisteredTextures )
        .AddStaticMethod( "GetInstance", elObjectRegistry::GetInstance );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
