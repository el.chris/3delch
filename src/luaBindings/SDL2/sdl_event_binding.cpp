#include "luaBindings/SDL2/sdl_event_binding.hpp"

#include <SDL.h>

namespace el3D
{
namespace luaBindings
{
namespace SDL2
{
#ifdef ADD_LUA_BINDINGS

bool
HasSpecifiedSDLEventOccured( const SDL_Event event, const uint32_t eventType,
                             const SDL_Keycode keycode )
{
    return event.type == eventType && event.key.keysym.sym == keycode;
}

LUA_F( sdl_event )
{
    std::string nameSpaceName = "SDL2";

    lua::elLuaScript::class_t newClass( "SDL_Event" );
    newClass.AddConstructor< SDL_Event >();
    lua::elLuaScript::RegisterLuaClass( newClass, nameSpaceName );

    lua::elLuaScript::RegisterFunction( "HasSpecifiedSDLEventOccured",
                                        HasSpecifiedSDLEventOccured,
                                        nameSpaceName );

    lua::elLuaScript::RegisterGlobal< int >(
        "SDL_EventType",
        {{"SDL_KEYUP", SDL_KEYUP}, {"SDL_KEYDOWN", SDL_KEYDOWN}},
        nameSpaceName );

    lua::elLuaScript::RegisterGlobal< int >(
        "SDL_Keys", {{"SDLK_a", SDLK_a}, {"SDLK_b", SDLK_b}, {"SDLK_c", SDLK_c},
                     {"SDLK_d", SDLK_d}, {"SDLK_e", SDLK_e}, {"SDLK_f", SDLK_f},
                     {"SDLK_g", SDLK_g}, {"SDLK_h", SDLK_h}, {"SDLK_i", SDLK_i},
                     {"SDLK_j", SDLK_j}, {"SDLK_k", SDLK_k}, {"SDLK_l", SDLK_l},
                     {"SDLK_m", SDLK_m}, {"SDLK_n", SDLK_n}, {"SDLK_o", SDLK_o},
                     {"SDLK_p", SDLK_p}, {"SDLK_q", SDLK_q}, {"SDLK_r", SDLK_r},
                     {"SDLK_s", SDLK_s}, {"SDLK_t", SDLK_t}, {"SDLK_u", SDLK_u},
                     {"SDLK_v", SDLK_v}, {"SDLK_w", SDLK_w}, {"SDLK_x", SDLK_x},
                     {"SDLK_y", SDLK_y}, {"SDLK_z", SDLK_z}},
        nameSpaceName );
}
#endif
} // namespace SDL2
} // namespace luaBindings
} // namespace el3D
