#include "luaBindings/GLAPI/elWindow_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/elWindow.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GLAPI
{
using namespace ::el3D::GLAPI;
#ifdef ADD_LUA_BINDINGS
LUA_F( elWindow )
{
    lua::elLuaScript::class_t newClass( "elWindow" );
    newClass.AddPrivateConstructor< elWindow >();
    newClass.AddMethod< elWindow >( "SetCaptureMouse",
                                    &elWindow::SetCaptureMouse );
    newClass.AddMethod< elWindow >( "SetRelativeMouseMode",
                                    &elWindow::SetRelativeMouseMode );
    newClass.AddMethod< elWindow >( "SetShowCursor", &elWindow::SetShowCursor );
    newClass.AddMethod< elWindow >( "SetWindowGrab", &elWindow::SetWindowGrab );
    newClass.AddMethod< elWindow >( "SetWindowSize", &elWindow::SetWindowSize );
    newClass.AddMethod< elWindow >( "SetWindowIsResizable",
                                    &elWindow::SetWindowIsResizable );
    newClass.AddMethod< elWindow >( "GetRuntime", &elWindow::GetRuntime );
    newClass.AddMethod< elWindow >( "GetDeltaTime", &elWindow::GetDeltaTime );
    newClass.AddMethod< elWindow >( "GetWindowSize", &elWindow::GetWindowSize );
    newClass.AddMethod< elWindow >( "GetOpenGLVersion",
                                    &elWindow::GetOpenGLVersion );
    newClass.AddMethod< elWindow >( "GetShaderVersion",
                                    &elWindow::GetShaderVersion );
    newClass.AddMethod< elWindow >( "GetFrameCounter",
                                    &elWindow::GetFrameCounter );
    newClass.AddMethod< elWindow >( "GetFramesPerSecond",
                                    &elWindow::GetFramesPerSecond );
    newClass.AddMethod< elWindow >( "GetEventHandler",
                                    &elWindow::GetEventHandler );

    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif


} // namespace GLAPI
} // namespace luaBindings
} // namespace el3D
