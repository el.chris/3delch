#include "luaBindings/utils/elFile_binding.hpp"

#include "utils/elFile.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elFile )
{
    using namespace ::el3D::utils;
    lua::elLuaScript::class_t newClass( "elFile" );
    newClass.AddConstructor< elFile, std::string, bool >()
        .AddMethod< elFile >( "Read", &elFile::Read )
        .AddMethod< elFile >( "Write", &elFile::Write )
        .AddMethod< elFile >( "GetPath", &elFile::GetPath )
        .AddMethod< elFile >( "GetContentAsString",
                              &elFile::GetContentAsString )
        .AddMethod< elFile >( "GetContentAsVector",
                              &elFile::GetContentAsVector )
        .AddMethod< elFile >( "SetContentFromString",
                              &elFile::SetContentFromString );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
