#include "luaBindings/utils/elByteSerializer_binding.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
template < typename T >
void
Register( const std::string &name )
{
    lua::elLuaScript::class_t newClass( name );
    using namespace ::el3D::utils;
    newClass.AddConstructor< T, byteStream_t, uint64_t >();

    newClass.AddMethod< T >( "GetUint8", &T::template Get< uint8_t > );
    newClass.AddMethod< T >( "GetUint16", &T::template Get< uint16_t > );
    newClass.AddMethod< T >( "GetUint32", &T::template Get< uint32_t > );
    newClass.AddMethod< T >( "GetUint64", &T::template Get< uint64_t > );
    newClass.AddMethod< T >( "GetInt8", &T::template Get< int8_t > );
    newClass.AddMethod< T >( "GetInt16", &T::template Get< int16_t > );
    newClass.AddMethod< T >( "GetInt32", &T::template Get< int32_t > );
    newClass.AddMethod< T >( "GetInt64", &T::template Get< int64_t > );
    newClass.AddMethod< T >( "GetFloat", &T::template Get< float > );
    newClass.AddMethod< T >( "GetDouble", &T::template Get< double > );
    newClass.AddMethod< T >( "GetString", &T::template Get< std::string > );

    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}


#ifdef ADD_LUA_BINDINGS
LUA_F( elByteDeserializer )
{
}
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
