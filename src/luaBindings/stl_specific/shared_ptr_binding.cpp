#include "luaBindings/stl_specific/shared_ptr_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/elImage.hpp"
#include "lua/elLuaScript.hpp"
#include "lua/lua_NAMESPACE_NAME.hpp"
#include "luaBindings/stl/shared_ptr_binding.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_F( shared_ptr )
{
    std::string nameSpaceName = "stl";

    stl::AddLuaSharedPtrBinding<::el3D::GLAPI::elImage >(
        "shared_ptr_elImage", ::el3D::GLAPI::NAMESPACE_NAME );

    stl::AddLuaSharedPtrBinding< lua::elLuaScript >( "shared_ptr_elLuaScript",
                                                     lua::NAMESPACE_NAME );
}
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
