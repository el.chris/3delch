#include "3Delch/elSceneManager.hpp"

#include "3Delch/3Delch.hpp"
#include "GLAPI/elFontCache.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "lua/elConfigHandler.hpp"

namespace el3D
{
namespace _3Delch
{
elSceneManager::elSceneManager( GLAPI::elWindow* const      window,
                                lua::elConfigHandler* const config ) noexcept
    : window( window ), config( config ),
      fontCache( std::make_unique< GLAPI::elFontCache >() )
{
    this->pipelineManager =
        std::make_unique< RenderPipeline::elRenderPipelineManager >(
            window, GetMultiSamplingFactor(), config );
}

elSceneManager::~elSceneManager()
{
    this->integratedConsole.reset();
}

elSceneManager::sceneElement_t
elSceneManager::CreateScene( const std::string& sceneName ) noexcept
{
    auto newScene = this->scenes.emplace_back( std::make_unique< elScene >(
        sceneName, this->config, this->window, this->pipelineManager.get(),
        this->fontCache.get() ) );

    if ( this->mainScene == nullptr ) this->SetMainScene( newScene.index() );

    return newScene;
}

void
elSceneManager::RemoveScene( const size_t index ) noexcept
{
    if ( this->mainScene == this->scenes[index].get() )
    {
        this->integratedConsole.reset();
        this->mainScene = nullptr;
    }

    this->scenes.erase( index );
}

void
elSceneManager::SetMainScene( const size_t index ) noexcept
{
    this->mainScene = this->scenes[index].get();
    this->integratedConsole.emplace(
        this->mainScene, GetInstance().Window()->GetEventHandler() );
}

const elScene&
elSceneManager::GetMainScene() const noexcept
{
    return *this->mainScene;
}

void
elSceneManager::SetMultiSamplingFactor( const float v ) noexcept
{
    if ( this->pipelineManager )
        this->pipelineManager->SetMultiSamplingFactor( v );
}

float
elSceneManager::GetMultiSamplingFactor() const noexcept
{
    return ( this->pipelineManager )
               ? this->pipelineManager->GetMultiSamplingFactor()
               : 1.0f;
}

void
elSceneManager::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->pipelineManager->Reshape( x, y );
}

void
elSceneManager::MainLoop() noexcept
{
    if ( mainScene != nullptr )
    {
        auto cursor = this->mainScene->GUI()->GetMouseCursor();
        if ( cursor != nullptr && !cursor->GetHasDisabledEventHandling() )
            this->pipelineManager->SetEventPosition(
                cursor->GetCursorPosition() );
        else
            this->pipelineManager->SetEventPosition( glm::vec2{ 0.0f } );
    }

    this->pipelineManager->Render();
}


} // namespace _3Delch
} // namespace el3D
