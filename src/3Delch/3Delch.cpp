#include "3Delch/3Delch.hpp"

#include "3Delch/elSceneManager.hpp"
#include "GLAPI/elFont.hpp"
#include "GLAPI/elFontCache.hpp"
#include "GuiGL/common.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elMeshLoader.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/elLog.hpp"
#include "network/elNetworkSocket_Client.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "utils/elFileSystem.hpp"
#include "utils/elStackTrace.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace _3Delch
{

static _3Delch*                   instance{ nullptr };
static std::unique_ptr< _3Delch > __internal_3Delch; // NOLINT

_3Delch&
Init( const std::string& configFile ) noexcept
{
    // utils::elStackTrace::RegisterInSignalHandler();
    __internal_3Delch = std::unique_ptr< _3Delch >( new _3Delch( configFile ) );
    animation::elAnimatableBase::SetGlobalCallbackVectorForUpdateAnimation(
        __internal_3Delch->mainLoopCallback );
    return *instance;
}

_3Delch&
GetInstance() noexcept
{
    return *instance;
}

const lua::elConfigHandler&
GetConfig() noexcept
{
    return instance->config;
}

float
GetMultiSamplingFactor() noexcept
{
    return instance->GetMultiSamplingFactor();
}

bb::product_ptr< GLAPI::elEvent >
CreateEvent() noexcept
{
    return instance->Window()->GetEventHandler()->CreateEvent();
}

bb::product_ptr< GLAPI::elEvent >
CreateKeyPressEvent( const GLAPI::KeyState keyState, const SDL_Keycode keyCode,
                     const std::function< void() >& action ) noexcept
{
    auto event = CreateEvent();
    event->SetCallback(
        [=]( const SDL_Event e )
        {
            if ( e.type == static_cast< uint32_t >( keyState ) &&
                 e.key.keysym.sym == keyCode && action )
            {
                action();
            }
        } );
    return event;
}

bb::product_ptr< GLAPI::elEvent >
CreateAndRegisterKeyPressEvent( const GLAPI::KeyState          keyState,
                                const SDL_Keycode              keyCode,
                                const std::function< void() >& action ) noexcept
{
    auto event = CreateKeyPressEvent( keyState, keyCode, action );
    event->Register();
    return event;
}

bb::product_ptr< GLAPI::elEvent >
CreateEventFor( const uint32_t colorIndex ) noexcept
{
    return instance->Window()->GetEventHandler()->CreateEventForColorIndex(
        colorIndex );
}


utils::CallbackGuard
CreateMainLoopCallback( const std::function< void() >& callback ) noexcept
{
    return instance->CreateMainLoopCallback( callback );
}

units::Time
GetRuntime() noexcept
{
    return instance->Window()->GetRuntime();
}

units::Time
GetDeltaTime() noexcept
{
    return instance->Window()->GetDeltaTime();
}

_3Delch::Startup_e::Startup_e( const _3Delch_Error e )
    : std::runtime_error( LOG_EX( 0, "_3Delch startup exception occurred" ) ),
      errorValue( e )
{
}

_3Delch::Startup_e::~Startup_e()
{
}

GLAPI::elWindow*
_3Delch::Window() noexcept
{
    return this->window.get();
}

int
_3Delch::SetInstance()
{
    instance = this;
    return 0;
}

std::string
_3Delch::GetRendererLookupDirectory() const noexcept
{
    auto result = this->config.Get< std::string >(
        { "global", "renderer", "lookupDirectory" }, { "" } );
    if ( result.empty() || result[0].empty() ) return "";

    return result[0] + utils::elFileSystem::PATH_DELIMITER;
}

int
_3Delch::SetupShaderIncludePath() noexcept
{
    auto paths = this->config.Get< std::string >(
        { "global", "shaderIncludePath" }, { "" } );
    for ( auto& p : paths )
        OpenGL::elGLSLParser::AddShaderIncludePath( p );

    return 0;
}

// object definitions
_3Delch::_3Delch( const std::string& configFile )
    : instanceSetter( SetInstance() ), terminalLog(),
      logInitSetter( LogInitSetter() ),
      config( std::move(
          *lua::elConfigHandler::Create( configFile )
               .OrElse(
                   [&]
                   {
                       LOG_FATAL( 0 )
                           << "unable to open configfile " << configFile;
                       throw Startup_e( _3Delch_Error::UnableToOpenConfigFile );
                   } )
               .GetValue() ) ),
      rendererLookupDirectory( GetRendererLookupDirectory() ),
      fileLog( config.Get< std::string >( { "global", "log", "file" },
                                          { "logfile.log" } )[0] ),
      logFinalSetter( LogFinalSetter() ),
      shaderIncludePathSetter( SetupShaderIncludePath() ),
      window( std::move(
          GLAPI::elWindow::Create(
              config.Get< int >( { "global", "window", "size" },
                                 { 800, 600 } )[0],
              config.Get< int >( { "global", "window", "size" },
                                 { 800, 600 } )[1],
              config.Get< std::string >( { "global", "window", "name" },
                                         { "3Delch" } )[0],
              config.Get< int >( { "global", "window", "openGLVersion" },
                                 { 4, 4 } )[0],
              config.Get< int >( { "global", "window", "openGLVersion" },
                                 { 4, 4 } )[1],
              config.Get< int >( { "global", "window", "useOpenGLCoreProfile" },
                                 { true } )[0],
              0u,
              config.Get< int >( { "global", "window", "enableVSync" },
                                 { 800, 600 } )[0] )
              .OrElse(
                  []
                  {
                      LOG_FATAL( 0 ) << "unable to create window";
                      throw Startup_e( _3Delch_Error::UnableToCreateWindow );
                  } )
              .GetValue() ) ),
      shaderVersion( this->window->GetShaderVersion() ),
      sceneManager( std::make_unique< elSceneManager >( this->window.get(),
                                                        &this->config ) )
{
    this->SetMultiSamplingFactor( this->config.Get< float >(
        { "global", "window", "multiSamplingFactor" },
        { this->multiSamplingFactor } )[0] );
    this->SetDPIScaling( config.Get< float >(
        { "global", "window", "dpiScaling" },
        { this->Window()->GetDisplayDPI() / DOTS_PER_DPI_SCALING *
          this->GetMultiSamplingFactor() } )[0] );

    this->window->SetWindowGrab( config.Get< int >(
        { "global", "window", "windowGrabMouse" }, { true } )[0] );
    this->window->SetShowCursor( config.Get< int >(
        { "global", "window", "showMouseCursor" }, { false } )[0] );
    this->window->SetRelativeMouseMode( config.Get< int >(
        { "global", "window", "relativeMouseMode" }, { true } )[0] );

    this->window->SetReshapeCallback(
        [&]( int x, int y )
        {
            this->sceneManager->Reshape( static_cast< uint64_t >( x ),
                                         static_cast< uint64_t >( y ) );
        } );
    this->window->SetMainLoopCallback( [&]()
                                       { return this->MainLoopCallback(); } );
}

_3Delch::~_3Delch()
{
    // TODO: this leads to a use after free?? dont know why
    //    elLog::RemoveOutput( &this->fileLog );
    //    elLog::RemoveOutput( &this->terminalLog );
}

int
_3Delch::LogInitSetter() noexcept
{
    this->terminalLog.SetFormatting( logging::format::DefaultColored );
    logging::elLog::AddOutput( 0, &this->terminalLog );
    logging::elLog::EnableOutputRedirection();
    return 0;
}

int
_3Delch::LogFinalSetter() noexcept
{
    this->terminalLog.SetLogLevel( static_cast< logging::logLevel_t >(
        this->config.Get< int >( { "global", "log", "level" }, { 3 } )[0] ) );

    this->fileLog.SetFormatting( logging::format::Detailled );
    this->fileLog.SetLogLevel( static_cast< logging::logLevel_t >(
        this->config.Get< int >( { "global", "log", "level" }, { 3 } )[0] ) );
    logging::elLog::AddOutput( 0, &this->fileLog );

    return 0;
}

void
_3Delch::StartMainLoop() noexcept
{
    this->window->MainLoop();
}

void
_3Delch::StopMainLoopAndDestroy() noexcept
{
    this->window->ExitMainLoop();
}

glm::ivec2
_3Delch::GetWindowSize() const noexcept
{
    auto winSize = this->window->GetWindowSize();
    return { winSize.x, winSize.y };
}

void
_3Delch::SetMultiSamplingFactor( const float v ) noexcept
{
    if ( this->sceneManager ) this->sceneManager->SetMultiSamplingFactor( v );
    this->multiSamplingFactor = v;
}

float
_3Delch::GetMultiSamplingFactor() const noexcept
{
    return this->multiSamplingFactor;
}

void
_3Delch::AddOpenGLThreadTask( const std::function< void() >& task ) noexcept
{
    this->openGLTasks.Push( task );
}

uint32_t
_3Delch::MainLoopCallback() noexcept
{
    auto callbacks =
        this->openGLTasks
            .TryMultiPop< std::vector< std::function< void() > > >();
    if ( callbacks.has_value() )
        for ( auto& c : callbacks.value() )
            c();

    this->mainLoopCallback.Execute();

    this->sceneManager->MainLoop();
    return this->sceneManager->GetMainScene().GetCurrentEventColorIndex();
}

utils::CallbackGuard
_3Delch::CreateMainLoopCallback(
    const std::function< void() >& callback ) noexcept
{
    return this->mainLoopCallback.CreateCallback( callback );
}

int
_3Delch::GetShaderVersion() const noexcept
{
    return this->shaderVersion;
}

float
_3Delch::SetDPIScaling( const float v ) noexcept
{
    GuiGL::SetDPIScaling( v );
    return v;
}

elSceneManager&
_3Delch::GetSceneManager() noexcept
{
    return *this->sceneManager;
}


} // namespace _3Delch
} // namespace el3D
