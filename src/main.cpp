#include "3Delch/3Delch.hpp"
#include "luaBindings/luaBindings.hpp"
#include "network/network.hpp"
#include "utils/elFileSystem.hpp"

using namespace ::el3D;

#ifdef ADD_LUA_BINDINGS
int
main( int argc, char* argv[] )
{
    LUA_INIT_PREDEF_FUNCTIONS;
    OpenGL::elGLSLParser::AddShaderIncludePath( "glsl/gui/" );

    if ( argc != 2 )
    {
        fprintf( stderr, "The first argument must be the path to the lua "
                         "script which should be executed!\n" );
        return -1;
    }
    else
    {
        auto path     = utils::elFileSystem::GetParentPath( argv[1] );
        auto fileName = utils::elFileSystem::GetFileName( argv[1] );
        printf( "using lua script \"%s\" in \"%s\" \n", fileName.c_str(),
                path.c_str() );

        auto luaScript = lua::elLuaScript::Create().GetValue();
        luaScript->RequireClass( "elLuaScript" );
        luaScript->RequireClass( "void_function_void" );
        luaScript->AddThisPointer();
        luaScript->AddModuleSearchPath( path + "/?.lua" );

        utils::elFile file( argv[1] );
        if ( file.Read() )
        {
            if ( luaScript->SetSource( file.GetContentAsString() ).HasError() )
            {
                fprintf( stderr, "syntax error in script %s\n",
                         fileName.c_str() );
                std::terminate();
            }
            if ( luaScript->Exec().HasError() )
            {
                fprintf( stderr, "unable to execute script %s\n",
                         fileName.c_str() );
                std::terminate();
            }
            _3Delch::GetInstance().StartMainLoop();
        }

        return 0;
    }
}
#else
int
main( int, char** )
{
    fprintf( stderr, "3Delch must be compiled with defined ADD_LUA_BINDINGS "
                     "compiler flag!\n" );
    return -1;
}
#endif
