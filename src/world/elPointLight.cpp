#include "world/elPointLight.hpp"

namespace el3D
{
namespace world
{
elPointLight::elPointLight( const objectSettings_t        settings,
                            const pointLightProperties_t& properties ) noexcept
    : elObject( settings ), properties( properties )
{
    this->light = this->CreatePointLight(
        properties.color, properties.attenuation, properties.position,
        properties.diffuseIntensity, properties.ambientIntensity );
    this->light->SetCastShadow( properties.doCastShadow );
}

void
elPointLight::SetAttenuation( const glm::vec3& v ) noexcept
{
    this->light->SetAttenuation( v );
}

void
elPointLight::SetPosition( const glm::vec3& v ) noexcept
{
    this->light->SetPosition( v );
}

void
elPointLight::SetColor( const glm::vec3& v ) noexcept
{
    this->light->SetColor( v );
}

void
elPointLight::SetDiffuseIntensity( const float v ) noexcept
{
    this->light->SetDiffuseIntensity( v );
}

void
elPointLight::SetAmbientIntensity( const float v ) noexcept
{
    this->light->SetAmbientIntensity( v );
}

const pointLightProperties_t&
elPointLight::GetProperties() const noexcept
{
    return this->properties;
}

void
elPointLight::Move( const glm::vec3& source, const glm::vec3& direction,
                    const float speed ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Linear >(
            &elPointLight::SetPosition )
        .SetSource( source )
        .SetDirection( direction )
        .SetVelocity( speed )
        .SetVelocityType( animation::AnimationVelocity::Absolute );
}

void
elPointLight::MoveTo( const glm::vec3& source, const glm::vec3& destination,
                      const float speed, const bool isInfiniteLoop ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Linear >(
            &elPointLight::SetPosition )
        .SetSource( source )
        .SetDestination( destination )
        .SetVelocity( speed )
        .SetVelocityType( animation::AnimationVelocity::Absolute )
        .SetLoop( isInfiniteLoop );
}


} // namespace world
} // namespace el3D
