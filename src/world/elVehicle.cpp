#include "world/elVehicle.hpp"

#include "3Delch/3Delch.hpp"
#include "animation/Interpolation.hpp"
#include "math/math.hpp"
#include "world/elWorld.hpp"

#include <glm/gtx/quaternion.hpp>

namespace el3D
{
namespace world
{
elVehicle::elWheel::elWheel( bb::product_ptr< elModelGroup >&& object,
                             elVehicle&                        vehicle,
                             const wheelProperties_t& properties ) noexcept
    : vehicle{ &vehicle }, id{ this->vehicle->GetNumberOfWheels() },
      object{ std::move( object ) },
      orientation{ properties.orientation }, scaling{ properties.scaling },
      maxSteeringValue( properties.maxSteeringValue ),
      maxEngineForce( properties.maxEngineForce ),
      maxBrakeForce( properties.maxBrakeForce ),
      steeringSpeed( properties.steeringSpeed ),
      isFrontWheel( properties.isFrontWheel )
{
    btVector3 wheelConnectionPoint(
        this->vehicle->baseDimensions.x + properties.width / 2.0f,
        properties.connectionHeight, this->vehicle->baseDimensions.z );
    this->vehicle->vehicle->vehicle.addWheel(
        wheelConnectionPoint * ToBulletVector( properties.relativePosition ),
        ToBulletVector( properties.direction ),
        ToBulletVector( properties.axis ), properties.suspensionRestLength,
        properties.radius, this->vehicle->vehicle->tuning,
        properties.isFrontWheel );

    btWheelInfo& wheel = this->vehicle->vehicle->vehicle.getWheelInfo(
        static_cast< int >( this->id ) );
    wheel.m_suspensionStiffness      = properties.suspensionStiffness;
    wheel.m_wheelsDampingCompression = properties.wheelsDampingCompression;
    wheel.m_wheelsDampingRelaxation  = properties.wheelsDampingRelaxation;
    wheel.m_frictionSlip             = properties.frictionSlip;
    wheel.m_rollInfluence            = properties.rollInfluence;
}

void
elVehicle::elWheel::SteerToLeft() noexcept
{
    this->SteerTo( this->maxSteeringValue );
}

void
elVehicle::elWheel::SteerToRight() noexcept
{
    this->SteerTo( -this->maxSteeringValue );
}

void
elVehicle::elWheel::SteerToCenter() noexcept
{
    this->SteerTo( 0.0f );
}

void
elVehicle::elWheel::SteerTo( const float value ) noexcept
{
    this->template Animate< animation::elAnimate_SourceDestination,
                            animation::interpolation::Hermite >(
            &elWheel::SetSteeringValue )
        .SetSource( this->GetSteeringValue() )
        .SetDestination( value )
        .SetVelocity( this->steeringSpeed )
        .SetVelocityType( animation::AnimationVelocity::Relative );
}

void
elVehicle::elWheel::Forward() noexcept
{
    this->vehicle->vehicle->vehicle.applyEngineForce(
        this->maxEngineForce.x, static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::Backward() noexcept
{
    this->vehicle->vehicle->vehicle.applyEngineForce(
        -this->maxEngineForce.y, static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::RollOut() noexcept
{
    this->vehicle->vehicle->vehicle.applyEngineForce(
        0.0f, static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::Brake() noexcept
{
    this->vehicle->vehicle->vehicle.setBrake( this->maxBrakeForce,
                                              static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::ReleaseBrake() noexcept
{
    this->vehicle->vehicle->vehicle.setBrake( 0.0f,
                                              static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::SetSteeringValue( const float value ) noexcept
{
    this->vehicle->vehicle->vehicle.setSteeringValue(
        ( this->isFrontWheel ) ? value : -value,
        static_cast< int >( this->id ) );
}

float
elVehicle::elWheel::GetSteeringValue() const noexcept
{
    float value = this->vehicle->vehicle->vehicle.getSteeringValue(
        static_cast< int >( this->id ) );
    return ( this->isFrontWheel ) ? value : -value;
}

void
elVehicle::elWheel::SetBrake( const float value ) noexcept
{
    this->vehicle->vehicle->vehicle.setBrake( btScalar( value ),
                                              static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::ApplyEngineForce( const float value ) noexcept
{
    this->vehicle->vehicle->vehicle.applyEngineForce(
        btScalar( value ), static_cast< int >( this->id ) );
}

void
elVehicle::elWheel::Reset() noexcept
{
    this->vehicle->vehicle->vehicle.updateWheelTransform(
        static_cast< int >( this->id ), true );
}

void
elVehicle::elWheel::UpdateModelMatrix() noexcept
{
    auto& wheel = this->vehicle->vehicle->vehicle.getWheelTransformWS(
        static_cast< int >( this->id ) );
    btScalar modelMatrix[16];
    wheel.getOpenGLMatrix( modelMatrix );

    glm::mat4 m = glm::make_mat4( modelMatrix );
    m           = glm::rotate( m, this->orientation.w,
                               glm::vec3( this->orientation.x, this->orientation.y,
                                          this->orientation.z ) );
    m           = glm::scale( m, this->scaling );
    this->object->SetModelMatrix( m );
}

elVehicle::elVehicle( const objectSettings_t settings,
                      vehicleProperties_t&&  properties ) noexcept
    : elObject( settings ), baseDimensions{ properties.baseDimension }
{
    this->bodyObject = std::move( properties.vehicleBodyModel );

    // chassis
    this->baseDimensions = this->bodyObject->GetDimension() / 2.0f;
    this->bodyObject->SetPhysicalProperties< btBoxShape >(
        physicalProperties_t{
            .mass = static_cast< float >( properties.mass.GetKilogramm() ) },
        btVector3{ this->baseDimensions.x, this->baseDimensions.y,
                   this->baseDimensions.z } );

    this->bodyObject->SetCustomPhysicalTransformGetter(
        [this]() -> const btTransform&
        { return this->vehicle->vehicle.getChassisWorldTransform(); } );

    this->vehicle = this->GetPhysics().CreateVehicle(
        *this->bodyObject->physical,
        [this] {
            bb::for_each( this->wheels,
                          []( auto& w ) { w.UpdateModelMatrix(); } );
        } );

    this->bodyObject->physical->DisableDeactivation();

    this->AddWheels( std::move( properties.wheels ) );

    int rightIndex   = 0;
    int upIndex      = 1;
    int forwardIndex = 2;
    this->vehicle->vehicle.setCoordinateSystem( rightIndex, upIndex,
                                                forwardIndex );

    this->Reset();
}

void
elVehicle::SteerToLeft() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.SteerToLeft(); } );
}

void
elVehicle::SteerToRight() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.SteerToRight(); } );
}

void
elVehicle::SteerToCenter() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.SteerToCenter(); } );
}

void
elVehicle::SteerTo( const float value ) noexcept
{
    bb::for_each( this->wheels, [value]( auto& w ) { w.SteerTo( value ); } );
}

void
elVehicle::Forward() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.Forward(); } );
}

void
elVehicle::Backward() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.Backward(); } );
}

void
elVehicle::RollOut() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.RollOut(); } );
}

void
elVehicle::Brake() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.Brake(); } );
}

void
elVehicle::ReleaseBrake() noexcept
{
    bb::for_each( this->wheels, []( auto& w ) { w.ReleaseBrake(); } );
}

void
elVehicle::SetBrake( const float value ) noexcept
{
    bb::for_each( this->wheels, [value]( auto& w ) { w.SetBrake( value ); } );
}

void
elVehicle::ApplyEngineForce( const float value ) noexcept
{
    bb::for_each( this->wheels,
                  [value]( auto& w ) { w.ApplyEngineForce( value ); } );
}

void
elVehicle::AddWheels(
    std::vector< wheelProperties_t >&& wheelSettings ) noexcept
{
    for ( uint64_t i = 0, limit = wheelSettings.size(); i < limit; ++i )
    {
        this->wheels.emplace_back(
            elWheel( std::move( wheelSettings[i].wheelModel ), *this,
                     wheelSettings[i] ) );
    }
}

void
elVehicle::Reset() noexcept
{
    this->bodyObject->physical->Reset();
    this->bodyObject->physical->body->setCenterOfMassTransform(
        btTransform::getIdentity() );
    this->vehicle->vehicle.resetSuspension();

    for ( auto& w : this->wheels )
        w.Reset();
}

units::Velocity
elVehicle::GetSpeed() const noexcept
{
    return units::Velocity::KilometerPerHour(
        this->vehicle->vehicle.getCurrentSpeedKmHour() );
}


elVehicle::elWheel&
elVehicle::GetWheel( const uint64_t id ) noexcept
{
    return this->wheels[id];
}

const elVehicle::elWheel&
elVehicle::GetWheel( const uint64_t id ) const noexcept
{
    return this->wheels[id];
}

uint64_t
elVehicle::GetNumberOfWheels() const noexcept
{
    return this->wheels.size();
}

glm::vec3
elVehicle::GetRotationAxis() const noexcept
{
    btQuaternion q =
        this->vehicle->vehicle.getChassisWorldTransform().getRotation();
    btVector3 axis = q.getAxis();

    return glm::vec3{ axis.getX(), axis.getY(), axis.getZ() };
}

float
elVehicle::GetRotationDegree() const noexcept
{
    btQuaternion q =
        this->vehicle->vehicle.getChassisWorldTransform().getRotation();
    return q.getAngle();
}

glm::quat
elVehicle::GetRotation() const noexcept
{
    auto q = this->vehicle->vehicle.getChassisWorldTransform().getRotation();
    return glm::quat( q.w(), q.x(), q.y(), q.z() );
}

glm::vec3
elVehicle::GetPosition() const noexcept
{
    auto pos = this->vehicle->vehicle.getChassisWorldTransform().getOrigin();
    return { pos.x(), pos.y(), pos.z() };
}

bool
elVehicle::AttachCamera( OpenGL::elCamera_Perspective& camera,
                         const glm::vec3&              positionOffset,
                         const glm::vec3               lookAtOffset ) noexcept
{
    this->cameraAttachment.emplace( camera, *this, positionOffset,
                                    lookAtOffset );
    return static_cast< bool >( this->cameraAttachment->IsAttachedToCamera() );
}
} // namespace world
} // namespace el3D
