#include "world/types.hpp"

namespace el3D
{
namespace world
{
namespace internal
{
glm::vec3
ToGlm( const color3_t& c ) noexcept
{
    return { static_cast< float >( c[0] ) / 255.0f,
             static_cast< float >( c[1] ) / 255.0f,
             static_cast< float >( c[2] ) / 255.0f };
}

glm::vec4
ToGlm( const color4_t& c ) noexcept
{
    return { static_cast< float >( c[0] ) / 255.0f,
             static_cast< float >( c[1] ) / 255.0f,
             static_cast< float >( c[2] ) / 255.0f,
             static_cast< float >( c[3] ) / 255.0f };
}
} // namespace internal
} // namespace world
} // namespace el3D
