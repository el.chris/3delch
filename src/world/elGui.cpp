#include "world/elGui.hpp"

namespace el3D
{
namespace world
{
elGui::elGui( _3Delch::elScene& scene ) noexcept : gui{ scene.GUI() }
{
}

GuiGL::elWidget_MouseCursor&
elGui::GetMouseCursor() noexcept
{
    return *this->gui->GetMouseCursor();
}

void
elGui::SetShowMouseCursor( const bool v ) noexcept
{
    this->gui->SetShowMouseCursor( v );
}

bool
elGui::DoesShowMouseCursor() const noexcept
{
    return this->gui->DoesShowMouseCursor();
}


} // namespace world
} // namespace el3D
