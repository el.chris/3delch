#include "world/elModelEffect.hpp"

#include "3Delch/3Delch.hpp"

namespace el3D
{
namespace world
{

elModelEffect::elModelEffect( const objectSettings_t          settings,
                              const OpenGL::objectGeometry_t& objectGeometry,
                              const ModelMatrixGetter_t&      modelMatrixGetter,
                              const ModelEffectProperties& properties ) noexcept
    : elObject( settings ), modelMatrixGetter( modelMatrixGetter )
{
    switch ( properties.effectType )
    {
        case ModelEffectType::Silhouette:
            this->object = this->GetSilhouetteUvo().CreateObject(
                objectGeometry.vertices, std::vector< GLfloat >(),
                std::vector< GLfloat >(), std::vector< GLfloat >(),
                std::vector< GLfloat >(), objectGeometry.elements );
            break;
        case ModelEffectType::Wireframe:
            this->object = this->GetWireframeUvo().CreateObject(
                objectGeometry.vertices, std::vector< GLfloat >(),
                std::vector< GLfloat >(), std::vector< GLfloat >(),
                std::vector< GLfloat >(), objectGeometry.elements );
            break;
    }

    if ( properties.performExpensiveVertexElimination )
        this->object->EliminateCloseVerticesInElements(
            ( properties.performExpensiveVertexElimination )
                ? HighGL::UnifiedVertexObject::VertexElimination::CloseVertices
                : HighGL::UnifiedVertexObject::VertexElimination::None,
            properties.vertexEliminationMinDistance );

    if ( this->modelMatrixGetter )
    {
        this->updateCallback = this->object->SetPreDrawCallback(
            [this]
            {
                auto modelMatrix = this->modelMatrixGetter();
                this->object->SetModelMatrix( modelMatrix );
                glLineWidth( this->lineSize * GuiGL::GetDPIScaling() );
            } );
    }

    this->SetDiffuseColor( properties.diffuseColor );
    this->SetGlowColor( properties.glowColor );
    this->SetLineSize( properties.lineSize );
}

elModelEffect::elModelEffect( const objectSettings_t       settings,
                              const elModel&               model,
                              const ModelEffectProperties& properties ) noexcept
    : elModelEffect(
          settings,
          { .vertices = model.GetVertices(), .elements = model.GetElements() },
          [&] { return model.GetModelMatrix(); }, properties )
{
}

elModelEffect::elModelEffect( const objectSettings_t       settings,
                              const elModelGroup&          modelGroup,
                              const ModelEffectProperties& properties ) noexcept
    : elModelEffect(
          settings,
          { .vertices = modelGroup.GetVertices(),
            .elements = modelGroup.GetElements() },
          [&] { return modelGroup.GetModelMatrix(); }, properties )
{
}


void
elModelEffect::SetLineSize( const float value ) noexcept
{
    this->lineSize = value;
}

void
elModelEffect::SetDiffuseColor( const color4_t& color ) noexcept
{
    this->SetBuffer( color, HighGL::UnifiedVertexObject::DIFFUSE_COLOR,
                     255.0f );
}

void
elModelEffect::SetGlowColor( const color4_t& glowColor ) noexcept
{
    this->SetBuffer( glowColor, HighGL::UnifiedVertexObject::EMISSION_COLOR,
                     255.0f );
}

void
elModelEffect::SetBuffer(
    const color4_t&                                      element,
    const HighGL::UnifiedVertexObject::floatBufferList_t bufferType,
    const float                                          adjustment ) noexcept
{
    HighGL::UnifiedVertexObject::floatBuffer_t buffer;
    buffer.reserve( 4 * this->object->GetNumberOfVertices() );

    for ( uint64_t i = 0u; i < this->object->GetNumberOfVertices(); ++i )
        for ( auto& e : element )
            buffer.emplace_back( e / adjustment );

    this->object->SetFloatBuffer( bufferType, buffer );
}


} // namespace world
} // namespace el3D

