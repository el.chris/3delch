#include "world/elSun.hpp"

namespace el3D
{
namespace world
{
elSun::elSun( const objectSettings_t settings,
              const SunProperties&   properties ) noexcept
    : elObject( settings ), properties( properties )
{
    this->light = this->CreateDirectionalLight(
        properties.color, properties.direction, properties.diffuseIntensity,
        properties.ambientIntensity );

    if ( !this->light )
    {
        LOG_FATAL( 0 ) << "unable to create directional light";
        std::terminate();
    }

    this->light->SetCastShadow( properties.doCastShadow );
    this->light->SetHasFog( properties.hasFog );
    this->light->SetFogIntensity( properties.fogIntensity );
}

void
elSun::SetPosition( const units::Angle xAngle,
                    const units::Angle yAngle ) noexcept
{
    this->SetPositionImpl( { xAngle.GetRadian(), yAngle.GetRadian() } );
}

void
elSun::SetPositionImpl( const glm::vec2 angleInRadians ) noexcept
{
    glm::vec3 direction{
        std::cos( angleInRadians.x ) * std::cos( angleInRadians.y ),
        std::sin( angleInRadians.x ) * std::cos( angleInRadians.y ),
        std::sin( angleInRadians.y ) };

    this->light->SetDirection( -direction );
}


void
elSun::SetDirection( const glm::vec3& direction ) noexcept
{
    this->light->SetDirection( direction );
}

void
elSun::Move( const units::Angle xSourceAngle, const units::Angle ySourceAngle,
             const glm::vec2 direction, const float speed ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Linear >( &elSun::SetPositionImpl )
        .SetSource( { xSourceAngle.GetRadian(), ySourceAngle.GetRadian() } )
        .SetDirection( glm::normalize( direction ) )
        .SetVelocity( speed );
}

void
elSun::MoveTo( const units::Angle xSourceAngle, const units::Angle ySourceAngle,
               const units::Angle xDestinationAngle,
               const units::Angle yDestinationAngle,
               const float        speed ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Hermite >(
            &elSun::SetPositionImpl )
        .SetSource( { xSourceAngle.GetRadian(), ySourceAngle.GetRadian() } )
        .SetDestination(
            { xDestinationAngle.GetRadian(), yDestinationAngle.GetRadian() } )
        .SetVelocity( speed )
        .SetVelocityType( animation::AnimationVelocity::Absolute );
}

const SunProperties&
elSun::GetProperties() const noexcept
{
    return this->properties;
}

void
elSun::SetColor( const glm::vec3& color ) noexcept
{
    this->light->SetColor( color );
}


void
elSun::SetDiffuseIntensity( const float intensity ) noexcept
{
    this->light->SetDiffuseIntensity( intensity );
}

void
elSun::SetAmbientIntensity( const float intensity ) noexcept
{
    this->light->SetAmbientIntensity( intensity );
}

void
elSun::SetCastShadow( const bool value ) noexcept
{
    this->light->SetCastShadow( value );
}


} // namespace world
} // namespace el3D
