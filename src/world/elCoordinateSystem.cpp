#include "world/elCoordinateSystem.hpp"

#include "3Delch/3Delch.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "utils/convert.hpp"

namespace el3D
{
namespace world
{
using namespace HighGL;
elCoordinateSystem::elCoordinateSystem(
    const objectSettings_t            settings,
    const CoordinateSystemProperties& properties ) noexcept
    : elObject( settings ),
      coordinateSystemGenerator(
          { properties.gridSize, properties.gridStyle, properties.position } )
{
    auto& geometry = coordinateSystemGenerator.GetGeometry();

    this->majorLines =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_NonVertexObject >(
            nullptr, RenderPipeline::RenderOrder::SolidObject,
            OpenGL::DrawMode::Lines, geometry.majorLines );
    this->SetMajorDrawSize( properties.majorDrawSize );
    this->SetMajorColor( properties.majorColor );

    this->minorLines =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_NonVertexObject >(
            nullptr, RenderPipeline::RenderOrder::SolidObject,

            ( properties.gridStyle ==
              HighGL::elCoordinateSystemGenerator::GridStyle::DotsAndCrosses )
                ? OpenGL::DrawMode::Points
                : OpenGL::DrawMode::Lines,
            geometry.minorLines );
    this->SetMinorDrawSize( properties.minorDrawSize );
    this->SetMinorColor( properties.minorColor );

    if ( properties.style == CoordinateSystemStyle::MayorMinorPlane )
    {
        this->plane =
            this->CreateCustomObject< OpenGL::RenderTarget::Deferred,
                                      OpenGL::elGeometricObject_VertexObject >(
                OpenGL::DrawMode::Triangles,
                HighGL::elObjectGeometryBuilder::CreateQuad()
                    .GetObjectGeometry() );

        this->planeColor = this->plane->CreateTexture< OpenGL::elTexture_2D >(
            OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
            "colorTexture", GL_TEXTURE0, "", 1U, 1U,
            static_cast< GLint >( GL_RGBA8 ), static_cast< GLenum >( GL_RGBA ),
            static_cast< GLenum >( GL_FLOAT ) );

        this->planeNormal = this->plane->CreateTexture< OpenGL::elTexture_2D >(
            OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
            "normalTexture", GL_TEXTURE1, "" );
        this->planeNormal->TexImage< utils::byte_t >(
            { 0x00, 0x00, 0x00, 0x00 } );

        this->planeSpecular =
            this->plane->CreateTexture< OpenGL::elTexture_2D >(
                OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
                "specularColorTexture", GL_TEXTURE2, "" );
        this->planeSpecular->TexImage< utils::byte_t >(
            { 0x00, 0x00, 0x00, 0x00 } );

        this->planeRoughness =
            this->plane->CreateTexture< OpenGL::elTexture_2D >(
                OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
                "roughnessTexture", GL_TEXTURE3, "" );
        this->planeRoughness->TexImage< utils::byte_t >(
            { 0x00, 0x00, 0x00, 0x00 } );

        this->planeEmission =
            this->plane->CreateTexture< OpenGL::elTexture_2D >(
                OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
                "emissionTexture", GL_TEXTURE4, "" );
        this->planeEmission->TexImage< utils::byte_t >(
            { 0x00, 0x00, 0x00, 0x00 } );
    }

    this->planeScalingFactor.x = properties.gridSize.x;
    this->planeScalingFactor.z = properties.gridSize.y;

    this->SetPlaneColor( properties.planeColor );
    this->SetPosition( { 0.0f, 0.0f, 0.0f } );
    this->SetScaling( { 1.0f, 1.0f, 1.0f } );

    if ( properties.doConnectToCamera )
        this->ConnectCamera( this->GetScene().GetCamera() );
}

void
elCoordinateSystem::ConnectCamera(
    const OpenGL::elCamera_Perspective& value ) noexcept
{
    this->DisconnectCamera();

    this->camera               = &value;
    this->cameraUpdateCallback = _3Delch::GetInstance().CreateMainLoopCallback(
        [this] { this->UpdateCameraPositionCallback(); } );
}

void
elCoordinateSystem::DisconnectCamera() noexcept
{
    if ( this->camera )
    {
        this->camera = nullptr;
        this->cameraUpdateCallback.Reset();
    }
}

void
elCoordinateSystem::UpdateCameraPositionCallback() noexcept
{
    auto cameraPosition = this->camera->GetState().position;
    this->coordinateSystemGenerator.UpdateBuffers(
        { cameraPosition.x, cameraPosition.z } );

    auto& geometry = this->coordinateSystemGenerator.GetGeometry();
    if ( this->majorLines )
        this->majorLines->UpdateBuffer( 0, geometry.majorLines );
    if ( this->minorLines )
        this->minorLines->UpdateBuffer( 0, geometry.minorLines );

    this->planePositionAdjustment.x = cameraPosition.x;
    this->planePositionAdjustment.y = -PLANE_POSITION_CORRECTOR;
    this->planePositionAdjustment.z = cameraPosition.z;
    this->SetPosition( this->position );
}

void
elCoordinateSystem::SetMajorDrawSize( const float size ) noexcept
{
    if ( this->majorLines )
        this->majorLines->SetDrawSize( size * GuiGL::GetDPIScaling() );
}

void
elCoordinateSystem::SetMinorDrawSize( const float size ) noexcept
{
    if ( this->minorLines )
        this->minorLines->SetDrawSize( size * GuiGL::GetDPIScaling() );
}

void
elCoordinateSystem::SetMajorColor( const color4_t& color ) noexcept
{
    if ( this->majorLines )
    {
        this->majorLines->UpdateBuffer(
            OpenGL::elGeometricObject_NonVertexObject::BUFFER_COLOR,
            utils::SetVectorFrom( this->majorLines->GetNumberOfVertices(),
                                  internal::ToGlm( color ) ) );
    }
}

void
elCoordinateSystem::SetMinorColor( const color4_t& color ) noexcept
{
    if ( this->minorLines )
        this->minorLines->UpdateBuffer(
            OpenGL::elGeometricObject_NonVertexObject::BUFFER_COLOR,
            utils::SetVectorFrom( this->minorLines->GetNumberOfVertices(),
                                  internal::ToGlm( color ) ) );
}

void
elCoordinateSystem::SetPlaneColor( const color4_t& color ) noexcept
{
    if ( this->plane )
    {
        this->planeColor->TexImage(
            glm::value_ptr( internal::ToGlm( color ) ) );
    }
}

void
elCoordinateSystem::SetPosition( const glm::vec3& v ) noexcept
{
    this->position = v;

    glm::vec3 planeCorrector =
        this->rotationMatrix * glm::vec4( this->planePositionAdjustment, 1.0 );

    if ( this->plane ) this->plane->SetPosition( v + planeCorrector );
    if ( this->minorLines ) this->minorLines->SetPosition( v );
    if ( this->majorLines ) this->majorLines->SetPosition( v );
}

void
elCoordinateSystem::SetScaling( const glm::vec3& v ) noexcept
{
    this->scaling = v;

    if ( this->plane ) this->plane->SetScaling( v * this->planeScalingFactor );
    if ( this->minorLines ) this->minorLines->SetScaling( v );
    if ( this->majorLines ) this->majorLines->SetScaling( v );
}

void
elCoordinateSystem::SetRotation( const glm::vec4& v ) noexcept
{
    this->rotation = v;

    this->rotationMatrix =
        ( this->rotation.w == 0.0f )
            ? glm::identity< glm::mat4 >()
            : glm::rotate(
                  glm::identity< glm::mat4 >(), this->rotation.w,
                  { this->rotation.x, this->rotation.y, this->rotation.z } );

    if ( this->plane ) this->plane->SetRotation( v );
    if ( this->minorLines ) this->minorLines->SetRotation( v );
    if ( this->majorLines ) this->majorLines->SetRotation( v );

    this->SetPosition( this->position );
}

void
elCoordinateSystem::SetModelMatrix( const glm::mat4& matrix ) noexcept
{
    if ( this->plane ) this->plane->SetModelMatrix( matrix );
    if ( this->minorLines ) this->minorLines->SetModelMatrix( matrix );
    if ( this->majorLines ) this->majorLines->SetModelMatrix( matrix );
}

glm::vec3
elCoordinateSystem::GetPosition() const noexcept
{
    return this->position;
}

glm::vec3
elCoordinateSystem::GetScaling() const noexcept
{
    return this->scaling;
}

glm::vec3
elCoordinateSystem::GetRotationAxis() const noexcept
{
    return { this->rotation.x, this->rotation.y, this->rotation.z };
}

float
elCoordinateSystem::GetRotationDegree() const noexcept
{
    return this->rotation.w;
}

} // namespace world
} // namespace el3D
