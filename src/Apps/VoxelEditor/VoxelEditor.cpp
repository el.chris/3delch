#include "Apps/VoxelEditor/VoxelEditor.hpp"

#include "3Delch/3Delch.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
VoxelEditor::VoxelEditor( _3Delch::elScene              &scene,
                          const voxelEditorProperties_t &p ) noexcept
    : world::elWorld( scene ), properties{ p }, voxelObject{ scene },
      coordinates( scene, p.coordinates, { .leftClick = [this]( auto p ) {
                       this->LeftClickCoordinatesCallback( p );
                   } } )
{
    this->sky = this->Create< world::elSky >(
        world::ColoredSkybox, glm::vec4{ glm::vec3{ 0.4f }, 1.0f } );

    this->sun = this->Create< world::elSun >( world::SunProperties{
        .diffuseIntensity = 0.8f,
        .ambientIntensity = 0.3f,
    } );
    this->sun->SetDirection( { 0.6f, -0.6f, 0.6f } );

    this->keyboardEvents = _3Delch::CreateEvent();
    this->keyboardEvents->SetCallback( [this]( const SDL_Event e )
                                       { this->KeyboardCallback( e ); } );
    this->keyboardEvents->Register();
}

void
VoxelEditor::KeyboardCallback( const SDL_Event e ) noexcept
{
    if ( e.type != SDL_KEYDOWN ) return;

    auto &key = e.key.keysym.sym;

    if ( key == this->properties.highlightToggleKey )
        this->coordinates.ToggleHighlight();
    else if ( key == this->properties.increaseHighlightLevelKey )
        this->coordinates.IncreaseHighlightLevel();
    else if ( key == this->properties.decreaseHighlightLevelKey )
        this->coordinates.DecreaseHighlightLevel();
}

const Coordinates &
VoxelEditor::GetCoordinates() const noexcept
{
    return this->coordinates;
}

void
VoxelEditor::LeftClickCoordinatesCallback( const glm::uvec3 p ) noexcept
{
    this->voxelObject.Add( p );
}


} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D

