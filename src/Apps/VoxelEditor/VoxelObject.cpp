#include "Apps/VoxelEditor/VoxelObject.hpp"

#include "Apps/VoxelEditor/config.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
VoxelObject::VoxelObject( _3Delch::elScene& scene ) noexcept
    : world::elWorld( scene )
{
}

void
VoxelObject::Add( const glm::uvec3& position ) noexcept
{
    auto model = this->Create< world::elModel >(
        HighGL::elObjectGeometryBuilder::CreateCube()
            .ApplyScaling( glm::vec3{ 0.05f } )
            .ApplyPosition(
                { glm::vec3{ position } / NUMBER_OF_VOXELS_PER_UNIT -
                  glm::vec3( 0.05f, -0.05f, 0.05f ) } )
            .GetObjectGeometry() );

    this->object.Insert( { position.x, position.y, position.z },
                         std::move( model ) );
}

world::elModel&
VoxelObject::Get( const glm::uvec3& position ) noexcept
{
    return *this->object.Get( { position.x, position.y, position.z } );
}

const world::elModel&
VoxelObject::Get( const glm::uvec3& position ) const noexcept
{
    return *this->object.Get( { position.x, position.y, position.z } );
}

void
VoxelObject::Remove( const glm::uvec3& position ) noexcept
{
    this->object.Remove( { position.x, position.y, position.z } );
}

bool
VoxelObject::DoesContainElementAt( const glm::uvec3& position ) const noexcept
{
    return this->object.DoesContainElementAt(
        { position.x, position.y, position.z } );
}
} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D

