#include "3Delch/3Delch.hpp"
#include "Apps/VoxelEditor/VoxelEditor.hpp"

//
#include "GuiGL/elWidget_Accordion.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_ColorPicker.hpp"
#include "GuiGL/elWidget_Grid.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "world/elGui.hpp"

using namespace el3D;

class Gui : public world::elGui
{
  public:
    Gui( _3Delch::elScene &scene ) noexcept;


  private:
    void
    CreateGuiElements() noexcept;

  private:
    using accordion_t =
        bb::product_ptr< GuiGL::elWidget_Accordion::accordion_t >;

    GuiGL::elWidget_Accordion_ptr objectPropertyWindow;

    struct
    {
        accordion_t                accordion;
        GuiGL::elWidget_Grid_ptr   buttonGrid;
        GuiGL::elWidget_Button_ptr newButton;
        GuiGL::elWidget_Button_ptr saveButton;
        GuiGL::elWidget_Button_ptr saveAsButton;
        GuiGL::elWidget_Button_ptr exitButton;
    } file;

    struct
    {
        accordion_t                accordion;
        GuiGL::elWidget_Grid_ptr   buttonGrid;
        GuiGL::elWidget_Label_ptr  colorLabel;
        GuiGL::elWidget_Button_ptr diffuseColorButton;
        GuiGL::elWidget_Button_ptr emissionColorButton;
        GuiGL::elWidget_Button_ptr specularColorButton;
        GuiGL::elWidget_Label_ptr  materialLabel;
        GuiGL::elWidget_Slider_ptr shininessSlider;
        GuiGL::elWidget_Slider_ptr roughnessSlider;

        GuiGL::elWidget_ColorPicker_ptr colorPicker;
    } object;

    struct
    {
        accordion_t accordion;
    } material;
};

Gui::Gui( _3Delch::elScene &scene ) noexcept : world::elGui( scene )
{
    this->CreateGuiElements();
}

void
Gui::CreateGuiElements() noexcept
{
    this->objectPropertyWindow = this->Create< GuiGL::elWidget_Accordion >();
    this->objectPropertyWindow->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_LEFT );
    this->objectPropertyWindow->SetStickToParentSize( { -1.0f, 0.0f } );
    this->objectPropertyWindow->SetIsMovable( false );
    this->objectPropertyWindow->SetIsResizable( false );
    this->objectPropertyWindow->SetSize( { 300.0f, 0.0f } );


    ////////
    // file
    ////////
    this->file.accordion =
        this->objectPropertyWindow->CreateAccordion( "file" );

    this->file.buttonGrid =
        this->file.accordion->frame->Create< GuiGL::elWidget_Grid >();
    this->file.buttonGrid->SetGridSpacing( glm::vec2{ 3.0f } );
    this->file.buttonGrid->SetStickToParentSize( { 0.0f, -1.0f } );
    this->file.buttonGrid->SetUniformElementHeight( 24 );
    this->file.buttonGrid->SetAdjustSizeToFitChildren( false, true );

    this->file.newButton =
        this->file.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 0, 0 } );
    this->file.newButton->SetText( "new" );

    this->file.exitButton =
        this->file.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 0, 1 } );
    this->file.exitButton->SetText( "exit" );

    this->file.saveButton =
        this->file.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 1, 0 } );
    this->file.saveButton->SetText( "save" );

    this->file.saveAsButton =
        this->file.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 1, 1 } );
    this->file.saveAsButton->SetText( "save as" );

    ////////////
    // object
    ///////////
    this->object.accordion =
        this->objectPropertyWindow->CreateAccordion( "object" );

    this->object.buttonGrid =
        this->object.accordion->frame->Create< GuiGL::elWidget_Grid >();
    this->object.buttonGrid->SetGridSpacing( glm::vec2{ 3.0f } );
    this->object.buttonGrid->SetStickToParentSize( { 0.0f, -1.0f } );
    this->object.buttonGrid->SetUniformElementHeight( 24 );
    this->object.buttonGrid->SetAdjustSizeToFitChildren( false, true );

    // object colors
    this->object.colorLabel =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Label >(
            { 0, 0 }, { .resizeOption = GuiGL::Grid::Resize::None } );
    this->object.colorLabel->SetText( "colors" );

    this->object.diffuseColorButton =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 0, 1 } );
    this->object.diffuseColorButton->SetText( "diffuse" );
    this->object.diffuseColorButton->SetClickCallback(
        [this]
        {
            this->object.colorPicker =
                this->Create< GuiGL::elWidget_ColorPicker >();
            this->object.colorPicker->SetHasTitlebar( true );
            this->object.colorPicker->GetTitlebar()->SetHasCloseButton( true );
        } );

    this->object.emissionColorButton =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 1, 1 } );
    this->object.emissionColorButton->SetText( "emission" );

    this->object.specularColorButton =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Button >(
            { 0, 2 } );
    this->object.specularColorButton->SetText( "specular" );

    // object material property
    this->object.materialLabel =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Label >(
            { 0, 3 }, { .resizeOption = GuiGL::Grid::Resize::None } );
    this->object.materialLabel->SetText( "material" );

    this->object.shininessSlider =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Slider >(
            { 0, 4 },
            { .resizeOption = GuiGL::Grid::Resize::SpanOverColumns_2 } );
    this->object.shininessSlider->SetShowValueOnButton( true, 255.0f, 0 );
    this->object.shininessSlider->SetDescriptionText( "shininess" );

    this->object.roughnessSlider =
        this->object.buttonGrid->CreateInGrid< GuiGL::elWidget_Slider >(
            { 0, 5 },
            { .resizeOption = GuiGL::Grid::Resize::SpanOverColumns_2 } );
    this->object.roughnessSlider->SetShowValueOnButton( true, 255.0f, 0 );
    this->object.roughnessSlider->SetDescriptionText( "roughness" );


    this->objectPropertyWindow->Reshape();
}


int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    scene->SetLengthOfOneMeter( 3.0f );
    scene->GetCamera().SetZFactor( units::Length::Meter( 0.01 ),
                                   units::Length::Meter( 10.0 ) );

    Apps::VoxelEditor::VoxelEditor editor(
        *scene, Apps::VoxelEditor::voxelEditorProperties_t{
                    .coordinates = Apps::VoxelEditor::CoordinatesProperties{
                        .voxelResolution = 12 } } );

    Gui gui( *scene );


    scene->SetupOrbitCamera( editor.GetCoordinates().GetCenter(), 2.0f );

    instance.StartMainLoop();
}
