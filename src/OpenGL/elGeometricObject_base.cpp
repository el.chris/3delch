#include "OpenGL/elGeometricObject_base.hpp"

#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace el3D
{
namespace OpenGL
{
elGeometricObject_ModelMatrix::elGeometricObject_ModelMatrix(
    const glm::vec3 &position, const glm::vec3 &scaling,
    const glm::vec3 &rotationAxis, const float rotationDegree ) noexcept
    : position( position ), scaling( scaling ), rotationAxis( rotationAxis ),
      rotationDegree( rotationDegree )
{
}

void
elGeometricObject_ModelMatrix::SetPosition( const glm::vec3 &v ) noexcept
{
    this->hasUpdatedValues  = true;
    this->hasModelMatrixSet = false;
    this->position          = v;
}

void
elGeometricObject_ModelMatrix::SetScaling( const glm::vec3 &v ) noexcept
{
    this->hasUpdatedValues  = true;
    this->hasModelMatrixSet = false;
    this->scaling           = v;
}

void
elGeometricObject_ModelMatrix::SetRotation( const glm::vec4 &rotation ) noexcept
{
    this->hasUpdatedValues  = true;
    this->hasModelMatrixSet = false;
    this->rotationAxis      = glm::vec3( rotation.x, rotation.y, rotation.z );
    this->rotationDegree    = rotation.w;
}

void
elGeometricObject_ModelMatrix::SetModelMatrix(
    const glm::mat4 &matrix ) noexcept
{
    this->hasUpdatedValues  = true;
    this->hasModelMatrixSet = true;
    this->modelMatrix       = matrix;
}

glm::vec3
elGeometricObject_ModelMatrix::GetPosition() const noexcept
{
    return this->position;
}

glm::vec3
elGeometricObject_ModelMatrix::GetScaling() const noexcept
{
    return this->scaling;
}

glm::vec3
elGeometricObject_ModelMatrix::GetRotationAxis() const noexcept
{
    return this->rotationAxis;
}

GLfloat
elGeometricObject_ModelMatrix::GetRotationDegree() const noexcept
{
    return this->rotationDegree;
}

glm::mat4
elGeometricObject_ModelMatrix::GetModelMatrix() const noexcept
{
    if ( this->hasUpdatedValues )
    {
        this->hasUpdatedValues = false;

        if ( !this->hasModelMatrixSet )
        {
            glm::mat4 rotated =
                ( this->rotationAxis.x == 0.0f &&
                  this->rotationAxis.y == 0.0f && this->rotationAxis.z == 0.0f )
                    ? glm::mat4( 1.0f )
                    : glm::rotate( glm::mat4( 1.0f ), this->rotationDegree,
                                   glm::vec3( this->rotationAxis ) );

            this->modelMatrix =
                glm::translate( glm::mat4( 1.0f ), this->position ) * rotated *
                glm::scale( glm::mat4( 1.0f ), this->scaling );
        }
    }
    return this->modelMatrix;
}

GLenum
elGeometricObject_base::GetDrawModeForOpenGL() const noexcept
{
    switch ( this->drawMode )
    {
        case DrawMode::Triangles:
            return static_cast< GLenum >( GL_TRIANGLES );
        case DrawMode::TrianglesWithAdjacency:
            return static_cast< GLenum >( GL_TRIANGLES_ADJACENCY );
        case DrawMode::Points:
            return static_cast< GLenum >( GL_POINTS );
        case DrawMode::LineStrip:
            return static_cast< GLenum >( GL_LINE_STRIP );
        case DrawMode::Lines:
            return static_cast< GLenum >( GL_LINES );
        default:
            LOG_ERROR( 0 ) << "geometric object with invalid draw mode";
            return static_cast< GLenum >( 0 );
    }
}

DrawMode
elGeometricObject_base::GetDrawMode() const noexcept
{
    return this->drawMode;
}

void
elGeometricObject_base::Draw( const size_t    shaderId,
                              const elCamera &camera ) const noexcept
{
    CullFace previousCullFace = CullFace::Default;
    auto     cullFace         = this->shaders[shaderId].cullFace;
    if ( cullFace != CullFace::Default )
    {
        previousCullFace = OpenGL::GetCullFaceState();
        OpenGL::SetCullFace( cullFace );
    }

    DepthTest previousDepthTest = DepthTest::Default;
    auto      depthTest         = this->shaders[shaderId].depthTest;
    if ( depthTest != DepthTest::Default )
    {
        previousDepthTest = OpenGL::GetDepthTestState();
        OpenGL::SetDepthTest( depthTest );
    }

    this->DrawImplementation( shaderId, camera );

    OpenGL::SetCullFace( previousCullFace );
    OpenGL::SetDepthTest( previousDepthTest );
}

void
elGeometricObject_base::SetCullFace( const uint64_t  shaderId,
                                     const CullFace &cullFace ) noexcept
{
    this->shaders[shaderId].cullFace = cullFace;
}

void
elGeometricObject_base::SetDepthTest( const uint64_t   shaderId,
                                      const DepthTest &depthTest ) noexcept
{
    this->shaders[shaderId].depthTest = depthTest;
}

elGeometricObject_Type
elGeometricObject_base::GetType() const noexcept
{
    return this->type;
}

void
elGeometricObject_base::SetPreDrawCallback(
    const std::function< void() > &f ) noexcept
{
    this->preDrawCallback = f;
}

void
elGeometricObject_base::SetPostDrawCallback(
    const std::function< void() > &f ) noexcept
{
    this->postDrawCallback = f;
}

void
elGeometricObject_base::CallPreDrawCallback() const noexcept
{
    if ( this->preDrawCallback ) this->preDrawCallback();
}

void
elGeometricObject_base::CallPostDrawCallback() const noexcept
{
    if ( this->postDrawCallback ) this->postDrawCallback();
}

const elShaderProgram *
elGeometricObject_base::GetShader( const size_t shaderID ) const noexcept
{
    return this->shaders[shaderID].shader;
}

size_t
elGeometricObject_base::InsertShader(
    const elShaderProgram *const shader ) noexcept
{
    auto index =
        this->shaders.find_if( [&]( auto &f ) { return f.shader == shader; } );
    if ( index != bb::elUniqueIndexVector< shader_t >::INVALID_INDEX )
        return index;

    return this->shaders.emplace_back( shader_t{ shader } ).index();
}

void
elGeometricObject_base::RemoveShader( const uint64_t shaderId ) noexcept
{
    this->shaders.erase( shaderId );
}

bb::product_guard< textureAttachment_t >
elGeometricObject_base::AttachTexture( const elTexture_base *const t,
                                       const size_t                shaderID,
                                       const std::string &         uniform,
                                       const GLenum                textureUnit )
{
    if ( shaderID == ATTACHED_TO_ALL_SHADERS )
    {
        for ( auto &s : this->shaders )
            if ( s.shader->HasUniform( uniform ) )
                s.AttachTexture( uniform, t, textureUnit );

        this->attached2AllTexturesVector.emplace_back(
            textureCache_t{ uniform, textureUnit, t } );
    }
    else
    {
        this->shaders[shaderID].AttachTexture( uniform, t, textureUnit );
    }

    return this->factory
        .CreateProductWithOnRemoveCallback< textureAttachment_t >(
            [this, t, shaderID]( auto ) {
                this->DetachTexture( t, shaderID );
            } );
}

void
elGeometricObject_base::DetachTexture( const elTexture_base *t,
                                       const size_t          shaderID ) noexcept
{
    if ( shaderID == ATTACHED_TO_ALL_SHADERS )
    {
        for ( auto &s : this->shaders )
            s.DetachTexture( t );

        bb::erase_if( this->attached2AllTexturesVector,
                      [&]( auto &e ) { return e.texture == t; } );
    }
    else
        this->shaders[shaderID].DetachTexture( t );
}

void
elGeometricObject_base::RemoveTexture(
    const elTexture_base *const texture ) noexcept
{
    for ( auto &s : this->shaders )
        s.DetachTexture( texture );
}


/*
 * shader_t implementation starts here
 */

elGeometricObject_base::shader_t::shader_t(
    const elShaderProgram *const shader ) noexcept
    : shader{ shader }
{
}

void
elGeometricObject_base::shader_t::RegisterUniforms(
    const std::vector< std::string > &uniformList ) noexcept
{
    size_t currentIndex{ this->uniforms.size() };
    this->uniforms.resize( currentIndex + uniformList.size() );
    for ( auto &uniformName : uniformList )
    {
        this->uniforms[currentIndex].name = uniformName;
        if ( this->shader->HasUniform( uniformName ) )
        {
            this->shader->GetUniformLocation( uniformName )
                .AndThen( [&]( const auto &r ) {
                    this->uniforms[currentIndex].id = r;
                } )
                .OrElse( [&] {
                    LOG_WARN( 0 )
                        << "unable to get uniform location of" << uniformName
                        << " even though the uniform is present in shader "
                        << this->shader->GetFileAndGroupName();
                    this->uniforms[currentIndex].id = -1;
                } );
        }
        else
        {
            this->uniforms[currentIndex].id = -1;
        }

        currentIndex++;
    }
}

void
elGeometricObject_base::shader_t::RegisterAttributes(
    const std::vector< elShaderProgram::shaderAttribute_t >
        &attributeList ) noexcept
{
    size_t currentIndex{ this->attributes.size() };
    this->attributes.resize( currentIndex + attributeList.size() );
    for ( auto &attribute : attributeList )
    {
        this->attributes[currentIndex] = attribute;
        if ( !attribute.identifier.empty() &&
             this->shader->HasAttribute( attribute.identifier ) )
        {
            this->shader
                ->GetAttribute( attribute.identifier, attribute.size,
                                attribute.dimension, attribute.divisor,
                                attribute.dataType )
                .AndThen( [&]( const auto &r ) {
                    this->attributes[currentIndex].id = r.id;
                } )
                .OrElse( [&] {
                    this->attributes[currentIndex].id =
                        elShaderProgram::shaderAttribute_t::INVALID_ID;
                } );
        }

        currentIndex++;
    }
}

bool
elGeometricObject_base::shader_t::AttachTexture(
    const std::string &uniformName, const elTexture_base *const texture,
    const GLenum textureUnit ) noexcept
{
    if ( this->shader->HasUniform( uniformName ) )
    {
        if ( this->shader->GetUniformLocation( uniformName )
                 .AndThen( [&]( const auto &r ) {
                     this->textures.push_back( { texture, r, textureUnit } );
                 } )
                 .HasError() )
        {
            LOG_WARN( 0 ) << "unable to get texture uniform location of"
                          << uniformName
                          << " even though the uniform is present in shader "
                          << this->shader->GetFileAndGroupName();
            return false;
        }

        return true;
    }
    else
    {
        LOG_WARN( 0 )
            << "unable to add texture to shader since the texture uniform "
            << uniformName << " is not present "
            << this->shader->GetFileAndGroupName();
    }

    return false;
}

void
elGeometricObject_base::shader_t::DetachTexture(
    const elTexture_base *const texture ) noexcept
{
    auto iter = bb::find_if( this->textures,
                             [&]( auto &e ) { return e.texture == texture; } );
    if ( iter == this->textures.end() ) return;

    this->textures.erase( iter );
}

void
elGeometricObject_base::shader_t::BindTextures() const noexcept
{
    for ( auto &t : this->textures )
        t.texture->BindActivateAndSetUniform( t.uniformID, t.textureUnit );
}

bb::elExpected< size_t, elShaderProgram_Error >
elGeometricObject_base::AddShader(
    const elShaderProgram *const shader ) noexcept
{
    size_t shaderId = this->InsertShader( shader );
    auto   result   = this->AttachShader( shaderId );
    if ( result.HasError() ) return result;

    for ( auto &t : this->factory.GetProducts< OpenGL::elTexture_base >() )
        if ( t.Extension().shaderId == ATTACHED_TO_ALL_SHADERS &&
             shader->HasUniform( t.Extension().uniform ) )
            this->shaders[shaderId].AttachTexture(
                t.Extension().uniform, t.Get(), t.Extension().textureUnit );

    for ( auto &t : this->attached2AllTexturesVector )
        if ( shader->HasUniform( t.uniform ) )
            this->shaders[shaderId].AttachTexture( t.uniform, t.texture,
                                                   t.textureUnit );

    return result;
}


} // namespace OpenGL
} // namespace el3D
