#include "OpenGL/elFrameBufferObject.hpp"

#include "OpenGL/elTexture_2D_Array.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "OpenGL/gl_Call.hpp"


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on


namespace el3D
{
namespace OpenGL
{
elFrameBufferObject::elFrameBufferObject(
    const size_t numberOfFrameBufferObjects ) noexcept
    : frameBuffer( numberOfFrameBufferObjects )
{
    gl( glGetIntegerv, static_cast< GLenum >( GL_VIEWPORT ),
        this->viewportSize );

    gl( glGenFramebuffers, static_cast< GLsizei >( numberOfFrameBufferObjects ),
        this->frameBuffer.ids.data() );
}

elFrameBufferObject::elFrameBufferObject( elFrameBufferObject&& rhs ) noexcept
    : frameBuffer( 0 )
{
    *this = std::move( rhs );
}

elFrameBufferObject::~elFrameBufferObject()
{
    if ( !this->frameBuffer.ids.empty() )
        gl( glDeleteFramebuffers,
            static_cast< GLsizei >( this->frameBuffer.ids.size() ),
            this->frameBuffer.ids.data() );
}

elFrameBufferObject&
elFrameBufferObject::operator=( elFrameBufferObject&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        if ( !this->frameBuffer.ids.empty() )
            gl( glDeleteFramebuffers,
                static_cast< GLsizei >( this->frameBuffer.ids.size() ),
                this->frameBuffer.ids.data() );

        this->frameBuffer = std::move( rhs.frameBuffer );

        rhs.frameBuffer.ids.clear();
        rhs.frameBuffer.data.clear();
    }
    return *this;
}

void
elFrameBufferObject::Clear() const noexcept
{
    for ( size_t n = 0, limit = this->frameBuffer.data.size(); n < limit; ++n )
        for ( size_t k     = 0,
                     limit = this->frameBuffer.data[k].attachments.size();
              k < limit; ++k )
            this->Clear( n, k );
}

void
elFrameBufferObject::Clear( const size_t n,
                            const size_t attachment ) const noexcept
{
    constexpr GLfloat ZEROF[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    constexpr GLint   ZEROI[] = { 0, 0, 0, 0 };

    switch ( this->frameBuffer.data[n].attachments[attachment] )
    {
        case GL_DEPTH_STENCIL_ATTACHMENT:
            gl( glClearBufferfi, static_cast< GLenum >( GL_DEPTH ), 0, 0.0f,
                0 );
            gl( glClearBufferfi, static_cast< GLenum >( GL_STENCIL ), 0, 0.0f,
                0 );
            break;
        case GL_DEPTH_ATTACHMENT:
            gl( glClearBufferfi, static_cast< GLenum >( GL_DEPTH ), 0, 0.0f,
                0 );
            break;
        default:
            switch ( this->frameBuffer.data[n].textures[attachment]->GetType() )
            {
                case GL_FLOAT:
                    gl( glClearBufferfv, static_cast< GLenum >( GL_COLOR ),
                        static_cast< GLint >( attachment ), ZEROF );
                    break;
                case GL_INT:
                    gl( glClearBufferiv, static_cast< GLenum >( GL_COLOR ),
                        static_cast< GLint >( attachment ), ZEROI );
                    break;
                default:
                    LOG_WARN( 0 ) << "undefined clear buffer texture type.";
                    break;
            }
    }
}

uint64_t
elFrameBufferObject::GetFrameBufferSize() const noexcept
{
    return this->frameBuffer.ids.size();
}

void
elFrameBufferObject::Reshape( const size_t n ) noexcept
{
    for ( auto ft : this->frameBuffer.data[n].textures )
        switch ( ft->GetTarget() )
        {
            case GL_TEXTURE_2D:
                ft->SetSize( this->frameBuffer.data[n].sizeX,
                             this->frameBuffer.data[n].sizeY );
                break;
            case GL_TEXTURE_2D_ARRAY:
                ft->SetSize( this->frameBuffer.data[n].sizeX,
                             this->frameBuffer.data[n].sizeY, 0,
                             ft->GetSize().arraySize );
                break;
            case GL_TEXTURE_CUBE_MAP:
                ft->SetSize( this->frameBuffer.data[n].sizeX,
                             this->frameBuffer.data[n].sizeY );
                break;
            default:
                LOG_ERROR( 0 )
                    << "Framebuffer contains unsupported texture! This could "
                       "cause an incomplete frameBuffer.";
                break;
        }
}

void
elFrameBufferObject::Bind( const size_t n ) const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );
    gl( glDrawBuffers,
        static_cast< GLsizei >(
            this->frameBuffer.data[n].attachmentsNoDepthStencil.size() ),
        this->frameBuffer.data[n].attachmentsNoDepthStencil.data() );

    gl( glGetIntegerv, static_cast< GLenum >( GL_VIEWPORT ),
        this->viewportSize );
    gl( glViewport, 0, 0,
        static_cast< GLsizei >( this->frameBuffer.data[n].sizeX ),
        static_cast< GLsizei >( this->frameBuffer.data[n].sizeY ) );
}

void
elFrameBufferObject::Bind( const std::vector< GLenum > buffer,
                           const size_t                n ) const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );
    if ( buffer.size() == 0 )
    {
        gl( glDrawBuffer, static_cast< GLenum >( GL_NONE ) );
    }
    else
        gl( glDrawBuffers, static_cast< GLsizei >( buffer.size() ),
            buffer.data() );

    gl( glGetIntegerv, static_cast< GLenum >( GL_VIEWPORT ),
        this->viewportSize );
    gl( glViewport, 0, 0,
        static_cast< GLsizei >( this->frameBuffer.data[n].sizeX ),
        static_cast< GLsizei >( this->frameBuffer.data[n].sizeY ) );
}

void
elFrameBufferObject::Unbind() const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_FRAMEBUFFER ), 0u );
    gl( glViewport, this->viewportSize[0], this->viewportSize[1],
        this->viewportSize[2], this->viewportSize[3] );
}

void
elFrameBufferObject::Read( const GLenum attachment,
                           const size_t n ) const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_READ_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );
    gl( glReadBuffer, attachment );
}

void
elFrameBufferObject::ReadStop() const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_READ_FRAMEBUFFER ), 0u );
}

void
elFrameBufferObject::SetSize( const uint64_t sizeX, const uint64_t sizeY,
                              const size_t n ) noexcept
{
    this->frameBuffer.data[n].sizeX =
        std::max( static_cast< uint64_t >( 1ul ), sizeX );
    this->frameBuffer.data[n].sizeY =
        std::max( static_cast< uint64_t >( 1ul ), sizeY );
    this->Reshape( n );
}

void
elFrameBufferObject::Draw( const size_t n ) const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );
}

void
elFrameBufferObject::DrawStop() const noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ), 0u );
}

uint64_t
elFrameBufferObject::AddTexture( const size_t          n,
                                 elTexture_base* const texture ) noexcept
{
    bool     hasTextureFound = false;
    uint64_t textureIndex    = 0u;
    for ( uint64_t i = 0, l = this->frameBuffer.data[n].textures.size(); i < l;
          ++i )
        if ( this->frameBuffer.data[n].textures[i] == texture )
        {
            textureIndex    = i;
            hasTextureFound = true;
            break;
        }

    if ( !hasTextureFound )
    {
        textureIndex = this->frameBuffer.data[n].textures.size();
        this->frameBuffer.data[n].textures.emplace_back( texture );
    }

    return textureIndex;
}

size_t
elFrameBufferObject::AddFramebufferTexture( elTexture_2D* tex,
                                            const GLenum  attachment,
                                            const size_t  n ) noexcept
{
    return this->AddFramebufferTexture2D( tex, GL_TEXTURE_2D, attachment, n );
}

size_t
elFrameBufferObject::AddFramebufferTexture( elTexture_2D_Array* tex,
                                            const GLint         layer,
                                            const GLenum        attachment,
                                            const size_t        n ) noexcept
{
    return this->AddFramebufferTextureLayer( tex, layer, attachment, n );
}

size_t
elFrameBufferObject::AddFramebufferTexture( elTexture_CubeMap* tex,
                                            const GLenum       cubeTarget,
                                            const GLenum       attachment,
                                            const size_t       n ) noexcept
{
    return this->AddFramebufferTexture2D( tex, cubeTarget, attachment, n );
}

size_t
elFrameBufferObject::AddFramebufferTexture2D( elTexture_base* const tex,
                                              const GLenum          target,
                                              const GLenum          attachment,
                                              const size_t          n ) noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );

    this->Attach( n, attachment );

    gl( glFramebufferTexture2D, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ),
        attachment, target, tex->GetID(), 0 );
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ), 0u );
    tex->SetSize( this->frameBuffer.data[n].sizeX,
                  this->frameBuffer.data[n].sizeY );

    return this->AddTexture( n, tex );
}

size_t
elFrameBufferObject::AddFramebufferTextureLayer( elTexture_base* const tex,
                                                 const GLint           layer,
                                                 const GLenum attachment,
                                                 const size_t n ) noexcept
{
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ),
        this->frameBuffer.ids[n] );

    this->Attach( n, attachment );

    gl( glFramebufferTextureLayer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ),
        attachment, tex->GetID(), 0, layer );
    gl( glBindFramebuffer, static_cast< GLenum >( GL_DRAW_FRAMEBUFFER ), 0u );
    tex->SetSize( this->frameBuffer.data[n].sizeX,
                  this->frameBuffer.data[n].sizeY, 0,
                  tex->GetSize().arraySize );

    return this->AddTexture( n, tex );
}

void
elFrameBufferObject::Attach( const size_t n, const GLenum attachment ) noexcept
{
    this->frameBuffer.data[n].attachments.emplace_back( attachment );

    if ( attachment != GL_DEPTH_STENCIL_ATTACHMENT &&
         attachment != GL_DEPTH_ATTACHMENT )
    {
        this->frameBuffer.data[n].attachmentsNoDepthStencil.emplace_back(
            attachment );
    }
}

void
elFrameBufferObject::BlitFrameBuffer( const GLint srcX0, const GLint srcY0,
                                      const GLint srcX1, const GLint srcY1,
                                      const GLint dstX0, const GLint dstY0,
                                      const GLint dstX1, const GLint dstY1,
                                      const GLbitfield mask,
                                      const GLenum     filter )
{
    gl( glBlitFramebuffer, srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1,
        dstY1, mask, filter );
}

elTexture_base*
elFrameBufferObject::GetTexture( const size_t textureIndex,
                                 const size_t n ) const noexcept
{
    return this->frameBuffer.data[n].textures[textureIndex];
}

glm::ivec2
elFrameBufferObject::GetSize( const size_t n ) const noexcept
{
    return glm::ivec2( this->frameBuffer.data[n].sizeX,
                       this->frameBuffer.data[n].sizeY );
}

elFrameBufferObject::frameBuffer_t::frameBuffer_t( const size_t n )
    : ids( n ), data( n )
{
}

GLint
elFrameBufferObject::GetMaximumNumberOfColorAttachments() const noexcept
{
    GLint returnValue;
    gl( glGetIntegerv, static_cast< GLenum >( GL_MAX_COLOR_ATTACHMENTS ),
        &returnValue );
    return returnValue;
}

GLenum
elFrameBufferObject::CheckFramebufferStatus( const size_t n ) const noexcept
{
    this->Bind( n );
    auto ret = gl( glCheckFramebufferStatus,
                   static_cast< GLenum >( GL_FRAMEBUFFER_EXT ) );
    this->Unbind();
    return ret;
}


} // namespace OpenGL
} // namespace el3D
