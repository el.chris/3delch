#include "OpenGL/elBufferObject.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/gl_Call.hpp"

namespace el3D
{
namespace OpenGL
{
elBufferObject::elBufferObject( const size_t& n, const GLenum& target,
                                const GLenum& usage ) noexcept
    : stdTarget( target ), stdUsage( usage ), bufferIDs( n ), bufferTarget( n ),
      bufferUsage( n )
{
    std::fill( this->bufferTarget.begin(), this->bufferTarget.end(), target );
    std::fill( this->bufferUsage.begin(), this->bufferUsage.end(), usage );

    gl( glGenBuffers, static_cast< GLsizei >( n ), this->bufferIDs.data() );
}

elBufferObject::elBufferObject( elBufferObject&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elBufferObject::~elBufferObject()
{
    if ( !this->bufferIDs.empty() )
    {
        gl( glDeleteBuffers, static_cast< GLsizei >( this->bufferIDs.size() ),
            this->bufferIDs.data() );
    }
}

elBufferObject&
elBufferObject::operator=( elBufferObject&& rhs ) noexcept
{
    if ( &rhs != this )
    {
        if ( !this->bufferIDs.empty() )
            gl( glDeleteBuffers,
                static_cast< GLsizei >( this->bufferIDs.size() ),
                this->bufferIDs.data() );

        this->bufferIDs    = std::move( rhs.bufferIDs );
        this->bufferTarget = std::move( rhs.bufferTarget );
        this->bufferUsage  = std::move( rhs.bufferUsage );
        this->stdTarget    = std::move( rhs.stdTarget );
        this->stdTarget    = std::move( rhs.stdUsage );

        rhs.bufferIDs.clear();
    }

    return *this;
}

size_t
elBufferObject::GetNumberOfBuffers() const noexcept
{
    return this->bufferIDs.size();
}

void
elBufferObject::Bind( const size_t n ) const noexcept
{
    gl( glBindBuffer, this->bufferTarget[n], this->bufferIDs[n] );
}

void
elBufferObject::Unbind( const size_t n ) const noexcept
{
    gl( glBindBuffer, this->bufferTarget[n], 0u );
}

void
elBufferObject::SetBufferTarget( const GLenum type, const size_t n ) noexcept
{
    this->bufferTarget[n] = type;
}

void
elBufferObject::SetBufferUsage( const GLenum usage, const size_t n ) noexcept
{
    this->bufferUsage[n] = usage;
}

GLint
elBufferObject::GetBufferParameteriv( const GLenum value, const size_t n ) const
    noexcept
{
    GLint retVal;
    this->Bind( n );
    gl( glGetBufferParameteriv, this->bufferTarget[n], value, &retVal );
    return retVal;
}

void
elBufferObject::BufferData( const GLsizeiptr size, const GLvoid* data,
                            const size_t n ) const noexcept
{
    this->Bind( n );
    gl( glBufferData, this->bufferTarget[n], size, data, this->bufferUsage[n] );
}

void
elBufferObject::BufferSubData( const GLintptr offset, const GLsizeiptr size,
                               const GLvoid* data, const size_t n ) const
    noexcept
{
    this->Bind( n );
    gl( glBufferSubData, this->bufferTarget[n], offset, size, data );
}

void
elBufferObject::GetBufferSubData( const GLintptr offset, const GLsizei size,
                                  GLvoid* data, const size_t n ) const noexcept
{
    this->Bind( n );
    gl( glGetBufferSubData, this->bufferTarget[n], offset, size, data );
}
} // namespace OpenGL
} // namespace el3D
