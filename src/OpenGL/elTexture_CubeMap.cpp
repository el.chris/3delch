#include "OpenGL/elTexture_CubeMap.hpp"

#include "OpenGL/gl_Call.hpp"

namespace el3D
{
namespace OpenGL
{
elTexture_CubeMap::elTexture_CubeMap( const std::string& debugIdentifier,
                                      const uint64_t     sizeX,
                                      const uint64_t     sizeY,
                                      const GLint        internalFormat,
                                      const GLenum       format,
                                      const GLenum       type ) noexcept
    : elTexture_base( debugIdentifier, internalFormat, format, type,
                      GL_TEXTURE_CUBE_MAP )
{
    this->SetSize( sizeX, sizeY );

    this->TexParameteri( GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    this->TexParameteri( GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    this->TexParameteri( GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );

    this->Reshape();
}

elTexture_CubeMap::elTexture_CubeMap( elTexture_CubeMap&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elTexture_CubeMap&
elTexture_CubeMap::operator=( elTexture_CubeMap&& rhs ) noexcept
{
    elTexture_base::operator=( std::move( rhs ) );
    return *this;
}

void
elTexture_CubeMap::TexImage( const GLvoid* const dataPositiveX,
                             const GLvoid* const dataNegativeX,
                             const GLvoid* const dataPositiveY,
                             const GLvoid* const dataNegativeY,
                             const GLvoid* const dataPositiveZ,
                             const GLvoid* const dataNegativeZ ) noexcept
{
    this->Bind();

    auto setImage = [&]( GLenum target, const GLvoid* data ) {
        gl( glTexImage2D, target, 0, this->internalFormat,
            static_cast< GLsizei >( this->size.x ),
            static_cast< GLsizei >( this->size.y ), 0, this->format, this->type,
            data );
    };

    setImage( GL_TEXTURE_CUBE_MAP_POSITIVE_X, dataPositiveX );
    setImage( GL_TEXTURE_CUBE_MAP_POSITIVE_Y, dataPositiveY );
    setImage( GL_TEXTURE_CUBE_MAP_POSITIVE_Z, dataPositiveZ );
    setImage( GL_TEXTURE_CUBE_MAP_NEGATIVE_X, dataNegativeX );
    setImage( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, dataNegativeY );
    setImage( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, dataNegativeZ );
}

} // namespace OpenGL
} // namespace el3D
