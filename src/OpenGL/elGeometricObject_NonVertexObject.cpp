#include "OpenGL/elGeometricObject_NonVertexObject.hpp"

#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"

#include <cmath>
#include <initializer_list>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
const std::vector< elShaderProgram::shaderAttribute_t >
    elGeometricObject_NonVertexObject::ATTRIBUTES = {
        { "coord", 3 }, { "", 0, 0, 0, GL_UNSIGNED_INT }, { "color", 4 } };


elGeometricObject_NonVertexObject::elGeometricObject_NonVertexObject(
    const DrawMode drawMode, const std::vector< GLfloat > &vertices )
    : elGeometricObject_base(
          drawMode, 3, elGeometricObject_Type::NonVertexObject, vertices,
          std::vector< GLuint >{}, 4u, std::vector< GLfloat >{} ),
      numberOfVertices( vertices.size() / 3u )
{
}

void
elGeometricObject_NonVertexObject::DrawImplementation(
    const uint64_t shaderID, const elCamera &camera ) const noexcept
{
    this->CallPreDrawCallback();

    this->Bind( shaderID, camera.GetMatrices().viewProjection );

    OpenGL::Enable( GL_LINE_SMOOTH );
    OpenGL::Enable( GL_BLEND );

    this->UpdateDrawSize();
    OpenGL::gl( glDrawArrays, this->GetDrawModeForOpenGL(), 0,
                static_cast< GLsizei >( this->numberOfVertices ) );
    this->Unbind( shaderID );

    this->CallPostDrawCallback();
}

bb::elExpected< size_t, elShaderProgram_Error >
elGeometricObject_NonVertexObject::AttachShader(
    const uint64_t shaderId ) noexcept
{
    auto &shader = this->shaders[shaderId];
    shader.RegisterAttributes( ATTRIBUTES );
    if ( !shader.attributes[BUFFER_VERTEX] )
    {
        LOG_ERROR( 0 ) << "the shader attribute "
                       << ATTRIBUTES[BUFFER_VERTEX].identifier
                       << " is required for the vertex object but not present "
                          "in the shader "
                       << shader.shader->GetFileAndGroupName();
        this->RemoveShader( shaderId );
        return bb::Error(
            elShaderProgram_Error::UnableToFindAttributeLocation );
    }

    shader.RegisterUniforms( { MVP_ATTRIBUTE_NAME } );

    return bb::Success( shaderId );
}

void
elGeometricObject_NonVertexObject::Bind(
    const size_t shaderID, const glm::mat4 &viewProjection ) const noexcept
{
    auto &shader = this->shaders[shaderID];
    shader.shader->Bind();
    if ( shader.uniforms[UNIFORM_MVP].id != -1 )
        shader.shader->SetUniform( shader.uniforms[UNIFORM_MVP].id,
                                   viewProjection * this->GetModelMatrix() );

    shader.BindTextures();

    this->objectBuffer.Bind();
    this->objectBuffer.Enable( { BUFFER_VERTEX },
                               shader.attributes[BUFFER_VERTEX] );
    for ( size_t i = 0; i < BUFFER_END; ++i )
    {
        if ( i == BUFFER_VERTEX || i == BUFFER_ELEMENT ) continue;
        this->objectBuffer.Enable( { i }, shader.attributes[i] );
    }
}

void
elGeometricObject_NonVertexObject::UpdateDrawSize() const noexcept
{
    switch ( this->GetDrawMode() )
    {
        case DrawMode::Lines:
            [[fallthrough]];
        case DrawMode::LineStrip:
            OpenGL::gl( glLineWidth, this->drawSize );
            break;
        case DrawMode::Points:
            OpenGL::gl( glPointSize, this->drawSize );
            break;
        default:
            LOG_ERROR( 0 )
                << "unable to set the size for this unsupported draw mode";
            break;
    }
}

void
elGeometricObject_NonVertexObject::SetDrawSize( const float size ) noexcept
{
    this->drawSize = size;
}

void
elGeometricObject_NonVertexObject::Unbind(
    const size_t shaderID ) const noexcept
{
    this->objectBuffer.Disable( this->shaders[shaderID].attributes );
}

uint64_t
elGeometricObject_NonVertexObject::GetNumberOfVertices() const noexcept
{
    return this->numberOfVertices;
}

} // namespace OpenGL
} // namespace el3D
