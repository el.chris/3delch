#include "OpenGL/elVertexArrayObject.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/gl_Call.hpp"
#include "OpenGL/types.hpp"


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
GLuint elVertexArrayObject::activeVertexArrayObject = 0;

elVertexArrayObject::elVertexArrayObject( const size_t &n ) noexcept
    : vaoIDs( n )
{
    gl( glGenVertexArrays, static_cast< GLsizei >( n ), this->vaoIDs.data() );
}

elVertexArrayObject::elVertexArrayObject( elVertexArrayObject &&rhs ) noexcept
{
    *this = std::move( rhs );
}

elVertexArrayObject::~elVertexArrayObject()
{
    gl( glDeleteVertexArrays, static_cast< GLsizei >( this->vaoIDs.size() ),
        this->vaoIDs.data() );
}

elVertexArrayObject &
elVertexArrayObject::operator=( elVertexArrayObject &&rhs ) noexcept
{
    if ( this != &rhs )
    {
        gl( glDeleteVertexArrays, static_cast< GLsizei >( this->vaoIDs.size() ),
            this->vaoIDs.data() );
        this->vaoIDs = std::move( rhs.vaoIDs );
        rhs.vaoIDs.clear();
    }

    return *this;
}

void
elVertexArrayObject::Bind( const size_t &n ) const noexcept
{
    if ( this->vaoIDs[n] == elVertexArrayObject::activeVertexArrayObject )
        return;

    gl( glBindVertexArray, this->vaoIDs[n] );
    elVertexArrayObject::activeVertexArrayObject = this->vaoIDs[n];
}

void
elVertexArrayObject::Unbind() const noexcept
{
    gl( glBindVertexArray, 0u );
    elVertexArrayObject::activeVertexArrayObject = 0;
}

void
elVertexArrayObject::EnableVertexAttribArray(
    const elShaderProgram::shaderAttribute_t &a ) const noexcept
{
    std::function< void( GLuint i ) >   attribPointerCall;
    std::function< void *( GLuint i ) > getOffset = [&]( GLuint i ) {
        return reinterpret_cast< void * >( static_cast< uintptr_t >(
            i * SizeOfType( GL_FLOAT ) * static_cast< GLuint >( a.size ) ) );
    };

    GLsizei stride =
        static_cast< GLsizei >( SizeOfType( GL_FLOAT ) *
                                static_cast< GLuint >( a.size ) * a.dimension );

    if ( a.dataType == GL_INT || a.dataType == GL_UNSIGNED_INT ||
         a.dataType == GL_SHORT || a.dataType == GL_UNSIGNED_SHORT ||
         a.dataType == GL_BYTE || a.dataType == GL_UNSIGNED_BYTE )
    {
        attribPointerCall = [&]( GLuint i ) {
            gl( glVertexAttribIPointer, a.id + i, a.size, a.dataType, stride,
                getOffset( i ) );
        };
    }
    else if ( a.dataType == GL_DOUBLE )
    {
        attribPointerCall = [&]( GLuint i ) {
            gl( glVertexAttribLPointer, a.id + i, a.size, a.dataType, stride,
                getOffset( i ) );
        };
    }
    else if ( a.dataType == GL_FLOAT )
    {
        attribPointerCall = [&]( GLuint i ) {
            gl( glVertexAttribPointer, a.id + i, a.size, a.dataType,
                static_cast< GLboolean >( GL_FALSE ), stride, getOffset( i ) );
        };
    }
    else
    {
        LOG_ERROR( 0 ) << "unsupported dataType " << a.dataType << " requested";
        return;
    }


    for ( GLuint i = 0; i < a.dimension; ++i )
    {
        gl( glEnableVertexAttribArray, a.id + i );
        attribPointerCall( i );
    }

    if ( a.divisor != 0 )
        for ( GLuint i = 0; i < a.dimension; ++i )
            gl( glVertexAttribDivisor, a.id + i, a.divisor );
}

void
elVertexArrayObject::DisableVertexAttribArray(
    const elShaderProgram::shaderAttribute_t &a ) const noexcept
{
    for ( GLuint i = 0; i < a.dimension; ++i )
        gl( glDisableVertexAttribArray, a.id + i );
}

void
elVertexArrayObject::VertexAttribDivisor(
    const elShaderProgram::shaderAttribute_t &a ) const noexcept
{
    gl( glVertexAttribDivisor, a.id, a.divisor );
}
} // namespace OpenGL
} // namespace el3D
