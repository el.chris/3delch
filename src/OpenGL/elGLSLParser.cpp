#include "OpenGL/elGLSLParser.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "utils/elFileSystem.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
std::vector< std::string > elGLSLParser::searchPaths;

elGLSLParser::elGLSLParser() : shaderFile( "empty" )
{
    this->create_isInitialized = false;
}

elGLSLParser::elGLSLParser( const std::string&     filePath,
                            const int              shaderVersion,
                            const staticDefines_t& defines )
    : shaderFile( filePath ), shaderVersion( shaderVersion ), defines( defines )
{
    std::string shaderFilePath = filePath;
    if ( !utils::elFileSystem::IsRegularFile( shaderFilePath ) )
    {
        shaderFilePath.clear();
        for ( auto& p : searchPaths )
        {
            if ( utils::elFileSystem::IsRegularFile(
                     p + utils::elFileSystem::PATH_DELIMITER + filePath

                     ) )
            {
                shaderFilePath =
                    p + utils::elFileSystem::PATH_DELIMITER + filePath;
                break;
            }
        }
    }

    if ( shaderFilePath.empty() )
    {
        LOG_ERROR( 0 ) << "could not find shader file " + filePath;
        this->create_isInitialized = false;
        this->create_error         = elGLSLParser_Error::CouldNotFindShaderFile;
        return;
    }

    shaderFile = utils::elFile( shaderFilePath );

    if ( !this->shaderFile.Read() )
    {
        LOG_ERROR( 0 ) << "could not read shader file " + filePath;
        this->create_isInitialized = false;
        this->create_error         = elGLSLParser_Error::UnableToReadShaderFile;
    }
    else
    {
        this->ParseShader()
            .AndThen( [&] { this->create_isInitialized = true; } )
            .OrElse( [&]( const auto& r ) {
                this->create_isInitialized = false;
                this->create_error         = r;
            } );
    }
}

bb::elExpected< std::vector< std::string >, elGLSLParser_Error >
elGLSLParser::ExtractSeparatedGroups( const std::string& rawGroup ) const
{
    std::vector< std::string > groups;
    size_t                     previous = 0;

    for ( size_t next = rawGroup.find( groupSeparator, previous );
          next != std::string::npos; previous = next + 1,
                 next = rawGroup.find( groupSeparator, previous ) )
        groups.emplace_back( rawGroup.substr( previous, next - previous ) );

    groups.emplace_back( rawGroup.substr( previous ) );

    for ( auto& group : groups )
    {
        if ( group.size() == 0 )
        {
            LOG_ERROR( 0 ) << "empty group names are not allowed ("
                           << this->shaderFile.GetPath() << ")";
            return bb::Error( elGLSLParser_Error::SyntaxError );
        }
    }

    return bb::Success( groups );
}

bb::elExpected< std::vector< std::string >, elGLSLParser_Error >
elGLSLParser::ExtractGroupNames( const std::string& name,
                                 const int          lineNumber ) const
{
    std::string retVal = name;
    if ( retVal.substr( retVal.size() - this->groupIdentifierEnd.size() )
             .compare( this->groupIdentifierEnd ) )
    {
        LOG_ERROR( 0 ) << "line " << lineNumber << " : " << name
                       << " is not a valid groupname, "
                       << "groupname-syntax: " << this->groupIdentifierBegin
                       << "NAME" << this->groupIdentifierEnd
                       << "\\n ( with "
                          "a linebreak after "
                       << this->groupIdentifierEnd + ")";
        return bb::Error( elGLSLParser_Error::SyntaxError );
    }
    retVal =
        retVal.substr( 0, retVal.size() - this->groupIdentifierEnd.size() );

    return this->ExtractSeparatedGroups( retVal );
}

bb::elExpected< GLenum, elGLSLParser_Error >
elGLSLParser::ExtractShaderType( const std::string& type,
                                 const int          lineNumber ) const
{
    if ( !type.compare( this->vertexIdentifier ) )
        return bb::Success< GLenum >( GL_VERTEX_SHADER );
    else if ( !type.compare( this->fragmentIdentifier ) )
        return bb::Success< GLenum >( GL_FRAGMENT_SHADER );
    else if ( !type.compare( this->geometryIdentifier ) )
        return bb::Success< GLenum >( GL_GEOMETRY_SHADER );
    else if ( !type.compare( this->tessEvaluationIdentifier ) )
        return bb::Success< GLenum >( GL_TESS_EVALUATION_SHADER );
    else if ( !type.compare( this->tessControlIdentifier ) )
        return bb::Success< GLenum >( GL_TESS_CONTROL_SHADER );
    else if ( !type.compare( this->computeIdentifier ) )
        return bb::Success< GLenum >( GL_COMPUTE_SHADER );
    else
    {
        LOG_ERROR( 0 ) << "line " << lineNumber << " : " << type
                       << " is not a valid shader type, possible types are '"
                       << this->vertexIdentifier << "', '"
                       << this->fragmentIdentifier << "', '"
                       << this->geometryIdentifier << "', '"
                       << this->tessEvaluationIdentifier << "', '"
                       << this->tessControlIdentifier << "', '"
                       << this->computeIdentifier << "'";
        return bb::Error( elGLSLParser_Error::SyntaxError );
    }
}

std::string
elGLSLParser::ExtractShaderPath() const noexcept
{
    std::string path     = shaderFile.GetPath();
    size_t      position = path.find_last_of( "\\/" );
    if ( position != std::string::npos )
    {
        path = path.substr( 0, position + 1 );
        return path;
    }

    return path;
}

bb::elExpected< elGLSLParser_Error >
elGLSLParser::ParseShader()
{
    std::vector< std::string > groupNames;
    GLenum                     shaderType          = GL_NONE;
    int                        lineNumber          = 1;
    bool                       areStaticDefinesSet = false;
    std::vector< std::string > shaderFileContent =
        this->shaderFile.GetContentAsVector();

    for ( size_t i = 0; i < shaderFileContent.size(); ++i )
    {
        auto line = shaderFileContent[i];
        // extract shadertype or group
        bool isLineUsed = false;
        if ( !line.substr( 0, this->groupIdentifierBegin.size() )
                  .compare( this->groupIdentifierBegin ) )
        {
            shaderType           = GL_NONE;
            auto groupNameResult = this->ExtractGroupNames(
                line.substr( this->groupIdentifierBegin.size() ), lineNumber );
            if ( groupNameResult.HasError() ) return groupNameResult;
            groupNames = groupNameResult.GetValue();
            isLineUsed = true;
        }
        else if ( !line.substr( 0, this->typeIdentifier.size() )
                       .compare( this->typeIdentifier ) )
        {
            auto shaderTypeResult = this->ExtractShaderType(
                line.substr( this->typeIdentifier.size() ), lineNumber );
            if ( shaderTypeResult.HasError() ) return shaderTypeResult;
            shaderType = shaderTypeResult.GetValue();
            isLineUsed = true;
        }
        // extract include files
        else if ( !line.substr( 0, this->includeIdentifier.size() )
                       .compare( this->includeIdentifier ) )
        {
            auto result = this->AddFileToShaderContent(
                line.substr( this->includeIdentifier.size() ),
                shaderFileContent, i );
            if ( result.HasError() ) return result;

            continue;
        }

        // syntax check: no shadertype before group
        if ( shaderType != GL_NONE && groupNames.empty() )
        {
            LOG_ERROR( 0 ) << "line " << lineNumber
                           << " : you need to open a group with "
                           << this->groupIdentifierBegin << "groupname"
                           << this->groupIdentifierEnd << " first before you "
                           << "can declare a shaderType";
            return bb::Error( elGLSLParser_Error::SyntaxError );
        }
        // syntax check:: no lines before group and shadertype
        if ( !isLineUsed && ( shaderType == GL_NONE || groupNames.empty() ) )
        {
            LOG_ERROR( 0 )
                << "line " << lineNumber
                << " : no empty lines are allowed before the group name "
                << "or the first shaderType";
            return bb::Error( elGLSLParser_Error::SyntaxError );
        }

        // set static defines right after version
        for ( auto& name : groupNames )
        {
            if ( this->content[name][shaderType].version != -1 &&
                 !areStaticDefinesSet )
            {
                auto iter =
                    this->defines.find( groupType_t{ name, shaderType } );
                if ( iter != this->defines.end() )
                    for ( auto& d : iter->second )
                        this->content[name][shaderType].code +=
                            "#define " + d.name + " " + d.value + "\n";
            }
        }
        areStaticDefinesSet = true;

        // check version; if its to high reduce it
        bool doIncrementLineNumber = false;

        for ( auto& name : groupNames )
        {
            if ( this->content[name][shaderType].version == -1 )
            {
                areStaticDefinesSet = false;
                this->content[name][shaderType].version =
                    this->ExtractVersion( line );
                if ( this->content[name][shaderType].version != -1 &&
                     this->content[name][shaderType].version >
                         this->shaderVersion )
                {

                    LOG_WARN( 0 )
                        << "Shader version: "
                        << this->content[name][shaderType].version
                        << " is not supported by the GPU! Reducing the version "
                           "to: "
                        << this->shaderVersion
                        << "! This can cause errors in the shader.";
                    if ( !isLineUsed )
                        this->content[name][shaderType].code +=
                            "#version " +
                            std::to_string( this->shaderVersion ) + "\n";

                    doIncrementLineNumber = true;
                }
            }
        }

        if ( doIncrementLineNumber )
        {
            lineNumber++;
            continue;
        }

        for ( auto& name : groupNames )
            if ( !isLineUsed )
                this->content[name][shaderType].code += line + "\n";

        lineNumber++;
    }

    this->FillWithDefaultGroup();
    return bb::Success<>();
}

void
elGLSLParser::FillWithDefaultGroup() noexcept
{
    auto defGroup = this->content.find( defaultGroupName );
    if ( defGroup == this->content.end() ) return;

    for ( auto group = this->content.begin(); group != this->content.end();
          ++group )
    {
        if ( group == defGroup ) continue;

        for ( auto defShaderType = defGroup->second.begin();
              defShaderType != defGroup->second.end(); ++defShaderType )
        {

            auto shaderType = group->second.find( defShaderType->first );
            if ( shaderType == group->second.end() )
            {
                this->content[group->first][defShaderType->first] =
                    defShaderType->second;
            }
        }
    }
}

int
elGLSLParser::ExtractVersion( const std::string& line ) const noexcept
{
    std::string spacing = " \t";
    std::string temp    = line;


    size_t hashSign = temp.find_first_not_of( spacing );
    if ( hashSign == std::string::npos )
        return -1;
    else if ( temp[hashSign] != '#' )
        return -1;
    temp = temp.substr( hashSign + 1 );


    size_t macroNamePos = temp.find_first_not_of( spacing );
    if ( macroNamePos == std::string::npos )
        return -1;
    else
        temp = temp.substr( macroNamePos );


    if ( !( temp.size() > 7 && temp.substr( 0, 7 ) == "version" ) ) return -1;
    temp = temp.substr( 8 );

    size_t valuePos = temp.find_first_not_of( spacing );
    if ( valuePos == std::string::npos )
        return -1;
    else
        temp = temp.substr( valuePos );

    return static_cast< int >( strtol( temp.c_str(), nullptr, 10 ) );
}

const std::map< std::string, std::map< GLenum, elGLSLParser::shader_t > >&
elGLSLParser::GetContent() const noexcept
{
    return content;
}

bb::elExpected< elGLSLParser_Error >
elGLSLParser::AddShaderIncludePath( const std::string& path ) noexcept
{
    if ( !utils::elFileSystem::IsDirectory( path ) )
        return bb::Error( elGLSLParser_Error::SearchPathIsNotADirectory );

    elGLSLParser::searchPaths.emplace_back( path );
    return bb::Success<>();
}

bb::elExpected< elGLSLParser_Error >
elGLSLParser::AddFileToShaderContent( const std::string&          fileName,
                                      std::vector< std::string >& shaderContent,
                                      const size_t insertPosition ) noexcept
{
    std::vector< std::string > paths;
    paths.emplace_back( this->ExtractShaderPath() );
    paths.insert( paths.end(), elGLSLParser::searchPaths.begin(),
                  elGLSLParser::searchPaths.end() );

    for ( const auto& lookupPath : paths )
    {
        std::string shaderIncludeFile =
            lookupPath + utils::elFileSystem::PATH_DELIMITER + fileName;

        if ( !utils::elFileSystem::IsRegularFile( shaderIncludeFile ) )
            continue;

        utils::elFile includeShader( shaderIncludeFile );
        if ( !includeShader.Read() ) continue;

        std::vector< std::string > content = includeShader.GetContentAsVector();
        shaderContent.insert( shaderContent.begin() +
                                  static_cast< long >( insertPosition ) + 1,
                              content.begin(), content.end() );
        return bb::Success<>();
    }

    LOG_ERROR( 0 ) << "unable to read shader include file " << fileName;
    return bb::Error( elGLSLParser_Error::UnableToReadShaderIncludeFile );
}

bool
elGLSLParser::groupType_t::operator<( const groupType_t& rhs ) const noexcept
{
    return ( ( this->group == rhs.group ) ? this->type < rhs.type
                                          : this->group < rhs.group );
}

} // namespace OpenGL
} // namespace el3D
