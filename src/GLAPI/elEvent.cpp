#include "GLAPI/elEvent.hpp"

#include "GLAPI/elEventHandler.hpp"

namespace el3D
{
namespace GLAPI
{
bool   elEvent::hasTextInput        = false;
size_t elEvent::hasTextInputCounter = 0;

elEvent::elEvent( elEventHandler *const parent ) noexcept : parent( parent )
{
    this->automaticDeRegistrationCallback = [&] { this->Unregister(); };
}

elEvent::~elEvent()
{
    this->Reset();
}

void
elEvent::Reset() noexcept
{
    this->Unregister();

    this->callback                        = compareCallback_t();
    this->afterRemovalCallback            = compareCallback_t();
    this->dtorComparator                  = compareCallback_t();
    this->automaticDeRegistrationCallback = [&] { this->Unregister(); };
}

elEvent &
elEvent::SetRemovalCondition( const compareCallback_t &comparision ) noexcept
{
    this->dtorComparator = comparision;
    return *this;
}

bool
elEvent::HasDestructionCondition( const SDL_Event rhs ) const noexcept
{
    if ( !this->dtorComparator ) return false;
    return this->dtorComparator( rhs );
}

elEvent &
elEvent::SetCallback( const callback_t &c ) noexcept
{
    this->callback = c;
    return *this;
}

elEvent &
elEvent::SetAfterRemovalCallback( const callback_t &c ) noexcept
{
    this->afterRemovalCallback = c;
    return *this;
}

void
elEvent::StartTextInput() noexcept
{
    if ( elEvent::hasTextInput ) return;

    SDL_StartTextInput();
    elEvent::hasTextInput = true;
}

void
elEvent::StopTextInput() noexcept
{
    if ( !elEvent::hasTextInput ) return;

    elEvent::hasTextInputCounter = 0;
    SDL_StopTextInput();
    elEvent::hasTextInput = false;
}

bool
elEvent::HasTextInput() noexcept
{
    return elEvent::hasTextInput;
}

void
elEvent::MultiInstanceStartTextInput() noexcept
{
    elEvent::hasTextInputCounter++;
    elEvent::StartTextInput();
}

void
elEvent::MultiInstanceStopTextInput() noexcept
{
    if ( elEvent::hasTextInputCounter > 1 )
        elEvent::hasTextInputCounter--;
    else
        elEvent::StopTextInput();
}

elEvent &
elEvent::Register() noexcept
{
    if ( !this->isRegistered )
    {
        if ( this->isActive ) this->parent->RegisterEvent( this );
        this->isRegistered = true;
    }
    return *this;
}

void
elEvent::Unregister() const noexcept
{
    if ( this->isRegistered )
    {
        if ( this->isActive ) this->parent->UnregisterEvent( this );
        this->isRegistered = false;
    }
}

bool
elEvent::IsRegistered() const noexcept
{
    return this->isRegistered;
}

void
elEvent::Activate() noexcept
{
    if ( this->isActive ) return;

    if ( this->isRegistered ) this->parent->UnregisterEvent( this );
}

void
elEvent::Deactivate() noexcept
{
    if ( !this->isActive ) return;

    if ( this->isRegistered ) this->parent->RegisterEvent( this );
}

} // namespace GLAPI
} // namespace el3D
