#include "GLAPI/sdlHelper.hpp"

namespace el3D
{
namespace GLAPI
{
SDL_Keycode
StringToSDLKeyCode( const std::string& key ) noexcept
{
    if ( key.size() == 1 )
    {
        char c = key[0];
        switch ( c )
        {
            case 'a':
                return SDLK_a;
            case 'A':
                return SDLK_a;
            case 'b':
                return SDLK_b;
            case 'B':
                return SDLK_b;
            case 'c':
                return SDLK_c;
            case 'C':
                return SDLK_c;
            case 'd':
                return SDLK_d;
            case 'D':
                return SDLK_d;
            case 'e':
                return SDLK_e;
            case 'E':
                return SDLK_e;
            case 'f':
                return SDLK_f;
            case 'F':
                return SDLK_f;
            case 'g':
                return SDLK_g;
            case 'G':
                return SDLK_g;
            case 'h':
                return SDLK_h;
            case 'H':
                return SDLK_h;
            case 'i':
                return SDLK_i;
            case 'I':
                return SDLK_i;
            case 'j':
                return SDLK_j;
            case 'J':
                return SDLK_j;
            case 'k':
                return SDLK_k;
            case 'K':
                return SDLK_k;
            case 'l':
                return SDLK_l;
            case 'L':
                return SDLK_l;
            case 'm':
                return SDLK_m;
            case 'M':
                return SDLK_m;
            case 'n':
                return SDLK_n;
            case 'N':
                return SDLK_n;
            case 'o':
                return SDLK_o;
            case 'O':
                return SDLK_o;
            case 'p':
                return SDLK_p;
            case 'P':
                return SDLK_p;
            case 'q':
                return SDLK_q;
            case 'Q':
                return SDLK_q;
            case 'r':
                return SDLK_r;
            case 'R':
                return SDLK_r;
            case 's':
                return SDLK_s;
            case 'S':
                return SDLK_s;
            case 't':
                return SDLK_t;
            case 'T':
                return SDLK_t;
            case 'u':
                return SDLK_u;
            case 'U':
                return SDLK_u;
            case 'v':
                return SDLK_v;
            case 'V':
                return SDLK_v;
            case 'w':
                return SDLK_w;
            case 'W':
                return SDLK_w;
            case 'x':
                return SDLK_x;
            case 'X':
                return SDLK_x;
            case 'y':
                return SDLK_y;
            case 'Y':
                return SDLK_y;
            case 'z':
                return SDLK_z;
            case 'Z':
                return SDLK_z;
            case '0':
                return SDLK_0;
            case '1':
                return SDLK_1;
            case '2':
                return SDLK_2;
            case '3':
                return SDLK_3;
            case '4':
                return SDLK_4;
            case '5':
                return SDLK_5;
            case '6':
                return SDLK_6;
            case '7':
                return SDLK_7;
            case '8':
                return SDLK_8;
            case '9':
                return SDLK_9;
        }
    }

    return SDLK_COMPUTER;
}
} // namespace GLAPI
} // namespace el3D

