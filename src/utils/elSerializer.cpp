#include "utils/elSerializer.hpp"

#include "utils/smart_c.hpp"

#include <cmath>

namespace el3D
{
namespace utils
{
elSerializer::elSerializer()
{
}

elSerializer::elSerializer( const std::string& value )
{
    std::string temp         = value;
    size_t      delimiterPos = temp.find_first_of( delimiter );
    if ( delimiterPos == std::string::npos )
        this->isValid = false;
    else
    {
        while ( this->isValid && !temp.empty() )
        {
            std::string sizeString = temp.substr( 0, delimiterPos );
            size_t      size       = strtoul( sizeString.c_str(), nullptr, 10 );
            if ( size == ULONG_MAX || temp.size() < size + delimiterPos + 1 )
                this->isValid = false;
            else
            {
                this->entry.emplace_back(
                    temp.substr( delimiterPos + 1, size ) );
                temp = temp.substr( delimiterPos + 1 + size );
            }
        }
    }
}

bool
elSerializer::operator==( const elSerializer& rhs ) const
{
    if ( this->entry.size() == rhs.entry.size() )
    {
        for ( size_t i = 0, limit = this->entry.size(); i < limit; ++i )
            if ( this->entry[i] != rhs.entry[i] ) return false;

        return true;
    }

    return false;
}

elSerializer::operator bool() const
{
    return this->isValid;
}

elSerializer::operator std::string() const
{
    return this->GetRaw();
}

std::string
elSerializer::GetRaw() const
{
    std::string returnValue;

    for ( auto& value : this->entry )
        returnValue += ToString( value.size() ) + this->delimiter + value;

    return returnValue;
}

std::string
elSerializer::Get( const size_t index ) const
{
    return this->entry[index];
}

elSerializer&
elSerializer::Add( const std::string& value )
{
    entry.emplace_back( value );
    return *this;
}

elSerializer&
elSerializer::Add( const elSerializer& value )
{
    entry.insert( this->entry.end(), value.entry.begin(), value.entry.end() );
    return *this;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value, int& t )
{
    auto retVal = smart_c(
        strtol, []( auto v ) { return v == LONG_MIN || v == LONG_MAX; },
        value.c_str(), nullptr, 10 );

    if ( retVal.HasError() ) return false;
    t = static_cast< int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value, long int& t )
{
    auto retVal = smart_c(
        strtol, []( auto v ) { return v == LONG_MIN || v == LONG_MAX; },
        value.c_str(), nullptr, 10 );
    if ( retVal.HasError() ) return false;
    t = static_cast< long int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value,
                                         long long int&     t )
{
    auto retVal = smart_c(
        strtoll, []( auto v ) { return v == LLONG_MIN || v == LLONG_MAX; },
        value.c_str(), nullptr, 10 );
    if ( retVal.HasError() ) return false;
    t = static_cast< long long int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value,
                                         unsigned int&      t )
{
    auto retVal = smart_c(
        strtoul, []( auto v ) { return v == ULONG_MAX; }, value.c_str(),
        nullptr, 10 );
    if ( retVal.HasError() ) return false;
    t = static_cast< unsigned int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value,
                                         unsigned long int& t )
{
    auto retVal = smart_c(
        strtoul, []( auto v ) { return v == ULONG_MAX; }, value.c_str(),
        nullptr, 10 );
    if ( retVal.HasError() ) return false;
    t = static_cast< unsigned long int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string&      value,
                                         unsigned long long int& t )
{
    auto retVal = smart_c(
        strtoull, []( auto v ) { return v == ULLONG_MAX; }, value.c_str(),
        nullptr, 10 );
    if ( retVal.HasError() ) return false;
    t = static_cast< unsigned long long int >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value, float& t )
{
    auto retVal = smart_c(
        strtof, []( auto v ) { return v == +HUGE_VALF || v == -HUGE_VALF; },
        value.c_str(), nullptr );
    if ( retVal.HasError() ) return false;
    t = static_cast< float >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value, double& t )
{
    auto retVal = smart_c(
        strtod, []( auto v ) { return v == +HUGE_VALF || v == -HUGE_VALF; },
        value.c_str(), nullptr );
    if ( retVal.HasError() ) return false;
    t = static_cast< double >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value,
                                         long double&       t )
{
    auto retVal = smart_c(
        strtold, []( auto v ) { return v == +HUGE_VALL || v == -HUGE_VALL; },
        value.c_str(), nullptr );
    if ( retVal.HasError() ) return false;
    t = static_cast< long double >( *retVal );
    return true;
}

bool
elSerializer::ConvertDirectlyFromString( const std::string& value, char& t )
{
    if ( value.size() != 1 ) return false;
    t = value[0];
    return true;
}
} // namespace utils
} // namespace el3D
