#include "utils/elGenericRAII.hpp"

namespace el3D
{
namespace utils
{
elGenericRAII::elGenericRAII( const std::function< void() >& ctor,
                              const std::function< void() >& dtor ) noexcept
    : elGenericRAII( dtor )
{
    if ( ctor ) ctor();
}

elGenericRAII::elGenericRAII( const std::function< void() >& dtor ) noexcept
    : dtor( dtor )
{
}

elGenericRAII::~elGenericRAII()
{
    this->Destroy();
}

elGenericRAII::elGenericRAII( elGenericRAII&& rhs ) noexcept
    : dtor( std::move( rhs.dtor ) )
{
    rhs.dtor = std::function< void() >();
}

elGenericRAII&
elGenericRAII::operator=( elGenericRAII&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Destroy();
        this->dtor = std::move( rhs.dtor );
        rhs.dtor   = std::function< void() >();
    }
    return *this;
}

elGenericRAII::operator bool() const noexcept
{
    return static_cast< bool >( this->dtor );
}

void
elGenericRAII::Reset() noexcept
{
    this->Destroy();
}

void
elGenericRAII::Destroy() noexcept
{
    if ( this->dtor ) this->dtor();
    this->dtor = std::function< void() >();
}

void
elGenericRAII::SetDestructor( const std::function< void() >& dtor ) noexcept
{
    this->dtor = dtor;
}

} // namespace utils
} // namespace el3D
