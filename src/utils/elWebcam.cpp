#include "utils/elWebcam.hpp"

#include "utils/smart_c.hpp"


#ifndef _WIN32
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#endif

namespace el3D
{
namespace utils
{
elWebcam::elWebcam( const uint32_t width, const uint32_t height,
                    const std::string& deviceFile ) noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    utils::smart_c(
        open, []( auto v ) { return v < 0; }, deviceFile.c_str(), O_RDWR )
        .AndThen( [&]( const auto& r ) {
            this->create_isInitialized = true;
            this->webcamHandle         = r;
            this->SetVideoFormat( width, height ).OrElse( [&]( const auto& r ) {
                this->create_isInitialized = false;
                this->create_error         = r;
            } );
        } )
        .OrElse( [&]( const auto& r ) {
            this->create_isInitialized = false;
            switch ( r )
            {
                case EACCES:
                {
                    this->create_error = elWebcamError::AccessDenied;
                    break;
                }
                default:
                {
                    this->create_error = elWebcamError::Undefined;
                    break;
                }
            }
        } );
#endif
}

elWebcam::elWebcam( elWebcam&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elWebcam&
elWebcam::operator=( elWebcam&& rhs ) noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    if ( this != &rhs )
    {
        this->Close();

        this->deviceFile         = std::move( rhs.deviceFile );
        this->webcamHandle       = std::move( rhs.webcamHandle );
        this->videoFormat        = std::move( rhs.videoFormat );
        this->isStreaming        = std::move( rhs.isStreaming );
        this->bufferInfo         = std::move( rhs.bufferInfo );
        this->memory             = std::move( rhs.memory );
        rhs.create_isInitialized = false;
    }
#endif
    return *this;
}

elWebcam::~elWebcam()
{
    this->StopStreaming();
    this->Close();
}

void
elWebcam::Close() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    if ( this->create_isInitialized )
    {
        utils::smart_c(
            close, []( auto v ) { return v == -1; }, this->webcamHandle )
            .OrElse( [&] {
                std::cerr << "unable to close webcam file handle for device "
                          << this->deviceFile << std::endl;
            } );
    }
#endif
}

void
elWebcam::UnmapMemory() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    if ( this->mmapStartAddress != nullptr )
    {
        utils::smart_c(
            munmap, []( auto v ) { return v == -1; }, this->mmapStartAddress,
            this->mmapLength )
            .OrElse( [&] {
                std::cerr << "unable to unmap memory for device "
                          << this->deviceFile << std::endl;
            } );
        this->mmapStartAddress = nullptr;
    }
#endif
}

#if !defined( _WIN32 ) and !defined( __APPLE__ )
bb::elExpected< v4l2_capability, elWebcamError >
elWebcam::GetCapability() const noexcept
{
    v4l2_capability returnValue;
    memset( &returnValue, 0, sizeof( returnValue ) );

    auto call = utils::smart_c(
                    ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
                    VIDIOC_QUERYCAP, &returnValue )
                    .OrElse( [&] {
                        std::cerr
                            << "unable to get v4l2 capabilities of device "
                            << this->deviceFile << std::endl;
                    } );

    if ( call.HasError() )
        return bb::Error( elWebcamError::UnableToGetCapability );
    else
        return bb::Success( returnValue );
}

bb::elExpected< elWebcamError >
elWebcam::SetVideoFormat( const uint32_t width, const uint32_t height,
                          const uint32_t pixelFormat,
                          const uint32_t bufferType ) noexcept
{
    this->videoFormat.type                = bufferType;
    this->videoFormat.fmt.pix.pixelformat = pixelFormat;
    this->videoFormat.fmt.pix.width       = width;
    this->videoFormat.fmt.pix.height      = height;

    auto call = utils::smart_c(
                    ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
                    VIDIOC_S_FMT, &this->videoFormat )
                    .OrElse( [&] {
                        std::cerr
                            << "unable to set video format [type=" << bufferType
                            << ", pixelFormat=" << pixelFormat
                            << ", width=" << width << ", height=" << height
                            << "] for device " << this->deviceFile << std::endl;
                    } );

    if ( call.HasError() )
        return bb::Error( elWebcamError::UnableToSetVideoFormat );
    else
        return bb::Success< void >();
}
#endif

bb::elExpected< elWebcamError >
elWebcam::SetupBufferCount( const uint32_t count ) const noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    v4l2_requestbuffers requestBuffer;
    requestBuffer.type   = this->videoFormat.type;
    requestBuffer.memory = this->memory;
    requestBuffer.count  = count;

    auto call = utils::smart_c(
                    ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
                    VIDIOC_REQBUFS, &requestBuffer )
                    .OrElse( [&] {
                        std::cerr << "unable to setup buffer count for device "
                                  << this->deviceFile << std::endl;
                    } );

    if ( call.HasError() )
        return bb::Error( elWebcamError::UnableToSetupBufferCount );
    else
        return bb::Success< void >();
#else
    return bb::Success< void >();
#endif
}

bb::elExpected< elWebcamError >
elWebcam::RefreshBufferInfo() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    memset( &this->bufferInfo, 0, sizeof( this->bufferInfo ) );

    this->bufferInfo.type   = this->videoFormat.type;
    this->bufferInfo.memory = this->memory;
    this->bufferInfo.index  = 0;

    auto call = utils::smart_c(
                    ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
                    VIDIOC_QUERYBUF, &this->bufferInfo )
                    .OrElse( [&] {
                        std::cerr
                            << "unable to get required buffer info for device "
                            << this->deviceFile << std::endl;
                    } );

    if ( call.HasError() )
        return bb::Error( elWebcamError::UnableToGetBufferInfo );

    return bb::Success< void >();
#else
    return bb::Success< void >();
#endif
}

bb::elExpected< elWebcamError >
elWebcam::MapWebcamMemory() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    auto bufferInfo = this->RefreshBufferInfo();
    if ( bufferInfo.HasError() ) return bufferInfo;

    auto call = utils::smart_c(
        mmap, []( auto v ) { return v == MAP_FAILED; }, nullptr,
        this->bufferInfo.length, PROT_READ | PROT_WRITE, MAP_SHARED,
        this->webcamHandle, this->bufferInfo.m.offset );

    if ( call.HasError() )
    {
        std::cerr << "unable to map webcam memory from device "
                  << this->deviceFile << std::endl;
        return bb::Error( elWebcamError::UnableToMapWebcamMemory );
    }

    this->mmapStartAddress = call.GetValue();
    this->mmapLength       = this->bufferInfo.length;
    memset( this->mmapStartAddress, 0, this->mmapLength );
    return bb::Success< void >();
#else
    return bb::Success<>();
#endif
}

bb::elExpected< elWebcamError >
elWebcam::StartStreaming() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    if ( this->isStreaming ) return bb::Success<>();

    auto setup = this->SetupBufferCount( 1 )
                     .AndThen( [&] { return this->RefreshBufferInfo(); } )
                     .AndThen( [&] { return this->MapWebcamMemory(); } );
    if ( setup.HasError() ) return setup;

    uint32_t type = this->bufferInfo.type;
    auto     call = utils::smart_c(
        ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
        VIDIOC_STREAMON, &type );
    if ( call.HasError() )
    {
        std::cerr << "unable to stream on device " << this->deviceFile
                  << std::endl;
        return bb::Error( elWebcamError::UnableToActivateStreaming );
    }

    this->isStreaming = true;
    return bb::Success<>();
#else
    return bb::Success<>();
#endif
}

bb::elExpected< elWebcamError >
elWebcam::StopStreaming() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    if ( !this->isStreaming ) return bb::Success<>();

    uint32_t type = this->bufferInfo.type;
    auto     call = utils::smart_c(
        ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
        VIDIOC_STREAMON, &type );
    if ( call.HasError() )
    {
        std::cerr << "unable to deactivate streaming on device "
                  << this->deviceFile << std::endl;
        return bb::Error( elWebcamError::UnableToDeactivateStreaming );
    }

    this->UnmapMemory();
    this->isStreaming = false;
    return bb::Success<>();
#else
    return bb::Success<>();
#endif
}

bb::elExpected< byteStream_t, elWebcamError >
elWebcam::GetNextFrame() noexcept
{
    byteStream_t returnValue;
    this->GetNextFrame( returnValue, 0 );
    return bb::Success( returnValue );
}


bb::elExpected< uint64_t, elWebcamError >
elWebcam::GetNextFrame( byteStream_t&  byteStream,
                        const uint64_t byteStreamOffset ) noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    auto result = this->UpdateBufferSize();
    if ( result.HasError() ) return bb::Error( result.GetError() );

    if ( byteStream.stream.empty() )
        byteStream.stream.resize( this->bufferInfo.length + byteStreamOffset );

    memcpy( byteStream.stream.data() + byteStreamOffset, this->mmapStartAddress,
            this->bufferInfo.length );
    return bb::Success< uint64_t >( this->bufferInfo.length );
#else
    return bb::Success< uint64_t >( 0 );
#endif
}

bb::elExpected< uint64_t, elWebcamError >
elWebcam::GetNextFrame( void* const memory ) noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    auto result = this->UpdateBufferSize();
    if ( result.HasError() ) return bb::Error( result.GetError() );

    memcpy( memory, this->mmapStartAddress, this->bufferInfo.length );
    return bb::Success< uint64_t >( this->bufferInfo.length );
#else
    return bb::Success< uint64_t >( 0 );
#endif
}

bb::elExpected< elWebcamError >
elWebcam::UpdateBufferSize() noexcept
{
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    auto call =
        utils::smart_c(
            ioctl, []( auto v ) { return v == -1; }, this->webcamHandle,
            VIDIOC_QBUF, &this->bufferInfo )
            // TODO: can be changed to AndThen([&] {}) - clang compile error
            // when missing (const auto)
            .AndThen( [&]( const auto ) {
                return utils::smart_c(
                           ioctl, []( auto v ) { return v == -1; },
                           this->webcamHandle, VIDIOC_DQBUF, &this->bufferInfo )
                    .OrElse( [&] {
                        std::cerr << "unable to dequeue buffer on device "
                                  << this->deviceFile << std::endl;
                    } );
            } )
            .OrElse( [&] {
                std::cerr << "unable to queue buffer on device "
                          << this->deviceFile << std::endl;
            } );

    if ( call.HasError() )
        return bb::Error( elWebcamError::UnableToGetNextFrame );
    return bb::Success<>();
#else
    return bb::Success<>();
#endif
}


} // namespace utils
} // namespace el3D
