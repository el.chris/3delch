#include "utils/elFileSystem.hpp"

namespace el3D
{
namespace utils
{
namespace fs = std::filesystem;

std::string
elFileSystem::FileSizeHumanReadable( const size_t size ) noexcept
{
    std::string                retVal;
    std::vector< std::string > unit{"b", "Kb", "Mb", "Gb", "Tb", "Eb"};

    size_t temp = size, i = 0;
    for ( ; temp > 1024; temp /= 1024, ++i )
        ;

    return std::to_string( temp ) + unit[i];
}

std::vector< elFileSystem::file_t >
elFileSystem::GetDirectoryContents( const std::string& path ) noexcept
{
    std::vector< file_t > retVal;

    try
    {
        fs::path directory( path );
        if ( !fs::is_directory( directory ) ) return retVal;

        for ( auto iter = fs::directory_iterator( path );
              iter != fs::directory_iterator(); ++iter )
        {
            retVal.push_back( {iter->path(), iter->status().type(), 0, "0b"} );
        }
    }
    catch ( ... )
    {
    }

    try
    {
        for ( auto& file : retVal )
        {
            file.size = fs::file_size( file.path );
            file.sizeHumanReadable =
                elFileSystem::FileSizeHumanReadable( file.size );
        }
    }
    catch ( ... )
    {
    }

    return retVal;
}

std::vector< std::string >
elFileSystem::ConvertStringToVectorPath( const std::string& v ) noexcept
{
    std::string                temp = v;
    std::vector< std::string > retVal;

    for ( auto pos = temp.find_first_of( elFileSystem::PATH_DELIMITER );
          pos != std::string::npos && pos < temp.size();
          temp     = temp.substr( pos + 1 ),
               pos = temp.find_first_of( elFileSystem::PATH_DELIMITER ) )
    {
        if ( pos > 0 ) retVal.emplace_back( temp.substr( 0, pos ) );
    }

    if ( !temp.empty() ) retVal.emplace_back( temp );

    return retVal;
}

std::string
elFileSystem::ConvertVectorPathToString(
    const std::vector< std::string >& v ) noexcept
{
    std::string path;
#ifdef _WIN32
    bool firstEntry = true;
#endif
    for ( auto& entry : v )
    {
#ifdef _WIN32
        if ( firstEntry )
        {
            firstEntry = false;
            path += entry;
            continue;
        }
#endif
        path += elFileSystem::PATH_DELIMITER + entry;
    }
    return path;
}


std::string
elFileSystem::FileTypeToString( const std::filesystem::file_type type ) noexcept
{
    switch ( type )
    {
        case fs::file_type::none:
            return "none";
        case fs::file_type::not_found:
            return "not_found";
        case fs::file_type::regular:
            return "regular";
        case fs::file_type::directory:
            return "directory";
        case fs::file_type::symlink:
            return "symlink";
        case fs::file_type::block:
            return "block";
        case fs::file_type::character:
            return "character";
        case fs::file_type::fifo:
            return "fifo";
        case fs::file_type::socket:
            return "socket";
        case fs::file_type::unknown:
            return "unknown";
        default:
            return "unknown";
    }
}

std::string
elFileSystem::RemoveDoubleDelimiterFromStringPath(
    const std::string& path ) noexcept
{
    std::string temp                     = path;
    bool        previousCharWasDelimiter = false;
    for ( long i = 0; i < static_cast< long >( temp.size() ); ++i )
    {
        if ( !previousCharWasDelimiter &&
             temp[static_cast< size_t >( i )] == PATH_DELIMITER )
            previousCharWasDelimiter = true;
        else if ( previousCharWasDelimiter &&
                  temp[static_cast< size_t >( i )] == PATH_DELIMITER )
        {
            previousCharWasDelimiter = false;
            temp.erase( temp.begin() + i );
            i -= 2;
        }
        else
        {
            previousCharWasDelimiter = false;
        }
    }


    return temp;
}

std::string
elFileSystem::GetCurrentPath() noexcept
{
    return fs::current_path().string();
}

std::string
elFileSystem::GetAbsolutPath( const std::string& path ) noexcept
{
    fs::path p( path );
    return fs::absolute( path ).string();
}

std::string
elFileSystem::GetParentPath( const std::string& path ) noexcept
{
    return fs::path( path ).parent_path().string();
}

std::string
elFileSystem::GetFileName( const std::string& path ) noexcept
{
    return fs::path( path ).filename().string();
}

bool
elFileSystem::IsRegularFile( const std::string& path ) noexcept
{
    fs::path p( path );
    return fs::is_regular_file( p );
}

bool
elFileSystem::IsDirectory( const std::string& path ) noexcept
{
    fs::path p( path );
    return fs::is_directory( p );
}
} // namespace utils
} // namespace el3D
