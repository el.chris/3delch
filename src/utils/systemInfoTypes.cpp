#include "utils/systemInfoTypes.hpp"

#include "buildingBlocks/elExpected.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

namespace el3D
{
namespace utils
{
namespace internal
{
uint64_t
cpuCore_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->frequencyInMhz,
                                                       this->load );
}

uint64_t
cpuCore_t::Serialize( const utils::Endian  endianness,
                      utils::byteStream_t& byteStream,
                      const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->frequencyInMhz,
        this->load );
}

bb::elExpected< utils::ByteStreamError >
cpuCore_t::Deserialize( const utils::Endian        endianness,
                        const utils::byteStream_t& byteStream,
                        const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->frequencyInMhz, this->load )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "cpuCore_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
cpu_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->model, this->numberOfCores, this->load, this->cores );
}

uint64_t
cpu_t::Serialize( const utils::Endian  endianness,
                  utils::byteStream_t& byteStream,
                  const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->model,
        this->numberOfCores, this->load, this->cores );
}

bb::elExpected< utils::ByteStreamError >
cpu_t::Deserialize( const utils::Endian        endianness,
                    const utils::byteStream_t& byteStream,
                    const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->model, this->numberOfCores, this->load,
                  this->cores )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "cpu_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
memory_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->total, this->used, this->cached, this->buffer, this->swapTotal,
        this->swapUsed );
}

uint64_t
memory_t::Serialize( const utils::Endian  endianness,
                     utils::byteStream_t& byteStream,
                     const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->total, this->used,
        this->cached, this->buffer, this->swapTotal, this->swapUsed );
}

bb::elExpected< utils::ByteStreamError >
memory_t::Deserialize( const utils::Endian        endianness,
                       const utils::byteStream_t& byteStream,
                       const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->total, this->used, this->cached,
                  this->buffer, this->swapTotal, this->swapUsed )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "memory_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
netStat_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->bytes, this->packets, this->byteLoad, this->packetLoad,
        this->errors );
}

uint64_t
netStat_t::Serialize( const utils::Endian  endianness,
                      utils::byteStream_t& byteStream,
                      const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->bytes, this->packets,
        this->byteLoad, this->packetLoad, this->errors );
}

bb::elExpected< utils::ByteStreamError >
netStat_t::Deserialize( const utils::Endian        endianness,
                        const utils::byteStream_t& byteStream,
                        const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->bytes, this->packets, this->byteLoad,
                  this->packetLoad, this->errors )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "netStat_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
networkInterface_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->incoming, this->outgoing, this->interfaceName, this->macAddress,
        this->ipAddress, this->broadCastAddress, this->networkMask );
}

uint64_t
networkInterface_t::Serialize( const utils::Endian  endianness,
                               utils::byteStream_t& byteStream,
                               const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->incoming,
        this->outgoing, this->interfaceName, this->macAddress, this->ipAddress,
        this->broadCastAddress, this->networkMask );
}

bb::elExpected< utils::ByteStreamError >
networkInterface_t::Deserialize( const utils::Endian        endianness,
                                 const utils::byteStream_t& byteStream,
                                 const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->incoming, this->outgoing,
                  this->interfaceName, this->macAddress, this->ipAddress,
                  this->broadCastAddress, this->networkMask )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "networkInterface_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
connectionPart_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->ipAddress,
                                                       this->port );
}

uint64_t
connectionPart_t::Serialize( const utils::Endian  endianness,
                             utils::byteStream_t& byteStream,
                             const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->ipAddress, this->port );
}

bb::elExpected< utils::ByteStreamError >
connectionPart_t::Deserialize( const utils::Endian        endianness,
                               const utils::byteStream_t& byteStream,
                               const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->ipAddress, this->port )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "connectionPart_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
connection_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->source,
                                                       this->destination );
}

uint64_t
connection_t::Serialize( const utils::Endian  endianness,
                         utils::byteStream_t& byteStream,
                         const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->source,
        this->destination );
}

bb::elExpected< utils::ByteStreamError >
connection_t::Deserialize( const utils::Endian        endianness,
                           const utils::byteStream_t& byteStream,
                           const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->source, this->destination )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "connection_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
network_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->dns, this->interfaces, this->connections );
}

uint64_t
network_t::Serialize( const utils::Endian  endianness,
                      utils::byteStream_t& byteStream,
                      const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->dns, this->interfaces,
        this->connections );
}

bb::elExpected< utils::ByteStreamError >
network_t::Deserialize( const utils::Endian        endianness,
                        const utils::byteStream_t& byteStream,
                        const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->dns, this->interfaces, this->connections )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "network_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
thread_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->tid, this->cpuLoad,
                                                       this->memoryUsage );
}

uint64_t
thread_t::Serialize( const utils::Endian  endianness,
                     utils::byteStream_t& byteStream,
                     const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->tid, this->cpuLoad,
        this->memoryUsage );
}

bb::elExpected< utils::ByteStreamError >
thread_t::Deserialize( const utils::Endian        endianness,
                       const utils::byteStream_t& byteStream,
                       const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->tid, this->cpuLoad, this->memoryUsage )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "thread_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
process_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->pid, this->ppid, this->numberOfThreads, this->threads, this->user,
        this->group, this->command, this->cpuLoad, this->memoryUsage );
}

uint64_t
process_t::Serialize( const utils::Endian  endianness,
                      utils::byteStream_t& byteStream,
                      const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->pid, this->ppid,
        this->numberOfThreads, this->threads, this->user, this->group,
        this->command, this->cpuLoad, this->memoryUsage );
}

bb::elExpected< utils::ByteStreamError >
process_t::Deserialize( const utils::Endian        endianness,
                        const utils::byteStream_t& byteStream,
                        const uint64_t             byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->pid, this->ppid, this->numberOfThreads,
                  this->threads, this->user, this->group, this->command,
                  this->cpuLoad, this->memoryUsage )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "process_t", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
processSummary_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->processList, this->numberOfProcesses );
}

uint64_t
processSummary_t::Serialize( const utils::Endian  endianness,
                             utils::byteStream_t& byteStream,
                             const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->processList,
        this->numberOfProcesses );
}

bb::elExpected< utils::ByteStreamError >
processSummary_t::Deserialize( const utils::Endian        endianness,
                               const utils::byteStream_t& byteStream,
                               const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->processList, this->numberOfProcesses )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "processSummary_t", r, byteStream, byteStreamOffset );
        } );
}
} // namespace internal

uint64_t
systemInfo_t::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->cpu, this->memory, this->network, this->process, this->uptime,
        this->hostname, this->kernelVersion );
}

uint64_t
systemInfo_t::Serialize( const utils::Endian  endianness,
                         utils::byteStream_t& byteStream,
                         const uint64_t       byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->cpu, this->memory,
        this->network, this->process, this->uptime, this->hostname,
        this->kernelVersion );
}

bb::elExpected< utils::ByteStreamError >
systemInfo_t::Deserialize( const utils::Endian        endianness,
                           const utils::byteStream_t& byteStream,
                           const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->cpu, this->memory, this->network,
                  this->process, this->uptime, this->hostname,
                  this->kernelVersion )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "systemInfo_t", r, byteStream, byteStreamOffset );
        } );
}
} // namespace utils
} // namespace el3D
