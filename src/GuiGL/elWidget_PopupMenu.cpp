#include "GuiGL/elWidget_PopupMenu.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "HighGL/elEventTexture.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_PopupMenu::elWidget_PopupMenu( const constructor_t& constructor )
    : elWidget_Menu( constructor ), popupEvent( this->CreateEvent() ),
      popupCloseEvent( this->CreateEvent() )
{
    this->SetDoRenderWidget( false );
    this->popupEvent->SetCallback( [this]( const SDL_Event e ) {
        if ( this->parent->DoesEventColorBelongsToMeOrChild(
                 this->GetColorToCurrentEvent() ) &&
             e.button.type == SDL_MOUSEBUTTONUP &&
             e.button.button == SDL_BUTTON_RIGHT )
        {
            this->SetDoRenderWidget( true );
            glm::vec2 newPosition =
                glm::vec2{ static_cast< float >( e.motion.x ) / GetDPIScaling(),
                           static_cast< float >( e.motion.y ) /
                               GetDPIScaling() } -
                this->parent->GetAbsolutPosition();
            this->SetPosition( newPosition );
            this->popupCloseEvent->Register();
        }
    } );
    this->popupEvent->Register();

    this->popupCloseEvent->SetCallback( [this]( const SDL_Event e ) {
        if ( this->parent->DoesEventColorBelongsToMeOrChild(
                 this->GetColorToCurrentEvent() ) &&
             e.button.type == SDL_MOUSEBUTTONUP &&
             e.button.button == SDL_BUTTON_LEFT )
        {
            this->SetDoRenderWidget( false );
            this->popupEvent->Register();
        }
    } );
}

} // namespace GuiGL
} // namespace el3D
