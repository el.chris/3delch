#include "GuiGL/elWidget_Progress.hpp"

#include "GuiGL/elWidget_ShaderAnimation.hpp"

#ifdef _WIN32
#undef min
#undef max
#endif

namespace el3D
{
namespace GuiGL
{
elWidget_Progress::elWidget_Progress( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->ThrowIfPropertyIsUnset< NoAnimationShaderFileSet_e >(
        PROPERTY_ANIMATION_SHADER_FILE );

    this->progressAnimation = this->Create< elWidget_ShaderAnimation >();
    this->progressAnimation->SetUp( this->GetAnimationShaderFile() );
    this->progressAnimation->SetPosition( { 0, 0 } );
    this->progressAnimation->SetStickToParentSize( { true, true } );
    this->progressAnimation->SetDoInfinitLoop( true );
}

elWidget_Progress::~elWidget_Progress()
{
}

void
elWidget_Progress::SetProgress( const float v ) noexcept
{
    this->progress        = std::min( 1.0f, std::max( 0.0f, v ) );
    glm::vec2 shaderValue = glm::vec2(
        this->progress, ( v == PROGRESS_INDIFFERENT ) ? 1.0f : 0.0f );
    this->progressAnimation->SetTextureUniformBlockData(
        0, UBO_SIZE, glm::value_ptr( shaderValue ), 2 * sizeof( GLfloat ) );
}

float
elWidget_Progress::GetProgress() const noexcept
{
    return this->progress;
}

void
elWidget_Progress::SetState( const widgetStates_t s ) noexcept
{
    elWidget_base::SetState( s );
    this->progressAnimation->SetState( s );
}
} // namespace GuiGL
} // namespace el3D
