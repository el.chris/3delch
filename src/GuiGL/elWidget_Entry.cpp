#include "GuiGL/elWidget_Entry.hpp"

#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_TextCursor.hpp"

#include <algorithm>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Entry::elWidget_Entry( const constructor_t& constructor )
    : elWidget_Window( constructor ),
      cursor( this->Create< elWidget_TextCursor >() ),
      ghostCursor( this->Create< elWidget_TextCursor >() ),
      textLabel( this->Create< elWidget_Label >() ),
      fontHeight( static_cast< float >( textLabel->GetFontHeight() ) ),
      editEvent( this->CreateEvent() ),
      clickedOnLabelEvent( this->CreateEventFor( UBO_EVENT_COLOR_INNER ) )
{
    this->onEnterKeyPressCallback = this->CreateCallback();
    this->cursor->SetPositionPointOfOrigin( POSITION_CENTER_LEFT );
    this->ghostCursor->SetPositionPointOfOrigin( POSITION_CENTER_LEFT );
    this->ghostCursor->SetIsGhostWidget( true );
    this->textLabel->SetPositionPointOfOrigin( POSITION_CENTER_LEFT );

    this->UpdateCursor();
    this->CreateClickedOnLabelEvent();
    this->CreateEditEvent();
}

elWidget_Entry::~elWidget_Entry()
{
    if ( this->GetState() == STATE_SELECTED )
        GLAPI::elEvent::MultiInstanceStopTextInput();
}

void
elWidget_Entry::RestrictInputSizeTo( const uint64_t& v ) noexcept
{
    this->maxTextSize = v;
}

void
elWidget_Entry::RestrictInputCharacters(
    const std::string& inputCharacters ) noexcept
{
    this->inputCharacterRestriction = inputCharacters;
}

uint64_t
elWidget_Entry::GetFontHeight() const noexcept
{
    return this->textLabel->GetFontHeight();
}

void
elWidget_Entry::CreateClickedOnLabelEvent() noexcept
{
    this->clickedOnLabelEvent->SetCallback(
        [this]( const SDL_Event e )
        {
            if ( ( e.button.button == SDL_BUTTON_LEFT ||
                   e.button.button == SDL_BUTTON_MIDDLE ) &&
                 !this->GetStickyExclusiveEvent().IsRegistered() )
            {
                this->MouseEventCallback( e );
            }
        } );
}

void
elWidget_Entry::MouseEventCallback( const SDL_Event e ) noexcept
{
    switch ( e.button.button )
    {
        case SDL_BUTTON_LEFT:
        {
            glm::vec2 currentMousePosition =
                this->mouseCursor->GetCursorPosition();

            if ( e.button.state == SDL_RELEASED &&
                 this->mouseCursorClickPosition == currentMousePosition )
            {
                this->MoveCursorToClickPosition();
            }
            else if ( e.button.state == SDL_PRESSED )
            {
                this->mouseCursorClickPosition = currentMousePosition;
            }
            else if ( this->mouseCursorClickPosition != currentMousePosition &&
                      this->mouseCursorClickPosition != glm::vec2( -1.0f ) )
            {
                this->SelectTextWithMouse( currentMousePosition.x );
            }
        }
        break;
        case SDL_BUTTON_MIDDLE:
            if ( e.button.type == SDL_MOUSEBUTTONUP )
            {
                if ( SDL_HasClipboardText() )
                {
                    const char* clipboardText = SDL_GetClipboardText();
                    if ( clipboardText == nullptr )
                    {
                        LOG_WARN( 0 ) << "could not read from clipboard, "
                                      << SDL_GetError();
                    }
                    else
                    {
                        if ( this->textRightPosition != 0 )
                        {
                            this->text.erase( this->textPosition,
                                              this->textRightPosition );
                        }
                        this->InsertText( this->textPosition, clipboardText );
                        this->MoveCursorToClickPosition();
                    }
                }
            }
            break;
        default:
            break;
    }
}

void
elWidget_Entry::SelectTextWithMouse( const float currentMousePosition ) noexcept
{
    float selectBegin =
        std::min( currentMousePosition, this->mouseCursorClickPosition.x );
    float selectEnd =
        std::max( currentMousePosition, this->mouseCursorClickPosition.x );

    this->textPosition = this->MousePositionToTextPosition( selectBegin );
    this->textRightPosition =
        this->MousePositionToTextPosition( selectEnd ) - this->textPosition + 1;

    if ( this->textPosition == this->text.size() &&
         this->textRightPosition != 0 )
        return;

    float textCursorStart =
        this->GetTextWidth( this->text.substr( 0, this->textPosition ) );

    this->MoveCursorTo( { textCursorStart, 0.0f } );
    this->UpdateCursor();
}

void
elWidget_Entry::MoveCursorToClickPosition() noexcept
{
    float cursorPosition = this->mouseCursor->GetCursorPosition().x;

    size_t newCursorPosition =
        this->MousePositionToTextPosition( cursorPosition );
    this->MoveCursorToPosition( newCursorPosition );
}

size_t
elWidget_Entry::MousePositionToTextPosition(
    const float cursorXPosition ) const noexcept
{
    float textWidth;
    float scrollPosition = this->slider.currentRelativePosition[0];
    float offset = std::max( scrollPosition * ( this->textLabel->GetSize().x -
                                                this->GetContentSize().x ),
                             0.0f );

    for ( size_t i = 0, l = this->text.size(); i <= l; ++i )
    {
        textWidth = this->GetTextWidth( this->text.substr( 0, i ) );
        if ( this->GetAbsolutPosition().x +
                 this->GetBorderSize()[POSITION_LEFT] + textWidth >
             cursorXPosition + offset )
        {
            return ( i > 0 ) ? i - 1 : 0;
        }
    }

    return this->text.size();
}

void
elWidget_Entry::SetState( const widgetStates_t state ) noexcept
{
    if ( state == this->GetState() ) return;

    if ( state == STATE_SELECTED )
    {
        GLAPI::elEvent::MultiInstanceStartTextInput();
        if ( !this->editEvent->IsRegistered() ) this->editEvent->Register();
        if ( this->textRightPosition != 0 ) this->cursor->SetState( state );
    }
    else if ( state != STATE_SELECTED )
    {
        if ( this->GetState() == STATE_SELECTED )
            GLAPI::elEvent::MultiInstanceStopTextInput();

        if ( this->editEvent->IsRegistered() ) this->editEvent->Unregister();
        this->mouseCursorClickPosition = glm::vec2( -1.0f );
        this->cursor->SetState( state );
    }

    elWidget_base::SetState( state );
}

void
elWidget_Entry::CreateEditEvent() noexcept
{
    this->editEvent->SetCallback(
        [this]( const SDL_Event e )
        {
            if ( ( e.key.type == SDL_KEYDOWN || e.type == SDL_TEXTINPUT ) &&
                 !this->GetStickyExclusiveEvent().IsRegistered() )
            {
                this->EditEventCallback( e );
            }
        } );
}

void
elWidget_Entry::RemoveSelectedText() noexcept
{
    if ( this->textRightPosition > 0 )
    {
        this->text.erase( this->textPosition, this->textRightPosition );
        this->textLabel->SetText( this->text );
        this->textRightPosition = 0;
        this->UpdateCursor();
    }
}

void
elWidget_Entry::BeginTextSelection(
    const SelectionDirection_t selectionDirection ) noexcept
{
    if ( !this->isSelecting )
    {
        this->isSelecting = true;
        this->isSelectingFromLeftToRight =
            ( selectionDirection == SELECT_FROM_LEFT_TO_RIGHT ) ? true : false;
    }
}

void
elWidget_Entry::EditEventCallback( const SDL_Event e ) noexcept
{
    switch ( e.key.keysym.sym )
    {
        case SDLK_BACKSPACE:
            if ( this->textPosition > 0 || this->textRightPosition != 0 )
            {
                if ( this->textRightPosition == 0 && this->textPosition > 0 )
                    this->MoveCursorLeft();
                if ( this->textRightPosition == 0 ) this->textRightPosition++;
                this->RemoveSelectedText();
            }
            this->isSelecting = false;
            break;
        case SDLK_DELETE:
            if ( this->textRightPosition == 0 ) this->textRightPosition++;
            this->RemoveSelectedText();
            this->isSelecting = false;
            break;
        case SDLK_LEFT:
            if ( e.key.keysym.mod & KMOD_SHIFT && !this->text.empty() )
            {
                this->BeginTextSelection( SELECT_FROM_RIGHT_TO_LEFT );
                if ( !this->isSelectingFromLeftToRight )
                {
                    if ( this->textRightPosition == 0 ) this->textPosition++;
                    this->IncreaseSelectionToTheLeft();
                }
                else
                    this->DecreaseSelectionToTheRight();
            }
            else
            {
                this->isSelecting = false;
                this->MoveCursorLeft();
            }
            break;
        case SDLK_RIGHT:
            if ( e.key.keysym.mod & KMOD_SHIFT )
            {
                this->BeginTextSelection( SELECT_FROM_LEFT_TO_RIGHT );
                if ( this->isSelectingFromLeftToRight )
                    this->IncreaseSelectionToTheRight();
                else
                {
                    this->DecreaseSelectionToTheLeft();
                }
            }
            else
            {
                if ( this->isSelecting )
                {
                    this->isSelecting = false;
                    this->MoveCursorToPosition( this->textPosition +
                                                this->textRightPosition );
                }
                else
                {
                    this->MoveCursorRight();
                }
            }
            break;
        case SDLK_HOME:
            if ( e.key.keysym.mod & KMOD_SHIFT )
            {
                this->BeginTextSelection( SELECT_FROM_RIGHT_TO_LEFT );
                size_t newRightPosition;

                if ( !this->isSelectingFromLeftToRight )
                    newRightPosition = std::max( static_cast< size_t >( 1 ),
                                                 this->textRightPosition ) +
                                       this->textPosition;
                else
                {
                    newRightPosition                 = this->textPosition + 1;
                    this->isSelectingFromLeftToRight = false;
                }

                this->MoveCursorToPosition( 0 );
                this->textRightPosition = newRightPosition;
                this->UpdateCursor();
            }
            else
            {
                this->MoveCursorToPosition( 0 );
                this->isSelecting = false;
            }
            break;
        case SDLK_END:
            if ( e.key.keysym.mod & KMOD_SHIFT )
            {
                this->BeginTextSelection( SELECT_FROM_LEFT_TO_RIGHT );

                if ( !this->isSelectingFromLeftToRight )
                {
                    this->MoveCursorToPosition( this->textPosition +
                                                this->textRightPosition - 1 );
                    this->isSelectingFromLeftToRight = true;
                }
                this->textRightPosition =
                    this->text.size() - this->textPosition;
                this->UpdateCursor();
            }
            else
            {
                this->MoveCursorToPosition( this->text.size() - 1 );
                this->MoveCursorRight( 1 );
                this->isSelecting = false;
            }
            break;
        case SDLK_TAB:
            this->InsertText( this->textPosition, this->tabText );
            this->MoveCursorRight( this->tabText.size() );
            this->isSelecting = false;
            break;
        case SDLK_RETURN:
            this->onEnterKeyPressCallback->Call();
            break;
        default:
            if ( e.type == SDL_TEXTINPUT )
            {
                this->RemoveSelectedText();
                this->InsertText( this->textPosition, e.text.text );
                this->MoveCursorRight();
            }
            this->isSelecting = false;
            break;
    }

    this->AutoScrollToCurrentPosition();
}

void
elWidget_Entry::InsertText( const uint64_t     position,
                            const std::string& insertedText ) noexcept
{
    std::string adjustedInsertedText;
    if ( !this->inputCharacterRestriction.empty() )
    {
        for ( auto& c : insertedText )
        {
            bool isCharacterAllowed = false;
            for ( auto& a : this->inputCharacterRestriction )
                if ( c == a )
                {
                    isCharacterAllowed = true;
                    break;
                }

            if ( isCharacterAllowed ) adjustedInsertedText += c;
        }
    }
    else
        adjustedInsertedText = insertedText;

    this->text.insert( position, adjustedInsertedText );
    if ( this->maxTextSize < this->text.size() )
        this->text.resize( this->maxTextSize );
    this->textLabel->SetText( this->text );
}

void
elWidget_Entry::SetOnEnterKeyPressCallback(
    const std::function< void() >& callback ) noexcept
{
    this->onEnterKeyPressCallback->Set( callback );
}

void
elWidget_Entry::AutoScrollToCurrentPosition() noexcept
{
    this->UpdateSliderOffsetAndSizeRemainder();

    float contentDimensions = this->textLabel->GetSize().x;
    float contentSize       = this->GetContentSize().x;

    size_t currentTextPosition = this->textPosition;
    if ( this->isSelecting && this->isSelectingFromLeftToRight )
        currentTextPosition += this->textRightPosition;

    size_t rightTextPosition = ( currentTextPosition + 1 < this->text.size() )
                                   ? currentTextPosition + 1
                                   : currentTextPosition;
    float  rightCursorPosition =
        this->GetTextWidth( this->text.substr( 0, rightTextPosition ) );
    float leftCursorPosition =
        this->GetTextWidth( this->text.substr( 0, currentTextPosition ) );
    float scrollStepSize =
        this->GetTextWidth( ( currentTextPosition < this->text.size() )
                                ? this->text.substr( currentTextPosition, 1 )
                                : " " );
    float addNewCharacter = ( currentTextPosition == this->text.size() )
                                ? this->GetTextWidth( " " )
                                : 0.0f;

    float currentScrollPosition = this->GetFinalSliderPosition()[0];


    float scrollLeft =
        std::max( 0.0f, ( leftCursorPosition - scrollStepSize ) /
                            ( contentDimensions - contentSize ) );

    float scrollRight =
        std::max( 0.0f, ( rightCursorPosition + addNewCharacter +
                          scrollStepSize - contentSize ) /
                            ( contentDimensions - contentSize ) );

    if ( scrollRight > currentScrollPosition )
        this->ScrollTo( { scrollRight, 0.0f } );
    else if ( scrollLeft < currentScrollPosition )
        this->ScrollTo( { scrollLeft, 0.0f } );
}

void
elWidget_Entry::IncreaseSelectionToTheLeft() noexcept
{
    if ( this->textPosition > 0 && !this->text.empty() )
    {
        if ( this->textPosition <= this->text.size() )
            this->textPosition--;
        else
            this->textPosition -= 2;

        this->textRightPosition++;
        float width =
            this->GetTextWidth( this->text.substr( 0, this->textPosition ) );
        this->MoveCursorTo( { width, 0.0f } );
        this->UpdateCursor();
    }
}

void
elWidget_Entry::DecreaseSelectionToTheLeft() noexcept
{
    if ( this->textPosition < this->text.size() )
    {
        if ( this->textRightPosition > 1 )
        {
            this->textPosition++;
            this->textRightPosition--;
            if ( this->textRightPosition == 1 )
                this->isSelectingFromLeftToRight =
                    !this->isSelectingFromLeftToRight;
        }
        else if ( this->textRightPosition == 1 )
        {
            this->textPosition--;
            this->textRightPosition++;
            this->isSelectingFromLeftToRight =
                !this->isSelectingFromLeftToRight;
        }

        float width =
            this->GetTextWidth( this->text.substr( 0, this->textPosition ) );
        this->MoveCursorTo( { width, 0.0f } );
        this->UpdateCursor();
    }
}

void
elWidget_Entry::IncreaseSelectionToTheRight() noexcept
{
    if ( this->textPosition + this->textRightPosition < this->text.size() )
    {
        this->textRightPosition++;
    }
    this->UpdateCursor();
}

void
elWidget_Entry::DecreaseSelectionToTheRight() noexcept
{
    if ( this->textRightPosition > 1 )
    {
        this->textRightPosition--;
        if ( this->textRightPosition == 1 )
            this->isSelectingFromLeftToRight =
                !this->isSelectingFromLeftToRight;
    }
    else if ( this->textRightPosition == 1 )
    {
        this->isSelectingFromLeftToRight = !this->isSelectingFromLeftToRight;
        if ( this->textPosition > 0 )
        {
            this->textPosition--;
            this->textRightPosition++;
        }
    }
    this->UpdateCursor();
}

void
elWidget_Entry::SelectRange( const size_t start, const size_t length ) noexcept
{
    this->textPosition = std::min( start, this->text.size() );
    this->textRightPosition =
        std::min( this->textPosition - this->text.size(), length );
    this->UpdateCursor();
}

void
elWidget_Entry::MoveCursorToPosition( const size_t p ) noexcept
{
    this->textRightPosition = 0;
    size_t adjustedPosition = std::min( p, this->text.size() );

    this->MoveCursorTo(
        { this->GetTextWidth( this->text.substr( 0, adjustedPosition ) ),
          0.0f } );

    this->textPosition = adjustedPosition;
    this->UpdateCursor();
}

void
elWidget_Entry::SetCursorToPosition( const size_t p ) noexcept
{
    this->textRightPosition = 0;
    this->MoveCursorTo(
        { this->GetTextWidth( this->text.substr( 0, p ) ), 0.0f } );
    this->textPosition = p;
    this->UpdateCursor();
}

float
elWidget_Entry::GetTextWidth( const std::string& t ) const noexcept
{
    return static_cast< float >(
        this->textLabel->font->GetTextSize( t ).width );
}

void
elWidget_Entry::MoveCursorLeft( const size_t steps ) noexcept
{
    this->textRightPosition = 0;
    if ( this->textPosition + 1 - steps > 0 )
    {
        this->textPosition -= steps;
        float width =
            this->GetTextWidth( this->text.substr( 0, this->textPosition ) );
        this->MoveCursorTo( { width, 0.0f } );
        this->UpdateCursor();
    }
}

void
elWidget_Entry::MoveCursorRight( const size_t steps ) noexcept
{
    this->textRightPosition = 0;
    if ( this->textPosition + steps <= this->text.size() )
    {
        this->textPosition += steps;
        float width =
            this->GetTextWidth( this->text.substr( 0, this->textPosition ) );

        this->MoveCursorTo( { width, 0.0f } );
        this->UpdateCursor();
    }
}

void
elWidget_Entry::MoveCursorTo( const glm::vec2& v ) noexcept
{
    this->cursor->MoveTo( v );
    this->ghostCursor->SetPosition( v );
}

void
elWidget_Entry::UpdateCursor() noexcept
{
    float width;

    if ( this->textRightPosition == 0 )
    {
        width = this->GetCharacterWidthAtPosition( this->textPosition );
        this->cursor->SetState( STATE_DEFAULT );
    }
    else
    {
        width = this->GetTextWidth(
            this->text.substr( this->textPosition, this->textRightPosition ) );
        if ( this->GetCopySelectedTextToClipboard() )
            this->CopyTextToClipboard();
        this->cursor->SetState( STATE_SELECTED );
    }

    this->cursor->ResizeTo( { width, this->fontHeight } );
    this->ghostCursor->SetSize( { width, this->fontHeight } );
}

void
elWidget_Entry::CopyTextToClipboard() const noexcept
{
    if ( SDL_SetClipboardText(
             this->text.substr( this->textPosition, this->textRightPosition )
                 .c_str() ) != 0 )
    {
        LOG_WARN( 0 ) << "could not copy text to clipboard : "
                      << SDL_GetError();
    }
}

float
elWidget_Entry::GetCharacterWidthAtPosition( const size_t pos ) const noexcept
{
    std::string c = " ";
    if ( pos != this->text.size() ) c = this->text[this->textPosition];

    return static_cast< float >(
        this->textLabel->font->GetTextSize( c ).width );
}

std::string
elWidget_Entry::GetKeySign( const SDL_Keycode k ) noexcept
{
    switch ( k )
    {
        case SDLK_SPACE:
            return " ";
        default:
            return SDL_GetKeyName( k );
    }
}

void
elWidget_Entry::SetText( const std::string& v ) noexcept
{
    if ( this->text == v ) return;

    this->text.clear();
    this->textPosition      = 0;
    this->textRightPosition = 0;
    this->isSelecting       = false;
    this->InsertText( 0, v );
    this->MoveCursorTo( { 0.0f, 0.0f } );
    this->UpdateCursor();
}

std::string
elWidget_Entry::GetText() const noexcept
{
    return this->text;
}

} // namespace GuiGL
} // namespace el3D
