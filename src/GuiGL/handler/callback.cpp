#include "GuiGL/handler/callback.hpp"

namespace el3D
{
namespace GuiGL
{
namespace handler
{
Callable::Callable( CallbackContainer_t* const     callbackContainer,
                    const std::function< void() >& callback ) noexcept
    : callbackContainer( callbackContainer ), callback( callback )
{
}

void
Callable::Call() const noexcept
{
    if ( this->callback )
        this->callbackContainer->emplace_back( this->callback );
}

void
Callable::Set( const std::function< void() >& v ) noexcept
{
    this->callback = v;
}


callback::callback( CallbackContainer_t* const callbackContainer ) noexcept
    : callbackContainer( callbackContainer )
{
}

callback::~callback()
{
    if ( this->onCloseCallback )
        callbackContainer->emplace_back( this->onCloseCallback );
}

void
callback::CallCallback( const widgetStates_t state,
                        const CallCondition  callCondition ) const noexcept
{
    std::function< void() > callback;
    switch ( callCondition )
    {
        case CallCondition::OnEnter:
            callback = this->callbacks[state].onEnter;
            break;
        case CallCondition::OnLeave:
            callback = this->callbacks[state].onLeave;
            break;
        case CallCondition::InState:
            callback = this->callbacks[state].inState;
            break;
    }
    if ( callback ) this->callbackContainer->emplace_back( callback );
}

void
callback::CallCallable( const std::function< void() >& f ) noexcept
{
    if ( f ) this->callbackContainer->emplace_back( f );
}

void
callback::SetOnCloseCallback( const std::function< void() >& callback ) noexcept
{
    this->onCloseCallback = callback;
}

bb::product_ptr< Callable >
callback::CreateCallback( const std::function< void() >& f ) noexcept
{
    return this->factory.CreateProduct< Callable >( this->callbackContainer,
                                                    f );
}

void
callback::SetCallback( const std::function< void() >& callback,
                       const widgetStates_t           state,
                       const CallCondition            callCondition ) noexcept
{
    switch ( callCondition )
    {
        case CallCondition::OnEnter:
            this->callbacks[state].onEnter = callback;
            break;
        case CallCondition::OnLeave:
            this->callbacks[state].onLeave = callback;
            break;
        case CallCondition::InState:
            this->callbacks[state].inState = callback;
            break;
    }
}

void
callback::RemoveCallback( const widgetStates_t state,
                          const CallCondition  callCondition ) noexcept
{
    this->SetCallback( std::function< void() >(), state, callCondition );
}
} // namespace handler
} // namespace GuiGL
} // namespace el3D
