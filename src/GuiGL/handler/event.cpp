#include "GuiGL/handler/event.hpp"

#include "GLAPI/elWindow.hpp"
#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_base.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
namespace handler
{
event::event( elWidget_base *const          me,
              HighGL::elEventTexture *const eventTexture,
              GLAPI::elEventHandler *const  eventHandler,
              GLAPI::elEvent *const         stickyExclusiveEvent )
    : stickyExclusiveEventPtr( stickyExclusiveEvent ), me( me ),
      eventTexture( eventTexture ), eventHandler( eventHandler ),
      deHoverEvent( this->CreateEvent() ), deSelectEvent( this->CreateEvent() )
{
    if ( this->stickyExclusiveEventPtr == nullptr )
    {
        this->stickyExclusiveEvent     = this->CreateEvent();
        this->stickyExclusiveEventPtr  = this->stickyExclusiveEvent.Get();
        this->ownsStickyExclusiveEvent = true;
    }

    this->eventColorRange = this->eventTexture->CreateEventColorRange(
        static_cast< uint32_t >( UBO_EVENT_END ) -
        static_cast< uint32_t >( UBO_COMMON_END ) );
    for ( size_t i = UBO_EVENT_COLOR_TOP; i < UBO_EVENT_END; ++i )
    {
        auto &handle = this->eventHandles[i - UBO_COMMON_END];
        handle.event =
            this->CreateEventFor( static_cast< uniformBufferEvents_t >( i ) );

        switch ( i )
        {
            case UBO_EVENT_COLOR_INNER:
                handle.event->SetCallback( [this]( auto e )
                                           { this->EventMoveWidget( e ); } );
                break;
            case UBO_EVENT_COLOR_TOP:
                handle.event->SetCallback(
                    [this]( auto e ) {
                        this->EventResizeWidget( e, { 0.0, -1.0 },
                                                 POSITION_LEFT, POSITION_TOP );
                    } );
                break;
            case UBO_EVENT_COLOR_BOTTOM:
                handle.event->SetCallback(
                    [this]( auto e )
                    {
                        this->EventResizeWidget( e, { 0.0, 1.0 }, POSITION_LEFT,
                                                 POSITION_BOTTOM );
                    } );
                break;
            case UBO_EVENT_COLOR_LEFT:
                handle.event->SetCallback(
                    [this]( auto e ) {
                        this->EventResizeWidget( e, { -1.0, 0.0 },
                                                 POSITION_LEFT, POSITION_TOP );
                    } );
                break;
            case UBO_EVENT_COLOR_RIGHT:
                handle.event->SetCallback(
                    [this]( auto e ) {
                        this->EventResizeWidget( e, { 1.0, 0.0 },
                                                 POSITION_RIGHT, POSITION_TOP );
                    } );
                break;
            case UBO_EVENT_COLOR_TOP_LEFT:
                handle.event->SetCallback(
                    [this]( auto e )
                    {
                        this->EventResizeWidget( e, { -1.0, -1.0 },
                                                 POSITION_LEFT, POSITION_TOP );
                    } );
                break;
            case UBO_EVENT_COLOR_TOP_RIGHT:
                handle.event->SetCallback(
                    [this]( auto e )
                    {
                        this->EventResizeWidget( e, { 1.0, -1.0 },
                                                 POSITION_RIGHT, POSITION_TOP );
                    } );
                break;
            case UBO_EVENT_COLOR_BOTTOM_LEFT:
                handle.event->SetCallback(
                    [this]( auto e )
                    {
                        this->EventResizeWidget(
                            e, { -1.0, 1.0 }, POSITION_LEFT, POSITION_BOTTOM );
                    } );
                break;
            case UBO_EVENT_COLOR_BOTTOM_RIGHT:
                handle.event->SetCallback(
                    [this]( auto e )
                    {
                        this->EventResizeWidget(
                            e, { 1.0, 1.0 }, POSITION_RIGHT, POSITION_BOTTOM );
                    } );
                break;
        }
    }

    this->CreateHoverEvent();
}

bb::product_ptr< GLAPI::elEvent >
event::CreateEventFor( const uniformBufferEvents_t area ) noexcept
{
    return this->eventHandler->CreateEventForColorIndex(
        this->eventColorRange->GetColorIndex(
            static_cast< uint64_t >( area ) -
            static_cast< uint64_t >( UBO_COMMON_END ) ) );
}

void
event::SetActivateEventHandling( const bool v ) noexcept
{
    for ( auto e : this->createdEvents )
        ( v ) ? e->Activate() : e->Deactivate();
}

bb::product_ptr< GLAPI::elEvent >
event::CreateEvent() noexcept
{
    auto newEvent = this->eventHandler->CreateEvent(
        [this]( auto e ) { this->RemoveEvent( e ); } );
    this->createdEvents.emplace_back( newEvent.Get() );
    if ( this->me->GetHasDisabledEventHandling() ) newEvent->Deactivate();
    return newEvent;
}

void
event::RemoveEvent( GLAPI::elEvent *const event ) noexcept
{
    auto iter = bb::find( this->createdEvents, event );
    if ( iter == this->createdEvents.end() ) return;
    this->createdEvents.erase( iter );
}

GLAPI::elEvent &
event::GetStickyExclusiveEvent() noexcept
{
    return *this->stickyExclusiveEventPtr;
}

uint32_t
event::GetColorToCurrentEvent() const noexcept
{
    return this->eventTexture->GetCurrentEventColor().GetIndex();
}

glm::vec4
event::GetRGBAColorToEvent( const size_t e ) const noexcept
{
    return this->eventColorRange->GetRGBAColor( e - UBO_COMMON_END );
}

uniformBufferEvents_t
event::GetCurrentEventColor() const noexcept
{
    uint32_t colorIndex = this->eventTexture->GetCurrentEventColor().GetIndex();

    for ( size_t i = 0, l = this->eventHandles.size(); i < l; ++i )
        if ( this->eventColorRange->GetColorIndex( i ) == colorIndex )
            return static_cast< uniformBufferEvents_t >( i + UBO_COMMON_END );

    return UBO_EVENT_END;
}

void
event::DetectWidgetState( const SDL_Event &e ) noexcept
{
    if ( this->me->GetState() != STATE_SELECTED )
    {
        // STATE_SELECTED
        if ( e.button.button == SDL_BUTTON_LEFT &&
             e.button.type == SDL_MOUSEBUTTONUP &&
             this->me->GetState() == STATE_CLICKED )
        {
            this->me->StateChangeTo( STATE_SELECTED );
        }

        // STATE_CLICKED
        else if ( e.button.button == SDL_BUTTON_LEFT &&
                  e.button.type == SDL_MOUSEBUTTONDOWN &&
                  this->me->GetState() != STATE_INACTIVE )
        {
            this->me->SetState( STATE_CLICKED );
        }

        // STATE_HOVER
        else if ( this->me->GetState() != STATE_CLICKED )
        {
            this->me->StateChangeTo( STATE_HOVER );
        }
    }
    else
    {
        if ( e.button.button == SDL_BUTTON_LEFT &&
             e.button.type == SDL_MOUSEBUTTONUP &&
             this->me->parent != nullptr && this->me->GetHasMultiSelection() )
        {
            this->me->StateChangeTo( STATE_HOVER );
        }
    }
}

void
event::PreventHorizontalWindowMovement(
    const glm::vec2 &oldSize, const float xMovement,
    const position_t resizingPosition ) noexcept
{
    if ( oldSize != this->me->GetSize() )
    {
        if ( this->me->GetHorizontalPointOfOrigin() == resizingPosition )
        {
            auto position = this->me->GetPosition();
            this->me->SetPosition( { position.x - xMovement, position.y } );
        }
        else if ( this->me->GetHorizontalPointOfOrigin() == POSITION_MIDDLE )
        {
            float signum = ( resizingPosition == POSITION_LEFT ) ? -1.0 : 1.0f;
            auto  position = this->me->GetPosition();
            this->me->SetPosition(
                { position.x + signum * xMovement / 2.0f, position.y } );
        }
    }
}

void
event::PreventVerticalWindowMovement(
    const glm::vec2 &oldSize, const float yMovement,
    const position_t resizingPosition ) noexcept
{
    if ( oldSize != this->me->GetSize() )
    {
        if ( this->me->GetVerticalPointOfOrigin() == resizingPosition )
        {
            auto position = this->me->GetPosition();
            this->me->SetPosition( { position.x, position.y - yMovement } );
        }
        else if ( this->me->GetVerticalPointOfOrigin() == POSITION_MIDDLE )
        {
            float signum   = ( resizingPosition == POSITION_TOP ) ? -1.0 : 1.0f;
            auto  position = this->me->GetPosition();
            this->me->SetPosition(
                { position.x, position.y + signum * yMovement / 2.0f } );
        }
    }
}

void
event::PerformRelativeMove( const glm::vec2 &v ) noexcept
{
    auto adjustedValue = v;
    if ( this->me->GetHorizontalPointOfOrigin() == POSITION_RIGHT )
        adjustedValue.x = -adjustedValue.x;
    if ( this->me->GetVerticalPointOfOrigin() == POSITION_BOTTOM )
        adjustedValue.y = -adjustedValue.y;

    this->me->SetPosition( this->me->GetPosition() + adjustedValue );
}

bool
event::HasMouseMotionJitter( const SDL_Event e ) const noexcept
{
    return ( abs( e.motion.xrel ) > 1000 || abs( e.motion.yrel ) > 1000 );
}

void
event::RegisterStickyExclusiveEvent(
    const std::function< void( SDL_Event ) > &callback ) noexcept
{
    if ( !stickyExclusiveEventPtr->IsRegistered() )
    {
        this->deSelectEvent->Unregister();
        stickyExclusiveEventPtr->Reset();
        stickyExclusiveEventPtr
            ->SetRemovalCondition(
                []( const SDL_Event e )
                { return e.button.type == SDL_MOUSEBUTTONUP; } )
            .SetCallback( [callback]( const SDL_Event e ) { callback( e ); } )
            .SetAfterRemovalCallback( [this]( const SDL_Event )
                                      { this->deSelectEvent->Register(); } );
        stickyExclusiveEventPtr->Register();
    }
}

void
event::EventMoveWidget( const SDL_Event e ) noexcept
{
    if ( stickyExclusiveEventPtr->IsRegistered() ) return;

    this->DetectWidgetState( e );

    if ( e.button.button != SDL_BUTTON_LEFT || this->HasMouseMotionJitter( e ) )
        return;

    if ( !this->me->GetIsMovable() || this->me->GetState() == STATE_INACTIVE )
    {
        if ( this->me->parent != nullptr &&
             this->me->GetForwardMoveEventToParent() )
            this->me->parent->EventMoveWidget( e );

        return;
    }

    this->RegisterStickyExclusiveEvent(
        [this]( auto )
        {
            this->PerformRelativeMove(
                this->me->mouseCursor->GetRelativeCursorPosition() );
        } );

    this->PerformRelativeMove(
        this->me->mouseCursor->GetRelativeCursorPosition() );
}

void
event::EventResizeWidget( const SDL_Event e, const glm::vec2 &direction,
                          const position_t horizontalMovement,
                          const position_t verticalMovement ) noexcept
{
    if ( stickyExclusiveEventPtr->IsRegistered() ) return;

    this->DetectWidgetState( e );

    if ( !this->me->GetIsResizable() || this->me->GetState() == STATE_INACTIVE )
        return;

    if ( e.button.button != SDL_BUTTON_LEFT || this->HasMouseMotionJitter( e ) )
        return;


    auto resizeCallback =
        [this, direction, horizontalMovement, verticalMovement]( SDL_Event )
    {
        auto      oldSize = this->me->GetSize();
        glm::vec2 mouseMovement =
            this->me->mouseCursor->GetRelativeCursorPosition();

        this->me->SetSize(
            glm::vec2( oldSize[0] + direction.x * mouseMovement.x,
                       oldSize[1] + direction.y * mouseMovement.y ) );

        this->PreventHorizontalWindowMovement(
            oldSize, direction.x * mouseMovement.x, horizontalMovement );
        this->PreventVerticalWindowMovement(
            oldSize, direction.y * mouseMovement.y, verticalMovement );
    };

    this->RegisterStickyExclusiveEvent( resizeCallback );

    resizeCallback( e );
}

void
event::CreateDeSelectEvent( elWidget_base *source ) noexcept
{
    this->deSelectEvent->SetCallback(
        [this, source]( const SDL_Event e )
        {
            if ( !source->DoesEventColorBelongsToMeOrChild(
                     this->GetColorToCurrentEvent() ) &&
                 e.button.type == SDL_MOUSEBUTTONUP &&
                 e.button.button == SDL_BUTTON_LEFT &&
                 !stickyExclusiveEventPtr->IsRegistered() )
            {
                this->me->SetState( STATE_DEFAULT );
            }
        } );
}

void
event::CreateDeSingleSelectEvent() noexcept
{
    this->CreateDeSelectEvent( this->me );
}

void
event::CreateDeMultiSelectEvent() noexcept
{
    this->CreateDeSelectEvent(
        ( this->me->parent != nullptr ) ? this->me->parent : this->me );
}

void
event::CreateHoverEvent() noexcept
{
    this->deHoverEvent->SetCallback(
        [this]( const SDL_Event )
        {
            if ( !this->DoesEventColorBelongsToMeOrChild(
                     this->GetColorToCurrentEvent() ) &&
                 !stickyExclusiveEventPtr->IsRegistered() )
            {
                this->me->SetState( STATE_DEFAULT );
            }
        } );
}

bool
event::DoesEventColorBelongsToMe( const uint32_t c ) const noexcept
{
    for ( uint32_t i = 0; i < this->eventColorRange->GetSize(); ++i )
        if ( this->eventColorRange->GetColorIndex( i ) == c ) return true;
    return false;
}

bool
event::DoesEventColorBelongsToMeOrChild( const uint32_t c ) const noexcept
{
    if ( this->DoesEventColorBelongsToMe( c ) ) return true;
    for ( auto w : this->me->GetChildren() )
        if ( w != nullptr && w->DoesEventColorBelongsToMeOrChild( c ) )
            return true;

    return false;
}

std::optional< elWidget_base * >
event::GetChildFromEventColor( const uint32_t c ) noexcept
{
    if ( this->DoesEventColorBelongsToMe( c ) ) return this->me;

    for ( auto child : this->me->GetChildren() )
        if ( child != nullptr && child->DoesEventColorBelongsToMeOrChild( c ) )
            return child->GetChildFromEventColor( c );

    return std::nullopt;
}

void
event::SetupStateEvents( const widgetStates_t state ) noexcept
{
    switch ( state )
    {
        case STATE_SELECTED:
            this->deSelectEvent->Register();
            this->deHoverEvent->Unregister();
            break;
        case STATE_HOVER:
            this->deSelectEvent->Unregister();
            this->deHoverEvent->Register();
            break;
        case STATE_INACTIVE:
            this->deSelectEvent->Unregister();
            this->deHoverEvent->Unregister();
            break;
        default:
            this->deSelectEvent->Unregister();
            this->deHoverEvent->Unregister();
            break;
    }
}

void
event::ScrollEventCallback( const SDL_Event e ) noexcept
{
    if ( this->me->parent ) this->me->parent->ScrollEventCallback( e );
}
} // namespace handler
} // namespace GuiGL
} // namespace el3D
