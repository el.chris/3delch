#include "GuiGL/elWidget_Menubar.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Menubar::elWidget_Menubar( const constructor_t& constructor )
    : elWidget_Menu( constructor )
{
}

void
elWidget_Menubar::AlignSubMenus() noexcept
{
    for ( auto& entry : this->root )
    {
        if ( entry.submenu ) entry.submenu->SetHasVerticalAlignment( true );
    }
}

void
elWidget_Menubar::Reshape()
{
    auto position = this->GetButtonAlignment();

    if ( position == POSITION_TOP || position == POSITION_BOTTOM )
    {
        this->SetHasVerticalAlignment( false );
        this->SetStickToParentSize( { 0.0f, -1.0f } );
    }
    else
    {
        this->SetHasVerticalAlignment( true );
        this->SetStickToParentSize( { -1.0f, 0.0f } );
    }

    if ( position == POSITION_TOP )
        this->SetPositionPointOfOrigin( POSITION_TOP_LEFT );
    else if ( position == POSITION_BOTTOM )
        this->SetPositionPointOfOrigin( POSITION_BOTTOM_LEFT );
    else if ( position == POSITION_LEFT )
        this->SetPositionPointOfOrigin( POSITION_TOP_LEFT );
    else if ( position == POSITION_RIGHT )
        this->SetPositionPointOfOrigin( POSITION_TOP_RIGHT );


    elWidget_Menu::Reshape();
}

} // namespace GuiGL
} // namespace el3D
