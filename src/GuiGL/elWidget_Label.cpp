#include "GuiGL/elWidget_Label.hpp"

#include "GLAPI/elFontCache.hpp"
#include "OpenGL/elTexture_2D.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Label::elWidget_Label( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->texture = this->vo.CreateTexture< OpenGL::elTexture_2D >(
        0, "image", GL_TEXTURE0, "" );
    if ( !this->texture )
    {
        throw InvalidShader_e(
            LOG_EX( 0, "unable to add texture to vertex object" ) );
    }

    this->ReloadConfigSettings();
    this->texture->TexParameteri( GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    this->texture->TexParameteri( GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    this->SetDoReadTexture( true );
}

void
elWidget_Label::ReloadConfigSettings() noexcept
{
    elWidget_base::ReloadConfigSettings();

    this->font =
        this->fontCache->Get( this->GetFontFile(), this->GetFontSize(), false )
            .OrElse( [&] {
                throw InvalidFont_e(
                    LOG_EX( 0, std::string( "unable to get font " ) +
                                   this->GetFontFile() + ":" +
                                   std::to_string( this->GetFontSize() ) +
                                   " from font cache" ) );
            } )
            .GetValue();
}

GLAPI::dimension_t
elWidget_Label::GetTextSize() const noexcept
{
    return this->font->GetTextSize( this->text );
}

uint64_t
elWidget_Label::GetFontHeight() const noexcept
{
    return this->font->GetFontHeight();
}

std::string
elWidget_Label::GetText() const noexcept
{
    return this->text;
}

uint64_t
elWidget_Label::GetFontSize() const noexcept
{
    return static_cast< uint64_t >(
        this->widgetSettings.Get< int >( PROPERTY_FONT_SIZE )[0] );
}

std::string
elWidget_Label::GetFontFile() const noexcept
{
    return this->widgetSettings.Get< std::string >( PROPERTY_FONT_FILE )[0];
}

void
elWidget_Label::SetText( const std::string& text ) noexcept
{
    if ( this->text == text ) return;

    this->text = text;
    this->font->SetText( text, { 0, 0, 0, 255 } );
    auto dim = this->font->GetTextureSize();
    if ( this->adjustSizeToTextSize )
    {
        this->SetSize( glm::vec2( static_cast< float >( dim.width ),
                                  static_cast< float >( dim.height ) ) /
                       GetDPIScaling() );
    }
    else
    {
        this->SetNonConformTextureSize( glm::vec2( dim.width, dim.height ) /
                                        GetDPIScaling() );
    }
    this->texture->SetSize( dim.width, dim.height );
    this->texture->TexImage( font->GetBytePointer() );
}

void
elWidget_Label::DoAdjustSizeToTextSize( const bool v ) noexcept
{
    this->adjustSizeToTextSize = v;
}

} // namespace GuiGL
} // namespace el3D
