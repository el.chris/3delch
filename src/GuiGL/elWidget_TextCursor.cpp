#include "GuiGL/elWidget_TextCursor.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_TextCursor::elWidget_TextCursor( const constructor_t& constructor )
    : elWidget_base( constructor )
{
}

elWidget_TextCursor::~elWidget_TextCursor()
{
}


} // namespace GuiGL
} // namespace el3D
