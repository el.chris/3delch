#include "HighGL/elObjectGeometryBuilder.hpp"

#include <cmath>
#include <glm/glm.hpp>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace HighGL
{
template < typename T >
static void
Add2Vector( std::vector< T >& v, const T value ) noexcept
{
    v.emplace_back( value );
}

template < typename T, typename... Tadd >
static void
Add2Vector( std::vector< T >& v, const T value, const Tadd... values ) noexcept
{
    v.emplace_back( value );
    Add2Vector( v, values... );
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateCube() noexcept
{
    elObjectGeometryBuilder cube;
    cube.objectGeometry.vertices = {
        -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f,
        -1.0f, 1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,
        1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f,
        -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f,
        1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f, -1.0f,
        1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f };

    cube.objectGeometry.elements = { 0,  1,  2,  2,  3,  0,  4,  5,  6,
                                     6,  7,  4,  8,  9,  10, 10, 11, 8,
                                     12, 13, 14, 14, 15, 12, 16, 17, 18,
                                     18, 19, 16, 20, 22, 21, 22, 20, 23 };

    cube.objectGeometry.textureCoordinates = {
        1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f };

    cube.objectGeometry.normals = {
        0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
        0.0f,  0.0f,  -1.0f, 1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
        -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  -1.0f, 0.0f,
        0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f };

    cube.objectGeometry.tangents = {
        1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };

    cube.objectGeometry.bitangents = {
        0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        0.0f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f, 0.0f,  0.0f, 1.0f,
        0.0f,  0.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f };

    cube.objectGeometry.innerElements = { 0,  2,  1,  2,  0,  3,  4,  6,  5,
                                          6,  4,  7,  8,  10, 9,  10, 8,  11,
                                          12, 14, 13, 14, 12, 15, 16, 18, 17,
                                          18, 16, 19, 20, 21, 22, 22, 23, 20 };
    cube.objectGeometry.innerNormals  = {
        0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
        0.0f,  0.0f,  1.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  0.0f,  0.0f,  -1.0f,
        0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f,
        1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
        1.0f,  0.0f,  0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,
        0.0f,  -1.0f, 0.0f,  0.0f,  -1.0f, 0.0f,  0.0f,  1.0f,  0.0f,
        0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f };

    return cube;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateScreenQuad() noexcept
{
    elObjectGeometryBuilder quad;
    quad.objectGeometry.vertices = { -1.0f, -1.0f, 0.0f, 1.0f,  -1.0f, 0.0f,
                                     1.0f,  1.0f,  0.0f, -1.0f, 1.0f,  0.0f };
    quad.objectGeometry.elements = { 0, 1, 2, 2, 3, 0 };

    // WARNING! if you change these every gui shader (like graph, mouse,
    // checkbutton) has to be adapted
    quad.objectGeometry.textureCoordinates = { 1.0f, 1.0f, 0.0f, 1.0f,
                                               0.0f, 0.0f, 1.0f, 0.0f };
    quad.objectGeometry.normals = { 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f,
                                    0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f };

    quad.objectGeometry.innerElements = { 0, 2, 1, 2, 0, 3 };
    quad.objectGeometry.innerNormals  = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                                         0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
    return quad;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateQuad() noexcept
{
    elObjectGeometryBuilder quad;
    quad.objectGeometry.vertices = { -1.0f, 0.0f, -1.0f, 1.0f,  0.0f, -1.0f,
                                     1.0f,  0.0f, 1.0f,  -1.0f, 0.0f, 1.0f };
    quad.objectGeometry.elements = { 0, 1, 2, 2, 3, 0 };

    quad.objectGeometry.textureCoordinates = { 1.0f, 1.0f, 0.0f, 1.0f,
                                               0.0f, 0.0f, 1.0f, 0.0f };
    quad.objectGeometry.normals = { 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                                    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };

    quad.objectGeometry.innerElements = { 0, 2, 1, 2, 0, 3 };
    quad.objectGeometry.innerNormals  = { 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f,
                                         0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f };
    return quad;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreatePyramid() noexcept
{
    elObjectGeometryBuilder pyramid;
    pyramid.objectGeometry.vertices = {
        -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, 0.0f, 1.0f,
        0.0f,  1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  0.0f,  1.0f, 0.0f,
        1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  0.0f,  1.0f,  0.0f, -1.0f,
        -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, 0.0f,  1.0f,  0.0f };
    pyramid.objectGeometry.elements = { 0, 2, 1, 2,  0,  3,  4,  5,  6,
                                        7, 8, 9, 10, 11, 12, 13, 14, 15 };

    pyramid.objectGeometry.textureCoordinates = {
        1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 0.0f };
    pyramid.objectGeometry.normals = {
        0.0f,   -1.0f,   0.0f,    0.0f,   -1.0f,   0.0f,    0.0f,    -1.0f,
        0.0f,   0.0f,    -1.0f,   0.0f,   0.0f,    0.447f,  -0.894f, 0.0f,
        0.447f, -0.894f, 0.0f,    0.447f, -0.894f, 0.894f,  0.447f,  0.0f,
        0.894f, 0.447f,  0.0f,    0.894f, 0.447f,  0.0f,    0.0f,    0.447f,
        0.894f, 0.0f,    0.447f,  0.894f, 0.0f,    0.447f,  0.894f,  -0.894f,
        0.447f, 0.0f,    -0.894f, 0.447f, 0.0f,    -0.894f, 0.447f,  0.0f };

    pyramid.objectGeometry.innerElements = { 0, 1, 2, 2,  3,  0,  4,  6,  5,
                                             7, 9, 8, 10, 12, 11, 13, 15, 14 };
    pyramid.objectGeometry.innerNormals  = {
        0.0f,    1.0f,    0.0f,    0.0f,    1.0f,    0.0f,    0.0f,    1.0f,
        0.0f,    0.0f,    1.0f,    0.0f,    0.0f,    -0.447f, 0.894f,  0.0f,
        -0.447f, 0.894f,  0.0f,    -0.447f, 0.894f,  -0.894f, -0.447f, 0.0f,
        -0.894f, -0.447f, 0.0f,    -0.894f, -0.447f, 0.0f,    0.0f,    -0.447f,
        -0.894f, 0.0f,    -0.447f, -0.894f, 0.0f,    -0.447f, -0.894f, 0.894f,
        -0.447f, 0.0f,    0.894f,  -0.447f, 0.0f,    0.894f,  -0.447f, 0.0f };

    return pyramid;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateHexagon() noexcept
{
    elObjectGeometryBuilder hexagon;

    hexagon.objectGeometry.vertices = {
        0.0f,   0.0f, 0.0f,    -1.0f,  0.0f,  0.0f,    -0.5f,
        0.866f, 0.0f, 0.5f,    0.866f, 0.0f,  1.0f,    0.0f,
        0.0f,   0.5f, -0.866f, 0.0f,   -0.5f, -0.866f, 0.0f,
    };
    hexagon.objectGeometry.elements           = { 0, 1, 2, 0, 2, 3, 0, 3, 4,
                                        0, 4, 5, 0, 5, 6, 0, 6, 1 };
    hexagon.objectGeometry.textureCoordinates = {
        0.5f,   0.5f, 0.0f, 0.5f,  0.25f,  0.933f, 0.75f,
        0.933f, 1.0f, 0.5f, 0.75f, 0.067f, 0.25f,  0.067f,
    };
    hexagon.objectGeometry.normals = {
        0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
    };
    hexagon.objectGeometry.innerElements = {
        0, 2, 1, 0, 3, 2, 0, 4, 3, 0, 5, 4, 0, 6, 5, 0, 1, 6,
    };
    hexagon.objectGeometry.innerNormals = {
        0.0f,  0.0f, -1.0f, 0.0f,  0.0f, -1.0f, 0.0f,  0.0f, -1.0f, 0.0f,  0.0f,
        -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f,
    };

    return hexagon;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateHexagonTube() noexcept
{
    elObjectGeometryBuilder hexagonTube;

    hexagonTube.objectGeometry.vertices = {
        0.0f,  0.0f,    1.0f,  -1.0f, 0.0f,    1.0f,  -0.5f, 0.866f,  1.0f,
        0.5f,  0.866f,  1.0f,  1.0f,  0.0f,    1.0f,  0.5f,  -0.866f, 1.0f,
        -0.5f, -0.866f, 1.0f,  0.0f,  0.0f,    -1.0f, -1.0f, 0.0f,    -1.0f,
        -0.5f, 0.866f,  -1.0f, 0.5f,  0.866f,  -1.0f, 1.0f,  0.0f,    -1.0f,
        0.5f,  -0.866f, -1.0f, -0.5f, -0.866f, -1.0f, -1.0f, 0.0f,    1.0f,
        -0.5f, 0.866f,  1.0f,  -1.0f, 0.0f,    -1.0f, -0.5f, 0.866f,  -1.0f,
        -0.5f, 0.866f,  1.0f,  0.5f,  0.866f,  1.0f,  -0.5f, 0.866f,  -1.0f,
        0.5f,  0.866f,  -1.0f, 0.5f,  0.866f,  1.0f,  1.0f,  0.0f,    1.0f,
        0.5f,  0.866f,  -1.0f, 1.0f,  0.0f,    -1.0f, 1.0f,  0.0f,    1.0f,
        0.5f,  -0.866f, 1.0f,  1.0f,  0.0f,    -1.0f, 0.5f,  -0.866f, -1.0f,
        0.5f,  -0.866f, 1.0f,  -0.5f, -0.866f, 1.0f,  0.5f,  -0.866f, -1.0f,
        -0.5f, -0.866f, -1.0f, -0.5f, -0.866f, 1.0f,  -1.0f, 0.0f,    1.0f,
        -0.5f, -0.866f, -1.0f, -1.0f, 0.0f,    -1.0f,
    };
    hexagonTube.objectGeometry.elements = {
        0,  1,  2,  0,  2,  3,  0,  3,  4,  0,  4,  5,  0,  5,  6,
        0,  6,  1,  7,  9,  8,  7,  10, 9,  7,  11, 10, 7,  12, 11,
        7,  13, 12, 7,  8,  13, 15, 14, 16, 16, 17, 15, 19, 18, 20,
        20, 21, 19, 23, 22, 24, 24, 25, 23, 27, 26, 28, 28, 29, 27,
        31, 30, 32, 32, 33, 31, 35, 34, 36, 36, 37, 35 };
    hexagonTube.objectGeometry.textureCoordinates = {
        0.5f,   0.5f,   0.0f,   0.5f,   0.25f,  0.933f, 0.75f,  0.933f, 1.0f,
        0.5f,   0.75f,  0.067f, 0.25f,  0.067f, 0.5f,   0.5f,   0.0f,   0.5f,
        0.25f,  0.933f, 0.75f,  0.933f, 1.0f,   0.5f,   0.75f,  0.067f, 0.25f,
        0.067f, 0.0f,   0.0f,   0.166f, 0.0f,   0.0f,   1.0f,   0.166f, 1.0f,
        0.166f, 0.0f,   0.333f, 0.0f,   0.166f, 1.0f,   0.333f, 1.0f,   0.333f,
        0.0f,   0.5f,   0.0f,   0.333f, 1.0f,   0.5f,   1.0f,   0.5f,   0.0f,
        0.666f, 0.0f,   0.5f,   1.0f,   0.666f, 1.0f,   0.666f, 0.0f,   0.833f,
        0.0f,   0.666f, 1.0f,   0.833f, 1.0f,   0.833f, 0.0f,   1.0f,   0.0f,
        0.833f, 1.0f,   1.0f,   1.0f,
    };
    hexagonTube.objectGeometry.normals = {
        0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,
        0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,
        0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f,
        0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f,
        0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f, -0.866f, 0.5f,  0.0f,
        -0.866f, 0.5f,  0.0f,  -0.866f, 0.5f,  0.0f,  -0.866f, 0.5f,  0.0f,
        0.0f,    1.0f,  0.0f,  0.0f,    1.0f,  0.0f,  0.0f,    1.0f,  0.0f,
        0.0f,    1.0f,  0.0f,  0.866f,  0.5f,  0.0f,  0.866f,  0.5f,  0.0f,
        0.866f,  0.5f,  0.0f,  0.866f,  0.5f,  0.0f,  0.866f,  -0.5f, 0.0f,
        0.866f,  -0.5f, 0.0f,  0.866f,  -0.5f, 0.0f,  0.866f,  -0.5f, 0.0f,
        0.0f,    -1.0f, 0.0f,  0.0f,    -1.0f, 0.0f,  0.0f,    -1.0f, 0.0f,
        0.0f,    -1.0f, 0.0f,  -0.866f, -0.5f, 0.0f,  -0.866f, -0.5f, 0.0f,
        -0.866f, -0.5f, 0.0f,  -0.866f, -0.5f, 0.0f,
    };
    hexagonTube.objectGeometry.tangents   = {};
    hexagonTube.objectGeometry.bitangents = {};

    hexagonTube.objectGeometry.innerElements = {
        0,  2,  1,  0,  3,  2,  0,  4,  3,  0,  5,  4,  0,  6,  5,  0,  1,  6,
        7,  8,  9,  7,  9,  10, 7,  10, 11, 7,  11, 12, 7,  12, 13, 7,  13, 8,
        15, 16, 14, 16, 15, 17, 19, 20, 18, 20, 19, 21, 23, 24, 22, 24, 23, 25,
        27, 28, 26, 28, 27, 29, 31, 32, 30, 32, 31, 33, 35, 36, 34, 36, 35, 37,
    };
    hexagonTube.objectGeometry.innerNormals = {
        0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f,
        0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  -1.0f,
        0.0f,    0.0f,  -1.0f, 0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,
        0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,
        0.0f,    0.0f,  1.0f,  0.0f,    0.0f,  1.0f,  0.866f,  -0.5f, 0.0f,
        0.866f,  -0.5f, 0.0f,  0.866f,  -0.5f, 0.0f,  0.866f,  -0.5f, 0.0f,
        0.0f,    -1.0f, 0.0f,  0.0f,    -1.0f, 0.0f,  0.0f,    -1.0f, 0.0f,
        0.0f,    -1.0f, 0.0f,  -0.866f, -0.5f, 0.0f,  -0.866f, -0.5f, 0.0f,
        -0.866f, -0.5f, 0.0f,  -0.866f, -0.5f, 0.0f,  -0.866f, 0.5f,  0.0f,
        -0.866f, 0.5f,  0.0f,  -0.866f, 0.5f,  0.0f,  -0.866f, 0.5f,  0.0f,
        0.0f,    1.0f,  0.0f,  0.0f,    1.0f,  0.0f,  0.0f,    1.0f,  0.0f,
        0.0f,    1.0f,  0.0f,  0.866f,  0.5f,  0.0f,  0.866f,  0.5f,  0.0f,
        0.866f,  0.5f,  0.0f,  0.866f,  0.5f,  0.0f,
    };

    return hexagonTube;
}

elObjectGeometryBuilder
elObjectGeometryBuilder ::CreateSphere( const uint64_t n ) noexcept
{
    elObjectGeometryBuilder sphere;

    sphere.objectGeometry.vertices.reserve( 3 * ( n + 1 ) * ( n + 1 ) );
    sphere.objectGeometry.elements.reserve( 3 * ( n ) * ( n ) );
    sphere.objectGeometry.textureCoordinates.reserve( 2 * ( n + 1 ) *
                                                      ( n + 1 ) );
    sphere.objectGeometry.normals.reserve( 3 * ( n + 1 ) * ( n + 1 ) );
    sphere.objectGeometry.innerNormals.reserve( 3 * ( n + 1 ) * ( n + 1 ) );
    sphere.objectGeometry.innerElements.reserve( 3 * ( n ) * ( n ) );

    GLfloat xStepSize =
        2.0f * static_cast< GLfloat >( M_PI ) / static_cast< GLfloat >( n );
    GLfloat yStepSize =
        static_cast< GLfloat >( M_PI ) / static_cast< GLfloat >( n );

    for ( uint64_t y = 0; y <= n; ++y )
    {
        GLfloat yAngle = static_cast< GLfloat >( M_PI ) / 2.0f -
                         static_cast< GLfloat >( y ) * yStepSize;

        GLuint e1 = static_cast< GLuint >( y * ( n + 1 ) );
        GLuint e2 = static_cast< GLuint >( e1 + n + 1 );

        for ( uint64_t x = 0; x <= n; ++x, ++e1, ++e2 )
        {
            GLfloat xAngle = static_cast< GLfloat >( x ) * xStepSize;

            GLfloat xCoord = cosf( xAngle ) * cosf( yAngle );
            GLfloat yCoord = sinf( xAngle ) * cosf( yAngle );
            GLfloat zCoord = sinf( yAngle );

            Add2Vector( sphere.objectGeometry.vertices, xCoord, yCoord,
                        zCoord );
            Add2Vector( sphere.objectGeometry.normals, xCoord, yCoord, zCoord );
            Add2Vector( sphere.objectGeometry.innerNormals, -xCoord, -yCoord,
                        -zCoord );
            Add2Vector( sphere.objectGeometry.tangents,
                        -sinf( xAngle ) * cosf( yAngle ),
                        cosf( xAngle ) * cosf( yAngle ), 0.0f );
            Add2Vector( sphere.objectGeometry.bitangents,
                        -cosf( xAngle ) * sinf( yAngle ),
                        -sinf( xAngle ) * sinf( yAngle ), cosf( yAngle ) );

            Add2Vector(
                sphere.objectGeometry.textureCoordinates,
                static_cast< GLfloat >( x ) / static_cast< GLfloat >( n ),
                static_cast< GLfloat >( y ) / static_cast< GLfloat >( n ) );

            if ( x < n && y < n )
            {
                if ( y != 0 )
                {
                    Add2Vector( sphere.objectGeometry.elements, e1, e1 + 1,
                                e2 );
                    Add2Vector( sphere.objectGeometry.innerElements, e1, e2,
                                e1 + 1 );
                }

                if ( y + 1 != n )
                {
                    Add2Vector( sphere.objectGeometry.elements, e1 + 1, e2 + 1,
                                e2 );
                    Add2Vector( sphere.objectGeometry.innerElements, e1 + 1, e2,
                                e2 + 1 );
                }
            }
        }
    }

    return sphere;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreateCylinder( const uint64_t n ) noexcept
{
    elObjectGeometryBuilder cylinder;
    // reserve
    cylinder.objectGeometry.vertices.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.normals.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.innerNormals.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.tangents.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.bitangents.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.textureCoordinates.reserve( 3 * ( n + 1 ) * 4 );
    cylinder.objectGeometry.elements.reserve( 3 * ( n * 4 - 2 ) );

    GLfloat stepSize =
        2.0f * static_cast< GLfloat >( M_PI ) / static_cast< GLfloat >( n );

    for ( uint64_t y = 0; y <= n; ++y )
    {
        GLfloat angle = static_cast< GLfloat >( M_PI ) / 2.0f -
                        static_cast< GLfloat >( y ) * stepSize;

        GLfloat xCoord = cosf( angle );
        GLfloat yCoord = sinf( angle );

        // top
        Add2Vector( cylinder.objectGeometry.vertices, xCoord, yCoord, 1.0f );
        Add2Vector( cylinder.objectGeometry.normals, 0.0f, 0.0f, 1.0f );
        Add2Vector( cylinder.objectGeometry.innerNormals, 0.0f, 0.0f, -1.0f );
        Add2Vector( cylinder.objectGeometry.tangents, xCoord, yCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.bitangents, -yCoord, xCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.textureCoordinates,
                    xCoord / 2.0f + 0.5f, yCoord / 2.0f + 0.5f );

        // bottom
        Add2Vector( cylinder.objectGeometry.vertices, xCoord, yCoord, -1.0f );
        Add2Vector( cylinder.objectGeometry.normals, 0.0f, 0.0f, -1.0f );
        Add2Vector( cylinder.objectGeometry.innerNormals, 0.0f, 0.0f, 1.0f );
        Add2Vector( cylinder.objectGeometry.tangents, xCoord, yCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.bitangents, -yCoord, xCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.textureCoordinates,
                    xCoord / 2.0f + 0.5f, yCoord / 2.0f + 0.5f );

        // body
        Add2Vector( cylinder.objectGeometry.vertices, xCoord, yCoord, -1.0f );
        Add2Vector( cylinder.objectGeometry.normals, xCoord, yCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.innerNormals, -xCoord, -yCoord,
                    0.0f );
        Add2Vector( cylinder.objectGeometry.tangents, 0.0f, 0.0f, 1.0f );
        Add2Vector( cylinder.objectGeometry.bitangents, -yCoord, xCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.textureCoordinates,
                    static_cast< GLfloat >( y ) / static_cast< GLfloat >( n ),
                    0.0f );

        Add2Vector( cylinder.objectGeometry.vertices, xCoord, yCoord, 1.0f );
        Add2Vector( cylinder.objectGeometry.normals, xCoord, yCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.innerNormals, -xCoord, -yCoord,
                    0.0f );
        Add2Vector( cylinder.objectGeometry.tangents, 0.0f, 0.0f, 1.0f );
        Add2Vector( cylinder.objectGeometry.bitangents, -yCoord, xCoord, 0.0f );
        Add2Vector( cylinder.objectGeometry.textureCoordinates,
                    static_cast< GLfloat >( y ) / static_cast< GLfloat >( n ),
                    1.0f );

        constexpr GLuint OFFSET_TOP    = 0;
        constexpr GLuint OFFSET_BOTTOM = 1;
        constexpr GLuint OFFSET_BODY   = 2;
        constexpr GLuint SECTIONS      = 4;

        if ( y + 1 < n )
        {
            // top
            Add2Vector(
                cylinder.objectGeometry.elements, OFFSET_TOP,
                static_cast< GLuint >( OFFSET_TOP + SECTIONS * ( y + 1 ) ),
                static_cast< GLuint >( OFFSET_TOP + SECTIONS * ( y + 2 ) ) );
            Add2Vector(
                cylinder.objectGeometry.innerElements, OFFSET_TOP,
                static_cast< GLuint >( OFFSET_TOP + SECTIONS * ( y + 2 ) ),
                static_cast< GLuint >( OFFSET_TOP + SECTIONS * ( y + 1 ) ) );

            // bottom
            Add2Vector(
                cylinder.objectGeometry.elements, OFFSET_BOTTOM,
                static_cast< GLuint >( OFFSET_BOTTOM + SECTIONS * ( y + 2 ) ),
                static_cast< GLuint >( OFFSET_BOTTOM + SECTIONS * ( y + 1 ) ) );
            Add2Vector(
                cylinder.objectGeometry.innerElements, OFFSET_BOTTOM,
                static_cast< GLuint >( OFFSET_BOTTOM + SECTIONS * ( y + 1 ) ),
                static_cast< GLuint >( OFFSET_BOTTOM + SECTIONS * ( y + 2 ) ) );
        }

        if ( y < n )
        {
            // body
            Add2Vector(
                cylinder.objectGeometry.elements,
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y + 1 ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) ) );
            Add2Vector(
                cylinder.objectGeometry.elements,
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y + 1 ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) +
                                       1 ) );
            Add2Vector(
                cylinder.objectGeometry.innerElements,
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y + 1 ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) ) );
            Add2Vector(
                cylinder.objectGeometry.innerElements,
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * y + 1 ),
                static_cast< GLuint >( OFFSET_BODY + SECTIONS * ( y + 1 ) +
                                       1 ) );
        }
    }

    return cylinder;
}

elObjectGeometryBuilder
elObjectGeometryBuilder::CreatePlane(
    const float width, const float height, const uint64_t numberOfElementsWidth,
    const uint64_t numberOfElementsHeight ) noexcept
{
    elObjectGeometryBuilder plane;

    uint64_t numberOfElements = numberOfElementsHeight * numberOfElementsWidth;
    plane.objectGeometry.vertices.resize( numberOfElements * 3 );
    plane.objectGeometry.normals.resize( numberOfElements * 3 );
    plane.objectGeometry.tangents.resize( numberOfElements * 3 );
    plane.objectGeometry.bitangents.resize( numberOfElementsWidth *
                                            numberOfElementsHeight * 3 );
    plane.objectGeometry.innerNormals.resize( numberOfElementsWidth *
                                              numberOfElementsHeight * 3 );
    plane.objectGeometry.textureCoordinates.resize(
        numberOfElementsWidth * numberOfElementsHeight * 2 );

    glm::vec2 stepWidth = glm::vec2(
        width / static_cast< float >( numberOfElementsWidth - 1 ),
        height / static_cast< float >( numberOfElementsHeight - 1 ) );

    glm::vec2 position( -width / 2.0f, -height / 2.0f );
    glm::vec2 texCoord( 0.0, 0.0 );
    for ( uint64_t x = 0, offset = 0, textureOffset = 0;
          x < numberOfElementsWidth; ++x )
    {
        texCoord.y = 0.0f;
        position.y = -height / 2.0f;
        for ( uint64_t y = 0; y < numberOfElementsHeight; ++y )
        {
            plane.objectGeometry.vertices[offset]     = position.x;
            plane.objectGeometry.vertices[offset + 1] = 0.0f;
            plane.objectGeometry.vertices[offset + 2] = position.y;

            plane.objectGeometry.normals[offset]     = 0.0f;
            plane.objectGeometry.normals[offset + 1] = 1.0f;
            plane.objectGeometry.normals[offset + 2] = 0.0f;

            plane.objectGeometry.tangents[offset]     = 1.0f;
            plane.objectGeometry.tangents[offset + 1] = 0.0f;
            plane.objectGeometry.tangents[offset + 2] = 0.0f;

            plane.objectGeometry.bitangents[offset]     = 0.0f;
            plane.objectGeometry.bitangents[offset + 1] = 0.0f;
            plane.objectGeometry.bitangents[offset + 2] = 1.0f;

            plane.objectGeometry.innerNormals[offset]     = 0.0f;
            plane.objectGeometry.innerNormals[offset + 1] = -1.0f;
            plane.objectGeometry.innerNormals[offset + 2] = 0.0f;

            plane.objectGeometry.textureCoordinates[textureOffset] = texCoord.x;
            plane.objectGeometry.textureCoordinates[textureOffset + 1] =
                texCoord.y;

            position.y += stepWidth.y;
            texCoord.y += 1.0f / static_cast< float >( numberOfElementsHeight );
            offset += 3;
            textureOffset += 2;
        }
        position.x += stepWidth.x;
        texCoord.x += 1.0f / static_cast< float >( numberOfElementsWidth );
    }

    plane.objectGeometry.elements.resize( 6 * ( numberOfElementsWidth - 1 ) *
                                          ( numberOfElementsHeight - 1 ) );
    plane.objectGeometry.innerElements.resize(
        6 * ( numberOfElementsWidth - 1 ) * ( numberOfElementsHeight - 1 ) );

    for ( uint64_t x = 0, offset = 0; x < numberOfElementsWidth - 1; ++x )
    {
        for ( uint64_t y = 0; y < numberOfElementsHeight - 1; ++y )
        {
            plane.objectGeometry.elements[offset] =
                static_cast< GLuint >( numberOfElementsHeight * x + y );
            plane.objectGeometry.elements[offset + 1] =
                static_cast< GLuint >( numberOfElementsHeight * ( x + 1 ) + y );
            plane.objectGeometry.elements[offset + 2] =
                static_cast< GLuint >( numberOfElementsHeight * x + y + 1 );

            plane.objectGeometry.elements[offset + 3] =
                static_cast< GLuint >( numberOfElementsHeight * x + y + 1 );
            plane.objectGeometry.elements[offset + 4] =
                static_cast< GLuint >( numberOfElementsHeight * ( x + 1 ) + y );
            plane.objectGeometry.elements[offset + 5] = static_cast< GLuint >(
                numberOfElementsHeight * ( x + 1 ) + y + 1 );

            plane.objectGeometry.innerElements[offset] =
                static_cast< GLuint >( numberOfElementsHeight * x + y );
            plane.objectGeometry.innerElements[offset + 1] =
                static_cast< GLuint >( numberOfElementsHeight * x + y + 1 );
            plane.objectGeometry.innerElements[offset + 2] =
                static_cast< GLuint >( numberOfElementsHeight * ( x + 1 ) + y );

            plane.objectGeometry.innerElements[offset + 3] =
                static_cast< GLuint >( numberOfElementsHeight * x + y + 1 );
            plane.objectGeometry.innerElements[offset + 4] =
                static_cast< GLuint >( numberOfElementsHeight * ( x + 1 ) + y +
                                       1 );
            plane.objectGeometry.innerElements[offset + 5] =
                static_cast< GLuint >( numberOfElementsHeight * ( x + 1 ) + y );

            offset += 6;
        }
    }
    return plane;
}

elObjectGeometryBuilder&
elObjectGeometryBuilder::ApplyPosition( const glm::vec3& position ) noexcept
{
    for ( uint64_t i = 0, limit = this->objectGeometry.vertices.size();
          i < limit; i += 3 )
    {
        this->objectGeometry.vertices[i] += position.x;
        this->objectGeometry.vertices[i + 1] += position.y;
        this->objectGeometry.vertices[i + 2] += position.z;
    }
    return *this;
}

elObjectGeometryBuilder&
elObjectGeometryBuilder::ApplyScaling( const glm::vec3& scaling ) noexcept
{
    for ( uint64_t i = 0, limit = this->objectGeometry.vertices.size();
          i < limit; i += 3 )
    {
        this->objectGeometry.vertices[i] *= scaling.x;
        this->objectGeometry.vertices[i + 1] *= scaling.y;
        this->objectGeometry.vertices[i + 2] *= scaling.z;
    }
    return *this;
}

const OpenGL::objectGeometry_t&
elObjectGeometryBuilder::GetObjectGeometry() const noexcept
{
    return this->objectGeometry;
}

OpenGL::objectGeometry_t
elObjectGeometryBuilder::GetInnerObjectGeometry() const noexcept
{
    auto g = this->objectGeometry;
    std::swap( g.elements, g.innerElements );
    std::swap( g.normals, g.innerNormals );
    return g;
}

} // namespace HighGL
} // namespace el3D
