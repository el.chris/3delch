#include "HighGL/elCameraController_base.hpp"

namespace el3D
{
namespace HighGL
{
elCameraController_base::elCameraController_base(
    const elCameraController      type,
    OpenGL::elCamera_Perspective& camera ) noexcept
    : type{ type }
{
    this->AttachToCamera( camera );
}

elCameraController
elCameraController_base::GetType() const noexcept
{
    return this->type;
}

bool
elCameraController_base::AttachToCamera(
    OpenGL::elCamera_Perspective& camera ) noexcept
{
    this->stateLoaderGuard =
        camera.SetStateLoader( [this]() -> OpenGL::elCamera_Perspective::state_t
                               { return this->GetState(); } );

    if ( !this->stateLoaderGuard ) return false;

    this->cameraState = &camera.GetState();
    this->camera      = &camera;

    return true;
}

OpenGL::elCamera_Perspective::state_t
elCameraController_base::GetState() noexcept
{
    if ( this->hasUpdate || this->camera->HasUpdatedState() ||
         this->hasContinuousUpdates )
        this->CalculateState();

    this->hasUpdate = false;
    return this->state;
}

bool
elCameraController_base::IsAttachedToCamera() const noexcept
{
    return static_cast< bool >( this->stateLoaderGuard );
}
} // namespace HighGL
} // namespace el3D
