#include "HighGL/Conversion.hpp"

#include "GLAPI/elImage.hpp"
#include "logging/elLog.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
texture_t
ConvertHeightMapToNormalMap( const uint64_t x, const uint64_t y,
                             const utils::byteStream_t& heightmap,
                             const float scale, const TextureFormat outFormat,
                             const TextureFormat inFormat ) noexcept
{
    texture_t texture;
    if ( x <= 2 || y <= 2 )
    {
        LOG_ERROR( 0 ) << "HeightMap must have at least the the dimension 2x2. "
                          "The provided HeightMap has the dimension "
                       << x << "x" << y << "! Returning empty NormalMap.";
        return texture;
    }

    texture.width  = x - 2;
    texture.height = y - 2;
    texture.data.stream.resize( texture.width * texture.height *
                                static_cast< uint64_t >( outFormat ) );

    auto accessNormalMap = [&]( uint64_t xPos, uint64_t yPos,
                                uint64_t dim ) -> utils::byte_t& {
        return texture.data.stream[( texture.width * yPos + xPos ) *
                                       static_cast< uint64_t >( outFormat ) +
                                   dim];
    };

    auto accessHeightMap = [&]( uint64_t xPos,
                                uint64_t yPos ) -> utils::byte_t {
        return heightmap
            .stream[( x * yPos + xPos ) * static_cast< uint64_t >( inFormat )];
    };

    for ( uint64_t xPos = 1; xPos + 1 < x; ++xPos )
    {
        for ( uint64_t yPos = 1; yPos + 1 < y; ++yPos )
        {
            glm::vec3 normal;
            normal.x = scale * -static_cast< float >(
                                   ( accessHeightMap( xPos + 1, yPos + 1 ) -
                                     accessHeightMap( xPos + 1, yPos - 1 ) ) +
                                   +2 * ( accessHeightMap( xPos, yPos + 1 ) -
                                          accessHeightMap( xPos, yPos - 1 ) ) +
                                   ( accessHeightMap( xPos - 1, yPos + 1 ) -
                                     accessHeightMap( xPos - 1, yPos - 1 ) ) );

            normal.y = scale * -static_cast< float >(
                                   ( accessHeightMap( xPos + 1, yPos - 1 ) -
                                     accessHeightMap( xPos - 1, yPos - 1 ) ) +
                                   +2 * ( accessHeightMap( xPos + 1, yPos ) -
                                          accessHeightMap( xPos - 1, yPos ) ) +
                                   ( accessHeightMap( xPos + 1, yPos + 1 ) -
                                     accessHeightMap( xPos - 1, yPos + 1 ) ) );

            normal.z = 1.0f;

            normal = glm::normalize( normal );

            normal = normal * 0.5f + 0.5f;

            accessNormalMap( xPos - 1, yPos - 1, 0 ) = static_cast< uint8_t >(
                normal.x * static_cast< float >( 0xff ) );
            accessNormalMap( xPos - 1, yPos - 1, 1 ) = static_cast< uint8_t >(
                normal.y * static_cast< float >( 0xff ) );
            accessNormalMap( xPos - 1, yPos - 1, 2 ) = static_cast< uint8_t >(
                normal.z * static_cast< float >( 0xff ) );

            if ( outFormat == TextureFormat::RGBA )
                accessNormalMap( xPos - 1, yPos - 1, 3 ) = 0xff;
        }
    }

    return texture;
}

texture_t
ConvertHeightMapToNormalMap( const std::string& imageFile, const float scale,
                             const TextureFormat outFormat,
                             const TextureFormat inFormat ) noexcept
{
    auto image = GLAPI::elImage::Create( GLAPI::FromFilePath, imageFile );
    if ( image.HasError() )
    {
        LOG_ERROR( 0 )
            << "Unable to open HeightMap file \"" << imageFile
            << "\" for NormalMap conversion. Returning empty NormalMap!";
        return texture_t();
    }

    auto dim = image->get()->GetDimensions();

    return ConvertHeightMapToNormalMap( dim.width, dim.height,
                                        image->get()->GetByteStream(), scale,
                                        outFormat, inFormat );
}

} // namespace HighGL
} // namespace el3D
