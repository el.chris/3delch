#include "RenderPipeline/elRenderPipelineManager.hpp"

#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "HighGL/elEventTexture.hpp"
#include "HighGL/elLight_DirectionalLight.hpp"
#include "HighGL/elLight_PointLight.hpp"
#include "OpenGL/gl_Call.hpp"
#include "RenderPipeline/elRenderPipeline.hpp"
#include "RenderPipeline/elRenderer_GUI.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "lua/elConfigHandler.hpp"


namespace el3D
{
namespace RenderPipeline
{
std::queue< std::function< void() > > elRenderPipelineManager::renderTasks;

elRenderPipelineManager::elRenderPipelineManager(
    const GLAPI::elWindow *const window, const float multiSamplingFactor,
    const lua::elConfigHandler *const config )
    : window( window ), config( config ),
      multiSamplingFactor( multiSamplingFactor )
{
    auto windowSize  = this->window->GetWindowSize();
    this->renderSize = glm::uvec2( windowSize.x, windowSize.y );
}

void
elRenderPipelineManager::Reshape( const uint64_t x, const uint64_t y )
{
    this->renderSize            = glm::uvec2( x, y );
    glm::uvec2 scaledRenderSize = glm::vec2( x, y ) * this->multiSamplingFactor;

    for ( auto &p : this->pipes )
    {
        p->Reshape( scaledRenderSize.x, scaledRenderSize.y );
    }

    OpenGL::gl( glGetIntegerv, static_cast< GLenum >( GL_VIEWPORT ),
                this->viewportSize );
}

void
elRenderPipelineManager::Render()
{
    OpenGL::gl( glViewport, this->viewportSize[0], this->viewportSize[1],
                this->viewportSize[2], this->viewportSize[3] );

    // update camera matrices
    for ( auto &p : this->pipes )
        p->Render();

    // draw primary render pipe on screen
    auto &primaryPipe = this->pipes[this->primaryPipeID];
    auto  winSize     = this->window->GetWindowSize();
    primaryPipe->DrawOutputToScreen( static_cast< GLint >( winSize.x ),
                                     static_cast< GLint >( winSize.y ) );

    // read value from event texture and call registered callback
    glm::vec2 cursorPosition = this->eventPosition * GuiGL::GetDPIScaling();
    auto      eventColor     = primaryPipe->GetEventColorAtPosition(
                 glm::ivec2( cursorPosition.x, static_cast< float >( winSize.y ) *
                                                       this->multiSamplingFactor -
                                                   cursorPosition.y ) );

    this->eventTexture.SetCurrentEventColor( eventColor );

    while ( !renderTasks.empty() )
    {
        renderTasks.front()();
        renderTasks.pop();
    }
}

bb::product_ptr< HighGL::elEventColor >
elRenderPipelineManager::AcquireEventColor() noexcept
{
    return this->eventTexture.CreateEventColor();
}

bb::product_ptr< HighGL::elEventColorRange >
elRenderPipelineManager::AcquireEventColorRange( const uint32_t size ) noexcept
{
    return this->eventTexture.CreateEventColorRange( size );
}

uint32_t
elRenderPipelineManager::GetCurrentEventColorIndex() const noexcept
{
    return this->eventTexture.GetCurrentEventColor().GetIndex();
}

elRenderPipelineManager::pipelineElement_t
elRenderPipelineManager::CreatePipeline(
    const std::string &sceneName, GLAPI::elFontCache *const fontCache,
    GLAPI::elEventHandler *const eventHandler )
{
    auto newPipe =
        this->pipes.emplace_back( std::make_unique< elRenderPipeline >(
            sceneName, this->config, &this->eventTexture, this->window,
            fontCache, eventHandler ) );

    newPipe.value()->Reshape( this->renderSize.x, this->renderSize.y );
    return newPipe;
}

void
elRenderPipelineManager::RemovePipeline( const size_t id )
{
    this->pipes.erase( id );
}

void
elRenderPipelineManager::AddRenderTask( const std::function< void() > &task )
{
    renderTasks.push( task );
}

void
elRenderPipelineManager::SetMultiSamplingFactor( const float value )
{
    if ( value != this->multiSamplingFactor )
    {
        this->multiSamplingFactor = value;
        this->Reshape( this->renderSize.x, this->renderSize.y );
    }
}

float
elRenderPipelineManager::GetMultiSamplingFactor() const noexcept
{
    return this->multiSamplingFactor;
}

void
elRenderPipelineManager::SetEventPosition( const glm::vec2 value )
{
    this->eventPosition = value;
}

} // namespace RenderPipeline
} // namespace el3D
