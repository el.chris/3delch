#include "RenderPipeline/elRenderer_base.hpp"

#include "OpenGL/elFrameBufferObject.hpp"
#include "RenderPipeline/internal.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"
#include "utils/elFileSystem.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderer_base::elRenderer_base(
    const elRenderer_Type type, const size_t numberOfFrameBufferObjects,
    const internal::rendererShared_t& shared ) noexcept
    : type( type ), sceneName( shared.sceneName ), config( shared.config ),
      window( shared.window ),
      fbo( new OpenGL::elFrameBufferObject( numberOfFrameBufferObjects ) ),
      directionalLights( shared.directionalLights ),
      pointLights( shared.pointLights )
{
    this->fboDetails.resize( numberOfFrameBufferObjects );

    auto result = this->config->Get< std::string >(
        { "global", "renderer", "lookupDirectory" }, { "" } );
    if ( !( result.empty() || result[0].empty() ) )
    {
        this->shaderLookupDirectory =
            result[0] + utils::elFileSystem::PATH_DELIMITER;
    }
}

elRenderer_base::~elRenderer_base()
{
}

elRenderer_Type
elRenderer_base::GetType() const noexcept
{
    return this->type;
}

void
elRenderer_base::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    if ( x > 0 && y > 0 )
        for ( uint64_t i = 0, limit = this->fbo->GetFrameBufferSize();
              i < limit; ++i )
            this->fbo->SetSize( x, y, i );
}

OpenGL::elTexture_2D&
elRenderer_base::CreateTexture( attachmentId_t&    id,
                                const std::string& identifier,
                                const GLint internalFormat, const GLenum format,
                                const GLenum type, const Attachment attachment,
                                const uint64_t frameBufferNumber ) noexcept
{
    OpenGL::elTexture_2D* newTexture = new OpenGL::elTexture_2D(
        identifier, 1, 1, internalFormat, format, type );
    this->AttachTexture( id, newTexture, attachment, frameBufferNumber );

    this->fboDetails[frameBufferNumber].ownedTextures.emplace_back(
        newTexture );
    return *newTexture;
}

OpenGL::elTexture_2D_Array&
elRenderer_base::CreateTextureArray( arrayAttachementId_t& ids,
                                     const std::string&    identifier,
                                     const GLint           internalFormat,
                                     const GLenum format, const GLenum type,
                                     const uint64_t arraySize,
                                     const uint64_t frameBufferNumber ) noexcept
{
    OpenGL::elTexture_2D_Array* newTexture = new OpenGL::elTexture_2D_Array(
        identifier, 1, 1, arraySize, internalFormat, format, type );
    ids.target.resize( arraySize );

    ids.textureId = this->fboDetails[frameBufferNumber].outputTextures.size();

    auto& attachmentOffset =
        this->fboDetails[frameBufferNumber].attachmentOffset;
    for ( uint64_t i = 0; i < arraySize; ++i, ++attachmentOffset )
    {
        ids.target[i] = attachmentOffset;

        auto fboId = this->fbo->AddFramebufferTexture(
            newTexture, static_cast< GLint >( i ),
            static_cast< GLenum >( GL_COLOR_ATTACHMENT0 + ids.target[i] ),
            frameBufferNumber );

        bb::Ensures( ids.textureId == fboId );
    }

    this->fboDetails[frameBufferNumber].outputTextures.emplace_back(
        newTexture );
    this->fboDetails[frameBufferNumber].ownedTextures.emplace_back(
        newTexture );

    return *newTexture;
}

OpenGL::elTexture_CubeMap&
elRenderer_base::CreateTextureCubeMap(
    cubeMapAttachmentId_t& ids, const std::string& identifier,
    const GLint internalFormat, const GLenum format, const GLenum type,
    const uint64_t frameBufferNumber ) noexcept
{
    OpenGL::elTexture_CubeMap* newTexture = new OpenGL::elTexture_CubeMap(
        identifier, 1, 1, internalFormat, format, type );

    ids.textureId = this->fboDetails[frameBufferNumber].outputTextures.size();

    auto& attachmentOffset =
        this->fboDetails[frameBufferNumber].attachmentOffset;

    static constexpr GLenum CUBE_SIDES[6] = {
        GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z };

    for ( uint64_t i = 0u; i < 6u; ++i, ++attachmentOffset )
    {
        ids.target[i] = attachmentOffset;
        auto fboId    = this->fbo->AddFramebufferTexture(
            newTexture, CUBE_SIDES[i],
            static_cast< GLenum >( GL_COLOR_ATTACHMENT0 + ids.target[i] ),
            frameBufferNumber );

        bb::Ensures( ids.textureId == fboId );
    }

    this->fboDetails[frameBufferNumber].outputTextures.emplace_back(
        newTexture );
    this->fboDetails[frameBufferNumber].ownedTextures.emplace_back(
        newTexture );
    return *newTexture;
}

void
elRenderer_base::AttachTexture( attachmentId_t&             id,
                                OpenGL::elTexture_2D* const texture,
                                const Attachment            attachment,
                                const uint64_t              frameBufferNumber )
{
    GLenum fboAttachment = GL_COLOR_ATTACHMENT0;

    switch ( attachment )
    {
        case Attachment::Color:
        {
            auto& attachmentOffset =
                this->fboDetails[frameBufferNumber].attachmentOffset;
            fboAttachment =
                OpenGL::ColorAttachmentFromOffset( attachmentOffset );
            id.target = attachmentOffset;
            ++attachmentOffset;
            break;
        }
        case Attachment::Depth:
            fboAttachment = GL_DEPTH_ATTACHMENT;
            break;
        case Attachment::DepthStencil:
            fboAttachment = GL_DEPTH_STENCIL_ATTACHMENT;
            break;
    }


    id.textureId = this->fboDetails[frameBufferNumber].outputTextures.size();

    auto fboId = this->fbo->AddFramebufferTexture( texture, fboAttachment,
                                                   frameBufferNumber );

    this->fboDetails[frameBufferNumber].outputTextures.emplace_back( texture );

    bb::Ensures( id.textureId == fboId );
}

std::unique_ptr< OpenGL::elShaderProgram >
elRenderer_base::CreateShaderProgram(
    const std::string& renderTarget, const std::string& geometricObjectType,
    const std::string&                           shaderGroup,
    const OpenGL::elGLSLParser::staticDefines_t& defines ) const noexcept
{
    auto shaderFileName = this->config->Get< std::string >(
        { "global", "scenes", this->sceneName,
          ( geometricObjectType.empty() ) ? renderTarget : geometricObjectType,
          ( geometricObjectType.empty() ) ? "shaderFile"
                                          : "shaderFile_" + renderTarget } );

    if ( shaderFileName.empty() )
    {
        LOG_FATAL( 0 )
            << "unable to find config file entry for geometricObjectType \""
            << geometricObjectType << "\" shaderFile in \"" << renderTarget
            << "\" renderer for scene \"" << this->sceneName << "\"";
        std::terminate();
    }

    std::string shaderFilePath =
        this->shaderLookupDirectory + shaderFileName[0];

    auto shader = OpenGL::elShaderProgram::Create(
        OpenGL::shaderFromFile, shaderFilePath, shaderGroup,
        this->window->GetShaderVersion(), defines );

    if ( shader.HasError() )
    {
        LOG_FATAL( 0 ) << "unable to create shader from shader file \""
                       << shaderFilePath << "\" which is required as \""
                       << shaderGroup << "\" for renderer \"" << renderTarget
                       << "\" in scene \"" << this->sceneName << "\"";
        std::terminate();
    }

    return std::move( shader.GetValue() );
}

std::vector< GLenum >
elRenderer_base::GenerateRenderTargets(
    const std::vector< uint64_t >& colorAttachmentTargets,
    const std::vector< GLenum >&   directTargets ) const noexcept
{
    std::vector< GLenum > retVal;
    for ( auto& id : colorAttachmentTargets )
        retVal.push_back( static_cast< GLenum >( GL_COLOR_ATTACHMENT0 + id ) );
    for ( auto& target : directTargets )
        retVal.push_back( target );

    return retVal;
}

} // namespace RenderPipeline
} // namespace el3D
