#include "RenderPipeline/elRenderer_ShaderTexture.hpp"

#include "HighGL/elShaderTexture.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"


namespace el3D
{
namespace RenderPipeline
{
elRenderer_ShaderTexture::elRenderer_ShaderTexture(
    internal::rendererShared_t shared )
    : elRenderer_base( elRenderer_Type::shaderTexture, 0, shared )
{
}

elRenderer_ShaderTexture::~elRenderer_ShaderTexture()
{
}

void
elRenderer_ShaderTexture::Reshape( const uint64_t, const uint64_t ) noexcept
{
}

void
elRenderer_ShaderTexture::Render( const OpenGL::elCamera_Perspective& ) noexcept
{
    OpenGL::Disable( GL_DEPTH_TEST );
    OpenGL::Disable( GL_CULL_FACE );

    for ( auto& e : this->shaderTextures )
    {
        switch ( e.refresh )
        {
            case Refresh::never:
                break;
            case Refresh::once:
                e.refresh = Refresh::never;
                [[fallthrough]];
            case Refresh::always:
                e.shader->Refresh();
                [[fallthrough]];
            default:
                break;
        }
    }
}

size_t
elRenderer_ShaderTexture::AddShaderTexture(
    const HighGL::elShaderTexture* s ) noexcept
{
    return this->shaderTextures.emplace_back( shader_t{ s, Refresh::always } )
        .index();
}

void
elRenderer_ShaderTexture::RemoveShaderTexture( const size_t n ) noexcept
{
    this->shaderTextures.erase( n );
}

void
elRenderer_ShaderTexture::SetDoRefresh( const size_t  index,
                                        const Refresh refresh ) noexcept
{
    this->shaderTextures[index].refresh = refresh;
}
} // namespace RenderPipeline
} // namespace el3D
