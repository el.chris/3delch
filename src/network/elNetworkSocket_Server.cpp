#include "network/elNetworkSocket_internal.hpp"

#include <string>

#ifndef _WIN32
#include <fcntl.h>
#include <unistd.h>
#else
#include <io.h>
#endif

#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/elLog.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "network/network_NAMESPACE_NAME.hpp"
#include "utils/smart_c.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace network
{
elNetworkSocket_Server::elNetworkSocket_Server()
{
}

elNetworkSocket_Server::~elNetworkSocket_Server()
{
    /// must be performed here since ListeningThread depends on child type
    this->doContinueListeningThreadCycle.store( false );
    this->StopListen();
    this->activeObject.AllowToAddNewTasks( false );
    this->activeObject.WaitForTasksToFinish();
}

std::optional< utils::byteStream_t >
elNetworkSocket_Server::BlockingReceive( const socketHandleType_t handle,
                                         const size_t numberOfBytes )
{
    auto buffer = this->connectionHandler->GetBuffer( handle );
    if ( !buffer.has_value() ) return std::nullopt;
    auto value =
        ( *buffer )->template BlockingMultiPop< utils::byteStream_t::stream_t >(
            numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;
    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

std::optional< utils::byteStream_t >
elNetworkSocket_Server::TryReceive( const socketHandleType_t handle,
                                    const size_t             numberOfBytes )
{
    auto buffer = this->connectionHandler->GetBuffer( handle );
    if ( !buffer.has_value() ) return std::nullopt;
    auto value =
        ( *buffer )->template TryMultiPop< utils::byteStream_t::stream_t >(
            numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;
    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

std::optional< utils::byteStream_t >
elNetworkSocket_Server::TimedReceive( const socketHandleType_t handle,
                                      const units::Time        timeout,
                                      const size_t             numberOfBytes )
{
    auto buffer = this->connectionHandler->GetBuffer( handle );
    if ( !buffer.has_value() ) return std::nullopt;
    auto value =
        ( *buffer )->template TimedMultiPop< utils::byteStream_t::stream_t >(
            timeout, numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;
    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Server::Open()
{
    auto openCall = elNetworkSocket_Base::Open();
    if ( openCall.HasError() ) return openCall;

#ifndef _WIN32
    auto fcntlCall = utils::smart_c(
        fcntl, []( auto v ) { return v == -1; }, this->socketFileDescriptor,
        F_SETFL, O_NONBLOCK );
    if ( fcntlCall.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to adjust socket file descriptor properties";
        this->Close( this->socketFileDescriptor );
        this->socketFileDescriptor = INVALID_SOCKET;

        return bb::Error( elNetworkSocket_Error::InternalFailure );
    }
#else
    unsigned long argp = 1;
    auto errorCode = ioctlsocket( this->socketFileDescriptor, FIONBIO, &argp );
    if ( errorCode != NO_ERROR )
    {
        LOG_ERROR( 0 ) << "Unable to adjust socket filedescriptor properties "
                          "(ioctlsocket), error code = "
                       << std::to_string( errorCode );

        this->Close( this->socketFileDescriptor );
        this->socketFileDescriptor = INVALID_SOCKET;

        return bb::Error( elNetworkSocket_Error::InternalFailure );
    }
#endif

    auto call = this->SetSocketOptions( this->socketFileDescriptor );
    if ( call.HasError() )
    {
        this->Close( this->socketFileDescriptor );
        this->socketFileDescriptor = INVALID_SOCKET;
        return call;
    }

    return bb::Success<>();
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Server::SetSocketOptions(
    const socketHandleType_t fileDescriptor )
{
#ifndef _WIN32
    int  reuseaddr = 1;
    auto call      = utils::smart_c(
        setsockopt, []( auto v ) { return v == -1; }, fileDescriptor,
        SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &reuseaddr,
        static_cast< unsigned int >( sizeof( reuseaddr ) ) );

    if ( call.HasError() )
#else
    char reuseaddr = 1;
    if ( setsockopt( fileDescriptor, SOL_SOCKET, SO_REUSEADDR, &reuseaddr,
                     sizeof( reuseaddr ) ) == -1 )
#endif
    {
        LOG_ERROR( 0 ) << "setsockopt failed : " << strerror( errno );
        return bb::Error( elNetworkSocket_Error::InternalFailure );
    }
    return bb::Success<>();
}

bb::elExpected< uint16_t, elNetworkSocket_Error >
elNetworkSocket_Server::Listen( const uint16_t port,
                                const bool     doAcceptOneConnectionOnly )
{
    this->StopListen();

    this->connectionHandler->Clear();
    this->doAcceptOneConnectionOnly = doAcceptOneConnectionOnly;

    auto openCall = this->Open();
    if ( openCall.HasError() ) return bb::Error( openCall.GetError() );

    struct sockaddr_in myAddress;
    memset( reinterpret_cast< void* >( &myAddress ), 0, sizeof( myAddress ) );
    myAddress.sin_family      = AF_INET;
    myAddress.sin_addr.s_addr = INADDR_ANY;
    myAddress.sin_port        = htons( port );


    auto bindCall = utils::smart_c(
        ::bind, []( auto v ) { return v == -1; }, this->socketFileDescriptor,
        reinterpret_cast< struct sockaddr* >( &myAddress ),
        static_cast< unsigned int >( sizeof( myAddress ) ) );

    if ( bindCall.HasError() )
    {
        LOG_ERROR( 0 ) << "Unable to bind port: " << port;
        switch ( bindCall.GetError() )
        {
            case EADDRINUSE:
            {
                LOG_ERROR( 0 ) << "address already in use";
                return bb::Error( elNetworkSocket_Error::AddressAlreadyInUse );
            }
            case EACCES:
            {
                LOG_ERROR( 0 ) << "address is protected";
                return bb::Error( elNetworkSocket_Error::AddressIsProtected );
            }
            default:
            {
                return bb::Error( elNetworkSocket_Error::InternalFailure );
            }
        }
    }

    auto listenCall = utils::smart_c(
        listen, []( auto v ) { return v == -1; }, this->socketFileDescriptor,
        this->backlog );

    if ( listenCall.HasError() )
    {
        switch ( listenCall.GetError() )
        {
            case EADDRINUSE:
            {
                LOG_ERROR( 0 ) << "address already in use";
                return bb::Error( elNetworkSocket_Error::AddressAlreadyInUse );
            }
            default:
            {
                LOG_ERROR( 0 ) << "Unable to listen on socket";
                return bb::Error( elNetworkSocket_Error::InternalFailure );
            }
        }
    }

    this->socketMonitor->AddFile( utils::elFileMonitor::FileType::Read,
                                  this->socketFileDescriptor );
    this->isConnected.store( true );
    this->doContinueListeningThreadCycle.store( true );
    this->activeObject.AddTask( [this] { this->ListeningThread(); } );

    struct sockaddr_in socketInfo;
    socklen_t          socketInfoLength =
        static_cast< socklen_t >( sizeof( socketInfo ) );

    auto getsocknameCall = utils::smart_c(
        getsockname, []( auto v ) { return v != 0; },
        this->socketFileDescriptor,
        reinterpret_cast< sockaddr* >( &socketInfo ), &socketInfoLength );
    if ( getsocknameCall.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to call getsockname - unable to determine "
                          "bounded port number";
        this->StopListen();
        return bb::Error( elNetworkSocket_Error::UnableToGetBoundedPortNumber );
    }

    uint16_t chosenPort = ntohs( socketInfo.sin_port );
    LOG_INFO( 0 ) << "start listening on port " << chosenPort;
    this->listeningPort = chosenPort;
    return bb::Success< uint16_t >( chosenPort );
}

void
elNetworkSocket_Server::StopListen()
{
    if ( this->isConnected.load() )
    {
        LOG_INFO( 0 ) << "stop listening on port " << this->listeningPort;
        this->isConnected.store( false );
        this->doContinueListeningThreadCycle.store( false );
        this->activeObject.WaitForTasksToFinish();

        this->connectionHandler->Terminate();
        this->socketMonitor->Clear();
        this->Close( this->socketFileDescriptor );
        this->socketFileDescriptor = INVALID_SOCKET;
    }
}

void
elNetworkSocket_Server::ListeningThread()
{
    auto fdSet = this->socketMonitor->Monitor( this->monitorTimeout );
    if ( fdSet.doesContainBadFileDescriptor )
    {
        auto inactiveSockets = this->connectionHandler->GetInactiveSockets();
        for ( auto socket : inactiveSockets )
        {
            this->CloseConnection( socket );
        }

        fdSet = this->socketMonitor->Monitor( this->monitorTimeout );
    }

    if ( fdSet.doesContainBadFileDescriptor )
    {
        LOG_ERROR( 0 ) << "unable to receiving messages caused by corrupted "
                          "file monitor set";
    }
    else
    {
        if ( fdSet.read.Contains( this->socketFileDescriptor ) )
        {
            this->AcceptIncomingConnection();
        }

        auto connections = this->connectionHandler->GetConnections();

        for ( auto c : connections )
        {
            if ( fdSet.read.Contains( c.socket ) )
            {
                this->ReceiveFromIncomingConnection( c.socket );
            }
        }
    }

    if ( this->doContinueListeningThreadCycle )
        this->activeObject.AddTask( [this] { this->ListeningThread(); } );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Server::ReceiveFromIncomingConnection(
    const socketHandleType_t socket )
{
    return this->Read( socket )
        .OrElse(
            [] {
                LOG_ERROR( 0 )
                    << "unable to read from established incoming connection";
            } )
        .AndThen(
            [&]( const auto& socketBuffer )
            {
                if ( socketBuffer.has_value() && !socketBuffer->stream.empty() )
                {
                    this->connectionHandler->PushMessage( socket,
                                                          *socketBuffer );

                    if ( this->onReceiveCallback )
                        this->onReceiveCallback( socket );
                }
                else
                {
                    auto info = this->GetConnectionInformation( socket );
                    this->CloseConnection( socket );
                }
            } );
}

void
elNetworkSocket_Server::CloseConnection( const socketHandleType_t socket )
{
    auto info = this->GetConnectionInformation( socket );
    if ( info.has_value() )
        LOG_DEBUG( 0 ) << "closing connection from " << info->hostIP << " ("
                       << info->hostName << ")";
    else
        LOG_DEBUG( 0 ) << "closing weirdly unknown connection";

    this->socketMonitor->RemoveFile( utils::elFileMonitor::FileType::Read,
                                     socket );
    this->connectionHandler->Remove( socket );

    this->Close( socket );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Server::AcceptIncomingConnection()
{
    if ( !this->connectionHandler->GetConnections().empty() &&
         this->doAcceptOneConnectionOnly )
        return bb::Error(
            elNetworkSocket_Error::MaximumNumberOfConnectionsExceeded );

    socketHandleType_t handle;
    struct sockaddr_in clientAddress;
    socklen_t          socketLength = sizeof( clientAddress );

    auto acceptCall =
        utils::smart_c(
            accept, []( auto v ) { return v == -1; },
            this->socketFileDescriptor,
            reinterpret_cast< sockaddr* >( &clientAddress ), &socketLength )
            .AndThen( [&]( const auto& r ) { handle = r; } );

    if ( acceptCall.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to accept incoming connection";
        switch ( acceptCall.GetError() )
        {
            case ECONNABORTED:
            {
                LOG_ERROR( 0 ) << "incoming connection has been aborted";
                return bb::Error( elNetworkSocket_Error::ConnectionAborted );
            }
            default:
            {
                return bb::Error( elNetworkSocket_Error::InternalFailure );
            }
        }
    }

    this->EnableTCPNoDelay( this->socketFileDescriptor );
    auto setSockOptCall =
        this->SetSocketOptions( handle ).OrElse(
            [] {
                LOG_ERROR( 0 )
                    << "unable to set socket options for incoming connection";
            } );

    if ( setSockOptCall.HasError() ) return setSockOptCall;

    this->socketMonitor->AddFile( utils::elFileMonitor::FileType::Read,
                                  handle );

    auto newConnection =
        this->CreateConnectionInformation( handle, clientAddress );
    this->connectionHandler->Add( handle, newConnection );
    if ( this->onConnectCallback ) this->onConnectCallback( handle );

    LOG_DEBUG( 0 ) << "accept incoming connection from " << newConnection.hostIP
                   << " (" << newConnection.hostName << ")";
    return bb::Success<>();
}

elNetworkSocket::internal::connection_t
elNetworkSocket_Server::CreateConnectionInformation(
    const socketHandleType_t handle,
    const sockaddr_in&       clientAddress ) const noexcept
{
    elNetworkSocket::internal::connection_t newConnection;
    newConnection.socket = handle;

    size_t      hostNameSize = 256;
    std::string hostName;
    hostName.reserve( hostNameSize );

    utils::smart_c(
        getnameinfo, []( auto v ) { return v != 0; },
        reinterpret_cast< const sockaddr* >( &clientAddress ),
        static_cast< unsigned int >( sizeof( clientAddress ) ), hostName.data(),
        static_cast< unsigned int >( hostNameSize ), nullptr, 0u, 0 )
        // TODO: can be changed to AndThen([&] {}) - clang compile error
        // when missing (const auto)
        .AndThen( [&]( const auto )
                  { newConnection.hostName.assign( hostName.c_str() ); } );

    size_t      ipAddressSize = 15;
    std::string ipAddress;
    ipAddress.reserve( ipAddressSize );

    utils::smart_c(
        getnameinfo, []( auto v ) { return v != 0; },
        reinterpret_cast< const sockaddr* >( &clientAddress ),
        static_cast< unsigned int >( sizeof( clientAddress ) ),
        ipAddress.data(), static_cast< unsigned int >( ipAddressSize ), nullptr,
        0u, NI_NUMERICHOST )
        // TODO: can be changed to AndThen([&] {}) - clang compile error
        // when missing (const auto)
        .AndThen( [&]( const auto )
                  { newConnection.hostIP.assign( ipAddress.c_str() ); } );

    return newConnection;
}


void
elNetworkSocket_Server::SetOnConnectCallback(
    const connectCallback_t& callback ) noexcept
{
    this->onConnectCallback = callback;
}

void
elNetworkSocket_Server::SetOnReceiveCallback(
    const receiveCallback_t& callback ) noexcept
{
    this->onReceiveCallback = callback;
}

std::vector< elNetworkSocket::internal::connection_t >
elNetworkSocket_Server::GetConnections() const noexcept
{
    return this->connectionHandler->GetConnections();
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Server::Send( const socketHandleType_t   destinationSocket,
                              const utils::byteStream_t& message ) noexcept
{
    return this->SendToSocket( destinationSocket, message )
        .OrElse(
            [&]
            {
                auto info = this->GetConnectionInformation( destinationSocket );
                if ( info.has_value() )
                    LOG_INFO( 0 ) << "lost connection to " << info->hostIP
                                  << " (" << info->hostName << ")";
                else
                    LOG_INFO( 0 ) << "loosing weirdly unknown connection ";

                this->CloseConnection( destinationSocket );
            } );
}

std::optional< elNetworkSocket::internal::connection_t >
elNetworkSocket_Server::GetConnectionInformation(
    const socketHandleType_t socket ) const noexcept
{
    auto conn = this->connectionHandler->GetConnections();
    auto iter =
        bb::find_if( conn, [=]( auto e ) { return e.socket == socket; } );
    if ( iter == conn.end() ) return std::nullopt;

    return *iter;
}


///// ConnectionHandler_t


void
elNetworkSocket_Server::ConnectionHandler_t::Clear() noexcept
{
    this->connections.clear();
}

void
elNetworkSocket_Server::ConnectionHandler_t::Terminate() noexcept
{
    for ( auto& c : this->connections )
        c.second.buffer->Clear();
}

void
elNetworkSocket_Server::ConnectionHandler_t::Remove(
    const socketHandleType_t handle ) noexcept
{

    auto iter = this->connections.find( handle );
    if ( iter != this->connections.end() )
    {
        iter->second.buffer->Terminate();
        this->connections.erase( iter );
    }
}

void
elNetworkSocket_Server::ConnectionHandler_t::Add(
    const socketHandleType_t                       handle,
    const elNetworkSocket::internal::connection_t& connection ) noexcept
{
    auto temp         = connection;
    temp.connectionID = this->connectionID++;
    this->connections.insert( std::make_pair(
        handle,
        elNetworkSocket::internal::connectionBuffer_t{
            temp,
            std::make_shared< elNetworkSocket::internal::buffer_t >() } ) );
}

std::vector< elNetworkSocket::internal::connection_t >
elNetworkSocket_Server::ConnectionHandler_t::GetConnections() const noexcept
{
    std::vector< elNetworkSocket::internal::connection_t > connections;
    for ( auto& e : this->connections )
        connections.emplace_back( e.second.connection );

    return connections;
}

std::vector< network::socketHandleType_t >
elNetworkSocket_Server::ConnectionHandler_t::GetInactiveSockets() const noexcept
{
    std::vector< network::socketHandleType_t > inactiveSockets;
    for ( auto& e : this->connections )
    {
        auto socket = e.second.connection.socket;
#ifdef _WIN32
        char error;
#else
        int error;
#endif
        socklen_t len = sizeof( error );
        if ( getsockopt( socket, SOL_SOCKET, SO_ERROR, &error, &len ) < 0 )
            inactiveSockets.emplace_back( socket );
    }
    return inactiveSockets;
}

void
elNetworkSocket_Server::ConnectionHandler_t::PushMessage(
    const socketHandleType_t   handle,
    const utils::byteStream_t& byteStream ) noexcept
{
    auto iter = this->connections.find( handle );

    if ( iter != this->connections.end() )
        iter->second.buffer->MultiPush( byteStream.stream );
    else
        LOG_ERROR( 0 ) << " unknown connection to socket " << handle;
}

std::optional< std::shared_ptr< elNetworkSocket::internal::buffer_t > >
elNetworkSocket_Server::ConnectionHandler_t::GetBuffer(
    const socketHandleType_t handle ) noexcept
{
    auto iter = this->connections.find( handle );

    if ( iter != this->connections.end() )
        return iter->second.buffer;
    else
        return std::nullopt;
}

} // namespace network
} // namespace el3D
