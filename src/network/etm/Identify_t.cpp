#include "network/etm/Identify_t.hpp"

#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

namespace el3D
{
namespace network
{
namespace etm
{
uint64_t
Identify_t::Request::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->protocol_version );
}

uint64_t
Identify_t::Request::Serialize( const utils::Endian  endianness,
                                utils::byteStream_t& byteStream,
                                const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->protocol_version );
}

bb::elExpected< utils::ByteStreamError >
Identify_t::Request::Deserialize( const utils::Endian        endianness,
                                  const utils::byteStream_t& byteStream,
                                  const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->protocol_version )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Identify_t::Request_t<T>", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
Identify_t::Response::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->protocol_version, this->id, this->service_protocol_version );
}

uint64_t
Identify_t::Response::Serialize(
    const utils::Endian endianness, utils::byteStream_t& byteStream,
    const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->protocol_version,
        this->id, this->service_protocol_version );
}

bb::elExpected< utils::ByteStreamError >
Identify_t::Response::Deserialize( const utils::Endian        endianness,
                                   const utils::byteStream_t& byteStream,
                                   const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->protocol_version, this->id,
                  this->service_protocol_version )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Identify_t::Response_t<T>", r, byteStream, byteStreamOffset );
        } );
}

} // namespace etm
} // namespace network
} // namespace el3D
