#include <cstring>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <io.h>
#endif


#include "logging/elLog.hpp"
#include "network/elNetworkSocket_Client.hpp"
#include "network/network_NAMESPACE_NAME.hpp"
#include "utils/smart_c.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D::network::elNetworkSocket::internal;

namespace el3D
{
namespace network
{
elNetworkSocket_Client::elNetworkSocket_Client()
{
}

elNetworkSocket_Client::~elNetworkSocket_Client()
{
    /// must be performed here since ListeningThread depends on the type of the
    /// child
    this->doContinueListeningThreadCycle.store( false );
    this->Disconnect();
    this->activeObject.AllowToAddNewTasks( false );
    this->activeObject.WaitForTasksToFinish();
}

bb::elExpected< std::shared_ptr< struct addrinfo >, elNetworkSocket_Error >
elNetworkSocket_Client::GetAddressInfo( const std::string& hostname,
                                        const uint16_t     port ) const
{
    struct addrinfo* addressInfoList;
    struct addrinfo  hints;

    memset( &hints, 0, sizeof( addrinfo ) );
    hints.ai_family   = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    auto call = utils::smart_c(
        getaddrinfo, []( auto v ) { return v != 0; }, hostname.c_str(),
        std::to_string( port ).c_str(), &hints, &addressInfoList );

    if ( call.HasError() )
    {
        LOG_ERROR( 0 ) << "getaddrinfo call failed ::: "
                       << gai_strerror( call.GetError() );

        switch ( call.GetError() )
        {
            case EAI_FAIL:
                [[fallthrough]];
            case EAI_AGAIN:
            {
                LOG_ERROR( 0 ) << "name server failure";
                return bb::Error( elNetworkSocket_Error::NameServerFailure );
            }
#ifndef WIN32
            case EAI_ADDRFAMILY:
            {
                LOG_ERROR( 0 ) << "address family not supported";
                return bb::Error(
                    elNetworkSocket_Error::AddressFamilyNotSupported );
            }
#endif
            default:
            {
                LOG_ERROR( 0 ) << "address resolution failed";
                return bb::Error(
                    elNetworkSocket_Error::AddressResolutionFailed );
            }
        }
    }

    return bb::Success( std::shared_ptr< struct addrinfo >(
        addressInfoList, []( auto* ptr ) { freeaddrinfo( ptr ); } ) );
}

bb::elExpected< std::string, elNetworkSocket_Error >
elNetworkSocket_Client::GetNameInfo( const struct addrinfo* const addressInfo,
                                     const int                    flags ) const
{
    std::string nameInfo;
    nameInfo.reserve( 256 );
    auto call = utils::smart_c(
        ::getnameinfo, []( auto v ) { return v != 0; }, addressInfo->ai_addr,
        addressInfo->ai_addrlen, nameInfo.data(),
        static_cast< socklen_t >( nameInfo.capacity() ), nullptr,
        static_cast< unsigned int >( 0 ), flags );

    if ( call.HasError() )
    {
        LOG_ERROR( 0 ) << "GetNameInfo failed, this should not be happening!";
        return bb::Error( elNetworkSocket_Error::InternalFailure );
    }

    return bb::Success( nameInfo );
}

bool
elNetworkSocket_Client::IsPortOpen( const std::string& hostname,
                                    const uint16_t     port ) const noexcept
{
    bool isPortOpen = false;
    int  socketDesciptor;

    if ( utils::smart_c(
             socket, []( auto v ) { return v < 0; }, AF_INET, SOCK_STREAM, 0 )
             .AndThen( [&]( const auto& r ) { socketDesciptor = r; } )
             .OrElse( [] { LOG_ERROR( 0 ) << "unable to open socket"; } )
             .HasError() )
        return false;

    auto getAddressCall = this->GetAddressInfo( hostname, port );
    if ( getAddressCall.HasError() ) return false;

    for ( auto addressInfo                    = getAddressCall.GetValue().get();
          addressInfo != nullptr; addressInfo = addressInfo->ai_next )
    {
        if ( connect( socketDesciptor, addressInfo->ai_addr,
                      addressInfo->ai_addrlen ) >= 0 )
        {
            isPortOpen = true;
            break;
        }
    }

    close( socketDesciptor );
    return isPortOpen;
}

std::shared_future< bb::elExpected< elNetworkSocket_Error > >
elNetworkSocket_Client::Connect( const std::string& hostname,
                                 const uint16_t     port )
{
    return std::async( std::launch::async,
                       [this, hostname, port] {
                           return this->ConnectImplementation( hostname, port );
                       } );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Client::ConnectImplementation( const std::string& hostname,
                                               const uint16_t     port )
{
    auto disconnectCall = this->Disconnect();
    if ( disconnectCall.HasError() ) return disconnectCall;
    this->inboxBuffer.Clear();

    auto openCall = this->Open();
    if ( openCall.HasError() ) return openCall;

    auto getAddressCall = this->GetAddressInfo( hostname, port );
    if ( getAddressCall.HasError() ) return getAddressCall;

    bool        success{ false };
    std::string hostIP, hostName;

    for ( auto addressInfo = getAddressCall.GetValue().get();
          addressInfo != nullptr && !success;
          addressInfo = addressInfo->ai_next )
    {
        auto result = this->GetNameInfo( addressInfo, NI_NUMERICHOST )
                          .AndThen(
                              [&]( const auto& r )
                              {
                                  hostIP = r;
                                  return this->GetNameInfo( addressInfo, 0 );
                              } )
                          .AndThen( [&]( const auto& r ) { hostName = r; } );

        if ( result.HasError() ) return result;

        utils::smart_c(
            connect, []( auto v ) { return v == -1; },
            this->socketFileDescriptor, addressInfo->ai_addr,
            addressInfo->ai_addrlen )
            .OrElse(
                [&] {
                    LOG_ERROR( 0 )
                        << "unable to connect to " << hostname << ":" << port;
                } )
            // TODO: can be changed to AndThen([&] {}) - clang compile error
            // when missing (const auto)
            .AndThen( [&]( const auto ) { success = true; } );
    }

    if ( !success )
    {
        LOG_ERROR( 0 ) << "unable to connect to " << hostname << ":" << port;
        return bb::Error( elNetworkSocket_Error::ConnectionRefused );
    }

    this->socketMonitor->AddFile( utils::elFileMonitor::FileType::Read,
                                  this->socketFileDescriptor );

    this->connection =
        connection_t{ this->socketFileDescriptor, 0, hostIP, hostName };

    this->doContinueListeningThreadCycle.store( true );
    this->isConnected.store( true );
    this->hostname = hostname;
    this->port     = port;

    this->activeObject.AddTask( [this] { this->ListeningThread(); } );
    LOG_DEBUG( 0 ) << "connected to " << hostname << ":" << port;
    return bb::Success<>();
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Client::Disconnect()
{
    return this->DisconnectImplementation( false );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Client::DisconnectImplementation( const bool pauseThread )
{
    if ( this->isConnected.load() )
    {
        this->connection = connection_t{ 0, 0, "", "" };
        this->isConnected.store( false );

        this->doContinueListeningThreadCycle.store( false );
        if ( !pauseThread ) this->activeObject.WaitForTasksToFinish();

        this->socketMonitor->RemoveFile( utils::elFileMonitor::FileType::Read,
                                         this->socketFileDescriptor );

        auto shutDownCall = this->Shutdown( this->socketFileDescriptor );
        this->Close( this->socketFileDescriptor );
        this->socketFileDescriptor = INVALID_SOCKET;

        this->inboxBuffer.Clear();

        if ( shutDownCall.HasError() ) return shutDownCall;

        LOG_DEBUG( 0 ) << "closing connection to " << this->hostname << ":"
                       << this->port;
    }

    this->hostname.clear();
    this->port = 0;
    return bb::Success<>();
}

void
elNetworkSocket_Client::ListeningThread()
{
    auto fdSet = this->socketMonitor->Monitor( this->monitorTimeout );
    if ( fdSet.read.Contains( this->socketFileDescriptor ) )
    {
        this->Read( this->socketFileDescriptor )
            .OrElse(
                [&]
                {
                    LOG_ERROR( 0 )
                        << "unable to read from established connection ::: "
                           "closing connection";
                    this->DisconnectImplementation( true );
                } )
            .AndThen(
                [&]( const auto& socketBuffer )
                {
                    if ( socketBuffer.has_value() )
                        this->inboxBuffer.MultiPush( socketBuffer->stream );
                    else
                        this->doContinueListeningThreadCycle.store( false );
                } );
    }

    if ( this->doContinueListeningThreadCycle.load() )
        this->activeObject.AddTask( [this] { this->ListeningThread(); } );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Client::Send( const utils::byteStream_t& data )
{
    if ( this->isConnected.load() )
    {
        return this->SendToSocket( this->connection.socket, data )
            .OrElse(
                [&]
                {
                    LOG_INFO( 0 ) << "lost connection to " << this->hostname
                                  << ":" << this->port;
                    this->Disconnect();
                } );
    }
    return bb::Error( elNetworkSocket_Error::IsNotConnected );
}

elNetworkSocket::internal::connection_t
elNetworkSocket_Client::GetConnectionInformation() const noexcept
{
    return this->connection;
}

std::optional< utils::byteStream_t >
elNetworkSocket_Client::BlockingReceive( const size_t numberOfBytes )
{
    if ( !this->isConnected ) return std::nullopt;
    auto value =
        this->inboxBuffer.BlockingMultiPop< utils::byteStream_t::stream_t >(
            numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;

    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

std::optional< utils::byteStream_t >
elNetworkSocket_Client::TryReceive( const size_t numberOfBytes )
{
    if ( !this->isConnected ) return std::nullopt;
    auto value = this->inboxBuffer.TryMultiPop< utils::byteStream_t::stream_t >(
        numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;
    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

std::optional< utils::byteStream_t >
elNetworkSocket_Client::TimedReceive( const units::Time& timeout,
                                      const size_t       numberOfBytes )
{
    if ( !this->isConnected ) return std::nullopt;
    auto value =
        this->inboxBuffer.TimedMultiPop< utils::byteStream_t::stream_t >(
            timeout, numberOfBytes );
    if ( !value.has_value() ) return std::nullopt;
    return std::make_optional< utils::byteStream_t >( std::move( *value ) );
}

} // namespace network
} // namespace el3D
